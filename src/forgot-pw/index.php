<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li>パスワードをお忘れの方</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">パスワードをお忘れの方</h2>
      </div>
      <div class="p-signup">
        <div class="contact-content">
          <div class="p-contact p-resetPW">
            <div class="contact-content">
              <div class="v-contact">
                <div class="p-signup--cnt">
                  <p class="desc3 mgb-25">パスワードをお忘れの方はご登録のメールアドレスをご入力ください。</p>
                  <form class="form p-login--form">
                  <div class="form-row mgb-30">
                    <div class="form-input">
                      <input type="text" name="name" class="input" value="" placeholder="メールアドレス">
                    </div>
                  </div>
                  <div class="mgt-40 align-center">
                    <a href="/" class="btn-submit">送信する</a>
                  </div>
                </form>
                </div>
              </div><!-- ./v-contact -->
            </div><!-- ./contact-content -->
          </div>
        </div><!-- ./contact-content -->
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>