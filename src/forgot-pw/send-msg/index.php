<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li>パスワードをお忘れの方</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">パスワードをお忘れの方</h2>
      </div>
      <div class="p-signup">
        <div class="contact-content">
          <div class="p-contact p-resetPW">
            <div class="contact-content">
              <div class="v-contact">
                <div class="p-signup--cnt">
                  <div class="p-signup--cnt-head">
                    <p class="desc3 mgb-10">入力いただいたメールアドレスに、パスワード再設定ページへのリンクを記載したメールをお送りしました。</p>
                    <p class="desc4 mgb-20">※メールが届かない場合は受信設定をご確認ください。</p>
                  </div>
                  <div class="mgt-40 align-center">
                    <a href="/login" class="btn-submit">ログインページへ</a>
                  </div>
                </form>
                </div>
              </div><!-- ./v-contact -->
            </div><!-- ./contact-content -->
          </div>
        </div><!-- ./contact-content -->
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>