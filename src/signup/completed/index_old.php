<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li>新規会員登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">新規会員登録</h2>
      </div>
      <div class="p-signup">
        <div class="contact-content">
          <div class="p-contact p-signup">
            <div class="contact-content">
              <div class="v-contact">
                <div class="container4">
                  <p class="p-contact--completed-ttl align-center">会員登録が完了しました。</p>
                  <div class="p-signup--cnt-head">
                    <p class="desc3 mgb-30 align-center">会員登録いただきありがとうございます。サービスのご利用には、マイページでの設定が必要になります。</p>
                  </div>

                  <div class="p-signup--completed-list row">
                    <div class="p-signup--completed-item col-lg-6 col-md-6 col-6">
                      <a href="" class="p-signup--completed-item-inner link">
                        <div class="p-signup--completed-item-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                        </div>
                        <div class="p-signup--completed-item-cnt">
                          <h4 class="ttl-bold4">ペット情報の登録</h4>
                          <p class="desc6">「登録ペット」から、ご自身のペットの情報を入力してください。</p>
                        </div>
                      </a>
                    </div>
                    <div class="p-signup--completed-item col-lg-6 col-md-6 col-6">
                      <a href="" class="p-signup--completed-item-inner link">
                        <div class="p-signup--completed-item-thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/common/dummy.jpg" alt="">
                        </div>
                        <div class="p-signup--completed-item-cnt">
                          <h4 class="ttl-bold4">ユーザー情報の編集</h4>
                          <p class="desc6">「登録情報編集」から、ご自身の情報を追加入力してください。</p>
                        </div>
                      </a>
                    </div>
                  </div>

                  <div class="p-signup--direct">
                    <a href="" class="btn-blue2">マイページへ</a>
                    <a href="" class="link-blue">トップページへ</a>
                  </div>
                </div>
              </div><!-- ./v-contact -->
            </div><!-- ./contact-content -->
          </div>
        </div><!-- ./contact-content -->
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>