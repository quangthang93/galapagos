<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li>新規会員登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">新規会員登録</h2>
      </div>
      <div class="p-signup">
        <div class="contact-content">
          <div class="p-contact p-signup">
            <div class="contact-content">
              <div class="v-contact">
                <div class="container5">
                  <p class="p-contact--completed-ttl align-center">認証メールが送信されました。</p>
                  <div class="p-signup--cnt-head">
                    <p class="p-signup--completed-msg">認証メールに記載されたURLにアクセスして会員登録を完了してください。 <br>サービスのご利用には、会員登録完了後マイページで登録者情報の設定が必要になります。</p>
                  </div>

                  <div class="p-signup--direct">
                    <!-- <a href="" class="btn-blue2">マイページへ</a> -->
                    <a href="" class="link-blue">トップページへ</a>
                  </div>
                </div>
              </div><!-- ./v-contact -->
            </div><!-- ./contact-content -->
          </div>
        </div><!-- ./contact-content -->
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>