<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li>新規会員登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">新規会員登録</h2>
      </div>
      <div class="p-signup">
        <div class="contact-content">
          <div class="p-contact p-signup">
            <div class="contact-content">
              <div class="v-contact">
                <div class="p-signup--cnt">
                  <div class="p-signup--cnt-head">
                    <p class="desc4 mgb-30">入力内容をご確認ください。<br>入力されたメールアドレスへ認証メールを送信します。<br>会員登録を完了するには認証メールに記載されたURLにアクセスして下さい。</p>
                  </div>
                  <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
                    <form method="post" action="" enctype="multipart/form-data">
                      <div class="c-form">
                        <div class="c-form__row">
                          <label for="nickname" class="c-form__row__label">
                            <span class="c-form__row__label__text">名前</span>
                          </label>
                          <div class="c-form__row__field">
                            山田 花子 <input type="hidden" name="fullname" value="ニックネームも可">
                          </div>
                        </div>
                        <div class="c-form__row">
                          <label for="nickname" class="c-form__row__label">
                            <span class="c-form__row__label__text">ニックネーム</span>
                          </label>
                          <div class="c-form__row__field">
                            長谷川アーリアジャスール<input type="hidden" name="fullname" value="ニックネームも可">
                          </div>
                        </div>
                        <div class="c-form__row">
                          <label for="nickname" class="c-form__row__label">
                            <span class="c-form__row__label__text">メールアドレス</span>
                          </label>
                          <div class="c-form__row__field">
                            例) example@xxxxxx.co.jp <input type="hidden" name="fullname" value="例) example@xxxxxx.co.jp">
                          </div>
                        </div>
                        <div class="c-form__row">
                          <label for="nickname" class="c-form__row__label">
                            <span class="c-form__row__label__text">パスワード</span>
                          </label>
                          <div class="c-form__row__field">
                             半角英数字<input type="hidden" name="fullname" value="半角英数字">
                          </div>
                        </div>
                      </div>
                      <ul class="c-contact__action">
                        <li><input type="submit" name="submitConfirm" value="戻る" class="btn-white3 c-contact__action__button">
                        </li>
                        <li><input type="submit" name="submitConfirm" value="認証メールを送る" class="btn-blue2 c-contact__action__button">
                        </li>
                        </li>
                      </ul>
                    </form>
                    <!-- end .mw_wp_form -->
                  </div>
                </div>
              </div><!-- ./v-contact -->
            </div><!-- ./contact-content -->
          </div>
        </div><!-- ./contact-content -->
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>