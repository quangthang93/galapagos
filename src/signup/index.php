<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li>新規会員登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">新規会員登録</h2>
      </div>
      <div class="p-signup">
        <div class="contact-content">
          <div class="p-contact p-signup">
            <div class="contact-content">
              <div class="v-contact">
                <div class="p-signup--cnt">
                  <div class="p-signup--cnt-head">
                    <p class="desc3 mgb-10">新規会員登録を行います。下記の項目を入力し、確認画面へお進みください。</p>
                    <p class="desc4 mgb-20">※登録完了後に設定変更ができます。 <br>※サービスのご利用には、登録完了後、マイページでの設定が必要になります。</p>
                  </div>
                  <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
                    <form method="post" action="" enctype="multipart/form-data">
                      <div class="c-form">
                        <div class="c-form__row"><label for="company" class="c-form__row__label"><span class="c-form__row__label__text">名前</span><span class="c-form__required">※</span></label>
                          <p></p>
                          <div class="c-form__row__field"><input type="text" name="company" id="company" class="c-form__input type2" size="60" value="" placeholder="入力してください">
                          </div>
                        </div>
                        <div class="c-form__row"><label for="company" class="c-form__row__label"><span class="c-form__row__label__text">ニックネーム</span><span class="c-form__required">※</span></label>
                          <p></p>
                          <div class="c-form__row__field"><input type="text" name="company" id="company" class="c-form__input type2" size="60" value="" placeholder="入力してください">
                          </div>
                        </div>
                        <div class="c-form__row"><label for="company" class="c-form__row__label"><span class="c-form__row__label__text">メールアドレス</span><span class="c-form__required">※</span></label>
                          <p></p>
                          <div class="c-form__row__field"><input type="text" name="company" id="company" class="c-form__input type2" size="60" value="" placeholder="例) example@xxxxxx.co.jp">
                          </div>
                        </div>
                        <div class="c-form__row"><label for="company" class="c-form__row__label"><span class="c-form__row__label__text">パスワード</span><span class="c-form__required">※</span></label>
                          <p></p>
                          <div class="c-form__row__field"><input type="text" name="company" id="company" class="c-form__input type2" size="60" value="" placeholder="半角英数字">
                          </div>
                        </div>
                      </div>
                      <ul class="c-contact__action">
                        <li><input type="submit" name="submitConfirm" value="確認画面へ" class="btn-blue2 c-contact__action__button">
                        </li>
                      </ul>
                      <input type="hidden" id="mw_wp_form_token" name="mw_wp_form_token" value="5f44e21288"><input type="hidden" name="_wp_http_referer" value="/contact/"><input type="hidden" name="mw-wp-form-form-id" value="215"><input type="hidden" name="mw-wp-form-form-verify-token" value="5568ba116390563b279f106b7571c3e8be9dee21">
                    </form>
                    <!-- end .mw_wp_form -->
                  </div>
                </div>
              </div><!-- ./v-contact -->
            </div><!-- ./contact-content -->
          </div>
        </div><!-- ./contact-content -->
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>