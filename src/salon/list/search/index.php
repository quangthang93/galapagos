<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li>飼い主情報</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">飼い主情報</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-hospitalList">
            <div class="p-reservation--item mgb-20">
              <h3 class="ttl-bg">検索結果</h3>
              <div class="p-reservation--item-cnt pt-20 pb-20">

                <div class="p-reservation--countWrap">
                  <p class="p-reservation--count">
                  <span>2,345</span>件
                  </p>
                  <select name="sub01" class="select input type8 mr-20">
                    <option value="">予約日で並べ替え</option>
                    <option value="" selected="selected">予約日で並べ替え</option>
                    <option value="">予約日で並べ替え</option>
                  </select>
                </div>
                <div class="p-reservation--table">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="pc-only3" scope="col">No.</th>
                        <th scope="col">ご利用日</th>
                        <th scope="col">オーナー様</th>
                        <th scope="col">ペット</th>
                        <th scope="col">メニュー</th>
                        <th scope="col">利用回数</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td class="pc-only3 align-center">1203070</td>
                        <td class="align-center">2021年08月10日</td>
                        <td>苗字 名前　様</td>
                        <td>はな（9歳）</td>
                        <td>初診　眼科</td>
                        <td class="align-center">1回</td>
                      </tr>
                      <tr>
                        <td class="pc-only3 align-center">1203070</td>
                        <td class="align-center">2021年08月10日</td>
                        <td>苗字 名前　様</td>
                        <td>はな（9歳）</td>
                        <td>初診　眼科</td>
                        <td class="align-center">1回</td>
                      </tr>
                      <tr>
                        <td class="pc-only3 align-center">1203070</td>
                        <td class="align-center">2021年08月10日</td>
                        <td>苗字 名前　様</td>
                        <td>はな（9歳）</td>
                        <td>初診　眼科</td>
                        <td class="align-center">1回</td>
                      </tr>
                      <tr>
                        <td class="pc-only3 align-center">1203070</td>
                        <td class="align-center">2021年08月10日</td>
                        <td>苗字 名前　様</td>
                        <td>はな（9歳）</td>
                        <td>初診　眼科</td>
                        <td class="align-center">1回</td>
                      </tr>
                      <tr>
                        <td class="pc-only3 align-center">1203070</td>
                        <td class="align-center">2021年08月10日</td>
                        <td>苗字 名前　様</td>
                        <td>はな（9歳）</td>
                        <td>初診　眼科</td>
                        <td class="align-center">1回</td>
                      </tr>
                      <tr>
                        <td class="pc-only3 align-center">1203070</td>
                        <td class="align-center">2021年08月10日</td>
                        <td>苗字 名前　様</td>
                        <td>はな（9歳）</td>
                        <td>初診　眼科</td>
                        <td class="align-center">1回</td>
                      </tr>
                      <tr>
                        <td class="pc-only3 align-center">1203070</td>
                        <td class="align-center">2021年08月10日</td>
                        <td>苗字 名前　様</td>
                        <td>はな（9歳）</td>
                        <td>初診　眼科</td>
                        <td class="align-center">1回</td>
                      </tr>
                      <tr>
                        <td class="pc-only3 align-center">1203070</td>
                        <td class="align-center">2021年08月10日</td>
                        <td>苗字 名前　様</td>
                        <td>はな（9歳）</td>
                        <td>初診　眼科</td>
                        <td class="align-center">1回</td>
                      </tr>
                      <tr>
                        <td class="pc-only3 align-center">1203070</td>
                        <td class="align-center">2021年08月10日</td>
                        <td>苗字 名前　様</td>
                        <td>はな（9歳）</td>
                        <td>初診　眼科</td>
                        <td class="align-center">1回</td>
                      </tr>
                      <tr>
                        <td class="pc-only3 align-center">1203070</td>
                        <td class="align-center">2021年08月10日</td>
                        <td>苗字 名前　様</td>
                        <td>はな（9歳）</td>
                        <td>初診　眼科</td>
                        <td class="align-center">1回</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="p-pagination mt-40">
                <p class="p-pagination-label">0,000件中｜1〜30件 表示</p>
                <div class="p-pagination-list">
                  <a class="ctrl prev" href=""></a>
                  <a class="active" href="">1</a>
                  <a href="">2</a>
                  <a href="">3</a>
                  <a href="">4</a>
                  <a href="">5</a>
                  <a href="">6</a>
                  <div class="p-pagination-spacer">…</div>
                  <a href="">12</a>
                  <a class="ctrl next" href=""></a>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>