<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li>レビュー</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">レビュー</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-hospitalReview">
            <div class="p-edit--dateWrap">
              <span class="label-gray">登録日</span>
              <span class="desc4">2021年10月01日</span>
            </div>
            <form class="form">
              <div class="p-hospitalReview--list">
                <div class="p-hospitalReview--item">
                  <div class="p-hospitalReview--item-head">
                    <p class="txt-bold2">ニックネーム</p>
                    <div class="p-hospitalReview--item-head-rating">
                      <div class="p-hospitalReview--item-head-rating-stars">
                        <span>
                          <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                        </span>
                        <span>
                          <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                        </span>
                        <span>
                          <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                        </span>
                        <span>
                          <img src="<?php echo $PATH;?>/assets/images/common/star-white-small.svg" alt="">
                        </span>
                        <span>
                          <img src="<?php echo $PATH;?>/assets/images/common/star-white-small.svg" alt="">
                        </span>
                      </div>
                      <div class="p-hospitalReview--item-head-rating-points">
                        3.2
                      </div>
                    </div>
                  </div>
                  <div class="p-hospitalReview--item-cnt">
                    <div class="p-hospitalReview--item-desc">
                      知人からの評判通り、院長先生が丁寧に診察してくださいました。 <br>初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。 <br>受付の方の対応も良かったですよ。
                    </div>
                    <div class="p-hospitalReview--item-dateWrap">
                      <div class="div p-hospitalReview--item-dateWrap-child">
                        <span class="label-gray">種類</span>
                        <span class="desc4">犬 | チワワ</span>
                      </div>
                      <div class="div p-hospitalReview--item-dateWrap-child">
                        <span class="label-gray">診察日</span>
                        <span class="desc4">2021年5月2日</span>
                      </div>
                    </div>
                  </div>
                  <div class="p-hospitalReview--item-msgBox">
                    <p class="ttl-bold5">返事</p>
                    <a href="" class="p-hospitalReview--item-msgBox-edit link-blue">編集する</a>
                    <p>今回はご利用いただきありがとうございました。 猫ちゃんの容態が良くなって本当に良かった。またお困りごとなどありましたらご来院ください。 スタッフ一同、お客様の猫ちゃんのために尽力いたします。</p>
                    <div class="d_flex2 mt-10">
                      <span class="label-gray">返信済</span>
                      <span class="desc4">2021年05月18日　10:24</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-ctrl2--link align-center">
                <a href="" class="link-blue">戻る</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>