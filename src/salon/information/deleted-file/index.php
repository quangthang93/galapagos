<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約設定</a></li>
      <li>ペット用施設情報編集</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">ペット用施設情報編集</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit pdt-40">
            <form class="form">
              <div class="form-row align-top">
                <div class="form-label">施設詳細<br>から探す</div>
                <div class="form-row--cnt">
                  <div class="list-cbox2">
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-1" type="checkbox" value="value1">
                      <label for="cbox1-1">本日営業</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-2" type="checkbox" value="value1">
                      <label for="cbox1-2">送迎</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-3" type="checkbox" value="value1">
                      <label for="cbox1-3">駐車場</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-4" type="checkbox" value="value1">
                      <label for="cbox1-4">ホテル併設</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-5" type="checkbox" value="value1">
                      <label for="cbox1-5">クレジット対応</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-6" type="checkbox" value="value1">
                      <label for="cbox1-6">当日対応OK</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-7" type="checkbox" value="value1">
                      <label for="cbox1-7">写真撮影</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-8" type="checkbox" value="value1">
                      <label for="cbox1-8">各種割引</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-9" type="checkbox" value="value1">
                      <label for="cbox1-9">しつけ教室</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-10" type="checkbox" value="value1">
                      <label for="cbox1-10">お手入れ教室</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-11" type="checkbox" value="value1">
                      <label for="cbox1-11">ケージレス対応可</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-12" type="checkbox" value="value1">
                      <label for="cbox1-12">日本犬相談可</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-13" type="checkbox" value="value1">
                      <label for="cbox1-13">シニア犬相談可</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-14" type="checkbox" value="value1">
                      <label for="cbox1-14">デザインカット 対応可</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-15" type="checkbox" value="value1">
                      <label for="cbox1-15">スマホ決済対応</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-16" type="checkbox" value="value1">
                      <label for="cbox1-16">マイクロバブル</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox1-17" type="checkbox" value="value1">
                      <label for="cbox1-17">パック対応可</label>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-row align-top">
                <div class="form-label">こだわり<br>項目</div>
                <div class="form-row--cnt">
                  <div class="list-cbox2">
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox2-1" type="checkbox" value="value1">
                      <label for="cbox2-1">病院併設</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox2-2" type="checkbox" value="value1">
                      <label for="cbox2-2">出張サービス</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox2-3" type="checkbox" value="value1">
                      <label for="cbox2-3">猫相談可</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox2-4" type="checkbox" value="value1">
                      <label for="cbox2-4">大型犬相談可</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox2-5" type="checkbox" value="value1">
                      <label for="cbox2-5">ハサミ対応可</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row align-top">
                <div class="form-label">併設サービス</div>
                <div class="form-row--cnt">
                  <div class="list-cbox2">
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox3-1" type="checkbox" value="value1">
                      <label for="cbox3-1">動物病院</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox3-2" type="checkbox" value="value1">
                      <label for="cbox3-2">ペットホテル</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox3-3" type="checkbox" value="value1">
                      <label for="cbox3-3">ドッグラン</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox3-4" type="checkbox" value="value1">
                      <label for="cbox3-4">同伴カフェ</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox3-5" type="checkbox" value="value1">
                      <label for="cbox3-5">しつけ教室</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox3-6" type="checkbox" value="value1">
                      <label for="cbox3-6">ペットシッター</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox3-7" type="checkbox" value="value1">
                      <label for="cbox3-7">里親</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox3-8" type="checkbox" value="value1">
                      <label for="cbox3-8">訓練</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox3-8" type="checkbox" value="value1">
                      <label for="cbox3-8">その他</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row align-top">
                <div class="form-label">対応動物種</div>
                <div class="form-row--cnt">
                  <div class="list-cbox2">
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox4-1" type="checkbox" value="value1">
                      <label for="cbox4-1">犬</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox4-2" type="checkbox" value="value1">
                      <label for="cbox4-2">猫</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox4-3" type="checkbox" value="value1">
                      <label for="cbox4-3">うさぎ</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox4-4" type="checkbox" value="value1">
                      <label for="cbox4-4">その他</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">最寄り駅</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" placeholder="" value="赤羽橋駅">
                  </div>
                </div>
              </div>

              <div class="form-row">
                <div class="form-label">駅からの距離</div>
                <div class="form-row--cnt">
                  <div class="form-groupField">
                    <div class="form-groupField--item">
                      <span>徒歩</span>
                      <input type="text" name="name" class="input type2" value="5">
                      <span>分</span>
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-row align-top">
                <div class="form-label">診療時間</div>
                <div class="form-row--cnt">
                  <div class="form-row2 mb-20">
                    <div class="form-row2--label">営業時間</div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <textarea type="text" name="name" class="input textarea2" placeholder="">月～金　10:00～13:00　14:00～17:00 &#13;&#10;土曜日　10:00～12:00 &#13;&#10;休診日　日曜日・祝日  
                        </textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-row2">
                    <div class="form-row2--label">備考</div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="text" name="name" class="input" placeholder="" value="年末年始、夏季休業あり">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">電話番号</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" placeholder="入力してください" value="0312345678">
                  </div>
                </div>
              </div>
              <div class="form-row align-top">
                <div class="form-label">SNS</div>
                <div class="form-row--cnt">
                  <div class="form-facilities mb-20">
                    <p class="desc5">Twitter</p>
                    <div class="mgt-5">
                      <input type="text" name="name" class="input" placeholder="入力してください" value="https://www.twitter.com/animal">
                    </div>
                  </div>
                  <div class="form-facilities mb-20">
                    <p class="desc5">Facebook</p>
                    <div class="mgt-5">
                      <input type="text" name="name" class="input" placeholder="入力してください" value="https://www.facebook.com/animal">
                    </div>
                  </div>
                  <div class="form-facilities">
                    <p class="desc5">Instagram</p>
                    <div class="mgt-5">
                      <input type="text" name="name" class="input" placeholder="入力してください" value="https://www.instagram.com/animal">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">アクセス</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <textarea class="input textarea5" name="" id="" cols="30" rows="10">東京都世田谷区北沢2-28-8 下北沢駅北口から出て、鎌倉通りを北へ進みます。 徒歩7分で着きます。</textarea>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">Googleマップ</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" placeholder="入力してください" value="https://goo.gl/maps/bceAvwnay5ZGNBe79">
                  </div>
                </div>
              </div>
              <div class="form-row type2 align-top">
                <div class="form-label">施設画像</div>
                <div class="form-row--cnt">
                  <div class="form-image">
                    <div class="form-image--item">
                      <div class="form-image--item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/hospital/hospital01.png" alt="">
                      </div>
                      <div class="form-image--item-ctrl">
                        <a href="javascript:void(0)" class="btn-del2 type2">
                          <span>削除</span>
                        </a>
                      </div>
                    </div>
                    <div class="form-image--item">
                      <div class="form-image--item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/hospital/hospital02.png" alt="">
                      </div>
                      <div class="form-image--item-ctrl">
                        <a href="javascript:void(0)" class="btn-del2 type2">
                          <span>削除</span>
                        </a>
                      </div>
                    </div>
                    <div class="form-image--item">
                      <div class="form-image--item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/hospital/hospital03.png" alt="">
                      </div>
                      <div class="form-image--item-ctrl">
                        <a href="javascript:void(0)" class="btn-del2 type2">
                          <span>削除</span>
                        </a>
                      </div>
                    </div>
                    <div class="form-image--item last">
                      <a class="form-row--upload-btn btn-blue">ファイルを選択</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row type2 align-top">
                <div class="form-label">施設紹介</div>
                <div class="form-row--cnt">
                  <div class="form-facilities mb-20">
                    <p class="desc5">概要</p>
                    <div class="mgt-5">
                      <textarea class="input textarea5" name="" id="" cols="30" rows="10">どんな些細なこともご相談ください。ていねいにお話を伺う動物病院です。あなたの大切なペットの為、私たちは真摯にお答えします。</textarea>
                    </div>
                  </div>
                  <div class="form-facilities">
                    <p class="desc5">本文</p>
                    <div class="mgt-5">
                      <textarea class="input textarea2" name="" id="" cols="30" rows="10">当院は、飼い主様のどんな些細な不安やお悩みもお話しいただけるような動物病院を目指しております。病気の治療をするためだけの場所ではなく、日頃の迷いをお気軽にご相談いただける場所としても、お役立てください。もちろん、病気の治療に際しても、飼い主様に対するていねいなヒアリングを心掛けております。また、ペットの痛みや辛さを少しでも軽減できるような診療に努めております。ほかにも、しつけ教室や歯磨き教室などの取り組みで、ペットとの暮らしをサポートさせていただきます。</textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row type2 align-top">
                <div class="form-label">スタッフ紹介</div>
                <div class="form-row--cnt">
                  <div class="form-facilities mb-20">
                    <p class="desc5">見出し</p>
                    <div class="mgt-5">
                      <textarea class="input textarea5" name="" id="" cols="30" rows="10">スタッフ一同、オーナー様のペットに対して丁寧に対応いたします。</textarea>
                    </div>
                  </div>
                  <div class="form-facilities mb-20">
                    <p class="desc5">本文</p>
                    <div class="mgt-5">
                      <textarea class="input textarea" name="" id="" cols="30" rows="10">ペットの現状を把握するためには、飼い主様に対してていねいなヒアリングをおこなうことが重要であると考えております。動物は自分で辛さを語ることができないうえ、生き抜くための本能によって症状を我慢し、隠してしまいます。そのため、ペットの代わりに、飼い主様から病状を詳しくお伝えいただくことが、病気の発見や、不要な検査を避けるきっかけとなります。もちろん、わからないことは「わからない」とありのままをお話しいただくことが大切です。飼い主様と連携し、ペットそれぞれに適した治療法を選択していければと思っております。</textarea>
                    </div>
                  </div>
                  <div class="form-image">
                    <div class="form-image--item">
                      <div class="form-image--item-thumb">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/hospital/hospital02.png" alt="">
                      </div>
                      <div class="form-image--item-ctrl">
                        <a href="javascript:void(0)" class="btn-del2">
                          <span>削除</span>
                        </a>
                      </div>
                    </div>
                    <div class="form-image--item last">
                      <a class="form-row--upload-btn btn-blue">ファイルを選択</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row type2 align-top">
                <div class="form-label">店長あいさつ</div>
                <div class="form-row--cnt">
                  <div class="form-facilities mb-20">
                    <p class="desc5">見出し</p>
                    <div class="mgt-5">
                      <textarea class="input textarea5" name="" id="" cols="30" rows="10">我慢してしまいがちなペットのために、ていねいなヒアリングをいたします。</textarea>
                    </div>
                  </div>
                  <div class="form-facilities">
                    <p class="desc5">本文</p>
                    <div class="mgt-5">
                      <textarea class="input textarea" name="" id="" cols="30" rows="10">ペットの現状を把握するためには、飼い主様に対してていねいなヒアリングをおこなうことが重要であると考えております。動物は自分で辛さを語ることができないうえ、生き抜くための本能によって症状を我慢し、隠してしまいます。そのため、ペットの代わりに、飼い主様から病状を詳しくお伝えいただくことが、病気の発見や、不要な検査を避けるきっかけとなります。もちろん、わからないことは「わからない」とありのままをお話しいただくことが大切です。飼い主様と連携し、ペットそれぞれに適した治療法を選択していければと思っております。</textarea>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row type2 align-top">
                <div class="form-label">こだわりポイント</div>
                <div class="form-row--cnt">
                  <div class="form-itemDel">
                    <div class="form-itemDel--head">
                      <p class="form-itemDel--ttl">ポイント1</p>
                      <a href="javascript:void(0)" class="btn-del2">
                          <span>削除</span>
                        </a>
                    </div>
                    <div class="form-facilities mb-20">
                      <p class="desc5">タイトル</p>
                      <div class="mgt-5">
                        <textarea class="input textarea5" name="" id="" cols="30" rows="10">駅から徒歩2分。専用駐車場と提携有料駐車場もあり。</textarea>
                      </div>
                    </div>
                    <div class="form-facilities">
                      <p class="desc5">見出し</p>
                      <div class="mgt-5">
                        <textarea class="input textarea2" name="" id="" cols="30" rows="10">小田急線、京王井の頭線「下北沢駅」北口または西口から徒歩2分の、駅近な立地です。複数の路線が乗り入れており、各方面からアクセスできます。また、1台分の専用駐車場に加え、提携の有料駐車場もあるため、車でのアクセスにも便利。</textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-itemDel">
                    <div class="form-itemDel--head">
                      <p class="form-itemDel--ttl">ポイント2</p>
                      <a href="javascript:void(0)" class="btn-del2">
                          <span>削除</span>
                        </a>
                    </div>
                    <div class="form-facilities mb-20">
                      <p class="desc5">タイトル</p>
                      <div class="mgt-5">
                        <textarea class="input textarea5" name="" id="" cols="30" rows="10">駅から徒歩2分。専用駐車場と提携有料駐車場もあり。</textarea>
                      </div>
                    </div>
                    <div class="form-facilities">
                      <p class="desc5">見出し</p>
                      <div class="mgt-5">
                        <textarea class="input textarea2" name="" id="" cols="30" rows="10">小田急線、京王井の頭線「下北沢駅」北口または西口から徒歩2分の、駅近な立地です。複数の路線が乗り入れており、各方面からアクセスできます。また、1台分の専用駐車場に加え、提携の有料駐車場もあるため、車でのアクセスにも便利。</textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-itemDel">
                    <div class="form-itemDel--head">
                      <p class="form-itemDel--ttl">ポイント3</p>
                      <a href="javascript:void(0)" class="btn-del2">
                          <span>削除</span>
                        </a>
                    </div>
                    <div class="form-facilities mb-20">
                      <p class="desc5">タイトル</p>
                      <div class="mgt-5">
                        <textarea class="input textarea5" name="" id="" cols="30" rows="10">駅から徒歩2分。専用駐車場と提携有料駐車場もあり。</textarea>
                      </div>
                    </div>
                    <div class="form-facilities">
                      <p class="desc5">見出し</p>
                      <div class="mgt-5">
                        <textarea class="input textarea2" name="" id="" cols="30" rows="10">小田急線、京王井の頭線「下北沢駅」北口または西口から徒歩2分の、駅近な立地です。複数の路線が乗り入れており、各方面からアクセスできます。また、1台分の専用駐車場に加え、提携の有料駐車場もあるため、車でのアクセスにも便利。</textarea>
                      </div>
                    </div>
                  </div>
                  <div class="align-center mgt-15">
                    <a href="javascript:void(0)" class="btn-add2 js-btnAdd"></a>
                  </div>
                </div>
              </div>

              <div class="form-ctrl2">
                <div class="btn-submitWrap">
                  <a href="" class="btn-submit type4">編集内容を保存する</a>
                </div>
                <div class="form-ctrl2--link">
                  <a href="" class="link-blue">マイページトップへ戻る</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- modal box -->
<div class="modal-complete">
  <div class="modal-complete--cnt">
    <div class="modal-complete--cnt-inner">
      <div class="modal-complete--cnt-icon"><img src="<?php echo $PATH;?>/assets/images/common/icon-check.svg" alt=""></div>
      <p class="modal-complete--cnt-msg">ファイルを<br>削除しました。</p>
    </div>
  </div>
</div>
<!-- ./modal box -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>