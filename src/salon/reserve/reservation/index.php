<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約状況</a></li>
      <li>新規予約登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約状況</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">メニュー選択</h2>
              <div class="p-reserve--list pb-0">
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">カット＆シャンプー（短毛犬種）</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">カット＆シャンプー（長毛犬種）</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円(税込)
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">眼科</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円(税込)
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">平日限定超得◎癒*カット＋フルカラー＋スパ＋トリートメント￥10500→￥6900超得◎癒しコース*カット＋フルカラー＋スパ＋トリートメント￥10500→￥7900</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円(税込)
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
              </div>
              <!-- <div class="align-center">
                <a href="" class="btn-white3 align-center">前のページへ戻る</a>
              </div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>