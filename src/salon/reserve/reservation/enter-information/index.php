<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約状況</a></li>
      <li>新規予約登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">本日の予約</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">情報入力</h2>
              <div class="p-reserve--list p-edit">
                <form class="form">
                  <p class="form-row--ttl border-bot">オーナー様の情報</p>
                  <div class="form-row">
                    <div class="form-label">
                      利用歴
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-radio type2">
                        <span class="form-radio--item type2">
                          <label>
                            <input type="radio" name="cb01" value="初めて" checked>
                            <span class="form-radio--label">初めて</span>
                          </label>
                        </span>
                        <span class="form-radio--item type2">
                          <label>
                            <input type="radio" name="cb01" value="過去に利用したことがある">
                            <span>過去に利用したことがある</span>
                          </label>
                        </span>
                      </div>
                      <div class="mt-10">
                        <a href="/salon/reserve/reservation/enter-information/search" class="form-row--upload-btn btn-blue">利用者情報から選ぶ</a>
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      名前
                      <span class="c-form__required">※</span>
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="text" name="name" class="input" placeholder="本名でご予約ください">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      電話番号
                      <span class="c-form__required">※</span>
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="text" name="name" class="input" placeholder="入力してください">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      E-mail
                      <span class="c-form__required">※</span>
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="email" name="name" class="input" placeholder="入力してください">
                      </div>
                    </div>
                  </div>
                  <p class="form-row--ttl type2">受診するファミリーの情報</p>
                  <div class="form-row border-top">
                    <div class="form-label">
                      名前
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="text" name="name" class="input" placeholder="入力してください">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      種類
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-groupField--item">
                        <div class="form-radio">
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="犬" checked>
                              <span class="form-radio--label">犬</span>
                            </label>
                          </span>
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="猫">
                              <span>猫</span>
                            </label>
                          </span>
                        </div>
                      </div>
                      <div class="form-groupField--item">
                        <div class="form-radio">
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="うさぎ" checked>
                              <span class="form-radio--label">うさぎ</span>
                            </label>
                          </span>
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="フェレット">
                              <span>フェレット</span>
                            </label>
                          </span>
                        </div>
                      </div>
                      <div class="form-groupField--item">
                        <div class="form-radio">
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="鳥類" checked>
                              <span class="form-radio--label">鳥類</span>
                            </label>
                          </span>
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="その他">
                              <span>その他</span>
                            </label>
                          </span>
                        </div>
                      </div>
                      <div class="form-groupField--item">
                        <p class="desc5 mgb-5">その他の詳細（任意）</p>
                        <input type="text" name="name" class="input" placeholder="品種や詳細を入力してください。">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      性別
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-radio type2">
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="オス" checked>
                            <span class="form-radio--label">オス</span>
                          </label>
                        </span>
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="メス">
                            <span>メス</span>
                          </label>
                        </span>
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="分からない">
                            <span>分からない</span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      去勢・避妊
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-radio type2">
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="未" checked>
                            <span class="form-radio--label">未</span>
                          </label>
                        </span>
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="済">
                            <span>済</span>
                          </label>
                        </span>
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="分からない">
                            <span>分からない</span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">備考</div>
                    <div class="form-row--cnt">
                      <textarea name="content" id="content" class="c-form__textarea" cols="50" rows="8" placeholder="ペットの症状や普段服用している薬などありましたらご記入ください。"></textarea>
                    </div>
                  </div>
                  <div class="form-ctrl type2">
                    <div class="form-ctrl--item btn-submitWrap">
                      <a href="" class="btn-white3 align-center">前のページへ戻る</a>
                    </div>
                    <div class="form-ctrl--item btn-submitWrap">
                      <a href="/search/confirm-information/" class="btn-submit">入力情報を確認する</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>