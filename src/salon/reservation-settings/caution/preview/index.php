<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">BiBi犬猫病院</a></li>
      <li>施設予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">施設予約</h2>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebarSearch">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-search.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-message type2">
            <div style="font-size: 14px;">
              <p><span style="color:#5DC1CF; font-size: 16px;"><strong>ご予約について</strong></span></p><br>
              <p><strong>・当院では、動物たちと飼い主様の利便性向上を目的として、『時間帯』予約制を採用しています。</strong></p>
              <br>
              <p>※ 完全予約制 完全予約制となっております。 また、施術の都合上、お預かりのお時間を長めに設定させて頂いております。 10:15の予約の場合：16時お返し 11:15の予約の場合：17時お返し 12:15の予約の場合：18時お返し なお、ネット予約が埋まっていても予約をお受けできる場合がございますので、その場合お電話にて直接お問い合わせください。</p>
              <p>&nbsp;</p>
              <br>
              <p><span style="color:#5DC1CF; font-size: 16px;"><strong>スキンケアサロンを初めてご利用の方へ</strong></span></p><br>
              <p>皮膚の健康チェックやサロン利用のご案内のために、事前の診察が必要となります。サロンのご予約日までに一般診察もしくは皮膚科診察の受診をお願い致します。 また、事前に狂犬病予防ワクチン(犬のみ)、混合ワクチン、ノミダニ予防をお願いしています。体調などによりこれらが行えない方はご相談ください。（なお、ワクチン接種から1週間はトリミングをお受けできません。）</p>
              <p>&nbsp;</p>
              <br>
              <div class="pt-15 pb-15" style="border-top: solid 1px #474F5F; border-bottom: solid 1px #474F5F;">
                <p>
                  皮膚の健康チェックやサロン利用のご案内のために、事前の診察が必要となります。サロンのご予約日までに一般診察もしくは皮膚科診察の受診をお願い致します。 また、事前に狂犬病予防ワクチン(犬のみ)、混合ワクチン、ノミダニ予防をお願いしています。体調などによりこれらが行えない方はご相談ください。（なお、ワクチン接種から1週間はトリミングをお受けできません。）
                </p>
              </div>
              <br>
              <br>
              <br>
            </div>
          </div>
          <div class="form-ctrl2">
                <div class="btn-submitWrap">
                  <a href="" class="btn-submit type4">保存する</a>
                </div>
                <div class="form-ctrl2--link">
                  <a href="" class="link-blue">編集に戻る</a>
                </div>
              </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>