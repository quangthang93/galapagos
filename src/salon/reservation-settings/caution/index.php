<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約設定</a></li>
      <li>予約時注意文</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約時注意文</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-favorite">
            <div class="tabs">
              <div class="tabs-navWrapper">
                <ul class="tabs-nav js-tabsNav">
                  <li class="tabs-item type3 js-tabsItem active">予約メニュー<br>画面</li>
                  <li class="tabs-item type3 js-tabsItem">予約時間<br>選択画面</li>
                  <li class="tabs-item type3 js-tabsItem">予約内容<br>確認画面</li>
                  <li class="tabs-item type3 js-tabsItem">予約完了<br class="sp-only3">画面</li>
                  <li class="tabs-item type3 js-tabsItem">飼い主<br class="sp-only3">マイページ</li>
                </ul>
              </div>
              <div class="tabs-cnt type2 js-tabsCnt">
                <div class="tabs-panel js-tabsPanel">
                  <div class="p-favorite--guide">
                    <a href="" class="btn-blue3">プレビュー</a>
                  </div>
                  <textarea class="text-editor"></textarea>
                  <script>
                    tinymce.init({
                      selector: '.text-editor',
                      plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
                      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
                      toolbar_mode: 'floating',
                      tinycomments_mode: 'embedded',
                      tinycomments_author: 'Author name',
                    });
                  </script>
                  <div class="form-ctrl2">
                    <div class="btn-submitWrap">
                      <a href="" class="btn-submit type4">保存する</a>
                    </div>
                    <div class="form-ctrl2--link">
                      <a href="" class="link-blue">戻る</a>
                    </div>
                  </div>
                </div>
                <div class="tabs-panel js-tabsPanel">tab2</div>
                <div class="tabs-panel js-tabsPanel">tab3</div>
                <div class="tabs-panel js-tabsPanel">tab4</div>
                <div class="tabs-panel js-tabsPanel">tab5</div>
              </div>
            </div>
          </div>
        </div><!-- ./p-reservation -->
      </div>
    </div>
  </div>
  </div>
</main><!-- ./main -->
<!-- Javascript -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>