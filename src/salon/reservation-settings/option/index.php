<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約設定</a></li>
      <li>新規オプション登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">新規オプション登録</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt type2">
          <div class="p-edit p-history pdt-40">
            <form class="form">
              <div class="form-row type3">
                <div class="form-label">オプション名</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" value="" placeholder="入力してください">
                  </div>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">オプション説明</div>
                <div class="form-row--cnt">
                  <textarea name="content" id="content" class="c-form__textarea" cols="50" rows="8" placeholder="入力してください"></textarea>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">所要時間</div>
                <div class="form-row--cnt">
                  <div class="form-groupField">
                    <select name="product" class="select type3 input">
                      <option value="">_</option>
                      <option value="">_</option>
                      <option value="" selected="selected">_</option>
                      <option value="">2022</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">適応メニュー</div>
                <div class="form-row--cnt">
                  <div class="form-radio mb-20">
                    <span class="form-radio--item type2">
                      <label>
                        <input type="radio" name="type" value="すべてのメニュー">
                        <span class="form-radio--label">すべてのメニュー</span>
                      </label>
                    </span>
                    <span class="form-radio--item type2">
                      <label>
                        <input type="radio" name="type" value="特定のメニュー" checked>
                        <span>特定のメニュー</span>
                      </label>
                    </span>
                  </div>
                  <div class="list-cbox type3">
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox01" type="checkbox" value="value1">
                      <label for="cbox01">カット＆シャンプー（短毛猫種）</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox02" type="checkbox" value="value1">
                      <label for="cbox02">カット＆シャンプー（長毛猫種）</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox03" type="checkbox" value="value1">
                      <label for="cbox03">カット＆シャンプー（短毛犬種）</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox04" type="checkbox" value="value1">
                      <label for="cbox04">カット＆シャンプー（長毛犬種）</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox05" type="checkbox" value="value1">
                      <label for="cbox05">シャンプー</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox06" type="checkbox" value="value1">
                      <label for="cbox06">マイクロバブル</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox07" type="checkbox" value="value1">
                      <label for="cbox07">平日限定超得◎癒*カット＋フルカラー</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox08" type="checkbox" value="value1">
                      <label for="cbox08">【お泊り】小型犬、猫　1泊2日</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox09" type="checkbox" value="value1">
                      <label for="cbox09">【お泊り】中型犬　1泊2日</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox10" type="checkbox" value="value1">
                      <label for="cbox10">【お泊り】大型犬　1泊2日</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox11" type="checkbox" value="value1">
                      <label for="cbox11">【お泊り】ハムスター・モルモット　1泊2日</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox12" type="checkbox" value="value1">
                      <label for="cbox12">【お預かり】小型犬・猫　半日</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox13" type="checkbox" value="value1">
                      <label for="cbox13">【お預かり】中型犬　半日</label>
                    </div>
                    <div class="checkboxWrap type2">
                      <input class="checkbox3 js-cbox" id="cbox14" type="checkbox" value="value1">
                      <label for="cbox14">【お預かり】大型犬　半日</label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">料金</div>
                <div class="form-row--cnt">
                  <div class="form-input type-btn">
                    <div class="_num"><input type="text" name="name" class="input" value="1,000" placeholder="入力してください"></div>
                    <div class="_unit"><input type="text" name="name" class="input mgl-10" value="" placeholder="円〜"></div>
                  </div>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">掲載期間</div>
                <div class="form-row--cnt">
                  <div class="form-radio">
                    <span class="form-radio--item type2">
                      <label>
                        <input type="radio" name="type" value="常に掲載">
                        <span class="form-radio--label">常に掲載</span>
                      </label>
                    </span>
                    <span class="form-radio--item type2">
                      <label>
                        <input type="radio" name="type" value="特定の期間" checked>
                        <span>特定の期間</span>
                      </label>
                    </span>
                  </div>
                </div>
              </div>
              <div class="form border-top-none">
                <div class="form-rowWrap2 mt-0">
                  <div class="form-row">
                    <div class="form-groupFieldWrap type4">
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">2021</option>
                            <option value="">2022</option>
                          </select>
                        </div>
                        <span class="_2dots">年</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">月</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">日</span>
                      </div>
                      <div class="form-groupFieldWrap--connect">〜</div>
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">2021</option>
                            <option value="">2022</option>
                          </select>
                        </div>
                        <span class="_2dots">年</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">月</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">日</span>
                      </div>
                    </div>
                  </div>
                  <div class="btn-del2Wrap align-center mgb-20">
                    <a href="javascript:void(0)" class="btn-del2">
                      <span>削除</span>
                    </a>
                  </div>
                </div>
                <div class="align-center mgt-15">
                  <a href="javascript:void(0)" class="btn-add2 js-btnAdd"></a>
                </div>
              </div>
              <div class="form-row type3 border-top mt-20">
                <div class="form-label">ステータス</div>
                <div class="form-row--cnt">
                  <div class="form-radio">
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="掲載前">
                        <span class="form-radio--label">掲載前</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="掲載中" checked>
                        <span>掲載中</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="掲載終了" checked>
                        <span>掲載終了</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="非表示" checked>
                        <span>非表示</span>
                      </label>
                    </span>
                  </div>
                </div>
              </div>
              <div class="form border-top-none">
                <div class="form-ctrl2">
                  <div class="btn-submitWrap">
                    <a href="" class="btn-submit type4">登録する</a>
                  </div>
                  <div class="form-ctrl2--link">
                    <a href="" class="link-blue">戻る</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>