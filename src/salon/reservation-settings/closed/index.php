<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約設定</a></li>
      <li>休業設定</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">休業設定</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt type2">
          <div class="p-edit p-history pdt-40">
            <p class="p-hospital--subttl desc4">設定した日時、メニューが休業扱いとなり、予約を受け付けなくなります。</p>
            <form class="form border-top-none">
              <div class="form-rowWrap2">
                <div class="form-row type3">
                  <div class="form-label">休業日</div>
                  <div class="form-row--cnt">
                    <div class="form-groupFieldWrap type2">
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">2021</option>
                            <option value="">2022</option>
                          </select>
                        </div>
                        <span class="_2dots">年</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">月</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">日</span>
                      </div>
                      <div class="form-groupFieldWrap--connect">〜</div>
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">2021</option>
                            <option value="">2022</option>
                          </select>
                        </div>
                        <span class="_2dots">年</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">月</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">日</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">時間指定</div>
                  <div class="form-row--cnt">
                    <div class="form-groupField--item mb-15">
                      <div class="form-radio">
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="sex" value="終日" checked="">
                            <span class="form-radio--label">終日</span>
                          </label>
                        </span>
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="sex" value="指定あり">
                            <span>指定あり</span>
                          </label>
                        </span>
                      </div>
                    </div>
                    <div class="form-groupFieldWrap type2">
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">00</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                          </select>
                        </div>
                        <span class="_2dots">:</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">00</option>
                            <option value="">30</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-groupFieldWrap--connect type2">〜</div>
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">00</option>
                            <option value="">19</option>
                            <option value="">20</option>
                          </select>
                          <span class="_2dots">:</span>
                        </div>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">00</option>
                            <option value="">30</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">メニュー</div>
                  <div class="form-row--cnt">
                    <div class="form-groupField--item mb-15">
                      <div class="form-radio">
                        <span class="form-radio--item type2">
                          <label>
                            <input type="radio" name="menu" value="すべてのメニュー" checked="">
                            <span class="form-radio--label">すべてのメニュー</span>
                          </label>
                        </span>
                        <span class="form-radio--item type2">
                          <label>
                            <input type="radio" name="menu" value="特定のメニュー">
                            <span>特定のメニュー</span>
                          </label>
                        </span>
                      </div>
                    </div>
                    <div class="list-cbox type2">
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02-1" type="checkbox" value="value1">
                        <label for="cbox02-1">混合ワクチン接種</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02-2" type="checkbox" value="value1">
                        <label for="cbox02-2">狂犬病予防接種</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02-3" type="checkbox" value="value1">
                        <label for="cbox02-3">フィラリア予防</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="btn-del2Wrap align-center mgb-20">
                  <a href="javascript:void(0)" class="btn-del2">
                    <span>削除</span>
                  </a>
                </div>
              </div>
              <div class="align-center mgt-15">
                <a href="javascript:void(0)" class="btn-add2 js-btnAdd"></a>
              </div>
              <div class="form-ctrl2">
                <div class="btn-submitWrap">
                  <a href="" class="btn-submit type4">登録する</a>
                </div>
                <div class="form-ctrl2--link">
                  <a href="" class="link-blue">戻る</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>