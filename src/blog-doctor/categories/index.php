<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-blog-doctor.php'; ?>
<main class="main blog">
  <div class="p-blog">
    <div class="p-blog--banner doctor">
      <div class="p-blog--banner-logo">
        <img src="<?php echo $PATH;?>/assets/images/blog/banner-logo-doctor.png" alt="">
      </div>
    </div>
    <div class="p-blog--cntWrap">
      <div class="breadcrumb p-0">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/">カテゴリ一覧</a></li>
          <li>記事詳細</a></li>
        </ul>
      </div>
      <div class="p-blog--cnt">
        <div class="p-blog--mainCnt col-lg-8 col-12">
          <h2 class="p-blog--ttl type2">カテゴリ1</h2>
          <div class="posts-wrap row">
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog01.png" alt="">
                    </a>
                  </div> 
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog02.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog03.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog04.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog05.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog06.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog07.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog08.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog09.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog10.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog09.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-4 col-md-4 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb type2">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog10.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat2">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt type2"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer type2">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-black.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-black.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
          </div>
          <div class="p-pagination mt-40">
            <p class="p-pagination-label">0,000件中｜1〜30件 表示</p>
            <div class="p-pagination-list">
              <a class="ctrl prev" href=""></a>
              <a class="active type2" href="">1</a>
              <a href="">2</a>
              <a href="">3</a>
              <a href="">4</a>
              <a href="">5</a>
              <a href="">6</a>
              <div class="p-pagination-spacer">…</div>
              <a href="">12</a>
              <a class="ctrl next" href=""></a>
            </div>
          </div>
        </div>
        <div class="p-blog--sidebar col-lg-4 col-12">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-blog-doctor.php'; ?>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<div class="p-blog--contact type2">
  <a class="link" href="/contact">CONTACT</a>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-blog-doctor.php'; ?>