<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li>ログイン・新規会員登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">ログイン・新規会員登録</h2>
      </div>
      <div class="p-login">
        <div class="contact-content">
          <div class="p-login row">
            <div class="p-login--col col-lg-6 col-md-6 col-12">
              <div class="p-login--col-inner js-loginForm">
                <p class="ttl-bold6 align-center">ログイン</p>
                <form class="form p-login--form">
                  <div class="form-row">
                    <div class="form-input">
                      <input type="text" name="name" class="input" value="" placeholder="メールアドレス">
                    </div>
                    <div class="form-input">
                      <input type="text" name="name" class="input" value="" placeholder="パスワード">
                    </div>
                  </div>
                  <div class="p-login--cbox">
                    <div class="checkboxWrap">
                      <input class="checkbox2 js-cbox" id="cbox01" type="checkbox" value="value1">
                      <label for="cbox01"></label>
                    </div>
                    <label for="cbox01" class="p-login--cbox-label">
                      次回から自動的にログインする
                    </label>
                  </div>
                  <div>
                    <div class="p-login--submit"><a href="/mypage/register/edit-complete" class="btn-submit type2 js-btnSave">ログイン</a></div>
                    <div class="align-center p-login--direct">
                      <a href="/forgot-pw" class="link-blue">パスワードをお忘れの方はこちら</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="p-login--col col-lg-6 col-md-6 col-12">
              <div class="p-login--col-inner js-loginForm">
                <p class="ttl-bold6 align-center">新規会員登録</p>
                <p class="p-login--intro desc6">会員登録することで施設のご予約、ご相談ができます。利用することでポイントが貯まり、貯めたポイントで寄附をすることができます。</p>
                <div class="p-login--submit">
                  <a href="/mypage/register/edit-complete" class="btn-yellow4 type2">新規会員登録</a>
                </div>
                <p class="p-login--guide desc6">会員登録の前に、<br> 当サイトの<a href="" class="link-blue type2">会員規約</a>をご確認ください。</p>
              </div>
            </div>
          </div>
        </div><!-- ./contact-content -->
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>