<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-teaser.php'; ?>
<div class="parallax"><span class="para1" id="para1"></span></div>
<div class="main-mv fadein ani">
  <img class="pc-only" src="<?php echo $PATH;?>/assets/images/top/window.png" alt="">
  <img class="sp-only" src="<?php echo $PATH;?>/assets/images/teaser/window-sp_t.png" alt="">
  <p class="main-msg fadeleft ani">ひとりの飼い主として、<br class="sp-only">利用した。</p>
</div>
<div class="parallax type2 js-section01">
  <span class="para2" id="para2"></span>
  <p class="main-msg fadeleft ani-later">ひとりの動物好きとして、<br class="sp-only">応援しようと思った。</p>
  <div class="section section01 js-section01Cnt">
    <div class="section01-magic fadeup heart01">
      <img class="fuwafuwa-t" src="<?php echo $PATH;?>/assets/images/common/heart01.png" alt="">
    </div>
    <div class="section01-magic fadeup heart02">
      <img class="fuwafuwa-t" src="<?php echo $PATH;?>/assets/images/common/heart02.png" alt="">
    </div>
    <div class="section01-magic fadeup heart03">
      <img class="fuwafuwa-t" src="<?php echo $PATH;?>/assets/images/common/heart03.png" alt="">
    </div>
    <div class="section01-magic fadeup heart04">
      <img class="fuwafuwa-t" src="<?php echo $PATH;?>/assets/images/common/heart04.png" alt="">
    </div>
    <div class="section01-magic fadeup heart05">
      <img class="fuwafuwa-t" src="<?php echo $PATH;?>/assets/images/common/heart05.png" alt="">
    </div>
    <div class="section01-magic fadeup heart06">
      <img class="fuwafuwa-t" src="<?php echo $PATH;?>/assets/images/common/heart06.png" alt="">
    </div>

    <picture>
      <!-- <source media="(min-width:1181px)" srcset="<?php echo $PATH;?>/assets/images/top/section01.png"> -->
      <source media="(min-width:769px)" srcset="<?php echo $PATH;?>/assets/images/top/section01.png">
      <source media="(min-width:400px)" srcset="<?php echo $PATH;?>/assets/images/top/section01-tb.png">
      <img src="<?php echo $PATH;?>/assets/images/top/section01-sp.png" alt="">
    </picture>
  </div>

  <div class="section01__msg ani">
    <h3><span><img src="<?php echo $PATH;?>/assets/images/teaser/section-01-msg.svg" alt=""><span></h3>
    <p>動物だけでなく、飼い主や<br>事業従事者、慈善活動をされている方。<br>みんなの負担を減らし、<br>より良い環境を作るために、<br>私たちは活動しています。</p>
  </div>
</div>

<div class="section-about">
  <div class="container2">
    <div class="section-titleWrap">
      <div class="section-titleWrap--inner">
        <h2 class="section-title">ABOUT</h2>
        <div class="section-titleWrap--img fadeup ani">
          <img src="<?php echo $PATH;?>/assets/images/top/title-about.svg" alt="">
        </div>
      </div>
      <div class="section-titleWrap--bg">
        <img src="<?php echo $PATH;?>/assets/images/top/bg-ttl-about.svg" alt="">
      </div>
    </div>
  </div>
  <div class="section-about--cnt">
    <div class="section-about--row about01">
      <div class="section-about--row-inner container2 row">
        <div class="section-about--row-thumb col-lg-6 col-md-6 col-12 fadeup ani">
          <img src="<?php echo $PATH;?>/assets/images/top/about01.png" alt="">
        </div>
        <div class="section-about--row-cnt col-lg-6 col-md-6 col-12">
          <h3 class="ttl-icon ani">施設とつながる</h3>
          <p class="ttl-hand ani"><span>家族(動物)のために施設を探す</span></p>
          <p class="desc7 fadeup ani">病気にさせないための病院、緊急時に行ける病院、ストレスの少ないサロン、アジリティが豊富なドッグラン。<br>目的にあった施設がすぐに見つかる、すぐに予約できる。</p>
          
          <div class="fadeup ani">
            <p class="btn-comingsoon"><span>COMING SOON</span></p>
          </div>
        </div>
      </div>
    </div>
    <div class="section-about--row about02">
      <div class="section-about--row-inner container2 row">
        <div class="section-about--row-thumb fadeup ani col-lg-6 col-md-6 col-12">
          <img src="<?php echo $PATH;?>/assets/images/top/about02.png" alt="">
        </div>
        <div class="section-about--row-cnt col-lg-6 col-md-6 col-12">
          <h3 class="ttl-icon ani doc">専門家とつながる</h3>
          <p class="ttl-hand ani"><span>日常の困った、をすぐに相談・解決</span></p>
          <p class="desc7 fadeup ani">暮らしの中で、ペットの体調はいつも気になるもの。<br>言葉を発せないから、すぐにはわからない。<br>そんなときにあなたのそばに専門家がいれば安心。 <br>いつでも気軽に専門家に相談することができます。</p>
          
          <div class="fadeup ani">
            <p class="btn-comingsoon"><span>COMING SOON</span></p>
          </div>
        </div>
      </div>

      <div class="section-about__start">
        <h3 class="ttl-icon dol ani">動物とつながる</h3>
        <p class="ttl-hand fadeup ani"><span>動物たちの明日のために気軽に応援</span></p>
        <p class="fadeup ani">施設の予約・専門家への相談で貯めたポイントを、<br class="pc-only">動物のために活動する慈善団体へ寄付できます。<br>動物たちの明るい明日のために、あなたも支援をはじめませんか。</p>
        <div class="fadeup ani">
          <div class="btn-comingsoon"><span>COMING SOON</span></div>
        </div>
      </div>
    </div>
  </div>
</div><!-- ./section-about -->

<div class="section-for">
  <div class="section-for__box">
    <h2 class="fadeup ani">掲載希望の店舗の方へ</h2>
    <p class="fadeup ani">掲載をご希望の店舗の方はこちらまでお問い合わせください。</p>
    <a href="tel:042-508-2999" class="section-for__tel fadeup ani"><span>TEL</span>042-508-2999</a>
    <p class="section-for__here fadeup ani">メールでのお問い合わせは<a href="mailto:">こちら</a></p>
  </div>
</div><!-- ./section-for -->

<div class="section-company">
  <div class="section-company__inner">
    <h2 class="fadeup ani">運営会社</h2>
    <table class="fadeup ani">
      <tbody>
        <tr>
          <th>会社名</th>
          <td>株式会社Emyu</td>
        </tr>
        <tr>
          <th>所在地</th>
          <td>東京都国立市東１丁目２番地の１１-２階</td>
        </tr>
        <tr>
          <th>設立日</th>
          <td>2021年4月1日</td>
        </tr>
        <tr>
          <th>資本金</th>
          <td>500万円</td>
        </tr>
        <tr>
          <th>役員</th>
          <td>代表取締役/獣医師 安藤美恵</td>
        </tr>
      </tbody>
    </table>
  </div>
</div><!-- ./section-company -->


<!--<div class="section-gift">
  <p class="section-gift--desc desc2 fadeup ani">施設の予約・専門家への相談で貯めたポイントを、<br>動物のために活動する慈善団体へ寄付できます。<br>動物たちの明るい明日のために、あなたも支援をはじめませんか。</p>
  <p class="section-gift--ttl ttl-bold fadeup ani">ポイントを利用した累計寄付金額</p>
  <p class="section-gift--date fadeup ani"><span class="date2">（2021年5月現在）</span></p>
  <p class="section-gift--price price fadeup ani"><span class="ani" id="topCounter">102,000</span> <span class="unit">円</span></p>
  <div class="section-gift--direct fadeup ani">
    <a href="" class="btn-pink">ギフトページへ</a>
  </div>
</div>--><!-- ./section-gift -->

<!--<div class="section-search">
  <div class="container2">
  	<div class="section-search--magic cat fadeup ani">
  		<img src="<?php echo $PATH;?>/assets/images/top/cat.png" alt="">
  	</div>
  	<div class="section-search--magic dog fadeup ani">
  		<img src="<?php echo $PATH;?>/assets/images/top/dog.png" alt="">
  	</div>
    <div class="section-titleWrap">
      <div class="section-titleWrap--inner">
        <h2 class="section-title">SEARCH</h2>
        <div class="section-titleWrap--img fadeup ani">
          <img src="<?php echo $PATH;?>/assets/images/top/title-search.svg" alt="">
        </div>
      </div>
      <div class="section-titleWrap--bg">
        <img src="<?php echo $PATH;?>/assets/images/top/bg-ttl-search.svg" alt="">
      </div>
    </div>
    <div class="section-search--cnt">
      <div class="tabs2">
        <div class="tabs2-navWrapper">
          <ul class="tabs2-nav row js-tabsNav">
            <li class="tabs2-item js-tabsItem col-lg-6 col-md-6 col-12">
              <div class="tabs2-title">
            	  <span>施設を探す</span>
              </div>

              <ul class="tabs2-menu col3">
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                    <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab1-doc.png" alt="">
                      <figcaption>動物病院</figcaption>
                    </figure>
                  </a>
                </li>
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                  <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab1-salon.png" alt="">
                      <figcaption>サロン</figcaption>
                    </figure>
                  </a>
                </li>
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                  <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab1-hotel.png" alt="">
                      <figcaption>ペットホテル</figcaption>
                    </figure>
                  </a>
                </li>
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                  <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab1-run.png" alt="">
                      <figcaption>ドッグラン</figcaption>
                    </figure>
                  </a>
                </li>
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                  <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab1-cafe.png" alt="">
                      <figcaption>ペット可カフェ</figcaption>
                    </figure>
                  </a>
                </li>
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                  <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab1-room.png" alt="">
                      <figcaption>しつけ教室</figcaption>
                    </figure>
                  </a>
                </li>
              </ul>
            </li>
            <li class="tabs2-item type2 js-tabsItem col-lg-6 col-md-6 col-12">
              <div class="tabs2-title tabs2-title-type2">
            	  <span>獣医師に相談する</span>
              </div>

              <ul class="tabs2-menu tabs2-menu-type2 col3">
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                    <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab2-daily.png" alt="">
                      <figcaption>日常行動</figcaption>
                    </figure>
                  </a>
                </li>
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                  <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab2-injury.png" alt="">
                      <figcaption>ケガ・病気</figcaption>
                    </figure>
                  </a>
                </li>
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                  <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab2-teach.png" alt="">
                      <figcaption>しつけ</figcaption>
                    </figure>
                  </a>
                </li>
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                  <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab2-life.png" alt="">
                      <figcaption>ライフプラン</figcaption>
                    </figure>
                  </a>
                </li>
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                  <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab2-grow.png" alt="">
                      <figcaption>育て方</figcaption>
                    </figure>
                  </a>
                </li>
                <li class="col3-item">
                  <a class="col3-item--inner link" href="">
                  <figure class="thumb">
                      <img src="<?php echo $PATH;?>/assets/images/top/icon-tab2-other.png" alt="">
                      <figcaption>その他</figcaption>
                    </figure>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
        
      </div>
    </div>
  </div>
</div>--><!-- ./section-search -->

<!--<div class="section-column">
	<div class="section-column--magic fadeup ani">
		<img src="<?php echo $PATH;?>/assets/images/top/men.png" alt="">
	</div>
  <div class="section-titleWrap--bg">
    <img src="<?php echo $PATH;?>/assets/images/top/bg-ttl-column.svg" alt="">
  </div>
  
	<div class="section-titleWrap">
    <div class="section-titleWrap--inner">
      <h2 class="section-title">COLUMN</h2>
      <div class="section-titleWrap--img fadeup ani">
        <img class="pc-only" src="<?php echo $PATH;?>/assets/images/top/title-column.svg" alt="">
        <img class="sp-only" src="<?php echo $PATH;?>/assets/images/top/title-column.svg" alt="">
      </div>
    </div>
    
  </div>
  <div class="section-column--cnt">
  	<div class="container2">
	  	<div class="section-column--list row">
	  		<div class="section-column--item fadeup ani col-lg-4 col-md-6 col-12">
	  			<a href="" class="link2">
	  				<div class="section-column--item-thumb">
	  					<img class="cover" src="<?php echo $PATH;?>/assets/images/top/column01.png" alt="">
	  				</div>
	  				<div class="section-column--item-cnt">
              <p class="date3">2021.04.01</p>
              <p class="desc3 txt-bold">伝えたいことがあるの？犬が飼い主のことを何度も見てくる時の心理5選～室内犬・小型犬編～</p>
            </div>
	  			</a>
	  		</div>
	  		<div class="section-column--item fadeup ani delay-2 col-lg-4 col-md-6 col-12">
	  			<a href="" class="link2">
	  				<div class="section-column--item-thumb">
	  					<img class="cover" src="<?php echo $PATH;?>/assets/images/top/column02.png" alt="">
	  				</div>
	  				<div class="section-column--item-cnt">
              <p class="date3">2021.04.01</p>
              <p class="desc3 txt-bold">猛禽類の飼育方法　～犬や猫並みに飼い主と意思疎通ができるようになるまでの正しいしつけと快適な飼育環境～</p>
            </div>
	  			</a>
	  		</div>
	  		<div class="section-column--item fadeup ani delay-4 col-lg-4 col-md-6 col-12">
	  			<a href="" class="link2">
	  				<div class="section-column--item-thumb">
	  					<img class="cover" src="<?php echo $PATH;?>/assets/images/top/column03.png" alt="">
	  				</div>
	  				<div class="section-column--item-cnt">
              <p class="date3">2021.04.01</p>
              <p class="desc3 txt-bold">初めての爬虫類　飼う前にこれだけは用意しておきたい10のこと、覚えてお行きたい10のこと</p>
            </div>
	  			</a>
	  		</div>
	  	</div>
	  	<div class="section-column--direct fadeup ani">
        <a href="" class="button--ninaWrap">
          <div class="button button--nina button--round-l button--text-thick button--inverted" data-text="VIEW MORE">
            <span>V</span>
            <span>I</span>
            <span>E</span>
            <span>W</span>
            <span>&nbsp;</span>
            <span>M</span>
            <span>O</span>
            <span>R</span>
            <span>E</span>
          </div>
        </a>
	  	</div>
  	</div>
  </div>
</div>--><!-- ./section-column -->

<!--<div class="section-banner fadeup ani">
	<a href="" class="link">
		<img class="pc-only2" src="<?php echo $PATH;?>/assets/images/top/banner.png" alt="">
    <img class="sp-only2" src="<?php echo $PATH;?>/assets/images/top/banner-sp.png" alt="">
	</a>
</div>--><!-- ./section-banner -->

<!--<section class="section-entry">
	<div class="section-entry--infor">
		<div class="section-entry--infor-logo">
			<img class="cover" src="<?php echo $PATH;?>/assets/images/top/try.svg" alt="">
		</div>
		<h3 class="ttl-bold2 fadeup ani">会員登録をして予約しよう</h3>
		<p class="desc3 fadeup ani">ペットサロンや病院の予約で貯めたポイントを、 <br>動物のために活動する慈善団体へ寄付できます。 <br>動物たちの明るい明日を一緒に創っていきませんか？</p>
		<div class="fadeup ani">
      <a href="" class="btn-yellow2">今すぐ新規会員登録</a>
    </div>
	</div>

	<div class="section-entry--img">
    <div class="section-entry--img-magic fadeup ani heart01">
      <img class="fuwafuwa" src="<?php echo $PATH;?>/assets/images/common/heart01.png" alt="">
    </div>
    <div class="section-entry--img-magic fadeup ani heart02">
      <img class="fuwafuwa" src="<?php echo $PATH;?>/assets/images/common/heart02.png" alt="">
    </div>
    <div class="section-entry--img-magic fadeup ani heart03">
      <img class="fuwafuwa" src="<?php echo $PATH;?>/assets/images/common/heart03.png" alt="">
    </div>
    <div class="section-entry--img-magic fadeup ani heart04">
      <img class="fuwafuwa" src="<?php echo $PATH;?>/assets/images/common/heart04.png" alt="">
    </div>
    <div class="section-entry--img-magic fadeup ani heart05">
      <img class="fuwafuwa" src="<?php echo $PATH;?>/assets/images/common/heart05.png" alt="">
    </div>
    <div class="section-entry--img-magic fadeup ani heart06">
      <img class="fuwafuwa" src="<?php echo $PATH;?>/assets/images/common/heart06.png" alt="">
    </div>
		<img class="pc-only cover" src="<?php echo $PATH;?>/assets/images/top/bg-heart.png" alt="">
    <img class="sp-only cover" src="<?php echo $PATH;?>/assets/images/top/bg-heart-sp.png" alt="">
	</div>
</section>--><!-- ./section-entry -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-teaser.php'; ?>