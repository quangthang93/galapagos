<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-blog-customer.php'; ?>
<main class="main blog customer">
  <div class="p-blog">
    <div class="p-blog--banner">
      <div class="p-blog--banner-logo">
        <img src="<?php echo $PATH;?>/assets/images/blog/banner-logo.png" alt="">
      </div>
    </div>
    <div class="p-blog--cntWrap">
      <div class="p-blog--magic magic01 pc-only2">
        <img src="<?php echo $PATH;?>/assets/images/blog/magic01.png" alt="">
      </div>
      <div class="p-blog--magic magic02 pc-only2">
        <img src="<?php echo $PATH;?>/assets/images/blog/magic02.png" alt="">
      </div>
      <div class="p-blog--magic magic03 pc-only2">
        <img src="<?php echo $PATH;?>/assets/images/blog/magic03.png" alt="">
      </div>
      <div class="p-blog--magic magic04 pc-only2">
        <img src="<?php echo $PATH;?>/assets/images/blog/magic04.png" alt="">
      </div>
      <div class="breadcrumb p-0">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/">カテゴリ一覧</a></li>
          <li>記事詳細</a></li>
        </ul>
      </div>
      <div class="p-blog--cnt">
        <div class="p-blog--mainCnt col-lg-8 col-12">
          <h1 class="heading1">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</h1>
          <div class="featured">
            <img src="<?php echo $PATH;?>/assets/images/blog/detail01.png" alt="">
          </div>
          <div class="post-content">
            <div class="row mb-40">
              <div class="col-6">
                <div class="post-categoriesWrap">
                  <ul class="post-categories">
                    <li><a href="" class="cat">カテゴリ1</a></li>
                  </ul>
                  <div class="meta-below m-0">
                    <time class="post-date type2" datetime="">2020.01.23</time>
                    <span class="post-cmt type2">コメント3</span>
                  </div>
                </div>
              </div>
              <div class="col-6 post-shareWrap">
                <ul class="post-share">
                  <li>
                    <a href="" class="link" target="_blank" title="Share on Facebook">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb-gray.svg" alt="">
                    </a>
                  </li>
                  <li>
                    <a href="" class="link" target="_blank" title="Share on Twitter">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter-gray.svg" alt="">
                    </a>
                  </li>
                  <li>
                    <a href="" class="link" target="_blank" title="Share on Line">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line-gray.svg" alt="">
                    </a>
                  </li>
                </ul>
              </div>
              <div style="clear: both;"></div>
            </div>
            <p>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。</p>
            <div id="toc_container" class="no_bullets">
              <p class="toc_title">この記事の目次</p>
              <ul class="toc_list">
                <li><a href="#i"><span class="toc_number toc_depth_1">1</span> 中見出しテキスト</a>
                  <ul>
                    <li><a href="#"><span class="toc_number toc_depth_2">2.1</span> 小見出しテキスト</a></li>
                  </ul>
                </li>
                <li><a href="#i-2"><span class="toc_number toc_depth_1">2</span> 中見出しテキスト</a>
                  <ul>
                    <li><a href="#"><span class="toc_number toc_depth_2">2.1</span> 小見出しテキスト</a></li>
                    <li><a href="#"><span class="toc_number toc_depth_2">2.1</span> 小見出しテキスト</a></li>
                    <li><a href="#"><span class="toc_number toc_depth_2">2.1</span> 小見出しテキスト</a></li>
                  </ul>
                </li>
                <li><a href="#i-2"><span class="toc_number toc_depth_1">2</span> 中見出しテキスト</a>
                  <ul>
                    <li><a href="#"><span class="toc_number toc_depth_2">2.1</span> 小見出しテキスト</a></li>
                    <li><a href="#"><span class="toc_number toc_depth_2">2.1</span> 小見出しテキスト</a></li>
                  </ul>
                </li>
                <li><a href="#i-12"><span class="toc_number toc_depth_1">7</span> まとめ</a></li>
              </ul>
            </div>
            <div class="mb-50">
              <h2><span id="i">中見出しテキスト</span></h2>
              <p>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。この文章はダミーです。</p>
            </div>
            <h2><span id="i-2">この記事に対するコメント</span></h2>
            <h3 class="mb-30"><span id="">吹き出しアイテム</span></h3>
            <div class="p-blog--balloon">
              <div class="p-blog--balloon--item">
                <div class="p-blog--balloon--item-thumbWrap">
                  <div class="p-blog--balloon--item-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/blog/ball0on02.png" alt="" loading="lazy">
                  </div>
                  <p class="p-blog--balloon--item-name">ご質問者様</p>
                </div>
                <div class="p-blog--balloon--item-cnt">吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。</div>
              </div>
              <div class="p-blog--balloon--item">
                <div class="p-blog--balloon--item-thumbWrap">
                  <div class="p-blog--balloon--item-thumb">
                    <img src="<?php echo $PATH;?>/assets/images/blog/ball0on01.png" alt="" loading="lazy">
                  </div>
                  <p class="p-blog--balloon--item-name">運営者</p>
                </div>
                <div class="p-blog--balloon--item-cnt">吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。吹き出し内コメント入ります。</div>
              </div>
            </div>
            <h2 class="mt-60"><span id="i-2">コメントを入力する</span></h2>
            <form class="form" action="">
              <div class="form-input">
                <input type="text" name="name" class="input mb-20" placeholder="名前">
                <textarea class="input textarea" id="msg01" name="msg01" rows="4" cols="50" placeholder="コメントをご入力ください。"></textarea>
              </div>
              <div class="form-ctrl">
                <div class="form-ctrl--item btn-submitWrap">
                  <a href=" " class="btn-submit">投稿する</a>
                </div>
              </div>
            </form>
            <div class="mt-50">
              <h3 class="mb-30"><span id="">写真を横に2枚(横長・縦長・正方形)</span></h3>
              <div class="row mb-30">
                <div class="col-6">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/detail02.png" alt="">
                </div>
                <div class="col-6">
                  <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/detail03.png" alt="">
                </div>
              </div>
            </div>
            <div class="row mb-30">
              <div class="col-6">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/detail04.png" alt="">
              </div>
              <div class="col-6">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/detail05.png" alt="">
              </div>
            </div>
            <div class="row mb-30">
              <div class="col-6">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/detail06.png" alt="">
              </div>
              <div class="col-6">
                <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/detail07.png" alt="">
              </div>
            </div>
            <div class="mt-50">
              <h3 class="mb-30"><span id="">テキスト背景色あり</span></h3>
              <p class="has-background" style="background-color:#f1f3f5">背景ありのテキストです。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。</p>
            </div>
            <div class="mt-50">
              <h3 class="mb-30"><span id="">テキスト囲いあり</span></h3>
              <p class="has-border">
                囲みありのテキストです。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。</p>
            </div>
            <hr>
            <div class="mt-50">
              <h3 class="mb-30"><span id="">引用テキスト</span></h3>
              <p class="has-quote">
                この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。この文章はダミーです。</p>
            </div>
            <div class="mt-50">
              <h3 class="mb-30"><span id="">テキストリンク</span></h3>
              <p><a href="" target="_blank" class="link-blue type3">テキスト</a></p>
              <p><a href="" target="_blank" class="link-icon2 blank type2">外部リンク</a></p>
              <p><a href="" target="_blank" class="link-icon2 pdf type2">PDFリンク</a></p>
            </div>
            <div class="mb-60 mt-80">
              <h2><span id="">まとめ</span></h2>
              <p>この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています。この文章はダミーです。この文章はダミーです。</p>
            </div>
            <div class="post-ctrl2">
              <a class="post-ctrl2--link back" href=""></a>
              <span class="post-ctrl2--count">1 / 3</span>
              <a class="post-ctrl2--link next" href=""></a>
            </div>
            <div class="mt-50">
              <div class="has-arrowWrap link">
                <h3><span>タイトル(未入力の場合は非表示)</span></h3>
                <div class="row">
                  <div class="col-lg-3 col-sm-3 col-5">
                    <img src="<?php echo $PATH;?>/assets/images/blog/detail01.png" alt="">
                  </div>
                  <div class="col-lg-9 col-sm-9 col-7">
                    <p class="mb-0">こちらにリンクカードが入ります。こちらにリンクカードが入ります。こちらにリンクカードが入ります。こちらにリンクカードが入ります。こちらにリンクカードが入ります。こちらにリンクカードが入ります。こちらにリンクカードが入ります...</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="mt-40 mb-30">
            <a href="" class="tag4 link"># 動物</a>
            <a href="" class="tag4 link"># 犬</a>
          </div>
          <hr>
          <section class="post-related mt-40">
            <h2 class="heading2"><span id="">この記事に関連する記事</span></h2>
            <div class="row">
              <article class="post-related--item col-lg-6 col-sm-6 col-12">
                <a href="" class="post-related--inner link">
                  <div class="post-related--thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/detail01.png" alt="">
                  </div>
                  <div class="post-related--cnt">
                    <div class="align-center mt-15"><a href="" class="cat">カテゴリ1</a></div>
                    <h3 class="heading3 mt-10 mb-10">
                      ダミーテキストです。
                    </h3>
                    <div class="align-center">
                        <time class="post-date type2" datetime="">2020.01.23</time>
                        <span class="post-cmt type2">コメント3</span>
                    </div>
                  </div>
                </a>
              </article>
              <article class="post-related--item col-lg-6 col-sm-6 col-12">
                <a href="" class="post-related--inner link">
                  <div class="post-related--thumb">
                    <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/detail01.png" alt="">
                  </div>
                  <div class="post-related--cnt">
                    <div class="align-center mt-15"><a href="" class="cat">カテゴリ1</a></div>
                    <h3 class="heading3 mt-10 mb-10">
                      ダミーテキストです。
                    </h3>
                    <div class="align-center">
                        <time class="post-date type2" datetime="">2020.01.23</time>
                        <span class="post-cmt type2">コメント3</span>
                    </div>
                  </div>
                </a>
              </article>
            </div>
          </section>
        </div>
        <div class="p-blog--sidebar col-lg-4 col-12">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-blog-customer.php'; ?>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<div class="p-blog--contact">
  <a class="link" href="/contact">CONTACT</a>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-blog-customer.php'; ?>