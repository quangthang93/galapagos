<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-blog-customer.php'; ?>
<main class="main blog customer">
  <div class="p-blog">
    <div class="p-blog--banner">
      <div class="p-blog--banner-logo">
        <img src="<?php echo $PATH;?>/assets/images/blog/banner-logo.png" alt="">
      </div>
    </div>
    <div class="p-blog--cntWrap">
      <div class="p-blog--magic magic01 pc-only2">
        <img src="<?php echo $PATH;?>/assets/images/blog/magic01.png" alt="">
      </div>
      <div class="p-blog--magic magic02 pc-only2">
        <img src="<?php echo $PATH;?>/assets/images/blog/magic02.png" alt="">
      </div>
      <div class="p-blog--magic magic03 pc-only2">
        <img src="<?php echo $PATH;?>/assets/images/blog/magic03.png" alt="">
      </div>
      <div class="p-blog--magic magic04 pc-only2">
        <img src="<?php echo $PATH;?>/assets/images/blog/magic04.png" alt="">
      </div>
      <div class="breadcrumb p-0">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>カテゴリ一覧</a></li>
        </ul>
      </div>
      <div class="p-blog--cnt">
        <div class="p-blog--mainCnt col-lg-8 col-12">
          <h2 class="p-blog--ttl">カテゴリ1</h2>
          <div class="posts-wrap row">
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog01.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog02.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog03.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog04.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog05.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog06.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog07.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog08.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog09.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog10.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog09.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date" datetime="">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
            <div class="post-item col-lg-6 col-md-6 col-12">
              <article class="grid-post">
                <div class="post-header">
                  <div class="post-thumb">
                    <a href="" class="link">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/blog10.png" alt="">
                    </a>
                  </div>
                  <div class="meta-title">
                    <div class="post-meta">
                      <div class="meta-above">
                        <span class="post-cat">
                          <a href="" class="cat">カテゴリ1</a>
                        </span>
                      </div>
                      <h2 class="post-title-alt"><a href="" class="link">この文章はダミーです。文字の大きさ、量、字間、行間等を確認するために入れています</a></h2>
                      <div class="meta-below">
                        <time class="post-date">2020.01.23</time>
                        <span class="post-cmt">コメント3</span>
                      </div>
                    </div>
                  </div>
                </div><!-- .post-header -->
                <div class="post-footer">
                  <ul class="social-share">
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Facebook">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-fb.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Twitter">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-twitter.svg" alt="">
                      </a>
                    </li>
                    <li>
                      <a href="" class="link" target="_blank" title="Share on Line">
                        <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/icon-line.svg" alt="">
                      </a>
                    </li>
                  </ul>
                </div>
              </article>
            </div>
          </div>
          <div class="p-pagination mt-40">
            <p class="p-pagination-label">0,000件中｜1〜30件 表示</p>
            <div class="p-pagination-list">
              <a class="ctrl prev" href=""></a>
              <a class="active" href="">1</a>
              <a href="">2</a>
              <a href="">3</a>
              <a href="">4</a>
              <a href="">5</a>
              <a href="">6</a>
              <div class="p-pagination-spacer">…</div>
              <a href="">12</a>
              <a class="ctrl next" href=""></a>
            </div>
          </div>
        </div>
        <div class="p-blog--sidebar col-lg-4 col-12">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-blog-customer.php'; ?>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<div class="p-blog--contact">
  <a class="link" href="/contact">CONTACT</a>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-blog-customer.php'; ?>