<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">BiBi犬猫病院</a></li>
      <li>施設予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">施設予約</h2>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebarSearch">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-search.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">メニュー選択</h2>
              <div class="p-reserve--list">
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8">眼科</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8">眼科</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円(税込)
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8">眼科</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円(税込)
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8">眼科</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円(税込)
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
              </div>
              <div class="align-center">
                <a href="" class="btn-white3 align-center">前のページへ戻る</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>