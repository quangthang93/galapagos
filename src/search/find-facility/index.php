<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">BiBi犬猫病院</a></li>
      <li>施設予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">施設予約</h2>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebarSearch">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-search.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">メニュー選択</h2>
              <div class="p-reserve--list">
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8">眼科</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8">眼科</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円(税込)
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8">眼科</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円(税込)
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8">眼科</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円(税込)
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">選択する</a>
                  </div>
                </div>
              </div>
              <div class="align-center">
                <a href="" class="btn-white3 align-center">前のページへ戻る</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->

<!-- Modal -->
<div class="modal fade" id="modalCancel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-find">
    <div class="modal-content modal-find--cnt">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-find--form">
        <div class="tabs">
          <div class="tabs-navWrapper">
            <ul class="tabs-nav js-tabsNav">
              <li class="tabs-item js-tabsItem checkboxWrap-outer">
                <div class="checkboxWrap">
                  <input class="checkbox3 js-cbox" id="cbox01-1" type="checkbox" value="value1">
                  <label for="cbox01-1">北海道・東北</label>
                </div>
              </li>
              <li class="tabs-item js-tabsItem checkboxWrap-outer">
                <div class="checkboxWrap">
                  <input class="checkbox3 js-cbox" id="cbox01-2" type="checkbox" value="value1">
                  <label for="cbox01-2">関東・甲信越</label>
                </div>
              </li>
              <li class="tabs-item js-tabsItem checkboxWrap-outer">
                <div class="checkboxWrap">
                  <input class="checkbox3 js-cbox" id="cbox01-3" type="checkbox" value="value1">
                  <label for="cbox01-3">北陸・東海</label>
                </div>
              </li>
              <li class="tabs-item js-tabsItem checkboxWrap-outer">
                <div class="checkboxWrap">
                  <input class="checkbox3 js-cbox" id="cbox01-4" type="checkbox" value="value1">
                  <label for="cbox01-4">関西</label>
                </div>
              </li>
              <li class="tabs-item js-tabsItem checkboxWrap-outer">
                <div class="checkboxWrap">
                  <input class="checkbox3 js-cbox" id="cbox01-5" type="checkbox" value="value1">
                  <label for="cbox01-5">中国</label>
                </div>
              </li>
              <li class="tabs-item js-tabsItem checkboxWrap-outer">
                <div class="checkboxWrap">
                  <input class="checkbox3 js-cbox" id="cbox01-6" type="checkbox" value="value1">
                  <label for="cbox01-6">四国</label>
                </div>
              </li>
              <li class="tabs-item js-tabsItem checkboxWrap-outer">
                <div class="checkboxWrap">
                  <input class="checkbox3 js-cbox" id="cbox01-7" type="checkbox" value="value1">
                  <label for="cbox01-7">九州・沖縄</label>
                </div>
              </li>
            </ul>
          </div>
          <div class="tabs-cnt js-tabsCnt">
            <div class="tabs-panel js-tabsPanel">
              <div class="tabs-cnt--list">
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-1" type="checkbox" value="value1">
                    <label for="cbox02-1">東京23区</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-2" type="checkbox" value="value1">
                    <label for="cbox02-2">東京23区すべて</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-3" type="checkbox" value="value1">
                    <label for="cbox02-3">東京その他</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-4" type="checkbox" value="value1">
                    <label for="cbox02-4">東京その他</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-5" type="checkbox" value="value1">
                    <label for="cbox02-5">埼玉県</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-6" type="checkbox" value="value1">
                    <label for="cbox02-6">足立区</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-7" type="checkbox" value="value1">
                    <label for="cbox02-7">神奈川県</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-8" type="checkbox" value="value1">
                    <label for="cbox02-8">荒川区</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-9" type="checkbox" value="value1">
                    <label for="cbox02-9">千葉県</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-10" type="checkbox" value="value1">
                    <label for="cbox02-10">板橋区</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-11" type="checkbox" value="value1">
                    <label for="cbox02-11">茨城県</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-12" type="checkbox" value="value1">
                    <label for="cbox02-12">江戸川区</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-13" type="checkbox" value="value1">
                    <label for="cbox02-13">栃木県</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-14" type="checkbox" value="value1">
                    <label for="cbox02-14">大田区</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-15" type="checkbox" value="value1">
                    <label for="cbox02-15">群馬県</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-16" type="checkbox" value="value1">
                    <label for="cbox02-16">葛飾区</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-17" type="checkbox" value="value1">
                    <label for="cbox02-17">山梨県</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-18" type="checkbox" value="value1">
                    <label for="cbox02-18">北区</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-19" type="checkbox" value="value1">
                    <label for="cbox02-19">長野県</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-20" type="checkbox" value="value1">
                    <label for="cbox02-20">江東区</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-21" type="checkbox" value="value1">
                    <label for="cbox02-21">新潟県</label>
                  </div>
                </div>
                <div class="tabs-cnt--item checkboxWrap-outer">
                  <div class="checkboxWrap">
                    <input class="checkbox3 js-cbox" id="cbox02-22" type="checkbox" value="value1">
                    <label for="cbox02-22">品川区</label>
                  </div>
                </div>

              </div>
            </div>
            <div class="tabs-panel js-tabsPanel">
              <h3 class="title-lv3">Content 02</h3>
            </div>
            <div class="tabs-panel js-tabsPanel">
              <h3 class="title-lv3">Content 03</h3>
            </div>
            <div class="tabs-panel js-tabsPanel">
              <h3 class="title-lv3">Content 04</h3>
            </div>
            <div class="tabs-panel js-tabsPanel">
              <h3 class="title-lv3">Content 05</h3>
            </div>
            <div class="tabs-panel js-tabsPanel">
              <h3 class="title-lv3">Content 06</h3>
            </div>
            <div class="tabs-panel js-tabsPanel">
              <h3 class="title-lv3">Content 07</h3>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-find--foot">
        <div class="modal-find--foot-direct">
          <a href="/mypage/register/edit-complete" class="btn-submit js-btnSave">ファミリー情報を登録する</a>
        </div>
        <a class="modal-confirm--close" data-bs-dismiss="modal">閉じる</a>
      </div>
    </div>
  </div>
</div>

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>