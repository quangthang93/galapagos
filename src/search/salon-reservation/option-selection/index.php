<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">BiBi犬猫病院</a></li>
      <li>施設予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">施設予約</h2>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebarSearch">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-search.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve type2">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">メニュー選択</h2>
              <div class="p-salon--row mgt-30">
                <h3 class="p-salon--subTtl">選択中のメニュー</h3>
                <div class="p-reserve--list type2">
                  <div class="p-reserve--item mgb-0">
                    <div class="p-reserve--item-head">
                      <p class="ttl-bold8">カット＆シャンプー（短毛犬種）</p>
                      <div class="p-reserve--item-head-infor">
                        <div class="p-reserve--item-head-infor-child">
                          <span class="label-time">目安時間</span>
                          <div class="p-reserve--item-head-infor-child-val">
                            <span>60</span>
                            分〜
                          </div>
                        </div>
                        <div class="p-reserve--item-head-infor-child">
                          <span class="label-time">基本料金</span>
                          <div class="p-reserve--item-head-infor-child-val">
                            <span>3,500</span>
                            円
                          </div>
                        </div>
                      </div>
                    </div>
                    <p class="desc6">短毛犬種のカット、シャンプーの基本コースです。爪切り・足裏バリカン・足まわりカット・耳掃除・肛門腺絞り＋全身のカットが含まれます。</p>
                  </div>
                </div>
              </div>
              <div class="p-salon--row">
                <h3 class="p-salon--subTtl">オプション</h3>
                <div class="p-salon--options">
                  <div class="p-salon--options-list">
                    <div class="p-salon--options-item">
                      <div class="p-salon--options-item-left">
                        <div class="p-reserve--item-head">
                          <p class="ttl-bold8">トリートメント</p>
                          <div class="p-reserve--item-head-infor">
                            <div class="p-reserve--item-head-infor-child">
                              <div class="p-reserve--item-head-infor-child-val">
                                <span>1,000</span>
                                円
                              </div>
                            </div>
                          </div>
                        </div>
                        <p class="desc6">軽めのダメージに作用するクイックタイプのトリートメントです！※薬剤使用施術のみ併用可＊カットやカラーやパーマと一緒にぜひ＊</p>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox01" type="checkbox" value="value1">
                        <label for="cbox01"></label>
                      </div>
                    </div>
                    <div class="p-salon--options-item">
                      <div class="p-salon--options-item-left">
                        <div class="p-reserve--item-head">
                          <p class="ttl-bold8">フェルエトリートメント☆2STEP☆スチーム付</p>
                          <div class="p-reserve--item-head-infor">
                            <div class="p-reserve--item-head-infor-child">
                              <div class="p-reserve--item-head-infor-child-val">
                                <span>1,000</span>
                                円
                              </div>
                            </div>
                          </div>
                        </div>
                        <p class="desc6">ナノイオンのスチームを当てる２STEPトリートメント＊しっとり系が好きな方に！！※トリートメントのみはSB代+￥1000</p>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox02" type="checkbox" value="value1">
                        <label for="cbox02"></label>
                      </div>
                    </div>
                    <div class="p-salon--options-item">
                      <div class="p-salon--options-item-left">
                        <div class="p-reserve--item-head">
                          <p class="ttl-bold8">ホイミホイミー☆３STEP☆スチーム付</p>
                          <div class="p-reserve--item-head-infor">
                            <div class="p-reserve--item-head-infor-child">
                              <div class="p-reserve--item-head-infor-child-val">
                                <span>1,000</span>
                                円
                              </div>
                            </div>
                          </div>
                        </div>
                        <p class="desc6">オーガニック系のサラッとしたトリートメントです。ボリュームを落としたくない方に＊＊※トリートメントのみの場合はSB＋￥1000</p>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox03" type="checkbox" value="value1">
                        <label for="cbox03"></label>
                      </div>
                    </div>
                    <div class="p-salon--options-item">
                      <div class="p-salon--options-item-left">
                        <div class="p-reserve--item-head">
                          <p class="ttl-bold8">リンケージトリートメント☆３STEP☆スチーム付</p>
                          <div class="p-reserve--item-head-infor">
                            <div class="p-reserve--item-head-infor-child">
                              <div class="p-reserve--item-head-infor-child-val">
                                <span>1,000</span>
                                円
                              </div>
                            </div>
                          </div>
                        </div>
                        <p class="desc6">有名メーカーミルボンの人気トリートメント☆しっかり水分補給！多毛乾燥毛の方にオススメ◎※トリートメントのみはSB代+￥1000</p>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox04" type="checkbox" value="value1">
                        <label for="cbox04"></label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="p-salon--direct align-center">
                <a href="" class="btn-white3 align-center">前のページへ戻る</a>
                <a href="" class="btn-blue2 align-center">予約日時を選ぶ</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>