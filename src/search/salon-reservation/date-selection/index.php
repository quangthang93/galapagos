<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">soup*spoon</a></li>
      <li>トリミングサロン予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">トリミングサロン予約</h2>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebarSearch">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-search.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve type2">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">日時選択</h2>
              <div class="p-reserve--list">
                <div class="calendar">
                  <div class="calendar-head">
                    <a href="javascript:void(0)" class="calendar-head--ctrl">前の月</a>
                    <div class="calendar-head--ttl">2021年8月</div>
                    <a href="javascript:void(0)" class="calendar-head--ctrl">次の月</a>
                  </div>
                  <table class="calendar-table">
                    <thead>
                      <tr>
                        <th class="sunday" scope="col"><span>日</span></th>
                        <th scope="col"><span>月</span></th>
                        <th scope="col"><span>火</span></th>
                        <th scope="col"><span>水</span></th>
                        <th scope="col"><span>木</span></th>
                        <th scope="col"><span>金</span></th>
                        <th class="saturday" scope="col"><span>土</span></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">1</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">2</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">3</a></td>
                        <td class="disable"><a href="">4</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">5</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">6</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">7</a></td>
                      </tr>
                      <tr>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">8</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">9</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">10</a></td>
                        <td class="disable"><a href="">11</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">12</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">13</a></td>
                        <td class="cancel"><a href="">14</a></td>
                      </tr>
                      <tr>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">15</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">16</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">17</a></td>
                        <td class="disable"><a href="">18</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">19</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">20</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">21</a></td>
                      </tr>
                      <tr>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">22</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">23</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">24</a></td>
                        <td class="disable"><a href="">25</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">26</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">27</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">28</a></td>
                      </tr>
                      <tr>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">29</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">30</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">31</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">1</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">2</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">3</a></td>
                        <td><a href="/search/salon-reservation/date-selection/time-selection">4</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="align-center">
                <a href="" class="btn-white3 align-center">前のページへ戻る</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>