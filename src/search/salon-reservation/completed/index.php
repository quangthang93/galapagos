<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">soup*spoon</a></li>
      <li>トリミングサロン予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">トリミングサロン予約</h2>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebarSearch">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-search2.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve p-salon">
            <div class="p-reserve--intro">
              <p class="ttl-bold8 mgb-20">ご予約について</p>
              <p class="txt-bold2 mgb-20">・当院では、動物たちと飼い主様の利便性向上を目的として、『時間帯』予約制を採用しています。</p>
              <p class="desc6 mgb-35">※ 完全予約制 完全予約制となっております。 また、施術の都合上、お預かりのお時間を長めに設定させて頂いております。 10:15の予約の場合：16時お返し 11:15の予約の場合：17時お返し 12:15の予約の場合：18時お返し なお、ネット予約が埋まっていても予約をお受けできる場合がございますので、その場合お電話にて直接お問い合わせください。</p>
              <p class="ttl-bold8 mgb-40">スキンケアサロンを初めてご利用の方へ</p>
              <p class="txt-bold2 mgb-40">皮膚の健康チェックやサロン利用のご案内のために、事前の診察が必要となります。サロンのご予約日までに一般診察もしくは皮膚科診察の受診をお願い致します。 また、事前に狂犬病予防ワクチン(犬のみ)、混合ワクチン、ノミダニ予防をお願いしています。体調などによりこれらが行えない方はご相談ください。（なお、ワクチン接種から1週間はトリミングをお受けできません。）</p>
              <div class="p-reserve--intro-box desc6">ネット予約すると予約完了メールが配信されますが、キャリアメール（docomo、Softbank、auなど）は迷惑メールに入り受信できないケースがあるためご注意ください。 迷惑メール設定をされている方は@apokul.jpを受信したい相手に設定をお願いいたします。</div>
            </div>
            <div class="p-reserve--cnt">
              <p class="p-salon--ttl txt-bold2">下記より選択し、次の画面へお進みください。</p>
              <div class="p-reserve--list">
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">カット＆シャンプー（短毛犬種）</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">基本料金</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>3,500</span>
                          円
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">短毛犬種のカット、シャンプーの基本コースです。爪切り・足裏バリカン・足まわりカット・耳掃除・肛門腺絞り＋全身のカットが含まれます。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">予約する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">カット＆シャンプー（長毛犬種）</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">基本料金</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>4,500</span>
                          円
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">短毛犬種のカット、シャンプーの基本コースです。爪切り・足裏バリカン・足まわりカット・耳掃除・肛門腺絞り＋全身のカットが含まれます。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">予約する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">平日限定超得◎癒*カット＋フルカラー＋スパ＋トリートメント￥10500→￥6900超得◎癒しコース*カット＋フルカラー＋スパ＋トリートメント￥10500→￥7900</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">基本料金</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>3,000</span>
                          円
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">短毛犬種のカット、シャンプーの基本コースです。爪切り・足裏バリカン・足まわりカット・耳掃除・肛門腺絞り＋全身のカットが含まれます。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">予約する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">カット＆シャンプー（短毛犬種）</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">基本料金</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>3,500</span>
                          円
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">短毛犬種のカット、シャンプーの基本コースです。爪切り・足裏バリカン・足まわりカット・耳掃除・肛門腺絞り＋全身のカットが含まれます。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">予約する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">カット＆シャンプー（長毛犬種）</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">基本料金</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>4,500</span>
                          円
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">短毛犬種のカット、シャンプーの基本コースです。爪切り・足裏バリカン・足まわりカット・耳掃除・肛門腺絞り＋全身のカットが含まれます。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">予約する</a>
                  </div>
                </div>
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">【お泊り】その他</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">基本料金</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>3,000</span>
                          円
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">短毛犬種のカット、シャンプーの基本コースです。爪切り・足裏バリカン・足まわりカット・耳掃除・肛門腺絞り＋全身のカットが含まれます。</p>
                  <div class="p-reserve--item-direct">
                    <a href="" class="btn-blue2">予約する</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>