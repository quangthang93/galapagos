<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">soup*spoon</a></li>
      <li>トリミングサロン予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">トリミングサロン予約</h2>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebarSearch">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-search2.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">予約情報入力</h2>
              <div class="p-reserve--list p-edit">
                <form class="form">
                  <p class="form-row--ttl border-bot">オーナー様の情報</p>
                  <div class="form-row">
                    <div class="form-label">
                      名前
                      <span class="c-form__required">※</span>
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="text" name="name" class="input" value="本名でご予約ください">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      電話番号
                      <span class="c-form__required">※</span>
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="text" name="name" class="input" value="08012345678">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      E-mail
                      <span class="c-form__required">※</span>
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="email" name="name" class="input" value="mailaddress@mail.com">
                      </div>
                    </div>
                  </div>
                  <p class="form-row--ttl type2">受診するファミリーの情報</p>
                  <div class="form-row type2">
                    <div class="form-label"></div>
                    <div class="form-row--cnt">
                      <div class="form-radio type2">
                        <span class="form-radio--item type2">
                          <label>
                            <input type="radio" name="cb01" value="ムギ" checked>
                            <span class="form-radio--label">ムギ</span>
                          </label>
                        </span>
                        <span class="form-radio--item type2">
                          <label>
                            <input type="radio" name="cb01" value="ソラ">
                            <span>ソラ</span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      種類
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-groupField--item">
                        <div class="form-radio">
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="犬" checked>
                              <span class="form-radio--label">犬</span>
                            </label>
                          </span>
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="猫">
                              <span>猫</span>
                            </label>
                          </span>
                        </div>
                      </div>
                      <div class="form-groupField--item">
                        <div class="form-radio">
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="うさぎ" checked>
                              <span class="form-radio--label">うさぎ</span>
                            </label>
                          </span>
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="フェレット">
                              <span>フェレット</span>
                            </label>
                          </span>
                        </div>
                      </div>
                      <div class="form-groupField--item">
                        <div class="form-radio">
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="鳥類" checked>
                              <span class="form-radio--label">鳥類</span>
                            </label>
                          </span>
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="その他">
                              <span>その他</span>
                            </label>
                          </span>
                        </div>
                      </div>
                      <div class="form-groupField--item">
                        <p class="desc5 sp-only3 mgb-5">その他の詳細（任意）</p>
                        <input type="text" name="name" class="input" value="アメリカンショートヘアー">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">備考</div>
                    <div class="form-row--cnt">
                      <textarea name="content" id="content" class="c-form__textarea" cols="50" rows="8" placeholder="ペットの症状や普段服用している薬などありましたらご記入ください。"></textarea>
                    </div>
                  </div>
                  <div class="form-ctrl type2">
                    <div class="form-ctrl--item btn-submitWrap">
                      <a href="" class="btn-white3 align-center">前のページへ戻る</a>
                    </div>
                    <div class="form-ctrl--item btn-submitWrap">
                      <a href="/mypage/edit-infor/completed" class="btn-submit">入力情報を確認する</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>