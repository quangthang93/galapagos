<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">BiBi犬猫病院</a></li>
      <li>施設予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">施設予約</h2>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebarSearch">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-search.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">予約情報入力</h2>
              <div class="p-reserve--list p-edit">
                <form class="form">
                  <p class="form-row--ttl border-bot">予約情報</p>
                  <div class="form-rowWrap">
                    <div class="form-row type3">
                      <div class="form-label">
                        メニュー
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">眼科｜初診</p>
                      </div>
                    </div>
                    <div class="form-row type3">
                      <div class="form-label">
                        日付
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">2021年10月1日(月)</p>
                      </div>
                    </div>
                    <div class="form-row type3">
                      <div class="form-label">
                        診察開始時間
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">09:00 ~ 09:30</p>
                      </div>
                    </div>
                  </div>
                  <p class="form-row--ttl border-bot mgt-30">オーナー様の情報</p>
                  <div class="form-rowWrap">
                    <div class="form-row type3">
                      <div class="form-label">
                        名前
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">山田 太郎</p>
                      </div>
                    </div>
                    <div class="form-row type3">
                      <div class="form-label">
                        電話番号
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">08012345678</p>
                      </div>
                    </div>
                    <div class="form-row type3">
                      <div class="form-label">
                        E-mail
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">mailaddress@mail.com</p>
                      </div>
                    </div>
                  </div>
                  <p class="form-row--ttl border-bot mgt-30">受診するファミリーの情報</p>
                  <div class="form-rowWrap">
                    <div class="form-row type3">
                      <div class="form-label">
                        名前
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">ムギ</p>
                      </div>
                    </div>
                    <div class="form-row type3">
                      <div class="form-label">
                        種類
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">猫｜アメリカンショートヘアー</p>
                      </div>
                    </div>
                    <div class="form-row type3">
                      <div class="form-label">
                        備考
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">食が細く、元気がないようです。</p>
                      </div>
                    </div>
                  </div>
                  
                  <div class="form-ctrl type2">
                    <div class="form-ctrl--item btn-submitWrap">
                      <a href="" class="btn-white3 align-center">前のページへ戻る</a>
                    </div>
                    <div class="form-ctrl--item btn-submitWrap">
                      <a href="/mypage/edit-infor/completed" class="btn-submit">予約を確定する</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>