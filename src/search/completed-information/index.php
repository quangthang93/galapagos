<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">BiBi犬猫病院</a></li>
      <li>予約完了</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約完了</h2>
      </div>
      <div class="p-mypage--inner type2">

        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">予約情報入力</h2>
              <div class="p-reserve--list p-completeInfor">
                <div class="p-completeInfor--head">
                  <div class="p-completeInfor--head-icon">
                    <div class="p-completeInfor--head-icon-img">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-complete-infor.svg" alt="">
                    </div>
                    <p class="p-completeInfor--head-icon-desc">予約を受け付けました</p>
                  </div>
                  <p class="p-completeInfor--head-intro txt-bold2">予約時間は診察開始の時間帯です。<br>時間に余裕をもってお越しください。</p>
                  <div class="align-center">
                    <a href="" class="btn-submit type3">ご予約状況を確認</a>
                  </div>
                </div>
                <div class="p-completeInfor--box">
                  <p class="p-completeInfor--box-ttl">ポイント付与について</p>
                  <p class="p-completeInfor--box-subTtl txt-bold2">訪問後、ご利用履歴より施設の評価をしてください。<br class="pc-only2">評価完了後にポイントが付与されます。</p>
                  <p class="p-completeInfor--box-desc desc4">ご利用履歴はマイページにログインしてご確認するか、ご予約完了メールに記載されたURLからご確認いただけます。</p>
                  <div class="align-center">
                    <a href="" class="btn-white3 align-center">ポイント付与について</a>
                  </div>
                </div>
                <div class="align-center">
                  <a href="" class="link-blue">トップページへ戻る</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>