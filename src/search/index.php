<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header3.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">BiBi犬猫病院</a></li>
      <li>施設予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">施設予約</h2>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebarSearch">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-search2.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--intro">
              <p class="ttl-bold8 mgb-20">ご予約について</p>
              <p class="txt-bold2 mgb-20">・当院では、動物たちと飼い主様の利便性向上を目的として、『時間帯』予約制を採用しています。</p>
              <p class="desc6 mgb-20">※ 1『時間帯予約制とは』 <br>同じ時間帯に複数名の方が来院される方式であり、例えば、10時~ の予約枠の方は10:00~10:30の時間帯での診察開始が目安となります。 <br><br>※ 2 動物病院の特性上、生命に関わる緊急対応を要する場合があります。その際には、ご予約の有無に関わらずおまたせしてしまう可能性がございますが日々生命と向き合う現場であること、予めご理解いただけますと幸いです。</p>
              <p class="txt-bold2 mgb-20">・急変時を含め、予約なしでの診察も可能となります。一方で、お待たせすることが予想されますので、可能な限り事前予約をオススメ致します。</p>
              <p class="txt-bold2">・獣医師の指名はお受けできないことをご了承ください。</p>
              <div class="p-reserve--working">
                <p class="p-reserve--working-ttl ttl-green">獣医師勤務表</p>
                <div class="p-reserve--working-table">
                  <div class="p-reserve--working-table-row">
                    <div class="p-reserve--working-table-label">平塚　（一般科＋歯科）</div>
                    <div class="p-reserve--working-table-value"><span>：　</span>月火水　土日</div>
                  </div>
                  <div class="p-reserve--working-table-row">
                    <div class="p-reserve--working-table-label">今井　（一般科）</div>
                    <div class="p-reserve--working-table-value"><span>：　</span>　火水金土日</div>
                  </div>
                  <div class="p-reserve--working-table-row">
                    <div class="p-reserve--working-table-label">佐々木（一般科）</div>
                    <div class="p-reserve--working-table-value"><span>：　</span>月　　金土日 </div>
                  </div>
                  <div class="p-reserve--working-table-row">
                    <div class="p-reserve--working-table-label">伊從　（皮膚科）</div>
                    <div class="p-reserve--working-table-value"><span>：　</span>　　　金土日 </div>
                  </div>
                  <div class="p-reserve--working-table-row">
                    <div class="p-reserve--working-table-label">田口　（皮膚科）</div>
                    <div class="p-reserve--working-table-value"><span>：　</span>月火　金土日</div>
                  </div>
                </div>
              </div>
            </div>
            <!-- <div class="p-reserve--cnt">
            </div> -->
          </div>
          <div class="p-reserve--working-choices">
            <p class="p-salon--ttl txt-bold2">下記より選択し、次の画面へお進みください。</p>
            <div class="p-reserve--working-choices-list">
              <div class="p-reserve--working-choices-item align-center">
                <a href="" class="btn-submit type3">初診</a>
              </div>
              <div class="p-reserve--working-choices-item align-center">
                <a href="" class="btn-submit type3">再診</a>
              </div>
              <div class="p-reserve--working-choices-item align-center">
                <a href="" class="btn-submit type3">予防接種・各種検査</a>
              </div>
              <div class="p-reserve--working-choices-item align-center">
                <a href="" class="btn-submit type3">常用薬・常用食</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>