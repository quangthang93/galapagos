<!__ include header __>
  <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/header5.php'; ?>

  <main class="main search-main">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li>探す・相談する</li>
      </ul>
    </div>
    <div class="p-mypage">
      <div class="container3">
        <div class="p-mypage__slider">
          <ul>
            <li><img src="/assets/images/search/hospital/bn-slide-1.png" alt=""></li>
            <li><img src="/assets/images/search/hospital/bn-slide-1.png" alt=""></li>
            <li><img src="/assets/images/search/hospital/bn-slide-1.png" alt=""></li>
          </ul>
        </div>

        <div class="p-search">
          <div class="p-tabbox">
            <div class="p-tabbox__btn-area">
              <div class="p-tabbox__btn active">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 16 14">
                    <g id="グループ_12" data-name="グループ 12" transform="translate(-893.558 -798.251)">
                      <path id="パス_63" data-name="パス 63" d="M906.887,812.251H896.228a2.7,2.7,0,0,1-2.67-2.708V800.9a2.666,2.666,0,0,1,2.638-2.648H906.9a2.67,2.67,0,0,1,2.656,2.668v8.623A2.7,2.7,0,0,1,906.887,812.251Zm-10.657-1.761h10.656a.946.946,0,0,0,.938-.95v-8.616a.916.916,0,0,0-.909-.913H896.23a.925.925,0,0,0-.939.9v8.627a.946.946,0,0,0,.937.948Z" fill="#818181" />
                      <path id="パス_60" data-name="パス 60" d="M909.263,809.854h-2.08v2.1H904.8v-2.1h-2.08v-2.4h2.08v-2.1h2.378v2.1h2.08Z" transform="translate(-4.436 -3.401)" fill="#818181" />
                    </g>
                  </svg>
                </div>
                <div class="text">施設を探す</div>
              </div>
              <div class="p-tabbox__btn">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                    <path id="Icon_awesome-stethoscope" data-name="Icon awesome-stethoscope" d="M12.225,3.064a1.75,1.75,0,0,0-.848,3.265V9.408a2.963,2.963,0,0,1-3.062,2.844,2.977,2.977,0,0,1-3.06-2.713A4.387,4.387,0,0,0,8.75,5.251V1A.656.656,0,0,0,8.222.359L6.5.015a.655.655,0,0,0-.771.514L5.644.958a.655.655,0,0,0,.514.771L7,1.9v3.32a2.625,2.625,0,1,1-5.25.033V1.9l.839-.167A.655.655,0,0,0,3.1.961L3.016.532A.655.655,0,0,0,2.245.018L.528.357A.659.659,0,0,0,0,1V5.251A4.382,4.382,0,0,0,3.5,9.539,4.718,4.718,0,0,0,8.313,14a4.714,4.714,0,0,0,4.813-4.594V6.329a1.75,1.75,0,0,0-.9-3.265Zm.025,2.188a.438.438,0,1,1,.438-.438A.439.439,0,0,1,12.25,5.251Z" transform="translate(0 -0.002)" fill="#818181" />
                  </svg>

                </div>
                <div class="text">専門家へ相談</div>
              </div>
            </div>
            <div class="p-tabbox__panel-area">
              <div class="p-tabbox__panel active">
                <p class="lead-text">ファミリーのための病院、サロン、動物関連施設を検索して予約できます</p>
                <div class="tabbox-2">
                  <div class="tabbox-2__btn-area">
                    <div class="tabbox-2__btn active">動物病院<span>を探す</span></div>
                    <div class="tabbox-2__btn">トリミングサロン<span>を探す</span></div>
                    <div class="tabbox-2__btn">動物関連施設<span>を探す</span></div>
                  </div>
                  <div class="tabbox-2__panel-area">
                    <div class="tabbox-2__panel active">

                      <div class="search-area">
                        <div class="search-area__top">
                          <form>
                            <div class="keyword-search">
                              <div class="keyword-search__inner">
                                <input class="input-search" type="search" placeholder="施設名・エリア・駅名など" value="" name="" />
                                <input class="input-submit" type="submit" value="検索" />
                              </div>
                            </div>
                            <div class="date-search">
                              <p>日付で探す</p>
                              <ul>
                                <li>
                                  <input type="checkbox" id="date-1" name="check-1" />
                                  <label for="date-1">
                                    <span class="date-name">今日</span><span class="date">10/2</span></label>
                                </li>
                                <li>
                                  <input type="checkbox" id="date-2" name="check-1" />
                                  <label for="date-2">
                                    <span class="date-name">明日</span><span class="date">10/3</span></label>
                                </li>
                                <li>
                                  <input type="checkbox" id="date-3" name="check-1" />
                                  <label for="date-3">
                                    <span class="date-name">日曜</span><span class="date">10/4</span></label>
                                </li>
                                <li class="datepicker-wrap">
                                  <input type="text" class="form-control" id="datepicker" placeholder="日付を指定">
                                </li>
                              </ul>
                            </div>
                            <div class="kodawari-search">
                              <p>こだわり条件で探す</p>
                              <ul>
                                <li>
                                  <input type="checkbox" id="kodawari-1" name="check-1" />
                                  <label for="kodawari-1">猫に優しい</label>
                                </li>
                                <li>
                                  <input type="checkbox" id="kodawari-2" name="check-1" />
                                  <label for="kodawari-2">来院ごほうび</label>
                                </li>
                                <li>
                                  <input type="checkbox" id="kodawari-3" name="check-1" />
                                  <label for="kodawari-3">ライフプラン相談</label>
                                </li>
                                <li>
                                  <input type="checkbox" id="kodawari-4" name="check-1" />
                                  <label for="kodawari-4">Web相談・診療</label>
                                </li>
                                <li>
                                  <input type="checkbox" id="kodawari-5" name="check-1" />
                                  <label for="kodawari-5">夜間緊急対応</label>
                                </li>
                              </ul>
                            </div>
                            <div class="search-submit">
                              <input type="submit" class="search-submit__btn" value="検索する" />
                            </div>
                          </form>
                        </div>

                        <div class="search-area__bottom">
                          <p>さらに条件を指定して探す</p>
                          <ul class="main-list">
                            <li>
                              <a href="/search/hospital/search-from-area/" class="link">
                                <div class="icon"><img src="/assets/images/search/hospital/icon-more-area.svg" alt=""></div>
                                <div class="text">
                                  エリア<span>から探す</span>
                                </div>
                              </a>
                            </li>
                            <li>
                              <a href="/search/hospital/search-from-facility/" class="link">
                                <div class="icon"><img src="/assets/images/search/hospital/icon-more-facilities.svg" alt=""></div>
                                <div class="text">
                                  施設詳細<span>から探す</span>
                                </div>
                              </a>
                            </li>
                            <li>
                              <a href="/search/hospital/search-from-onayami/" class="link">
                                <div class="icon"><img src="/assets/images/search/hospital/icon-more-nayami.svg" alt=""></div>
                                <div class="text">
                                  お悩み<span>から探す</span>
                                </div>
                              </a>
                            </li>
                            <li>
                              <a href="/search/hospital/search-from-animal/" class="link">
                                <div class="icon"><img src="/assets/images/search/hospital/icon-more-category.svg" alt=""></div>
                                <div class="text">
                                  動物種<span>から探す</span>
                                </div>
                              </a>
                            </li>
                          </ul>
                          <ul class="sub-list">
                            <li>
                              <a href="" class="link">予約履歴から探す</a>
                            </li>
                            <li>
                              <a href="" class="link">お気に入りから探す</a>
                            </li>
                          </ul>
                        </div>

                      </div>
                    </div>
                    <div class="tabbox-2__panel">
                      <p>tabbox-2の2</p>
                      <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト22222</p>
                    </div>
                    <div class="tabbox-2__panel">
                      <p>tabbox-2の3</p>
                      <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト33333</p>
                    </div>
                  </div>
                </div>
              </div>

              <div class="p-tabbox__panel">
                <p>タブ22222</p>
                <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト22222</p>
              </div>
            </div>
          </div>
        </div>


        <div class="tolist-area">
          <ul>
            <li>
              <a href="">
                <div class="icon"><img src="/assets/images/search/hospital/icon-hospital.svg" alt=""></div>
                <div class="text">動物病院一覧へ</div>
              </a>
            </li>
            <li>
              <a href="">
                <div class="icon"><img src="/assets/images/search/hospital/icon-trimming.svg" alt=""></div>
                <div class="text">トリミングサロン一覧へ</div>
              </a>
            </li>
            <li>
              <a href="">
                <div class="icon"><img src="/assets/images/search/hospital/icon-facilities.svg" alt=""></div>
                <div class="text">動物関連施設一覧へ</div>
              </a>
            </li>
          </ul>
        </div>

        <div class="nearyou-area">
          <div class="nearyou-area__head">
            <p class="head-ttl">お近くの施設</p>
            <p class="head-link">
              <a href="" class="link">一覧を見る</a>
            </p>
          </div>
          <ul class="nearyou-list">
            <li class="nearyou-list__item">
              <a href="">
                <div class="item-img"><img src="/assets/images/search/hospital/nearyou-1.jpg" alt=""></div>
                <div class="item-text">
                  <p class="item-ttl">BiBi犬猫病院</p>
                  <div class="item-address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="item-access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
              </a>
            </li>
            <li class="nearyou-list__item">
              <a href="">
                <div class="item-img"><img src="/assets/images/search/hospital/nearyou-2.jpg" alt=""></div>
                <div class="item-text">
                  <p class="item-ttl">ペットカンパニーパイン</p>
                  <div class="item-address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="item-access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
              </a>
            </li>
            <li class="nearyou-list__item">
              <a href="">
                <div class="item-img"><img src="/assets/images/search/hospital/nearyou-3.jpg" alt=""></div>
                <div class="item-text">
                  <p class="item-ttl">アマノ動物病院</p>
                  <div class="item-address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="item-access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
              </a>
            </li>
            <li class="nearyou-list__item">
              <a href="">
                <div class="item-img"><img src="/assets/images/search/hospital/nearyou-4.jpg" alt=""></div>
                <div class="item-text">
                  <p class="item-ttl">BiBi犬猫病院</p>
                  <div class="item-address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="item-access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div>


        <div class="received-consultation">
          <div class="received-consultation__head">
            <p class="head-ttl">これまでお寄せいただいたご相談</p>
            <p class="head-link">
              <a href="" class="link">一覧を見る</a>
            </p>
          </div>
          <div class="received-consultation__cont">
            <div class="received-consultation__unit">
              <div class="unit__inner">
                <div class="unit-top">
                  <div class="question">臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満気味です。散歩以外で運動って何かありますか？<a href="" class="link">[回答を見る]</a></div>
                </div>
                <div class="unit-bottom">
                  <div class="data">
                    <div class="time"><img src="/assets/images/search/hospital/icon-clock.svg" alt="">2021/07/14/12:16</div>
                    <div class="tags">
                      <ul>
                        <li>犬</li>
                        <li>あそび・散歩</li>
                      </ul>
                    </div>
                    <div class="good link">
                      <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
                          <g id="" data-name="" transform="translate(-0.1 -19.923)">
                            <path id="パス_2555" data-name="パス 2555" d="M149.253,26.254c-.727-.11-4.44-.508-5.09-.55-.493-.032-1.818-.22-.969-1.211,1.7-2.489,1.337-3.773.771-4.58-.474-.676-1.9-.881-2.225.727a5.523,5.523,0,0,1-1.975,2.978c-1.248,1.327-3.842,3.8-3.842,3.8v6.932s5.654.912,7.029,1.038a3.484,3.484,0,0,0,2.914-1.038,45.263,45.263,0,0,0,3.146-5.456C150.387,27.764,149.98,26.364,149.253,26.254Z" transform="translate(-129.822 0.515)" fill="#ff9d00" />
                            <rect id="長方形_5262" data-name="長方形 5262" width="4" height="7" transform="translate(0.1 27.923)" fill="#ff9d00" />
                          </g>
                        </svg>

                      </div>
                      <div class="ttl">参考になった</div>
                      <div class="number">99</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clip">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="19.333" height="20" viewBox="0 0 19.333 20">
                    <path id="パス_2647" data-name="パス 2647" d="M1172.667,657.992a6.258,6.258,0,0,1-4.391-1.779,6,6,0,0,1,0-8.6l8.555-8.377a4.416,4.416,0,0,1,6.146,0,4.193,4.193,0,0,1,0,6.019l-8.564,8.378a2.523,2.523,0,0,1-3.512,0,2.4,2.4,0,0,1,0-3.439l7.9-7.731a.63.63,0,0,1,.877,0,.6.6,0,0,1,0,.859l-7.9,7.731a1.2,1.2,0,0,0,0,1.721,1.264,1.264,0,0,0,1.757,0l8.564-8.378a3,3,0,0,0,0-4.3,3.155,3.155,0,0,0-4.391,0l-8.555,8.377a4.8,4.8,0,0,0,0,6.881,5.047,5.047,0,0,0,7.026,0l8.554-8.378a.63.63,0,0,1,.878,0,.6.6,0,0,1,0,.859l-8.555,8.378A6.256,6.256,0,0,1,1172.667,657.992Z" transform="translate(-1166.46 -637.992)" fill="#ebebeb" />
                  </svg>

                </div>
                <div class="text">相談を<br>
                  保存する</div>
              </div>
            </div>

            <div class="received-consultation__unit">
              <div class="unit__inner">
                <div class="unit-top">
                  <div class="question">臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満気味です。散歩以外で運動って何かありますか？<a href="" class="link">[回答を見る]</a></div>
                </div>
                <div class="unit-bottom">
                  <div class="data">
                    <div class="time"><img src="/assets/images/search/hospital/icon-clock.svg" alt="">2021/07/14/12:16</div>
                    <div class="tags">
                      <ul>
                        <li>犬</li>
                        <li>あそび・散歩</li>
                      </ul>
                    </div>
                    <div class="good link">
                      <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
                          <g id="" data-name="" transform="translate(-0.1 -19.923)">
                            <path id="パス_2555" data-name="パス 2555" d="M149.253,26.254c-.727-.11-4.44-.508-5.09-.55-.493-.032-1.818-.22-.969-1.211,1.7-2.489,1.337-3.773.771-4.58-.474-.676-1.9-.881-2.225.727a5.523,5.523,0,0,1-1.975,2.978c-1.248,1.327-3.842,3.8-3.842,3.8v6.932s5.654.912,7.029,1.038a3.484,3.484,0,0,0,2.914-1.038,45.263,45.263,0,0,0,3.146-5.456C150.387,27.764,149.98,26.364,149.253,26.254Z" transform="translate(-129.822 0.515)" fill="#ff9d00" />
                            <rect id="長方形_5262" data-name="長方形 5262" width="4" height="7" transform="translate(0.1 27.923)" fill="#ff9d00" />
                          </g>
                        </svg>

                      </div>
                      <div class="ttl">参考になった</div>
                      <div class="number">99</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clip">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="19.333" height="20" viewBox="0 0 19.333 20">
                    <path id="パス_2647" data-name="パス 2647" d="M1172.667,657.992a6.258,6.258,0,0,1-4.391-1.779,6,6,0,0,1,0-8.6l8.555-8.377a4.416,4.416,0,0,1,6.146,0,4.193,4.193,0,0,1,0,6.019l-8.564,8.378a2.523,2.523,0,0,1-3.512,0,2.4,2.4,0,0,1,0-3.439l7.9-7.731a.63.63,0,0,1,.877,0,.6.6,0,0,1,0,.859l-7.9,7.731a1.2,1.2,0,0,0,0,1.721,1.264,1.264,0,0,0,1.757,0l8.564-8.378a3,3,0,0,0,0-4.3,3.155,3.155,0,0,0-4.391,0l-8.555,8.377a4.8,4.8,0,0,0,0,6.881,5.047,5.047,0,0,0,7.026,0l8.554-8.378a.63.63,0,0,1,.878,0,.6.6,0,0,1,0,.859l-8.555,8.378A6.256,6.256,0,0,1,1172.667,657.992Z" transform="translate(-1166.46 -637.992)" fill="#ebebeb" />
                  </svg>

                </div>
                <div class="text">相談を<br>
                  保存する</div>
              </div>
            </div>

            <div class="received-consultation__unit">
              <div class="unit__inner">
                <div class="unit-top">
                  <div class="question">臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満気味です。散歩以外で運動って何かありますか？<a href="" class="link">[回答を見る]</a></div>
                </div>
                <div class="unit-bottom">
                  <div class="data">
                    <div class="time"><img src="/assets/images/search/hospital/icon-clock.svg" alt="">2021/07/14/12:16</div>
                    <div class="tags">
                      <ul>
                        <li>犬</li>
                        <li>あそび・散歩</li>
                      </ul>
                    </div>
                    <div class="good link">
                      <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
                          <g id="" data-name="" transform="translate(-0.1 -19.923)">
                            <path id="パス_2555" data-name="パス 2555" d="M149.253,26.254c-.727-.11-4.44-.508-5.09-.55-.493-.032-1.818-.22-.969-1.211,1.7-2.489,1.337-3.773.771-4.58-.474-.676-1.9-.881-2.225.727a5.523,5.523,0,0,1-1.975,2.978c-1.248,1.327-3.842,3.8-3.842,3.8v6.932s5.654.912,7.029,1.038a3.484,3.484,0,0,0,2.914-1.038,45.263,45.263,0,0,0,3.146-5.456C150.387,27.764,149.98,26.364,149.253,26.254Z" transform="translate(-129.822 0.515)" fill="#ff9d00" />
                            <rect id="長方形_5262" data-name="長方形 5262" width="4" height="7" transform="translate(0.1 27.923)" fill="#ff9d00" />
                          </g>
                        </svg>

                      </div>
                      <div class="ttl">参考になった</div>
                      <div class="number">99</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clip">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="19.333" height="20" viewBox="0 0 19.333 20">
                    <path id="パス_2647" data-name="パス 2647" d="M1172.667,657.992a6.258,6.258,0,0,1-4.391-1.779,6,6,0,0,1,0-8.6l8.555-8.377a4.416,4.416,0,0,1,6.146,0,4.193,4.193,0,0,1,0,6.019l-8.564,8.378a2.523,2.523,0,0,1-3.512,0,2.4,2.4,0,0,1,0-3.439l7.9-7.731a.63.63,0,0,1,.877,0,.6.6,0,0,1,0,.859l-7.9,7.731a1.2,1.2,0,0,0,0,1.721,1.264,1.264,0,0,0,1.757,0l8.564-8.378a3,3,0,0,0,0-4.3,3.155,3.155,0,0,0-4.391,0l-8.555,8.377a4.8,4.8,0,0,0,0,6.881,5.047,5.047,0,0,0,7.026,0l8.554-8.378a.63.63,0,0,1,.878,0,.6.6,0,0,1,0,.859l-8.555,8.378A6.256,6.256,0,0,1,1172.667,657.992Z" transform="translate(-1166.46 -637.992)" fill="#ebebeb" />
                  </svg>

                </div>
                <div class="text">相談を<br>
                  保存する</div>
              </div>
            </div>

            <div class="received-consultation__unit">
              <div class="unit__inner">
                <div class="unit-top">
                  <div class="question">臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満気味です。散歩以外で運動って何かありますか？<a href="" class="link">[回答を見る]</a></div>
                </div>
                <div class="unit-bottom">
                  <div class="data">
                    <div class="time"><img src="/assets/images/search/hospital/icon-clock.svg" alt="">2021/07/14/12:16</div>
                    <div class="tags">
                      <ul>
                        <li>犬</li>
                        <li>あそび・散歩</li>
                      </ul>
                    </div>
                    <div class="good link">
                      <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
                          <g id="" data-name="" transform="translate(-0.1 -19.923)">
                            <path id="パス_2555" data-name="パス 2555" d="M149.253,26.254c-.727-.11-4.44-.508-5.09-.55-.493-.032-1.818-.22-.969-1.211,1.7-2.489,1.337-3.773.771-4.58-.474-.676-1.9-.881-2.225.727a5.523,5.523,0,0,1-1.975,2.978c-1.248,1.327-3.842,3.8-3.842,3.8v6.932s5.654.912,7.029,1.038a3.484,3.484,0,0,0,2.914-1.038,45.263,45.263,0,0,0,3.146-5.456C150.387,27.764,149.98,26.364,149.253,26.254Z" transform="translate(-129.822 0.515)" fill="#ff9d00" />
                            <rect id="長方形_5262" data-name="長方形 5262" width="4" height="7" transform="translate(0.1 27.923)" fill="#ff9d00" />
                          </g>
                        </svg>

                      </div>
                      <div class="ttl">参考になった</div>
                      <div class="number">99</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clip">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="19.333" height="20" viewBox="0 0 19.333 20">
                    <path id="パス_2647" data-name="パス 2647" d="M1172.667,657.992a6.258,6.258,0,0,1-4.391-1.779,6,6,0,0,1,0-8.6l8.555-8.377a4.416,4.416,0,0,1,6.146,0,4.193,4.193,0,0,1,0,6.019l-8.564,8.378a2.523,2.523,0,0,1-3.512,0,2.4,2.4,0,0,1,0-3.439l7.9-7.731a.63.63,0,0,1,.877,0,.6.6,0,0,1,0,.859l-7.9,7.731a1.2,1.2,0,0,0,0,1.721,1.264,1.264,0,0,0,1.757,0l8.564-8.378a3,3,0,0,0,0-4.3,3.155,3.155,0,0,0-4.391,0l-8.555,8.377a4.8,4.8,0,0,0,0,6.881,5.047,5.047,0,0,0,7.026,0l8.554-8.378a.63.63,0,0,1,.878,0,.6.6,0,0,1,0,.859l-8.555,8.378A6.256,6.256,0,0,1,1172.667,657.992Z" transform="translate(-1166.46 -637.992)" fill="#ebebeb" />
                  </svg>

                </div>
                <div class="text">相談を<br>
                  保存する</div>
              </div>
            </div>

            <div class="received-consultation__unit">
              <div class="unit__inner">
                <div class="unit-top">
                  <div class="question">臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満気味です。散歩以外で運動って何かありますか？<a href="" class="link">[回答を見る]</a></div>
                </div>
                <div class="unit-bottom">
                  <div class="data">
                    <div class="time"><img src="/assets/images/search/hospital/icon-clock.svg" alt="">2021/07/14/12:16</div>
                    <div class="tags">
                      <ul>
                        <li>犬</li>
                        <li>あそび・散歩</li>
                      </ul>
                    </div>
                    <div class="good link">
                      <div class="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
                          <g id="" data-name="" transform="translate(-0.1 -19.923)">
                            <path id="パス_2555" data-name="パス 2555" d="M149.253,26.254c-.727-.11-4.44-.508-5.09-.55-.493-.032-1.818-.22-.969-1.211,1.7-2.489,1.337-3.773.771-4.58-.474-.676-1.9-.881-2.225.727a5.523,5.523,0,0,1-1.975,2.978c-1.248,1.327-3.842,3.8-3.842,3.8v6.932s5.654.912,7.029,1.038a3.484,3.484,0,0,0,2.914-1.038,45.263,45.263,0,0,0,3.146-5.456C150.387,27.764,149.98,26.364,149.253,26.254Z" transform="translate(-129.822 0.515)" fill="#ff9d00" />
                            <rect id="長方形_5262" data-name="長方形 5262" width="4" height="7" transform="translate(0.1 27.923)" fill="#ff9d00" />
                          </g>
                        </svg>

                      </div>
                      <div class="ttl">参考になった</div>
                      <div class="number">99</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="clip">
                <div class="icon">
                  <svg xmlns="http://www.w3.org/2000/svg" width="19.333" height="20" viewBox="0 0 19.333 20">
                    <path id="パス_2647" data-name="パス 2647" d="M1172.667,657.992a6.258,6.258,0,0,1-4.391-1.779,6,6,0,0,1,0-8.6l8.555-8.377a4.416,4.416,0,0,1,6.146,0,4.193,4.193,0,0,1,0,6.019l-8.564,8.378a2.523,2.523,0,0,1-3.512,0,2.4,2.4,0,0,1,0-3.439l7.9-7.731a.63.63,0,0,1,.877,0,.6.6,0,0,1,0,.859l-7.9,7.731a1.2,1.2,0,0,0,0,1.721,1.264,1.264,0,0,0,1.757,0l8.564-8.378a3,3,0,0,0,0-4.3,3.155,3.155,0,0,0-4.391,0l-8.555,8.377a4.8,4.8,0,0,0,0,6.881,5.047,5.047,0,0,0,7.026,0l8.554-8.378a.63.63,0,0,1,.878,0,.6.6,0,0,1,0,.859l-8.555,8.378A6.256,6.256,0,0,1,1172.667,657.992Z" transform="translate(-1166.46 -637.992)" fill="#ebebeb" />
                  </svg>

                </div>
                <div class="text">相談を<br>
                  保存する</div>
              </div>
            </div>


          </div>

        </div>


      </div><!-- /.container3 -->

      <div class="column-area">
        <div class="column-area__inner">
          <div class="column-area__head">
            <p class="head-ttl">COLUMN</p>
          </div>
          <div class="column-wrap">
            <article class="column-post">
              <a href="">
                <div class="post-thumb">
                  <img src="/assets/images/search/hospital/column-1.jpg" alt="">
                </div>
                <div class="post-text">
                  <time>2021.04.01</time>
                  <p class="post-ttl">伝えたいことがあるの？犬が飼い主のことを何度も見てくる時の心理5選～室内犬・小型犬編～</p>
                </div>
              </a>
            </article>
            <article class="column-post">
              <a href="">
                <div class="post-thumb">
                  <img src="/assets/images/search/hospital/column-2.jpg" alt="">
                </div>
                <div class="post-text">
                  <time>2021.04.01</time>
                  <p class="post-ttl">伝えたいことがあるの？犬が飼い主のことを何度も見てくる時の心理5選～室内犬・小型犬編～</p>
                </div>
              </a>
            </article>
            <article class="column-post">
              <a href="">
                <div class="post-thumb">
                  <img src="/assets/images/search/hospital/column-3.jpg" alt="">
                </div>
                <div class="post-text">
                  <time>2021.04.01</time>
                  <p class="post-ttl">伝えたいことがあるの？犬が飼い主のことを何度も見てくる時の心理5選～室内犬・小型犬編～</p>
                </div>
              </a>
            </article>
          </div>
          <div class="lets-post">
            <a href=""><img src="/assets/images/search/hospital/bn-photo-post.png" alt=""></a>
          </div>
        </div>
      </div>



      <div class="container3">

        <div class="news-area">
          <div class="news-area__head">
            <p class="head-ttl">NEWS</p>
            <p class="head-link">
              <a href="" class="link">一覧を見る</a>
            </p>
          </div>
          <div class="news-area__cont">
            <ul class="p-news--list">
              <li class="p-news--item">
                <div class="p-news--item-dateWrap">
                  <span class="date8">2021.10.12</span>
                  <span class="tag3">キャンペーン</span>
                </div>
                <a href="/news/detail" class="p-news--item-ttl link">ポイントキャンペーン実施中</a>
              </li>
              <li class="p-news--item">
                <div class="p-news--item-dateWrap">
                  <span class="date8">2021.10.12</span>
                  <span class="tag3">サービス</span>
                </div>
                <a href="/news/detail" class="p-news--item-ttl link">ペット手帳サービスはじめました。</a>
              </li>
              <li class="p-news--item">
                <div class="p-news--item-dateWrap">
                  <span class="date8">2021.10.12</span>
                  <span class="tag3 pink">重要</span>
                </div>
                <a href="/news/detail" class="p-news--item-ttl link">ページリニューアルに伴う、既存口コミ掲載終了のお知らせ</a>
              </li>
              <li class="p-news--item">
                <div class="p-news--item-dateWrap">
                  <span class="date8">2021.10.12</span>
                  <span class="tag3">キャンペーン</span>
                </div>
                <a href="/news/detail" class="p-news--item-ttl link">ポイントキャンペーン実施中</a>
              </li>
              <li class="p-news--item">
                <div class="p-news--item-dateWrap">
                  <span class="date8">2021.10.12</span>
                  <span class="tag3">サービス</span>
                </div>
                <a href="/news/detail" class="p-news--item-ttl link">ペット手帳サービスはじめました。</a>
              </li>
              <li class="p-news--item">
                <div class="p-news--item-dateWrap">
                  <span class="date8">2021.10.12</span>
                  <span class="tag3 pink">重要</span>
                </div>
                <a href="/news/detail" class="p-news--item-ttl link">ページリニューアルに伴う、既存口コミ掲載終了のお知らせ</a>
              </li>
            </ul>
          </div>
        </div>


        <div class="fbn-area">
          <div class="point-gift">
            <a href=""><img src="/assets/images/search/hospital/bn-point-gift.jpg" alt=""></a>
          </div>
          <div class="fbn-area__flex">
            <ul class="fbn-list">
              <li>
                <a href="">
                  <img src="/assets/images/search/hospital/bnr1.jpg" alt="">
                </a>
              </li>
              <li>
                <a href="">
                  <img src="/assets/images/search/hospital/bnr2.jpg" alt="">
                </a>
              </li>
              <li>
                <a href="">
                  <img src="/assets/images/search/hospital/bnr3.jpg" alt="">
                </a>
              </li>
            </ul>
          </div>
        </div>

      </div>


    </div><!-- /.p-mypage -->
  </main>
  <!__ ./main __>

    <!-- Modal -->
    <div class="modal fade" id="modalCancel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-dialog__inner">
          <div class="modal-header">
            <p class="modal-header__ttl">エリアから探す</p>
            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
          </div>
          <form action="">
            <div class="modal-cont">
              <ul class="search-from-list area-list">
                <li>
                  <div class="search-from-list__item-inner">
                    <input type="checkbox" id="area-1" name="area" />
                    <label for="area-1">埼玉県</label>
                  </div>
                </li>
                <li class="parent parent-1">
                  <div class="search-from-list__item-inner">
                    <input type="checkbox" id="area-2" name="area" />
                    <label for="area-2">東京都</label>
                  </div>
                  <div class="trigger">
                    <img class="down" src="/assets/images/common/icon-arrow-down-blue.svg" alt="">
                    <img class="up" src="/assets/images/common/icon-arrow-up-blue.svg" alt="">
                  </div>
                  <ul class="child child-1">
                    <li class="parent parent-2">
                      <div class="search-from-list__item-inner">
                        <input type="checkbox" id="area-3" name="area" />
                        <label for="area-3">東京23区</label>
                      </div>
                      <div class="trigger">
                        <img class="down" src="/assets/images/common/icon-arrow-down-blue.svg" alt="">
                        <img class="up" src="/assets/images/common/icon-arrow-up-blue.svg" alt="">
                      </div>
                      <ul class="child child-2">
                        <li>
                          <div class="search-from-list__item-inner">
                            <input type="checkbox" id="area-4" name="area" />
                            <label for="area-4">東京23区すべて</label>
                          </div>
                        </li>
                        <li>
                          <div class="search-from-list__item-inner">
                            <input type="checkbox" id="area-5" name="area" />
                            <label for="area-5">足立区</label>
                          </div>
                        </li>
                        <li>
                          <div class="search-from-list__item-inner">
                            <input type="checkbox" id="area-6" name="area" />
                            <label for="area-6">荒川区</label>
                          </div>
                        </li>
                        <li>
                          <div class="search-from-list__item-inner">
                            <input type="checkbox" id="area-7" name="area" />
                            <label for="area-7">板橋区</label>
                          </div>
                        </li>
                        <li>
                          <div class="search-from-list__item-inner">
                            <input type="checkbox" id="area-8" name="area" />
                            <label for="area-8">江戸川区</label>
                          </div>
                        </li>
                      </ul>
                    </li>
                  </ul>
                </li>
                <li>
                  <div class="search-from-list__item-inner">
                    <input type="checkbox" id="area-9" name="area" />
                    <label for="area-9">神奈川県</label>
                  </div>
                </li>
              </ul>
            </div>
            <div class="modal-foot">
              <div class="submit">
                <input type="submit" value="この条件で検索する" />
              </div>
              <div class="reset">
                <input type="reset" value="リセット">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

    <!__ include footer __>

      <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/footer4.php'; ?>