<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/header5.php'; ?>

<main class="main search-main">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li>施設を探す</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">施設予約</h2>
      </div>
      <div class="hospital-search-wrap">

        <div class="sidebar-hospital-search">

          <div class="keyword-search">
            <div class="keyword-search__inner">
              <form action="">
                <input class="input-search" type="search" placeholder="施設名・エリア・駅名など" value="" name="" />
                <input class="input-submit" type="submit" value="検索" />
              </form>
            </div>
          </div>

          <div class="refined-search">

            <form action="">
              <div class="date-search">
                <p>日付で探す</p>
                <ul>
                  <li>
                    <input type="radio" id="date-1" name="check-1" />
                    <label for="date-1">
                      当日</label>
                  </li>
                  <li>
                    <input type="radio" id="date-2" name="check-1" />
                    <label for="date-2">
                      明日</label>
                  </li>
                  <li>
                    <input type="radio" id="date-3" name="check-1" />
                    <label for="date-3">
                      明後日</label>
                  </li>
                  <li>
                    <input type="radio" id="date-4" name="check-1" />
                    <label for="date-4">
                      日付を指定</label>
                  </li>
                  <li class="datepicker-wrap">
                    <input type="text" class="form-control" id="datepicker" placeholder="日付を指定">
                  </li>
                </ul>
              </div>
              <div class="kodawari-search">
                <p>こだわり条件で探す</p>
                <ul>
                  <li>
                    <input type="checkbox" id="kodawari-1" name="check-1" />
                    <label for="kodawari-1">猫に優しい</label>
                  </li>
                  <li>
                    <input type="checkbox" id="kodawari-2" name="check-1" />
                    <label for="kodawari-2">来院ごほうび</label>
                  </li>
                  <li>
                    <input type="checkbox" id="kodawari-3" name="check-1" />
                    <label for="kodawari-3">ライフプラン相談</label>
                  </li>
                  <li>
                    <input type="checkbox" id="kodawari-4" name="check-1" />
                    <label for="kodawari-4">Web相談・診療</label>
                  </li>
                  <li>
                    <input type="checkbox" id="kodawari-5" name="check-1" />
                    <label for="kodawari-5">夜間緊急対応</label>
                  </li>
                </ul>
              </div>
              <div class="area-search">
                <p>エリアから探す</p>
                <div class="prefectures">
                  <p>都道府県</p>
                  <div class="select-wrap">
                    <select>
                      <option value="0">東京都</option>
                      <option value="1">埼玉県</option>
                      <option value="2">神奈川県</option>
                      <option value="3">千葉県</option>
                    </select>
                  </div>
                </div>
                <div class="municipalities">
                  <p>市区町村</p>
                  <div class="select-wrap">
                    <select>
                      <option value="10">荒川区</option>
                      <option value="11">板橋区</option>
                      <option value="12">江戸川区</option>
                      <option value="13">世田谷区</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="more-search">
                <p>さらに条件を指定して探す</p>
                <div class="more-search__inner">
                  <p class="trigger">施設詳細から探す</p>
                  <ul>
                    <li>
                      <input type="checkbox" id="facility-1" name="facility" />
                      <label for="facility-1">本日営業</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-2" name="facility" />
                      <label for="facility-2">駐車場</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-3" name="facility" />
                      <label for="facility-3">往診</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-4" name="facility" />
                      <label for="facility-4">送迎</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-5" name="facility" />
                      <label for="facility-5">クレジット対応</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-6" name="facility" />
                      <label for="facility-6">ペットホテル</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-7" name="facility" />
                      <label for="facility-7">ドッグラン</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-8" name="facility" />
                      <label for="facility-8">トリミング</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-8" name="facility" />
                      <label for="facility-8">しつけ相談</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-9" name="facility" />
                      <label for="facility-9">バリアフリー</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-10" name="facility" />
                      <label for="facility-10">パピーパーティ</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-11" name="facility" />
                      <label for="facility-11">外国語対応</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-12" name="facility" />
                      <label for="facility-12">猫専用の時間あり</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-13" name="facility" />
                      <label for="facility-13">認定医・専門医</label>
                    </li>
                    <li>
                      <input type="checkbox" id="facility-14" name="facility" />
                      <label for="facility-14">19時以降の受診可能</label>
                    </li>
                  </ul>
                </div>
                <div class="more-search__inner">
                  <p class="trigger">お悩みから探す</p>
                  <ul>
                    <li>
                      <input type="checkbox" id="onayami-0" name="onayami" />
                      <label for="onayami-0">眼科</label>
                    </li>
                    <li>
                      <input type="checkbox" id="onayami-1" name="onayami" />
                      <label for="onayami-1">皮膚</label>
                    </li>
                    <li>
                      <input type="checkbox" id="onayami-2" name="onayami" />
                      <label for="onayami-2">循環器</label>
                    </li>
                    <li>
                      <input type="checkbox" id="onayami-3" name="onayami" />
                      <label for="onayami-3">呼吸器</label>
                    </li>
                    <li>
                      <input type="checkbox" id="onayami-4" name="onayami" />
                      <label for="onayami-4">整形(骨)</label>
                    </li>
                    <li>
                      <input type="checkbox" id="onayami-5" name="onayami" />
                      <label for="onayami-5">腫瘍</label>
                    </li>
                    <li>
                      <input type="checkbox" id="onayami-6" name="onayami" />
                      <label for="onayami-6">画像診断</label>
                    </li>
                    <li>
                      <input type="checkbox" id="onayami-7" name="onayami" />
                      <label for="onayami-7">その他</label>
                    </li>
                  </ul>
                </div>
                <div class="more-search__inner">
                  <p class="trigger">動物種から探す</p>
                  <ul>
                    <li>
                      <input type="checkbox" id="animal-1" name="animal" />
                      <label for="animal-1">犬</label>
                    </li>
                    <li>
                      <input type="checkbox" id="animal-2" name="animal" />
                      <label for="animal-2">猫</label>
                    </li>
                    <li>
                      <input type="checkbox" id="animal-3" name="animal" />
                      <label for="animal-3">うさぎ</label>
                    </li>
                    <li>
                      <input type="checkbox" id="animal-4" name="animal" />
                      <label for="animal-4">フェレット</label>
                    </li>
                    <li>
                      <input type="checkbox" id="animal-5" name="animal" />
                      <label for="animal-5">鳥類</label>
                    </li>
                    <li>
                      <input type="checkbox" id="animal-6" name="animal" />
                      <label for="animal-6">爬虫類</label>
                    </li>
                    <li>
                      <input type="checkbox" id="animal-7" name="animal" />
                      <label for="animal-7">両生類</label>
                    </li>
                    <li>
                      <input type="checkbox" id="animal-8" name="animal" />
                      <label for="animal-8">その他</label>
                    </li>
                  </ul>
                </div>
              </div>

              <div class="refined-search__bottom">

                <div class="form-btn-wrap">
                  <div class="submit">
                    <input type="submit" value="検索" />
                  </div>
                  <div class="reset">
                    <input type="reset" value="リセット">
                  </div>
                </div>

                <div class="search-from-history">
                  <a href="">
                    予約履歴から探す
                  </a>
                </div>

                <div class="search-from-like">
                  <a href="">
                    お気に入りからから探す
                  </a>
                </div>

              </div>

            </form>

          </div>
          <!--/.refined-search -->

          <div class="bn-area">
            <ul>
              <li>
                <a href="">
                  <img src="/assets/images/search/hospital/side-bnr1.jpg" alt="">
                </a>
              </li>
              <li>
                <a href="">
                  <img src="/assets/images/search/hospital/side-bnr2.jpg" alt="">
                </a>
              </li>
              <li>
                <a href="">
                  <img src="/assets/images/search/hospital/side-bnr3.jpg" alt="">
                </a>
              </li>
            </ul>
          </div>


        </div><!-- /.sidebar-hospital-search -->

        <main class="main-hospital-search">

          <div class="main-hospital-search__head">
            <div class="search-result-ttl">検索結果<span>114</span>件</div>
            <div class="rearrange">
              <form action="">
                <div class="select-wrap">
                  <select>
                    <option value="0">評価が高い順</option>
                    <option value="1">評価が低い順</option>
                    <option value="0">評価が高い順</option>
                    <option value="1">評価が低い順</option>
                  </select>
                </div>
              </form>
            </div>
          </div>

          <div class="main-hospital-search__cont">

            <div class="hospital-unit">
              <!--.hospital-unit-->
              <div class="hospital-unit__inner">
                <div class="hospital-unit__ttl">BiBi犬猫病院</div>
                <div class="hospital-unit__ratingWrap">
                  <div class="hospital-unit__rating mgr-10">
                    評価
                    <div class="hospital-unit__rating-stars mgr-5">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-white.svg" alt="">
                    </div>
                    <div class="hospital-unit__rating-number ttl-blue2">3.5</div>
                  </div>
                  <a href="" class="hospital-unit__rating-cmt ttl-blue2">
                    100件の評価
                  </a>
                </div>
                <div class="hospital-unit__rating-cmt">
                  口コミ数
                  <a href="" class="link">
                    32
                  </a>
                </div>
                <div class="hospital-unit__address-wrap">
                  <div class="hospital-unit__address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="hospital-unit__access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
                <div class="hospital-unit__img">
                  <ul>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                  </ul>
                </div>
                <div class="hospital-unit__tags-1">
                  <span class="tag-1 green-1">はじめて</span>
                  <span class="tag-1 yellow">２回目以降</span>
                  <span class="tag-1 gray">予防</span>
                  <span class="tag-1 green-2">健康診断</span>
                </div>
                <div class="hospital-unit__tags-2">
                  <span class="tag-2"><img src="" alt="">猫に優しい</span>
                  <span class="tag-2"><img src="" alt="">来院ごほうび</span>
                  <span class="tag-2"><img src="" alt="">ライフプラン<br>相談</span>
                  <span class="tag-2"><img src="" alt="">Web相談・診療</span>
                  <span class="tag-2"><img src="" alt="">夜間緊急対応</span>
                </div>
                <div class="hospital-unit__message">
                  どんな些細なこともご相談ください。私たちはていねいにお話を伺う動物病院です。
                </div>
              </div>
              <div class="hospital-unit__info">
                <div class="info-row">
                  <div class="info-label">営業時間</div>
                  <div class="info-cont">
                    平日8:30～21:00/土日祝9:30～19:00
                  </div>
                </div>
                <div class="info-row">
                  <div class="info-label">定休日</div>
                  <div class="info-cont">
                    水曜日
                  </div>
                </div>
              </div>
              <div class="hospital-unit__foot">
                <div class="reserve">
                  <a href="">予約する</a>
                </div>
                <div class="like">
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-heart.svg" alt="">
                  </div>
                  <div class="text">
                    お気に入り
                  </div>
                </div>
              </div>
            </div>
            <!--/.hospital-unit-->

            <div class="hospital-unit">
              <!--.hospital-unit-->
              <div class="hospital-unit__inner">
                <div class="hospital-unit__ttl">BiBi犬猫病院</div>
                <div class="hospital-unit__ratingWrap">
                  <div class="hospital-unit__rating mgr-10">
                    評価
                    <div class="hospital-unit__rating-stars mgr-5">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-white.svg" alt="">
                    </div>
                    <div class="hospital-unit__rating-number ttl-blue2">3.5</div>
                  </div>
                  <a href="" class="hospital-unit__rating-cmt ttl-blue2">
                    100件の評価
                  </a>
                </div>
                <div class="hospital-unit__rating-cmt">
                  口コミ数
                  <a href="" class="link">
                    32
                  </a>
                </div>
                <div class="hospital-unit__address-wrap">
                  <div class="hospital-unit__address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="hospital-unit__access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
                <div class="hospital-unit__img">
                  <ul>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                  </ul>
                </div>
                <div class="hospital-unit__tags-1">
                  <span class="tag-1 green-1">はじめて</span>
                  <span class="tag-1 yellow">２回目以降</span>
                  <span class="tag-1 gray">予防</span>
                  <span class="tag-1 green-2">健康診断</span>
                </div>
                <div class="hospital-unit__tags-2">
                  <span class="tag-2"><img src="" alt="">猫に優しい</span>
                  <span class="tag-2"><img src="" alt="">来院ごほうび</span>
                </div>
                <div class="hospital-unit__message">
                  どんな些細なこともご相談ください。私たちはていねいにお話を伺う動物病院です。
                </div>
              </div>
              <div class="hospital-unit__info">
                <div class="info-row">
                  <div class="info-label">営業時間</div>
                  <div class="info-cont">
                    平日8:30～21:00/土日祝9:30～19:00
                  </div>
                </div>
                <div class="info-row">
                  <div class="info-label">定休日</div>
                  <div class="info-cont">
                    水曜日
                  </div>
                </div>
              </div>
              <div class="hospital-unit__foot">
                <div class="reserve">
                  <a href="">予約する</a>
                </div>
                <div class="like">
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-heart.svg" alt="">
                  </div>
                  <div class="text">
                    お気に入り
                  </div>
                </div>
              </div>
            </div>
            <!--/.hospital-unit-->

            <div class="hospital-unit">
              <!--.hospital-unit-->
              <div class="hospital-unit__inner">
                <div class="hospital-unit__ttl">BiBi犬猫病院</div>
                <div class="hospital-unit__ratingWrap">
                  <div class="hospital-unit__rating mgr-10">
                    評価
                    <div class="hospital-unit__rating-stars mgr-5">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-white.svg" alt="">
                    </div>
                    <div class="hospital-unit__rating-number ttl-blue2">3.5</div>
                  </div>
                  <a href="" class="hospital-unit__rating-cmt ttl-blue2">
                    100件の評価
                  </a>
                </div>
                <div class="hospital-unit__rating-cmt">
                  口コミ数
                  <a href="" class="link">
                    32
                  </a>
                </div>
                <div class="hospital-unit__address-wrap">
                  <div class="hospital-unit__address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="hospital-unit__access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
                <div class="hospital-unit__img">
                  <ul>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                  </ul>
                </div>
                <div class="hospital-unit__tags-1">
                  <span class="tag-1 green-1">はじめて</span>
                  <span class="tag-1 yellow">２回目以降</span>
                  <span class="tag-1 gray">予防</span>
                  <span class="tag-1 green-2">健康診断</span>
                </div>
                <div class="hospital-unit__tags-2">
                  <span class="tag-2"><img src="" alt="">Web相談・診療</span>
                  <span class="tag-2"><img src="" alt="">夜間緊急対応</span>
                </div>
                <div class="hospital-unit__message">
                  どんな些細なこともご相談ください。私たちはていねいにお話を伺う動物病院です。
                </div>
              </div>
              <div class="hospital-unit__info">
                <div class="info-row">
                  <div class="info-label">営業時間</div>
                  <div class="info-cont">
                    平日8:30～21:00/土日祝9:30～19:00
                  </div>
                </div>
                <div class="info-row">
                  <div class="info-label">定休日</div>
                  <div class="info-cont">
                    水曜日
                  </div>
                </div>
              </div>
              <div class="hospital-unit__foot">
                <div class="reserve">
                  <a href="">予約する</a>
                </div>
                <div class="like">
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-heart.svg" alt="">
                  </div>
                  <div class="text">
                    お気に入り
                  </div>
                </div>
              </div>
            </div>
            <!--/.hospital-unit-->

            <div class="hospital-unit">
              <!--.hospital-unit-->
              <div class="hospital-unit__inner">
                <div class="hospital-unit__ttl">BiBi犬猫病院</div>
                <div class="hospital-unit__ratingWrap">
                  <div class="hospital-unit__rating mgr-10">
                    評価
                    <div class="hospital-unit__rating-stars mgr-5">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-white.svg" alt="">
                    </div>
                    <div class="hospital-unit__rating-number ttl-blue2">3.5</div>
                  </div>
                  <a href="" class="hospital-unit__rating-cmt ttl-blue2">
                    100件の評価
                  </a>
                </div>
                <div class="hospital-unit__rating-cmt">
                  口コミ数
                  <a href="" class="link">
                    32
                  </a>
                </div>
                <div class="hospital-unit__address-wrap">
                  <div class="hospital-unit__address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="hospital-unit__access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
                <div class="hospital-unit__img">
                  <ul>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                  </ul>
                </div>
                <div class="hospital-unit__tags-1">
                  <span class="tag-1 green-1">はじめて</span>
                  <span class="tag-1 yellow">２回目以降</span>
                  <span class="tag-1 gray">予防</span>
                  <span class="tag-1 green-2">健康診断</span>
                </div>
                <div class="hospital-unit__tags-2">
                  <span class="tag-2"><img src="" alt="">猫に優しい</span>
                  <span class="tag-2"><img src="" alt="">来院ごほうび</span>
                </div>
                <div class="hospital-unit__message">
                  どんな些細なこともご相談ください。私たちはていねいにお話を伺う動物病院です。
                </div>
              </div>
              <div class="hospital-unit__info">
                <div class="info-row">
                  <div class="info-label">営業時間</div>
                  <div class="info-cont">
                    平日8:30～21:00/土日祝9:30～19:00
                  </div>
                </div>
                <div class="info-row">
                  <div class="info-label">定休日</div>
                  <div class="info-cont">
                    水曜日
                  </div>
                </div>
              </div>
              <div class="hospital-unit__foot">
                <div class="reserve">
                  <a href="">予約する</a>
                </div>
                <div class="like">
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-heart.svg" alt="">
                  </div>
                  <div class="text">
                    お気に入り
                  </div>
                </div>
              </div>
            </div>
            <!--/.hospital-unit-->

            <div class="hospital-unit">
              <!--.hospital-unit-->
              <div class="hospital-unit__inner">
                <div class="hospital-unit__ttl">BiBi犬猫病院</div>
                <div class="hospital-unit__ratingWrap">
                  <div class="hospital-unit__rating mgr-10">
                    評価
                    <div class="hospital-unit__rating-stars mgr-5">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-white.svg" alt="">
                    </div>
                    <div class="hospital-unit__rating-number ttl-blue2">3.5</div>
                  </div>
                  <a href="" class="hospital-unit__rating-cmt ttl-blue2">
                    100件の評価
                  </a>
                </div>
                <div class="hospital-unit__rating-cmt">
                  口コミ数
                  <a href="" class="link">
                    32
                  </a>
                </div>
                <div class="hospital-unit__address-wrap">
                  <div class="hospital-unit__address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="hospital-unit__access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
                <div class="hospital-unit__img">
                  <ul>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                  </ul>
                </div>
                <div class="hospital-unit__tags-1">
                  <span class="tag-1 green-1">はじめて</span>
                  <span class="tag-1 yellow">２回目以降</span>
                  <span class="tag-1 gray">予防</span>
                  <span class="tag-1 green-2">健康診断</span>
                </div>
                <div class="hospital-unit__tags-2">
                  <span class="tag-2"><img src="" alt="">猫に優しい</span>
                  <span class="tag-2"><img src="" alt="">来院ごほうび</span>
                  <span class="tag-2"><img src="" alt="">ライフプラン<br>相談</span>
                  <span class="tag-2"><img src="" alt="">Web相談・診療</span>
                  <span class="tag-2"><img src="" alt="">夜間緊急対応</span>
                </div>
                <div class="hospital-unit__message">
                  どんな些細なこともご相談ください。私たちはていねいにお話を伺う動物病院です。
                </div>
              </div>
              <div class="hospital-unit__info">
                <div class="info-row">
                  <div class="info-label">営業時間</div>
                  <div class="info-cont">
                    平日8:30～21:00/土日祝9:30～19:00
                  </div>
                </div>
                <div class="info-row">
                  <div class="info-label">定休日</div>
                  <div class="info-cont">
                    水曜日
                  </div>
                </div>
              </div>
              <div class="hospital-unit__foot">
                <div class="reserve">
                  <a href="">予約する</a>
                </div>
                <div class="like">
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-heart.svg" alt="">
                  </div>
                  <div class="text">
                    お気に入り
                  </div>
                </div>
              </div>
            </div>
            <!--/.hospital-unit-->

            <div class="hospital-unit">
              <!--.hospital-unit-->
              <div class="hospital-unit__inner">
                <div class="hospital-unit__ttl">BiBi犬猫病院</div>
                <div class="hospital-unit__ratingWrap">
                  <div class="hospital-unit__rating mgr-10">
                    評価
                    <div class="hospital-unit__rating-stars mgr-5">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                      <img src="/assets/images/search/icon-start-white.svg" alt="">
                    </div>
                    <div class="hospital-unit__rating-number ttl-blue2">3.5</div>
                  </div>
                  <a href="" class="hospital-unit__rating-cmt ttl-blue2">
                    100件の評価
                  </a>
                </div>
                <div class="hospital-unit__rating-cmt">
                  口コミ数
                  <a href="" class="link">
                    32
                  </a>
                </div>
                <div class="hospital-unit__address-wrap">
                  <div class="hospital-unit__address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="hospital-unit__access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
                <div class="hospital-unit__img">
                  <ul>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                    <li>
                      <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                    </li>
                  </ul>
                </div>
                <div class="hospital-unit__tags-1">
                  <span class="tag-1 green-1">はじめて</span>
                  <span class="tag-1 yellow">２回目以降</span>
                  <span class="tag-1 gray">予防</span>
                  <span class="tag-1 green-2">健康診断</span>
                </div>
                <div class="hospital-unit__tags-2">
                  <span class="tag-2"><img src="" alt="">猫に優しい</span>
                  <span class="tag-2"><img src="" alt="">来院ごほうび</span>
                </div>
                <div class="hospital-unit__message">
                  どんな些細なこともご相談ください。私たちはていねいにお話を伺う動物病院です。
                </div>
              </div>
              <div class="hospital-unit__info">
                <div class="info-row">
                  <div class="info-label">営業時間</div>
                  <div class="info-cont">
                    平日8:30～21:00/土日祝9:30～19:00
                  </div>
                </div>
                <div class="info-row">
                  <div class="info-label">定休日</div>
                  <div class="info-cont">
                    水曜日
                  </div>
                </div>
              </div>
              <div class="hospital-unit__foot">
                <div class="reserve">
                  <a href="">予約する</a>
                </div>
                <div class="like">
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-heart.svg" alt="">
                  </div>
                  <div class="text">
                    お気に入り
                  </div>
                </div>
              </div>
            </div>
            <!--/.hospital-unit-->


          </div>

          <div class="main-hospital-search__pagination">

            <div class="pagination">
              <p>0,000件中｜1〜30件 表示</p>
              <ul>
                <li class="prev"><a href=""><img src="/assets/images//common/icon-arrow-left-blue2.svg" alt=""></a></li>
                <li><a href="">1</a></li>
                <li class="current">2</li>
                <li><a href="">3</a></li>
                <li><a href="">4</a></li>
                <li><a href="">5</a></li>
                <li><a href="">6</a></li>
                <li>・・・</li>
                <li><a href="">12</a></li>
                <li class="next"><a href=""><img src="/assets/images//common/icon-arrow-right-blue2.svg" alt=""></a></li>
              </ul>
            </div>
          </div>

        </main>



      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/footer4.php'; ?>