<!__ include header __>
  <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/header5.php'; ?>

  <main class="main search-main">
    <div class="breadcrumb">
      <ul>
        <li><a href="/">トップページ</a></li>
        <li><a href="">探す・相談する</a></li>
        <li>施設を探す</li>
      </ul>
    </div>
    <div class="p-mypage">
      <div class="container3">


        <div class="p-hospital">

          <div class="p-hospital__top">
            <div class="top__img">
              <ul class="top__img__main-slider">
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
              </ul>
              <ul class="top__img__nav-slider">
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
                <li>
                  <img src="/assets/images/search/hospital/hospital-img-1.jpg" alt="">
                </li>
              </ul>
            </div>
            <div class="top__text">
              <div class="top__text__ttl">BiBi犬猫病院</div>
              <div class="top__text__ratingWrap">
                <div class="top__text__rating mgr-10">
                  <div class="top__text__rating-stars mgr-5">
                    <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                    <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                    <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                    <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                    <img src="/assets/images/search/icon-start-white.svg" alt="">
                  </div>
                  <div class="top__text__rating-number ttl-blue2">3.5</div>
                </div>
                <a href="" class="link">
                  100件の評価
                </a>
                <div class="top__text__rating-cmt">
                  <a href="" class="link">
                    32
                  </a>
                </div>
              </div>

              <div class="top__text__address-wrap">
                <div class="top__text__address">
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                  </div>東京都世田谷区
                </div>
                <div class="top__text__access">
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                  </div>京王線 笹塚駅
                </div>
              </div>
              <div class="top__text__tags-2">
                <span class="tag-2"><img src="" alt="">猫に優しい</span>
                <span class="tag-2"><img src="" alt="">来院ごほうび</span>
              </div>
              <div class="top__text__message">
                <p class="lead">どんな些細なこともご相談ください。私たちはていねいにお話を伺う動物病院です。</p>
                <p>当院は、飼い主様のどんな些細な不安やお悩みもお話しいただけるような動物病院を目指しております。病気の治療をするためだけの場所ではなく、日頃の迷いをお気軽にご相談いただける場所としても、お役立てください。もちろん、病気の治療に際しても、飼い主様に対するていねいなヒアリングを心掛けております。また、ペットの痛みや辛さを少しでも軽減できるような診療に努めております。</p>
              </div>
              <div class="top__text__foot">
                <div class="reserve">
                  <a href="">予約する</a>
                </div>
                <div class="like">
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-heart.svg" alt="">
                  </div>
                  <div class="text">
                    お気に入り
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="p-hospital__middle">
            <div class="p-tabbox">
              <div class="p-tabbox__btn-area">
                <div class="p-tabbox__btn active">
                  施設概要
                </div>
                <div class="p-tabbox__btn">
                  施設詳細
                </div>
                <div class="p-tabbox__btn">
                  レビュー
                </div>
              </div>
              <div class="p-tabbox__panel-area">
                <div class="p-tabbox__panel active">

                  <div class="overview-wrap">
                    <div class="overview-text">
                      <div class="overview-text__info">
                        <div class="info-row">
                          <table>
                            <tbody>
                              <tr>
                                <th>本日営業</th>
                                <td>◯</td>
                                <th>駐車場</th>
                                <td>◯</td>
                              </tr>
                              <tr>
                                <th>往診</th>
                                <td>◯</td>
                                <th>送迎</th>
                                <td>◯</td>
                              </tr>
                              <tr>
                                <th>クレジット対応</th>
                                <td>-</td>
                                <th>ホテル併設</th>
                                <td>◯</td>
                              </tr>
                              <tr>
                                <th>当日対応OK</th>
                                <td>◯</td>
                                <th>写真撮影</th>
                                <td>-</td>
                              </tr>
                              <tr>
                                <th>各種割引</th>
                                <td>-</td>
                                <th>しつけ教室</th>
                                <td>◯</td>
                              </tr>
                              <tr>
                                <th>お手入れ教室</th>
                                <td>◯</td>
                                <th>ケージレス<br>対応可</th>
                                <td>◯</td>
                              </tr>
                              <tr>
                                <th>日本犬相談可</th>
                                <td>◯</td>
                                <th>シニア犬相談可</th>
                                <td>◯</td>
                              </tr>
                              <tr>
                                <th>デザインカット<br>対応可</th>
                                <td>◯</td>
                                <th>スマホ決済対応</th>
                                <td>-</td>
                              </tr>
                              <tr>
                                <th>マイクロバブル</th>
                                <td>◯</td>
                                <th>パック対応可</th>
                                <td>-</td>
                              </tr>
                            </tbody>
                          </table>
                        </div>
                        <div class="info-row">
                          <div class="info-label">対応動物種</div>
                          <div class="info-cont">
                            <span class="label-search">犬</span>
                            <span class="label-search green">猫</span>
                            <span class="label-search blue2">うさぎ</span>
                            <span class="label-search pink">フェレット</span>
                            <span class="label-search green2">鳥類</span>
                            <span class="label-search pink2">爬虫類</span>
                            <span class="label-search blue">両生類</span>
                            <span class="label-search gray">その他</span>
                          </div>
                        </div>
                        <div class="info-row">
                          <div class="info-label">サービス内容</div>
                          <div class="info-cont">
                            <span class="label-search2">眼</span>
                            <span class="label-search2">皮膚</span>
                            <span class="label-search2">循環器</span>
                            <span class="label-search2">腫瘍</span>
                            <span class="label-search2">画像診断</span>
                            <span class="label-search2">整形（骨）</span>
                            <span class="label-search2">その他</span>
                          </div>
                        </div>
                        <div class="info-row">
                          <div class="info-label">営業時間</div>
                          <div class="info-cont">
                            <p class="desc4">平日8:30～21:00/土日祝9:30～19:00</p>
                          </div>
                        </div>
                        <div class="info-row">
                          <div class="info-label">定休日</div>
                          <div class="info-cont">
                            <p class="desc4">水曜日</p>
                          </div>
                        </div>
                        <div class="info-row">
                          <div class="info-label">SNS</div>
                          <div class="info-cont">
                            <a href="" class="link-blue mgr-10" target="_blank">Instagram</a>
                            <a href="" class="link-blue mgr-10" target="_blank">Facebook</a>
                            <a href="" class="link-blue" target="_blank">Twitter</a>
                          </div>
                        </div>
                        <div class="info-row">
                          <div class="info-label">電話番号</div>
                          <div class="info-cont">
                            <a href="tel:03-1234-5678" class="link desc4">03-1234-5678</a>
                          </div>
                        </div>
                        <div class="info-row">
                          <div class="info-label">アクセス</div>
                          <div class="info-cont">
                            <p class="desc4">東京都世田谷区北沢2-28-8 下北沢駅北口から出て、鎌倉通りを北へ進みます。徒歩7分で着きます。</p>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="overview-map">
                      <div class="overview-map__map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3241.5506634617886!2d139.66345825081194!3d35.66344078010124!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f36b57fe1dcb%3A0x356726b5bb7f0c39!2z44CSMTU1LTAwMzEg5p2x5Lqs6YO95LiW55Sw6LC35Yy65YyX5rKi77yS5LiB55uu77yS77yY4oiS77yY!5e0!3m2!1sja!2sjp!4v1642661583890!5m2!1sja!2sjp" width="100%" height="590" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                      </div>
                      <div class="overview-map__btn">
                        <a href="" class="link">Google Mapを開く<img class="icon" src="/assets/images/search/hospital/icon-extlink.svg" alt=""></a>
                      </div>
                    </div>

                  </div>

                </div>

                <div class="p-tabbox__panel">
                  <div class="introduction-wrap">

                    <div class="introduction-unit">
                      <!--.introduction-unit-->
                      <div class="introduction-unit__ttl">
                        スタッフ紹介
                      </div>
                      <div class="introduction-unit__flex">
                        <div class="introduction-unit__text">
                          <p class="introduction-unit__text-heading">病院は、病気を治すだけではありません。</p>
                          <div class="introduction-unit__text-cont">
                            <p>日々の診察の中、病気の相談で多いのは、「インターネットで調べてみたのですが、この子の具合を心配した方が良いのかしら？それとも考え過ぎかしら？」また、病気以外の相談も多く、「複数の知人、お店や雑誌からいろいろなアドバイスをもらうのですが、どれを信じて良いのか迷ってます。」という“迷いの相談”が、実に多いと感じています。</p>
                          </div>
                        </div>
                        <div class="introduction-unit__img">
                          <img src="/assets/images/search/hospital/introduction-1.jpg" alt="">
                        </div>
                      </div>
                    </div>
                    <!--/.introduction-unit-->

                    <div class="introduction-unit">
                      <!--.introduction-unit-->
                      <div class="introduction-unit__ttl">
                        診療時間外の対応
                      </div>
                      <div class="introduction-unit__flex">
                        <div class="introduction-unit__text">
                          <p class="introduction-unit__text-heading">診療時間外の対応（休診日や夜間など）</p>
                          <div class="introduction-unit__text-cont">
                            <p>当院をかかりつけにされている患者様で、生命に影響を及ぼすような緊急性のある場合に限り対応させていただきます。必ず番号「通知」でお電話いただき、獣医師の指示に従ってください。（時間外診察をご利用の際は別途時間外料金がかかります）
                              獣医師の都合により時間外対応が出来ない場合もあります。15分以内に折り返しの連絡もない場合には救急病院を受診してください。</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--/.introduction-unit-->

                    <div class="introduction-unit">
                      <!--.introduction-unit-->
                      <div class="introduction-unit__ttl">
                        病院からのお願い
                      </div>
                      <div class="introduction-unit__flex">
                        <div class="introduction-unit__text">
                          <p class="introduction-unit__text-heading">ネコちゃんをお連れの方へ</p>
                          <div class="introduction-unit__text-cont">
                            <p>待合室ではネコちゃんをキャリーケース・洗濯ネットに入れて、脱走しないようにしてお待ちください。また、興奮しやすい子・逃げ出す恐れのある子は病院スタッフに申し出てください。首輪がついていない子、リードをお持ちでない方も、病院スタッフにお知らせください。</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--/.introduction-unit-->

                    <div class="introduction-unit">
                      <!--.introduction-unit-->
                      <div class="introduction-unit__ttl">
                        体調不良以外での利用シーン
                      </div>
                      <div class="introduction-unit__flex">
                        <div class="introduction-unit__text">
                          <div class="introduction-unit__text-cont">
                            <p>健康なペットをお預かりするサービスもございます。
                              犬と猫の部屋は分けてお預かりしています。
                              動物病院併設のため、ホテル中の体調不良も早期に対応することが出来ます。つめきりや耳処置等、お預かり中に実施することも可能です。お迎えの際はホテルスタッフより、お預かり時の様子をお伝えいたします。
                              体調不良や治療が必要な症状があった場合は、獣医師からのお話があります。</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <!--/.introduction-unit-->

                  </div>
                </div>
                <div class="p-tabbox__panel">

                  <div class="review-wrap">
                    <div class="review-write">
                      <div class="review-write__btn">
                        <a href="">
                          レビューを書く
                        </a>
                      </div>
                    </div>
                    <div class="overall-rating">
                      <p class="overall-rating__ttl">
                        総合評価
                      </p>
                      <div class="overall-rating__head">
                        <div class="overall-rating-stars-wrap">
                          <div class="overall-rating-stars">
                            <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                            <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                            <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                            <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                            <img src="/assets/images/search/icon-start-white.svg" alt="">
                          </div>
                          <div class="overall-rating-number">4.0 / 5.0</div>
                        </div>
                        <a href="" class="link">
                          100件の評価
                        </a>
                      </div>
                      <div class="overall-rating__cont">
                        <div class="star-wrap">
                          <div class="bar-ttl">星５つ</div>
                          <div class="bar" style="width:32%;"></div>
                        </div>
                        <div class="star-wrap">
                          <div class="bar-ttl">星4つ</div>
                          <div class="bar" style="width:21%;"></div>
                        </div>
                        <div class="star-wrap">
                          <div class="bar-ttl">星3つ</div>
                          <div class="bar" style="width:25%;"></div>
                        </div>
                        <div class="star-wrap">
                          <div class="bar-ttl">星2つ</div>
                          <div class="bar" style="width:14%;"></div>
                        </div>
                        <div class="star-wrap">
                          <div class="bar-ttl">星1つ</div>
                          <div class="bar" style="width:7%;"></div>
                        </div>
                      </div>


                    </div>
                    <div class="review-voice">
                      <div class="review-voice__ttl-wrap">
                        <p class="review-voice__ttl">
                          口コミ
                        </p>
                        <div class="review-voice__rating-cmt">
                          <a href="" class="link">
                            32
                          </a>
                        </div>
                      </div>
                      <div class="review-voice__cont">

                        <div class="review-voice__unit">
                          <!--.review-voice__unit-->
                          <div class="review-voice__unit-head">
                            <div class="review-voice__unit-name">
                              ニックネーム
                            </div>
                            <div class="review-voice__unit-stars-wrap">
                              <div class="review-voice__unit-stars">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-white.svg" alt="">
                                <img src="/assets/images/search/icon-start-white.svg" alt="">
                              </div>
                              <div class="review-voice__unit-rating-number">3.2</div>
                            </div>
                          </div>
                          <div class="review-voice__unit-cont">
                            <p>知人からの評判通り、院長先生が丁寧に診察してくださいました。
                              初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。
                              受付の方の対応も良かったですよ。</p>
                          </div>
                          <div class="review-voice__unit-foot">
                            <div class="review-voice__unit-foot-data-wrap">
                              <div class="review-voice__unit-foot-data">
                                <div class="review-voice__unit-foot-data-label">種類</div>
                                <div class="review-voice__unit-foot-data-cont">犬 | チワワ</div>
                              </div>
                              <div class="review-voice__unit-foot-data">
                                <div class="review-voice__unit-foot-data-label">診察日</div>
                                <div class="review-voice__unit-foot-data-cont">2021年5月2日</div>
                              </div>
                            </div>
                            <div class="review-voice__unit-foot-time">2021年5月17日</div>
                          </div>
                        </div>
                        <!--/.review-voice__unit-->

                        <div class="review-voice__unit">
                          <!--.review-voice__unit-->
                          <div class="review-voice__unit-head">
                            <div class="review-voice__unit-name">
                              ニックネーム
                            </div>
                            <div class="review-voice__unit-stars-wrap">
                              <div class="review-voice__unit-stars">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-white.svg" alt="">
                                <img src="/assets/images/search/icon-start-white.svg" alt="">
                              </div>
                              <div class="review-voice__unit-rating-number">3.2</div>
                            </div>
                          </div>
                          <div class="review-voice__unit-cont">
                            <p>知人からの評判通り、院長先生が丁寧に診察してくださいました。
                              初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。
                              受付の方の対応も良かったですよ。</p>
                          </div>
                          <div class="review-voice__unit-foot">
                            <div class="review-voice__unit-foot-data-wrap">
                              <div class="review-voice__unit-foot-data">
                                <div class="review-voice__unit-foot-data-label">種類</div>
                                <div class="review-voice__unit-foot-data-cont">犬 | チワワ</div>
                              </div>
                              <div class="review-voice__unit-foot-data">
                                <div class="review-voice__unit-foot-data-label">診察日</div>
                                <div class="review-voice__unit-foot-data-cont">2021年5月2日</div>
                              </div>
                            </div>
                            <div class="review-voice__unit-foot-time">2021年5月17日</div>
                          </div>
                        </div>
                        <!--/.review-voice__unit-->

                        <div class="review-voice__unit">
                          <!--.review-voice__unit-->
                          <div class="review-voice__unit-head">
                            <div class="review-voice__unit-name">
                              ニックネーム
                            </div>
                            <div class="review-voice__unit-stars-wrap">
                              <div class="review-voice__unit-stars">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-white.svg" alt="">
                                <img src="/assets/images/search/icon-start-white.svg" alt="">
                              </div>
                              <div class="review-voice__unit-rating-number">3.2</div>
                            </div>
                          </div>
                          <div class="review-voice__unit-cont">
                            <p>知人からの評判通り、院長先生が丁寧に診察してくださいました。
                              初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。
                              受付の方の対応も良かったですよ。</p>
                          </div>
                          <div class="review-voice__unit-foot">
                            <div class="review-voice__unit-foot-data-wrap">
                              <div class="review-voice__unit-foot-data">
                                <div class="review-voice__unit-foot-data-label">種類</div>
                                <div class="review-voice__unit-foot-data-cont">犬 | チワワ</div>
                              </div>
                              <div class="review-voice__unit-foot-data">
                                <div class="review-voice__unit-foot-data-label">診察日</div>
                                <div class="review-voice__unit-foot-data-cont">2021年5月2日</div>
                              </div>
                            </div>
                            <div class="review-voice__unit-foot-time">2021年5月17日</div>
                          </div>
                        </div>
                        <!--/.review-voice__unit-->

                        <div class="review-voice__unit">
                          <!--.review-voice__unit-->
                          <div class="review-voice__unit-head">
                            <div class="review-voice__unit-name">
                              ニックネーム
                            </div>
                            <div class="review-voice__unit-stars-wrap">
                              <div class="review-voice__unit-stars">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-yellow.svg" alt="">
                                <img src="/assets/images/search/icon-start-white.svg" alt="">
                                <img src="/assets/images/search/icon-start-white.svg" alt="">
                              </div>
                              <div class="review-voice__unit-rating-number">3.2</div>
                            </div>
                          </div>
                          <div class="review-voice__unit-cont">
                            <p>知人からの評判通り、院長先生が丁寧に診察してくださいました。
                              初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。
                              受付の方の対応も良かったですよ。</p>
                          </div>
                          <div class="review-voice__unit-foot">
                            <div class="review-voice__unit-foot-data-wrap">
                              <div class="review-voice__unit-foot-data">
                                <div class="review-voice__unit-foot-data-label">種類</div>
                                <div class="review-voice__unit-foot-data-cont">犬 | チワワ</div>
                              </div>
                              <div class="review-voice__unit-foot-data">
                                <div class="review-voice__unit-foot-data-label">診察日</div>
                                <div class="review-voice__unit-foot-data-cont">2021年5月2日</div>
                              </div>
                            </div>
                            <div class="review-voice__unit-foot-time">2021年5月17日</div>
                          </div>
                        </div>
                        <!--/.review-voice__unit-->

                        <div class="review-voice__more">
                          <a href="" class="link">
                            口コミをもっと見る
                          </a>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div class="p-hospital__bottom">

            <div class="point-area">
              <p class="point-ttl">おすすめポイント</p>
              <ul class="point-list">
                <li>
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-point1.svg" alt="">
                  </div>
                  <div class="text">
                    下北沢駅から徒歩2分。専用駐車場と提携有料駐車場もあり
                  </div>
                </li>
                <li>
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-point2.svg" alt="">
                  </div>
                  <div class="text">
                    痛みや辛さを軽減するためのコツを活かした治療が受けられる
                  </div>
                </li>
                <li>
                  <div class="icon">
                    <img src="/assets/images/search/hospital/icon-point3.svg" alt="">
                  </div>
                  <div class="text">
                    病院が苦手にならないよう、初診日こそ気を遣ってくれる
                  </div>
                </li>
              </ul>
            </div>

            <div class="reserve-area">
              <div class="reserve-area__inner">
                <div class="reserve-text">
                  <div class="reserve-text__ttl">ご予約はガラパゴスで</div>
                  <div class="reserve-text__cont">予約で貯まったポイントは慈善団体へ寄付できます。</div>
                </div>
                <div class="reserve-btn">
                  <a href="">
                    この施設を予約する
                  </a>
                </div>
                <div class="reserve-illust">
                  <img src="/assets/images/search/hospital/point-illust.svg" alt="">
                </div>
              </div>
            </div>

          </div>

        </div>



        <div class="nearyou-area">
          <div class="nearyou-area__head">
            <p class="head-ttl">お近くの施設</p>
            <p class="head-link">
              <a href="" class="link">一覧を見る</a>
            </p>
          </div>
          <ul class="nearyou-list">
            <li class="nearyou-list__item">
              <a href="">
                <div class="item-img"><img src="/assets/images/search/hospital/nearyou-1.jpg" alt=""></div>
                <div class="item-text">
                  <p class="item-ttl">BiBi犬猫病院</p>
                  <div class="item-address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="item-access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
              </a>
            </li>
            <li class="nearyou-list__item">
              <a href="">
                <div class="item-img"><img src="/assets/images/search/hospital/nearyou-2.jpg" alt=""></div>
                <div class="item-text">
                  <p class="item-ttl">ペットカンパニーパイン</p>
                  <div class="item-address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="item-access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
              </a>
            </li>
            <li class="nearyou-list__item">
              <a href="">
                <div class="item-img"><img src="/assets/images/search/hospital/nearyou-3.jpg" alt=""></div>
                <div class="item-text">
                  <p class="item-ttl">アマノ動物病院</p>
                  <div class="item-address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="item-access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
              </a>
            </li>
            <li class="nearyou-list__item">
              <a href="">
                <div class="item-img"><img src="/assets/images/search/hospital/nearyou-4.jpg" alt=""></div>
                <div class="item-text">
                  <p class="item-ttl">BiBi犬猫病院</p>
                  <div class="item-address">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-map.svg" alt="">
                    </div>東京都世田谷区
                  </div>
                  <div class="item-access">
                    <div class="icon">
                      <img src="/assets/images/search/hospital/icon-train.svg" alt="">
                    </div>京王線 笹塚駅
                  </div>
                </div>
              </a>
            </li>
          </ul>
        </div>

      </div><!-- /.container3 -->

    </div><!-- /.p-mypage -->
  </main>
  <!__ ./main __>

    <!__ include footer __>

      <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/footer4.php'; ?>