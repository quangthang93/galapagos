<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/pre/">トップページ</a></li>
      <li>運営会社</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">運営会社</h2>
      </div>
      <div class="p-operating">
        <table>
          <tbody>
            <tr>
              <th>会社名</th>
              <td>株式会社Emyu</td>
            </tr>
            <tr>
              <th>所在地</th>
              <td>東京都国立市東１丁目２番地の１１-２階</td>
            </tr>
            <tr>
              <th>設立日</th>
              <td>2021年4月1日</td>
              .
              
            </tr>
            <tr>
              <th>資本金</th>
              <td>500万円</td>
            </tr>
            <tr>
              <th>役員</th>
              <td>代表取締役/獣医師 安藤美恵</td>
            </tr>
          </tbody>
        </table>
        <div class="align-center">
          <a href="" class="btn-blue2">トップページへ</a>
        </div>
      </div>
    </div>
  </div>
  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>