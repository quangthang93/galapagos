<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php
'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/pre/">トップページ</a></li>
      <li><a href="/publish-facility/">掲載ご希望の慈善団体様へ</a></li>
      <li>掲載のお申込み</li>
    </ul>
  </div>

  <div class="container3">
    <div class="p-mypage--ttlWrap">
      <h2 class="p-mypage--ttl">掲載のお申込み</h2>
    </div>

    <div class="p-publish-form">
      <div class="p-publish-form__content">

        <div class="p-publish-form__first">
          <p>入力項目をご確認いただき、送信ボタンを押してください。</p>
        </div>

        <form class="form">
          <div class="form-row">
            <div class="form-label">団体名</div>
            <div class="form-row--cnt">
              <p class="desc4">ガラパゴス保護団体</p>
            </div>
          </div>
          <div class="form-row2">
            <div class="form-label">郵便番号</div>
            <div class="form-row--cnt">
              <p class="desc4">1001000</p>
            </div>
          </div>
          <div class="form-row2">
            <div class="form-label">都道府県</div>
            <div class="form-row--cnt">
              <p class="desc4">東京都</p>
            </div>
          </div>
          <div class="form-row2">
            <div class="form-label">市区町村</div>
            <div class="form-row--cnt">
              <p class="desc4">港区北青山</p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">以降</div>
            <div class="form-row--cnt">
              <p class="desc4">3-3-5東京建物青山ビル4F</p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">代表者名</div>
            <div class="form-row--cnt">
              <p class="desc4">山田 太郎</p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">代表電話番号</div>
            <div class="form-row--cnt">
              <p class="desc4">0310001000</p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">代表メールアドレス</div>
            <div class="form-row--cnt">
              <p class="desc4">example@xxxxxx.co.jp</p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">担当者名</div>
            <div class="form-row--cnt">
              <p class="desc4">伊藤 翔</p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">担当電話番号</div>
            <div class="form-row--cnt">
              <p class="desc4">0310001000</p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">担当メールアドレス</div>
            <div class="form-row--cnt">
              <p class="desc4">example@xxxxxx.co.jp</p>
            </div>
          </div>
          <div class="form-ctrl type2">
            <div class="form-ctrl--item btn-submitWrap">
              <a href="" class="btn-white4 align-center">戻る</a>
            </div>
            <div class="form-ctrl--item btn-submitWrap">
              <a href="" class="btn-submit js-btnSave">送信する</a>
            </div>
          </div>
        </form>
      </div><!-- ./contact-content -->
    </div>
  </div>

  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>