<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php'; ?>
<main class="main">

  <div class="p-publish p-publish--charity">
    <section class="p-publish__sec01">  
      <div class="breadcrumb">
        <ul>
          <li><a href="/pre/">トップページ</a></li>
          <li>掲載ご希望の慈善団体様へ</li>
        </ul>
      </div>
      
      <div class="container3">
        <div class="p-mypage--ttlWrap">
          <h2 class="p-mypage--ttl">掲載ご希望の慈善団体様へ</h2>
        </div>

        <div class="try">
          <h3>掲載のお申込み
            <span>
              <img src="<?php echo $PATH;?>/assets/images/publish/title-try.svg" alt="Try!">
            </span>
          </h3>
          <a href="" class="btn-blue6">お申込み希望の方はコチラ</a>
        </div>

        <div class="wish">
          <div class="wish__title">
            <h3><img src="<?php echo $PATH;?>/assets/images/publish/title-wish.svg" alt=""></h3>
            <span><img src="<?php echo $PATH;?>/assets/images/publish/bg-line-03.svg" alt=""></span>
            <p>という想いから作ったサイトです。</p>
          </div>

          <p class="wish__txt1">飼い主さんと獣医・トリマー等を<br>繋ぐサービスをメインに行っています。<br>ただ動物達の為に尽力されている<br>慈善団体の方の負担が大きくなっている現実を受け止め、<br>少しでも多くの方の<br>応援を形にする為に、<br>ポイントギフト制度を作りました。</p>
          <p class="wish__txt2">幸せな気持ちのお裾分けが<br>積み重なることで、<br>新たな幸せの連鎖が生まれることを<br>願っております。</p>
          <img src="<?php echo $PATH;?>/assets/images/publish/illust-heart-small.png" alt="">
          <div class="wish__img">
            <img src="<?php echo $PATH;?>/assets/images/publish/illust-02.png" alt="">
          </div>
        </div>
      </div>
    </section>

    <section class="p-publish__sec02">
      <div class="container3">

        <div class="point">
          <div class="point__title">
            <h3>サービスの特徴</h3>
            <picture>
              <source srcset="<?php echo $PATH;?>/assets/images/publish/bg-line-04.svg" media="(min-width: 1051px)">
              <img src="<?php echo $PATH;?>/assets/images/publish/bg-line-06.png" alt="">
            </picture>
          </div>
          <ul class="point__list">
            <li>
              <div class="point__heart">POINT<span>1</span></div>
              <img src="<?php echo $PATH;?>/assets/images/publish/icon-free.svg" alt="">
              <h4>利用料不要</h4>
              <p>慈善団体の掲載やポイント交換等において、一切費用はいただきません。</p>
            </li>

            <li>
              <div class="point__heart">POINT<span>2</span></div>
              <img src="<?php echo $PATH;?>/assets/images/publish/icon-inkind.svg" alt="">
              <h4>現物給付</h4>
              <p>会員の方からポイントギフトが一定額貯まった時点で、商品へ交換しギフトいたします。現金での寄付は行っておりませんので、ご希望の交換商品をご選択ください。</p>
            </li>

            <li>
              <div class="point__heart">POINT<span>3</span></div>
              <img src="<?php echo $PATH;?>/assets/images/publish/icon-smartphone.svg" alt="">
              <h4>応援が続く</h4>
              <p>商品到着後は、ポイントギフトをしてくださった方々へ、使用の報告を行っていただきます。</p>
            </li>
          </ul>

          <div class="point__title">
            <h3>掲載までの流れ</h3>
            <picture>
              <source srcset="<?php echo $PATH;?>/assets/images/publish/bg-line-04.svg" media="(min-width: 1051px)">
              <img src="<?php echo $PATH;?>/assets/images/publish/bg-line-06.png" alt="">
            </picture>
          </div>

          <ul class="point__step">
            <li>
              <span class="point__num">
                <img src="<?php echo $PATH;?>/assets/images/publish/step-01.svg" alt="">
              </span>
              <h4>掲載のお申込み</h4>
              <p>このページで申し込みをいただいた後、活動内容・実績・掲載中の他の団体・社内規定などを加味し、掲載の可否を決定いたします。</p>
            </li>

            <li>
              <span class="point__num">
                <img src="<?php echo $PATH;?>/assets/images/publish/step-02.svg" alt="">
              </span>
              <h4>公開準備</h4>
              <p>審査後は、サイトに公開する原稿やギフト希望品などをお伺いします。 準備が整い次第公開となります。</p>
            </li>

            <li>
              <span class="point__num">
                <img src="<?php echo $PATH;?>/assets/images/publish/step-03.svg" alt="">
              </span>
              <h4>ポイントが貯まったら、指定の希望品をギフト</h4>
              <p>希望品の変更があれば、再度ご指定いただき、新たな募集がスタートします。</p>
            </li>
          </ul>
        </div>
      </div>
    </section>

    <section class="p-publish__sec03">
      <div class="container3">
        <div class="try">
          <h3>掲載のお申込み
            <span>
              <img src="<?php echo $PATH;?>/assets/images/publish/title-try.svg" alt="Try!">
            </span>
          </h3>
          <a href="" class="btn-blue6">お申込み希望の方はコチラ</a>
        </div>
        <div class="info">
          <div class="info__mail">
            <h4>掲載について</h4>
            <a href="">お問い合わせ</a>
          </div>
          <div class="info__tel">
            <h4>お電話でのお問い合わせはコチラ</h4>
            <a href="tel:042-508-2999"><img src="<?php echo $PATH;?>/assets/images/publish/txt-tel.svg" alt=""></a>
            <p>受付時間：平日9:00-18:00</p>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>