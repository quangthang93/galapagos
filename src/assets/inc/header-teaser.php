<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Title</title>
  <meta name="description" content="">
  <meta property="og:site_name" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:image" content="">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
</head>

<body>
  <div class="wrapper">
    <header class="header-teaser fadedown">
      <h1 class="header-logo">
        <a class="logo link" href="/">
          <img src="<?php echo $PATH;?>/assets/images/teaser/logo.svg" alt="">
        </a>
      </h1>
      <!-- <div class="header-menu js-menu">
        <ul>
          <li><a class="link" href="/works">制作実績</a></li>
          <li><a class="link" href="/service">サービス</a></li>
          <li><a class="link" href="/news">新着プレスリリース</a></li>
          <li><a class="link" href="/faq">よくあるご質問</a></li>
        </ul>
        <div class="header-logoNav">
          <a class="logo01" href="/"><img src="<?php echo $PATH;?>/assets/images/common/logo01.svg" alt=""></a>
          <a class="logo02" href="/"><img src="<?php echo $PATH;?>/assets/images/common/logo02.svg" alt=""></a>
        </div>
        <div class="header-menu--contactWrap js-headerContactWrap">
          <a href="/contact" class="header-menu--contact js-headerContact"><span>Contact</span></a>
          <div class="spin-containerWrap">
            <div class="spin-container">
              <div class="shape">
                <div class="bd"></div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <!-- <a href="/login" class="header-login btn-yellow pc-only">ログイン・新規登録</a> -->
      <!-- <a class="header-ctrl js-menuTrigger" href="javascript:void(0);">
        <img src="<?php echo $PATH;?>/assets/images/common/menu-icon.svg" alt="">
      </a> -->
    </header><!-- ./header -->