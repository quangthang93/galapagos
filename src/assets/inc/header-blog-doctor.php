<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Title</title>
  <meta name="description" content="">
  <meta property="og:site_name" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:image" content="">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
</head>

<body>
  <div class="wrapper">
    <header class="header type2 blog js-header fadedown">
      <h1 class="header-logo">
        <a class="logo2 link js-logo" href="/">
          <img src="<?php echo $PATH;?>/assets/images/blog/logo-blog-doctor.png" alt="">
        </a>
      </h1>

      <div class="p-blog--navCtrl js-blogNavCtrl sp-only2">
        <span></span>
        <span></span>
        <span></span>
      </div>

      <div class="header-nav2 js-blogNav">
        <div class="header-nav2--inner">
          <div class="header-nav2--item">
            <a class="header-nav2--item-link link active" href="">カテゴリ1</a>
          </div>
          <div class="header-nav2--item">
            <a class="header-nav2--item-link link doc" href="">カテゴリ2</a>
          </div>
          <div class="header-nav2--item">
            <a class="header-nav2--item-link link doc" href="">カテゴリ3</a>
          </div>
          <div class="header-nav2--item">
            <a class="header-nav2--item-link link doc" href="">カテゴリ4</a>
          </div>
        </div>
      </div>
    </header><!-- ./header -->