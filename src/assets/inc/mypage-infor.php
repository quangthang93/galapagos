<div class="p-mypage--gift">
  <div class="p-mypage--gift-left">
    <div class="p-mypage--gift-name borderR">長谷川アーリアジャスール<span> さん</span></div>
    <div class="p-mypage--gift-points borderR">
      <p class="p-mypage--gift-points-label">現在の会員ポイント</p>
      <p class="p-mypage--gift-points-value">120 <span>P</span></p>
    </div>
  </div>
  <div class="p-mypage--gift-right">
    <div class="p-mypage--gift-msg">貯まった会員ポイントは寄贈品に還元し<br> 慈善団体へ寄付できます。</div>
    <a href="" class="btn-yellow3">ギフトに還元する</a>
  </div>
</div>