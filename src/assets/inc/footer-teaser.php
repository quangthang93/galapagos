    <footer class="footer footer-teaser">
      
      <div class="footer-logo">
        <p class="footer-logo--ttl">動物を愛するすべての人がつながる情報サイト</p>
        <div class="footer-logo--imgT">
          <a href="" class="link">
            <!-- <img class="cover" src="<?php echo $PATH;?>/assets/images/common/f-logo.svg" alt=""> -->
            <img class="cover" src="<?php echo $PATH;?>/assets/images/teaser/logo.svg" alt="">
          </a>
        </div>
      </div>

      <!--<ul class="footer-direct type2 type3">
        <li>
          <a href="" class="link">施設・獣医師をさがす</a>
        </li>
      </ul>

      <ul class="footer-direct2">
        <li>
          <a href="" class="link">コラム</a>
        </li>
        <li>
          <a href="" class="link">ポイントギフト</a>
        </li>
        <li>
          <a href="" class="link">仲間とつながる</a>
        </li>
      </ul> -->

      <ul class="footer-sns footer-sns--teaser">
        <li>
          <a href="" class="link">
            <img class="insta cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.svg" alt="">
          </a>
        </li>
        <li>
          <a href="" class="link">
            <img class="tw cover" src="<?php echo $PATH;?>/assets/images/common/icon-twitter.svg" alt="">
          </a>
        </li>
        <li>
          <a href="" class="link">
            <img class="fb cover" src="<?php echo $PATH;?>/assets/images/common/icon-fb.svg" alt="">
          </a>
        </li>
      </ul>

      <!-- <nav class="footer-nav">
        <ul class="footer-nav--list">
          <li>
            <a href="" class="link2">ログイン・会員登録</a>
          </li>
          <li>
            <a href="" class="link2">掲載ご希望の施設様へ</a>
          </li>
          <li>
            <a href="" class="link2">お知らせ</a>
          </li>
          <li>
            <a href="" class="link2">ご利用ガイド</a>
          </li>
          <li>
            <a href="" class="link2">運営会社</a>
          </li>
          <li>
            <a href="" class="link2">会員規約</a>
          </li>
          <li>
            <a href="" class="link2">プライバシーポリシー</a>
          </li>
          <li>
            <a href="" class="link2">お問い合わせ</a>
          </li>
        </ul>
      </nav> -->

      <div class="footer-copy">
        <p>© Galapagos For Family</p>
      </div>


    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/parallax.js/1.4.2/parallax.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/jquery-ui.min.js"></script> -->
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/datepicker-ja.js"></script> -->
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/remodal.min.js"></script> -->
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script> -->
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/gsap.min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/top.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/countUp.js"></script>

</body>

</html>