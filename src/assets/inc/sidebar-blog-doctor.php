<div class="widget_search">
  <form method="get" class="search-form" action="">
    <input type="search" class="search-field" placeholder="記事を検索" value="" name="s">
    <button type="submit" class="search-submit link type2"></button>
  </form>
</div>
<div class="widget">
  <h5 class="widget-title type2">LATEST ARTICLE</h5>
  <ul class="wpp-list">
    <li class="wpp-card">
      <a href="" class="link wpp-card-inner">
        <div class="wpp-thumb">
          <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/article01.png" alt="">
        </div>
        <div class="wpp-item-data">
          <p class="wpp-post-title type2">この文章はダミーです。この文章はダミーです。</p>
          <div class="mt-15">
            <time class="post-date">2020.01.23</time>
            <span class="post-cmt">コメント3</span>
          </div>
        </div>
      </a>
    </li>
    <li class="wpp-card">
      <a href="" class="link wpp-card-inner">
        <div class="wpp-thumb">
          <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/article02.png" alt="">
        </div>
        <div class="wpp-item-data">
          <p class="wpp-post-title type2">この文章はダミーです。この文章はダミーです。</p>
          <div class="mt-15">
            <time class="post-date">2020.01.23</time>
            <span class="post-cmt">コメント3</span>
          </div>
        </div>
      </a>
    </li>
    <li class="wpp-card">
      <a href="" class="link wpp-card-inner">
        <div class="wpp-thumb">
          <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/article03.png" alt="">
        </div>
        <div class="wpp-item-data">
          <p class="wpp-post-title type2">この文章はダミーです。この文章はダミーです。</p>
          <div class="mt-15">
            <time class="post-date">2020.01.23</time>
            <span class="post-cmt">コメント3</span>
          </div>
        </div>
      </a>
    </li>
  </ul>
</div>
<div class="widget">
  <h5 class="widget-title type2">RECOMMEND</h5>
  <ul class="wpp-list">
    <li class="wpp-card">
      <a href="" class="link wpp-card-inner">
        <div class="wpp-thumb">
          <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/article04.png" alt="">
        </div>
        <div class="wpp-item-data">
          <p class="wpp-post-title type2">この文章はダミーです。この文章はダミーです。</p>
          <div class="mt-15">
            <time class="post-date">2020.01.23</time>
            <span class="post-cmt">コメント3</span>
          </div>
        </div>
      </a>
    </li>
    <li class="wpp-card">
      <a href="" class="link wpp-card-inner">
        <div class="wpp-thumb">
          <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/article05.png" alt="">
        </div>
        <div class="wpp-item-data">
          <p class="wpp-post-title type2">この文章はダミーです。この文章はダミーです。</p>
          <div class="mt-15">
            <time class="post-date">2020.01.23</time>
            <span class="post-cmt">コメント3</span>
          </div>
        </div>
      </a>
    </li>
    <li class="wpp-card">
      <a href="" class="link wpp-card-inner">
        <div class="wpp-thumb">
          <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/article06.png" alt="">
        </div>
        <div class="wpp-item-data">
          <p class="wpp-post-title type2">この文章はダミーです。この文章はダミーです。</p>
          <div class="mt-15">
            <time class="post-date">2020.01.23</time>
            <span class="post-cmt">コメント3</span>
          </div>
        </div>
      </a>
    </li>
  </ul>
</div>
<div class="widget">
  <h5 class="widget-title type2">CATEGORY</h5>
  <ul>
    <li class="cat-item">
      <a href="" class="cat-item--ttl type2 link">今月の1枚</a>
      <span class="cat-item--count">(15)</span>
    </li>
    <li class="cat-item">
      <a href="" class="cat-item--ttl type2 link">カテゴリ1</a>
      <span class="cat-item--count">(15)</span>
    </li>
    <li class="cat-item">
      <a href="" class="cat-item--ttl type2 link">カテゴリ2</a>
      <span class="cat-item--count">(15)</span>
    </li>
    <li class="cat-item">
      <a href="" class="cat-item--ttl type2 link">カテゴリ3</a>
      <span class="cat-item--count">(15)</span>
    </li>
    <li class="cat-item">
      <a href="" class="cat-item--ttl type2 link">カテゴリ4</a>
      <span class="cat-item--count">(15)</span>
    </li>
    <li class="cat-item">
      <a href="" class="cat-item--ttl type2 link">カテゴリ5</a>
      <span class="cat-item--count">(15)</span>
    </li>
  </ul>
</div>
<div class="widget">
  <h5 class="widget-title type2">#TAG</h5>
  <div>
    <a href="" class="tag5 link"># 動物</a>
    <a href="" class="tag5 link"># 犬</a>
    <a href="" class="tag5 link"># 猫</a>
    <a href="" class="tag5 link"># ペット</a>
    <a href="" class="tag5 link"># 撮影</a>
    <a href="" class="tag5 link"># 悩み</a>
    <a href="" class="tag5 link"># ハムスター</a>
    <a href="" class="tag5 link"># うさぎ</a>
    <a href="" class="tag5 link"># お手入れ</a>
    <a href="" class="tag5 link"># ペットラン</a>
  </div>
</div>