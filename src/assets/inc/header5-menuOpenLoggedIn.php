<?php include_once $_SERVER['DOCUMENT_ROOT'] . '/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Title</title>
    <meta name="description" content="">
    <meta property="og:site_name" content="">
    <meta property="og:title" content="">
    <meta property="og:description" content="">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:site" content="">
    <meta name="twitter:image" content="">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $PATH; ?>/assets/css/index.css">
</head>

<body class="body-menuOpen">
    <div class="wrapper">
        <header class="header type2 js-header l-header fadedown">
            <h1 class="header-logo">
                <a class="logo link js-logo" href="/">
                    <img src="<?php echo $PATH; ?>/assets/images/common/f-logo.svg" alt="">
                </a>
            </h1>

            <a class="header-search btn-green" href="">探す・相談する</a>

            <a href="/login" class="header-login btn-yellow pc-only">ログイン・新規登録</a>
            <a class="header-ctrl js-menuTrigger" href="javascript:void(0);">
                <img src="<?php echo $PATH; ?>/assets/images/common/menu-icon.svg" alt="">
            </a>
        </header><!-- ./header -->


        <div class="js-menu">

            <div class="js-menu__inner">
                <a class="header-ctrl js-menuTrigger" href="javascript:void(0);">
                    <img src="<?php echo $PATH; ?>/assets/images/common/icon-close-2.svg" alt="">
                </a>
                <div class="member-name logged-in">
                    <span class="name">長谷川アーリアジャスール</span>さん
                </div>
                <div class="purpose">
                    <div>
                        <a href="" class="link">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="14" viewBox="0 0 16 14">
                                    <g id="グループ_12" data-name="グループ 12" transform="translate(-893.558 -798.251)">
                                        <path id="パス_63" data-name="パス 63" d="M906.887,812.251H896.228a2.7,2.7,0,0,1-2.67-2.708V800.9a2.666,2.666,0,0,1,2.638-2.648H906.9a2.67,2.67,0,0,1,2.656,2.668v8.623A2.7,2.7,0,0,1,906.887,812.251Zm-10.657-1.761h10.656a.946.946,0,0,0,.938-.95v-8.616a.916.916,0,0,0-.909-.913H896.23a.925.925,0,0,0-.939.9v8.627a.946.946,0,0,0,.937.948Z" fill="#818181" />
                                        <path id="パス_60" data-name="パス 60" d="M909.263,809.854h-2.08v2.1H904.8v-2.1h-2.08v-2.4h2.08v-2.1h2.378v2.1h2.08Z" transform="translate(-4.436 -3.401)" fill="#818181" />
                                    </g>
                                </svg>
                            </div>
                            <div class="text">施設を探す</div>
                        </a>
                    </div>
                    <div>
                        <a href="" class="link">
                            <div class="icon">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14">
                                    <path id="Icon_awesome-stethoscope" data-name="Icon awesome-stethoscope" d="M12.225,3.064a1.75,1.75,0,0,0-.848,3.265V9.408a2.963,2.963,0,0,1-3.062,2.844,2.977,2.977,0,0,1-3.06-2.713A4.387,4.387,0,0,0,8.75,5.251V1A.656.656,0,0,0,8.222.359L6.5.015a.655.655,0,0,0-.771.514L5.644.958a.655.655,0,0,0,.514.771L7,1.9v3.32a2.625,2.625,0,1,1-5.25.033V1.9l.839-.167A.655.655,0,0,0,3.1.961L3.016.532A.655.655,0,0,0,2.245.018L.528.357A.659.659,0,0,0,0,1V5.251A4.382,4.382,0,0,0,3.5,9.539,4.718,4.718,0,0,0,8.313,14a4.714,4.714,0,0,0,4.813-4.594V6.329a1.75,1.75,0,0,0-.9-3.265Zm.025,2.188a.438.438,0,1,1,.438-.438A.439.439,0,0,1,12.25,5.251Z" transform="translate(0 -0.002)" fill="#818181" />
                                </svg>
                            </div>
                            <div class="text">専門家へ相談</div>
                        </a>
                    </div>
                </div>
                <ul class="js-menu__list">
                    <li>
                        <a href="" class="link">ポイントギフト団体を探す</a>
                    </li>
                    <li>
                        <a href="" class="link">コラム</a>
                    </li>
                    <li>
                        <a href="" class="link">写真投稿</a>
                    </li>
                    <li>
                        <a href="" class="link">よくあるご質問</a>
                    </li>
                    <li>
                        <a href="" class="link">お知らせ</a>
                    </li>
                    <li>
                        <a href="" class="link">お問い合わせ</a>
                    </li>
                </ul>
                <div class="js-menu__bottom">
                    <div class="mypage">
                        <a href="" class="link">マイページへ</a>
                    </div>
                </div>
            </div>
        </div>