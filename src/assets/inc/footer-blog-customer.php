    <footer class="footer">
      
      <div class="footer-logo">
        <p class="footer-logo--ttl">動物を愛するすべての人がつながる情報サイト</p>
        <div class="footer-logo--img2">
          <a href="/pre/" class="link">
            <img class="cover" src="<?php echo $PATH;?>/assets/images/blog/f-logo.png" alt=""></a>
        </div>
      </div>

      <ul class="footer-direct2">
        <li>
          <a href="/publish-facility/" class="link">掲載ご希望の施設様へ</a>
        </li>
        <li>
          <a href="/publish-charity/" class="link">掲載ご希望の慈善団体様へ</a>
        </li>
        <li>
          <a href="/blog-customer/" class="link">コラム</a>
        </li>
      </ul>

      <ul class="footer-sns">
        <li>
          <a href="" class="link">
            <img class="insta cover" src="<?php echo $PATH;?>/assets/images/common/icon-insta.svg" alt="">
          </a>
        </li>
        <li>
          <a href="" class="link">
            <img class="fb cover" src="<?php echo $PATH;?>/assets/images/common/icon-fb.svg" alt="">
          </a>
        </li>
      </ul>

      <nav class="footer-nav">
        <ul class="footer-nav--list">
          <li>
            <a href="/news/" class="link2">お知らせ</a>
          </li>
          <li>
            <a href="/operating/" class="link2">運営会社</a>
          </li>
          <li>
            <a href="/agreement/" class="link2">会員規約</a>
          </li>
          <li>
            <a href="/privacy-policy/" class="link2">プライバシーポリシー</a>
          </li>
          <li>
            <a href="/contact/" class="link2">お問い合わせ</a>
          </li>
        </ul>
      </nav>

      <div class="footer-copy">
        <p>© Galapagos For Family</p>
      </div>


    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/parallax.js/1.4.2/parallax.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-ui.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/datepicker-ja.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/remodal.min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/gsap.min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/mypage.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/blog.js"></script>

</body>

</html>