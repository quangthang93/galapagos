		<footer class="footer type2">
      <div class="footer-copy">
        <p>© Galapagos For Family</p>
      </div>


    </footer><!-- ./footer -->
  </div>
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery-3.5.1.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"></script>
  <script src="https://cdn.jsdelivr.net/parallax.js/1.4.2/parallax.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/ofi.min.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/libs/slick.min.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/jquery-ui.min.js"></script> -->
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/datepicker-ja.js"></script> -->
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/remodal.min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/libs/jquery.matchHeight-min.js"></script>
  <!-- <script src="<?php echo $PATH;?>/assets/js/libs/gsap.min.js"></script> -->
  <script src="<?php echo $PATH;?>/assets/js/common.js"></script>
  <script src="<?php echo $PATH;?>/assets/js/mypage.js"></script>

</body>

</html>