<div class="sidebar-search">
  <div class="sidebar-search--head">
    <div class="sidebar-search--img">
      <img src="<?php echo $PATH;?>/assets/images/search/doctor.jpg" alt="">
    </div>
    <div class="sidebar-search--ttl ttl-bold7">soup*spoon</div>
    <div class="sidebar-search--ratingWrap">
      <div class="sidebar-search--rating mgr-10">
        <div class="sidebar-search--rating-stars mgr-5">
          <img src="<?php echo $PATH;?>/assets/images/search/icon-start-yellow.svg" alt="">
          <img src="<?php echo $PATH;?>/assets/images/search/icon-start-yellow.svg" alt="">
          <img src="<?php echo $PATH;?>/assets/images/search/icon-start-yellow.svg" alt="">
          <img src="<?php echo $PATH;?>/assets/images/search/icon-start-yellow.svg" alt="">
          <img src="<?php echo $PATH;?>/assets/images/search/icon-start-white.svg" alt="">
        </div>
        <div class="sidebar-search--rating-number ttl-blue2">3.5</div>
      </div>
      <a href="" class="sidebar-search--rating-cmt ttl-blue2">
        100
        <!-- <img class="mgr-5" src="<?php echo $PATH;?>/assets/images/search/icon-msg.svg" alt=""> -->
      </a>
    </div>
    <div class="sidebar-search--tags">
      <span class="label-green2">猫に優しい</span>
    </div>
  </div>
  <div class="form">
    <div class="form-row">
      <div class="form-label">対応動物種</div>
      <div class="form-row--cnt">
        <span class="label-search">犬</span>
        <span class="label-search green">猫</span>
        <span class="label-search pink">フェレット</span>
        <span class="label-search green2">鳥類</span>
        <span class="label-search blue">両生類</span>
        <span class="label-search gray">その他</span>
        <span class="label-search blue2">うさぎ</span>
        <span class="label-search pink2">爬虫類</span>
      </div>
    </div>
    <div class="form-row">
      <div class="form-label">サービス内容</div>
      <div class="form-row--cnt">
        <span class="label-search2">カット</span>
        <span class="label-search2">シャンプー</span>
        <span class="label-search2">ブラッシング</span>
        <span class="label-search2">エクステ</span>
        <span class="label-search2">ヘアカラー</span>
        <span class="label-search2">ハーブパック</span>
        <span class="label-search2">耳掃除</span>
        <span class="label-search2">爪切り</span>
        <span class="label-search2">歯磨き</span>
        <span class="label-search2">肛門絞り</span>
        <span class="label-search2">肛門バリカン</span>
        <span class="label-search2">毛玉取り</span>
        <span class="label-search2">エステ</span>
        <span class="label-search2">アロマ</span>
        <span class="label-search2">入浴</span>
      </div>
    </div>
    <div class="form-row">
      <div class="form-label">営業時間</div>
      <div class="form-row--cnt">
      	<p class="desc4">平日8:30～21:00/土日祝9:30～19:00</p>
      </div>
    </div>
    <div class="form-row">
      <div class="form-label">定休日</div>
      <div class="form-row--cnt">
      	<p class="desc4">水曜日</p>
      </div>
    </div>
    <div class="form-row">
      <div class="form-label">公式HP</div>
      <div class="form-row--cnt">
      	<a href="https://garapagos.jp/" class="desc4 link">https://garapagos.jp/</a>
      </div>
    </div>
    <div class="form-row">
      <div class="form-label">SNS</div>
      <div class="form-row--cnt">
      	<a href="" class="link-blue mgr-10" target="_blank">Instagram</a>
      	<a href="" class="link-blue mgr-10" target="_blank">Facebook</a>
      	<a href="" class="link-blue" target="_blank">Twitter</a>
      </div>
    </div>
    <div class="form-row">
      <div class="form-label">電話番号</div>
      <div class="form-row--cnt">
      	<a href="tel:03-1234-5678" class="link desc4">03-1234-5678</a>
      </div>
    </div>
    <div class="form-row">
      <div class="form-label">アクセス</div>
      <div class="form-row--cnt">
      	<p class="desc4">東京都世田谷区北沢2-28-8 下北沢駅北口から出て、鎌倉通りを北へ進みます。徒歩7分で着きます。</p>
      </div>
    </div>
  </div>
  <div class="sidebar-search--map">
  	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d4584.31956941592!2d139.66514826847688!3d35.66214045402141!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6018f36b57fe1dcb%3A0x356726b5bb7f0c39!2z44CSMTU1LTAwMzEg5p2x5Lqs6YO95LiW55Sw6LC35Yy65YyX5rKi77yS5LiB55uu77yS77yY4oiS77yY!5e0!3m2!1sja!2sjp!4v1632731265336!5m2!1sja!2sjp" width="100%" height="200" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
  </div>
  <div class="sidebar-search--direct">
  	<a href="" class="btn-map">Google Mapを開く &nbsp;<img src="<?php echo $PATH;?>/assets/images/search/icon-blank.svg" alt=""> </a>
  </div>
</div>
