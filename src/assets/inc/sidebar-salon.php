<div class="sidebar js-sidebar">
<!-- 	<div class="sidebar-head">
		<div class="sidebar-head--name">苗字 名前<span>様</span></div>
		<div class="sidebar-head--points">120<span>pt</span></div>
	</div> -->
	<div class="sidebar--name pc-only">soup*spoon<span> さん</span></div>
	<div class="sidebar-btnClose js-closeNav"></div>
	<div class="sidebar-main">
		<ul class="sidebar-list">
			<li>
				<a href="">マイページトップ</a>
			</li>
			<li>
				<a href="">予約状況</a>
			</li>
			<li>
				<a href="">予約確認・履歴</a>
			</li>
			<li class="has_sub">
				<a class="_link js-subMenuCtrl" href="javascript:void(0)">予約設定</a>
				<ul class="_sub js-subMenu">
					<li>
						<a href="">受付設定</a>
					</li>
					<li>
						<a href="">休業設定</a>
					</li>
					<li>
						<a href="">新規メニュー登録</a>
					</li>
					<li>
						<a href="">新規オプション登録</a>
					</li>
					<li>
						<a href="">メニュー・オプション</a>
					</li>
					<li>
						<a href="">予約時注意文</a>
					</li>
				</ul>
			</li>
			<li>
				<a href="">送信メール設定</a>
			</li>
			<li>
				<a href="">登録情報編集</a>
			</li>
			<li>
				<a href="">ペット施設情報編集</a>
			</li>
			<li>
				<a href=""><span class="countWrap">メッセージ<i class="count">01</i></span></a>
			</li>
			<li>
				<a href="">レビュー</a>
			</li>
			<li>
				<a href="">飼い主情報</a>
			</li>
		</ul>
		<div class="sidebar-link">
			<a href="">ログアウト</a>
			<a href="">退会</a>
		</div>
	</div>
</div>