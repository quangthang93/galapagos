<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/variables.php'; ?>
<!DOCTYPE html>
<html lang="ja">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Title</title>
  <meta name="description" content="">
  <meta property="og:site_name" content="">
  <meta property="og:title" content="">
  <meta property="og:description" content="">
  <meta property="og:type" content="website">
  <meta property="og:url" content="">
  <meta property="og:image" content="">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="">
  <meta name="twitter:image" content="">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400;500;600;700&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo $PATH;?>/assets/css/index.css">
</head>

<body>
  <div class="wrapper">
    <header class="header js-header fadedown">
      <h1 class="header-logo">
        <a class="logo link js-logo" href="/">
          <svg xmlns="http://www.w3.org/2000/svg" width="101.034" height="81.402" viewBox="0 0 101.034 81.402">
            <defs>
              <style>
              .a {
                fill: #fff;
              }
              </style>
            </defs>
            <g transform="translate(-903.327 -158.899)">
              <g transform="translate(903.327 158.899)">
                <path class="a" d="M968.166,188.586a1.818,1.818,0,1,0,1.626,1.037A1.8,1.8,0,0,0,968.166,188.586Z" transform="translate(-903.53 -158.994)" />
                <path class="a" d="M935.319,191.39a1.8,1.8,0,1,0-1.8-1.8A1.8,1.8,0,0,0,935.319,191.39Z" transform="translate(-903.424 -158.992)" />
                <path class="a" d="M979.459,213.377c-2.388-5.111-5.419-11.133-11.063-10.433-.187.023-.367.046-.545.07-3.143.413-4.931.77-6.7-2.778a30.28,30.28,0,0,1-1.781-4.459c-.721-2.279-.413-2.218,1.212-3.709a15.762,15.762,0,0,0,3.646-4.357,12.77,12.77,0,0,1,4.341-4.2,14.826,14.826,0,0,1,4.364-1.734c.174-1.05.413-2.436.65-3.637.995-5.052,1.785-4.655,4.679-1,7.1,8.965,15.207,18.852,25.829,19.188A33.174,33.174,0,0,0,955.936,162.6a4.55,4.55,0,0,1-4.191,0A33.157,33.157,0,0,0,903.339,193c11.217-2.408,12.388-9.817,17.149-19.294,1.731-3.443,4.3-4.3,4.329.573l.029,4.214c2.893.343,7.286,1.159,7.286,1.159a12.215,12.215,0,0,1,6.877,4.233,9.684,9.684,0,0,1,1.316,2.443c.866,2.352.9,2.447,4.179,3.775l5.712,2.314c3.195,1.295,3.445,2.622,1.277,5.221-3.629,4.356-5.274,7.7-10.815,7.565a16.542,16.542,0,0,1-2.145-.192c-11.435-1.757-16.566,6.386-18.718,15.778a57.741,57.741,0,0,0,7.725,3.258,33.241,33.241,0,0,0,24.2-2.459,4.549,4.549,0,0,1,4.193,0,33.273,33.273,0,0,0,28.363.987c-.013-.125-.024-.245-.038-.371A65.619,65.619,0,0,1,979.459,213.377Z" transform="translate(-903.327 -158.899)" />
              </g>
              <g transform="translate(909.931 233.517)">
                <path class="a" d="M915.113,234.993a.649.649,0,0,1-.638.668c-.438,0-.748-.449-1.534-.449-1.126,0-1.465.937-1.465,1.873,0,1.006.32,2.082,1.425,2.082.678,0,1.255-.359,1.255-1,0-.289-.229-.318-.508-.318-.14,0-.379.009-.458.009a.676.676,0,0,1-.608-.737c0-.608.777-.618,1.295-.618,1.186,0,1.824.179,1.824,1.445a2.49,2.49,0,0,1-2.8,2.57c-2.182,0-2.949-1.584-2.949-3.267,0-2.172,1.007-3.5,3-3.5C913.749,233.758,915.113,233.987,915.113,234.993Z" transform="translate(-909.952 -233.758)" />
                <path class="a" d="M925.079,235.1c.318.946.487,1.713.776,2.669a7.271,7.271,0,0,1,.557,2.1c0,.528-.5.6-.906.6-.8,0-.668-1.325-1.235-1.345s-1.136-.01-1.545-.01a.519.519,0,0,0-.507.309c-.279.537-.239,1.056-1.006,1.056a.721.721,0,0,1-.8-.826,15.07,15.07,0,0,1,.976-3.129c.439-1.414.349-1.833,1.116-2.451a1.584,1.584,0,0,1,.936-.218C924.51,233.858,924.809,234.277,925.079,235.1Zm-1.555,2.9c.319,0,.758-.06.758-.477a7.949,7.949,0,0,0-.409-1.555c-.06-.169-.189-.487-.409-.487-.4,0-.707,1.145-.766,1.395a3.006,3.006,0,0,0-.1.668C922.6,237.982,923.206,238,923.524,238Z" transform="translate(-909.986 -233.758)" />
                <path class="a" d="M933,234.993c0,1.007-.151,1.923-.151,2.94,0,.627.08,1.105.6,1.105.5,0,1.065-.219,1.564-.219.578,0,.807.329.807.846,0,.578-.469.8-.977.8-.318,0-.628-.05-.956-.05-.439,0-.9.04-1.345.04-.748,0-1.226-.189-1.226-1.036,0-.678.01-1.105.01-2.471,0-.748-.079-1.434-.079-1.943,0-.648.14-1.165.9-1.165C932.8,233.838,933,234.2,933,234.993Z" transform="translate(-910.021 -233.758)" />
                <path class="a" d="M945.257,235.1c.319.946.489,1.713.778,2.669a7.271,7.271,0,0,1,.557,2.1c0,.528-.5.6-.906.6-.8,0-.668-1.325-1.235-1.345s-1.136-.01-1.545-.01a.518.518,0,0,0-.507.309c-.279.537-.239,1.056-1.007,1.056a.72.72,0,0,1-.8-.826,15.074,15.074,0,0,1,.976-3.129c.439-1.414.349-1.833,1.115-2.451a1.585,1.585,0,0,1,.937-.218C944.689,233.858,944.988,234.277,945.257,235.1ZM943.7,238c.319,0,.758-.06.758-.477a7.893,7.893,0,0,0-.41-1.555c-.059-.169-.188-.487-.408-.487-.4,0-.708,1.145-.767,1.395a3.006,3.006,0,0,0-.1.668C942.776,237.982,943.384,238,943.7,238Z" transform="translate(-910.051 -233.758)" />
                <path class="a" d="M956.636,236.258a2.293,2.293,0,0,1-2.5,2.431,2.788,2.788,0,0,1-1.016-.14c.01.379.08.548.08.937,0,.588-.19,1.016-.877,1.016-.607,0-.856-.439-.856-1.007,0-.717.059-1.474.059-2.2,0-.947-.09-1.654-.09-2.232,0-1.295,1.655-1.295,2.461-1.295C955.582,233.768,956.636,234.4,956.636,236.258Zm-1.534-.06a1.064,1.064,0,0,0-1.215-1.125,1.108,1.108,0,0,0-.738.229,1.465,1.465,0,0,0-.228.907,1.3,1.3,0,0,0,.239.936,1.1,1.1,0,0,0,.657.209C954.575,237.354,955.1,237,955.1,236.2Z" transform="translate(-910.086 -233.758)" />
                <path class="a" d="M965.693,235.1c.318.946.487,1.713.776,2.669a7.27,7.27,0,0,1,.558,2.1c0,.528-.5.6-.906.6-.8,0-.668-1.325-1.236-1.345s-1.135-.01-1.544-.01a.517.517,0,0,0-.507.309c-.28.537-.24,1.056-1.007,1.056a.721.721,0,0,1-.8-.826,15.068,15.068,0,0,1,.977-3.129c.438-1.414.348-1.833,1.115-2.451a1.584,1.584,0,0,1,.936-.218C965.125,233.858,965.423,234.277,965.693,235.1Zm-1.555,2.9c.32,0,.758-.06.758-.477a7.949,7.949,0,0,0-.409-1.555c-.06-.169-.189-.487-.408-.487-.4,0-.708,1.145-.768,1.395a3,3,0,0,0-.1.668C963.212,237.982,963.82,238,964.138,238Z" transform="translate(-910.116 -233.758)" />
                <path class="a" d="M976.822,234.993a.649.649,0,0,1-.638.668c-.438,0-.748-.449-1.534-.449-1.126,0-1.465.937-1.465,1.873,0,1.006.32,2.082,1.425,2.082.678,0,1.255-.359,1.255-1,0-.289-.229-.318-.508-.318-.14,0-.379.009-.458.009a.676.676,0,0,1-.608-.737c0-.608.778-.618,1.295-.618,1.186,0,1.824.179,1.824,1.445a2.49,2.49,0,0,1-2.8,2.57c-2.182,0-2.949-1.584-2.949-3.267,0-2.172,1.007-3.5,3-3.5C975.458,233.758,976.822,233.987,976.822,234.993Z" transform="translate(-910.151 -233.758)" />
                <path class="a" d="M985.225,240.542a2.913,2.913,0,0,1-2.968-3.228c0-2.092.917-3.537,3.039-3.537s2.968,1.554,2.968,3.367C988.265,239.058,987.268,240.542,985.225,240.542Zm1.475-3.477c0-1.006-.429-1.923-1.525-1.923-1.045,0-1.394,1.115-1.394,1.953,0,1.067.419,2.082,1.455,2.082C986.352,239.178,986.7,238.142,986.7,237.065Z" transform="translate(-910.185 -233.758)" />
                <path class="a" d="M993.023,238.978a.694.694,0,0,1,.747-.7c.558,0,.826.827,1.724.827.438,0,1.015-.13,1.015-.668,0-.708-1.344-.587-2.37-1.066a1.673,1.673,0,0,1-.987-1.575c0-1.463,1.176-2.031,2.441-2.031a2.524,2.524,0,0,1,1.873.678.766.766,0,0,1,.189.487.745.745,0,0,1-.707.748c-.419,0-.847-.5-1.614-.5-.31,0-.739.1-.739.488,0,1.325,3.448.06,3.448,2.789,0,1.465-1.335,2.072-2.621,2.072C994.419,240.532,993.023,240.143,993.023,238.978Z" transform="translate(-910.219 -233.758)" />
              </g>
            </g>
          </svg>
        </a>
      </h1>
      <!-- <div class="header-menu js-menu">
        <ul>
          <li><a class="link" href="/works">制作実績</a></li>
          <li><a class="link" href="/service">サービス</a></li>
          <li><a class="link" href="/news">新着プレスリリース</a></li>
          <li><a class="link" href="/faq">よくあるご質問</a></li>
        </ul>
        <div class="header-logoNav">
          <a class="logo01" href="/"><img src="<?php echo $PATH;?>/assets/images/common/logo01.svg" alt=""></a>
          <a class="logo02" href="/"><img src="<?php echo $PATH;?>/assets/images/common/logo02.svg" alt=""></a>
        </div>
        <div class="header-menu--contactWrap js-headerContactWrap">
          <a href="/contact" class="header-menu--contact js-headerContact"><span>Contact</span></a>
          <div class="spin-containerWrap">
            <div class="spin-container">
              <div class="shape">
                <div class="bd"></div>
              </div>
            </div>
          </div>
        </div>
      </div> -->
      <a href="/login" class="header-login btn-yellow pc-only">ログイン・新規登録</a>
      <a class="header-ctrl js-menuTrigger" href="javascript:void(0);">
        <img src="<?php echo $PATH;?>/assets/images/common/menu-icon.svg" alt="">
      </a>
    </header><!-- ./header -->