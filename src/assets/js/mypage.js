jQuery(function($) {
  var $favoriteCheck = $('.js-favoriteCheck');
  var $favoriteCtrl = $('.js-favoriteCtrl');
  var $favoriteReset = $('.js-favoriteReset');

  // var $favoriteCBox = 
 $favoriteCheck.click(function () {
 	var dataTxt = $(this).data('txt');
 	var defaultTxt = $(this).html();
	var $listCbox = $(this).parent().siblings('.js-favoriteList').find('.js-cbox');

 	if ($(this).hasClass('check')) {
 		$(this).removeClass('check').html(dataTxt);

	 	$.each( $listCbox, function() {
		  $(this).prop('checked', true);
		});

		$favoriteCtrl.addClass('active');
 	} else {
 		$(this).addClass('check').html('すべてを選択する');

	 	$.each( $listCbox, function() {
		  $(this).prop('checked', false);
		});

		$favoriteCtrl.removeClass('active');
 	}
 });

 $favoriteReset.click(function () {
 	var $listCbox = $('.js-favoriteList').find('.js-cbox');
 	$.each( $listCbox, function() {
	  $(this).prop('checked', false);
	});
	$favoriteCtrl.removeClass('active');
	$favoriteCheck.addClass('check').html('すべてを選択する');
 });


	$('.js-cbox').change(function () {
		var dataTxt = $favoriteCheck.data('txt');
		var $listCbox = $('.js-favoriteList').find('.js-cbox');
		if(this.checked) {
      $favoriteCheck.removeClass('check').html(dataTxt);
      $favoriteCtrl.addClass('active');
    } else {
    	$favoriteCheck.addClass('check').html('すべてを選択する');
    	$favoriteCtrl.removeClass('active');
    }
	});

});