$('.p-mypage__slider ul').slick({ //{}を入れる
    autoplay: true, //「オプション名: 値」の形式で書く
    dots: true, //複数書く場合は「,」でつなぐ
	arrows: false,
  });
  $('.hospital-unit__img ul').slick({ //{}を入れる
    autoplay: true, //「オプション名: 値」の形式で書く
	arrows: false,
	centerMode: true,
	centerPadding: "20px",
  });
  $('.p-hospital .top__img .top__img__main-slider').slick({ //{}を入れる
	fade: true,
	arrows: false,
	autoplay: true, // 自動再生ON
	autoplaySpeed: 3000,
	arrows: false, // 矢印非表示
	asNavFor: ".p-hospital .top__img .top__img__nav-slider", // サムネイルと同期
	pauseOnFocus: false,
	pauseOnHover: false,
	pauseOnDotsHover: false,
  });
  $(".p-hospital .top__img .top__img__nav-slider").slick({
	arrows: false,
	slidesToShow: 16, // サムネイルの表示数
	asNavFor: ".p-hospital .top__img .top__img__main-slider", // メイン画像と同期
	focusOnSelect: true, // サムネイルクリックを有効化
});


  $('.p-tabbox .p-tabbox__btn ').click(function() {
	var index = $('.p-tabbox .p-tabbox__btn ').index(this);
	$('.p-tabbox .p-tabbox__btn , .p-tabbox .p-tabbox__panel').removeClass('active');
	$(this).addClass('active');
	$('.p-tabbox .p-tabbox__panel').eq(index).addClass('active');
});

$('.tabbox-2 .tabbox-2__btn ').click(function() {
	var index = $('.tabbox-2 .tabbox-2__btn ').index(this);
	$('.tabbox-2 .tabbox-2__btn , .tabbox-2 .tabbox-2__panel').removeClass('active');
	$(this).addClass('active');
	$('.tabbox-2 .tabbox-2__panel').eq(index).addClass('active');
});

$('#datepicker').datepicker();

$('.received-consultation__cont .data .good').on('click', function () {
    $('.received-consultation__cont .data .good').toggleClass('on');
});


$(function () {
	// id[js-menuTrigger]の要素をクリックしたとき
	$('.js-menuTrigger').on('click',function () {
	  // もしclass[l-header]の要素がclass[l-header-active]を持っているなら
	  if ($('.l-header').hasClass('l-header-active')) {
		// class[l-header]の要素からclass[l-header-active]を削除する
		$('.l-header').removeClass('l-header-active');
		// 要素[body]からclass[body-menuOpen]を削除する
		$('body').removeClass('body-menuOpen');
	  //それ以外(class[l-header]の要素がclass[l-header-active]を持っていない)なら
	  } else {
		// class[l-header]の要素にclass[l-header-active]を付与する
		$('.l-header').addClass('l-header-active');
		// 要素[body]にclass[body-menuOpen]を付与する
		$('body').addClass('body-menuOpen');
	  }
	});
  });


  $(function () {
    $(".parent-1 > .trigger").on("click", function () {
		$(this).next().slideToggle();
		$(this).toggleClass("active"); //追加部分
		$(this).parents(".parent-1").toggleClass("parent-active");
	  });
	  $(".parent-2 > .trigger").on("click", function () {
		$(this).next().slideToggle();
		$(this).toggleClass("active"); //追加部分
		$(this).parents(".parent-2").toggleClass("parent-active");
	  });
	  $(".sidebar-hospital-search .trigger").on("click", function () {
		$(this).next().slideToggle();
		$(this).toggleClass("active"); //追加部分
	  });
  });

  $(".area-list input[type='checkbox']").change(function(){
	$("input[type='checkbox'][name='"+ $(this).attr("name")+ "']").each(function(){
	  if($(this).is(":checked")){
		$(this).parent().addClass("selected");
	  }else{
		$(this).parent().removeClass("selected");
	  }
	});
  });

  $(".reset input[type='reset']").click(function(e){
    e.preventDefault();
    $(this).closest('form').get(0).reset();
	$(".area-list__item-inner").removeClass("selected");
});


$(function () {
	var $demo1 = $('.main-hospital-search__cont');   //コンテナとなる要素を指定
  
	$demo1.imagesLoaded(function(){ //imagesLoadedを使用し、画像が読み込みまれた段階でMasonryの関数を実行させる
	  //Masonryの関数↓
	  $demo1.masonry({              //オプション指定箇所
		itemSelector: '.hospital-unit',   //コンテンツを指定
	  });
	});
  });
