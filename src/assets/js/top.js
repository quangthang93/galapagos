jQuery(function($) {
  var wWindow = $(window).outerWidth();

  var $heart = $('.section01-magic');
  var $section01Cnt = $('.js-section01Cnt');
  var $prallax = $('.parallax');

  $(window).scroll(function() {
    var scroll = $(this).scrollTop();
    var ss01PosTop = $('.js-section01').offset().top;

    if (wWindow >= 768 && wWindow <= 1180) {
      if (scroll >= ss01PosTop) {
        $prallax.addClass('scroll');
        $heart.addClass('in');
      } else {
        $prallax.removeClass('scroll');
        $heart.removeClass('in');
      }
    } else {
      $section01Cnt.hide();
      if (scroll >= ss01PosTop) {
        $prallax.addClass('scroll');
        $section01Cnt.show();
        $heart.addClass('in');
      } else {
        $prallax.removeClass('scroll');
        $section01Cnt.hide();
        $heart.removeClass('in');
      }
    }

    if (wWindow < 768) {
      var para1 = document.getElementById('para1');
      var para2 = document.getElementById('para2');
      var wh = window.innerHeight;
      window.addEventListener('scroll', function() {
        var per = Math.floor( 100 - ( window.pageYOffset / wh * 100 ));
        if (per >= 0) {
          para1.style.height = ( per - 1 ) + '%';
          para2.style.opacity = 1;
        } else {
          para1.style.height = 0;
          para2.style.opacity = 0;
        }
      });
    }
  });
});