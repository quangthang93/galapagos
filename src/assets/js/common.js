// $(function() {
jQuery(function($) {

  /* --------------------
  GLOBAL VARIABLE
  --------------------- */
  // Selector
  // var $pageTop = $('.js-pageTop');

  // Init Value
  var breakpointSP = 767,
      breakpointTB = 1050,
      wWindow = $(window).outerWidth();


  /* --------------------
  FUNCTION COMMON
  --------------------- */
  // Setting anchor link
  var anchorLink = function() {
    // Scroll to section
    $('a[href^="#"]').not("a[class*='carousel-control-']").click(function() {
      var href= $(this).attr("href");
      var hash = href == "#" || href == "" ? 'html' : href;
      var position = $(hash).offset().top - 100;
      $('body,html').stop().animate({scrollTop:position}, 1000);
      return false;
    });
  }

  // Animation scroll to top
  var clickPageTop = function() {
    var $pageTop = $('.js-pageTop');
    $pageTop.click(function(e) {
      $('html,body').animate({ scrollTop: 0 }, 300);
    });
  }

  // Animation on scroll
  var scrollLoad = function() {
   var scroll = $(this).scrollTop();
    $('.ani').each(function() {
      var elemPos = $(this).offset().top;
      var windowHeight = $(window).height();
      if (scroll > elemPos - windowHeight + 100) {
        $(this).addClass('in');
      }
    });
  }

  // Animation on scroll
  var scrollLoad2 = function() {
    var scroll2 = $(this).scrollTop();
     $('.ani-later').each(function() {
       var elemPos2 = $(this).offset().top;
       var windowHeight2 = $(window).height();
       if (scroll2 > elemPos2 - windowHeight2 + 500) {
         $(this).addClass('in');
       }
     });
   }

  // Trigger Pagetop
  var triggerPageTop = function() {
    var $pageTop = $('.js-pageTop');
    if ($(this).scrollTop() > 200) {
      $pageTop.addClass('active');
    } else {
      $pageTop.removeClass('active');
    }
  }  

  // Trigger background for header
  var triggerBackground = function() {
    if ($(this).scrollTop() > 100) {
      $('.js-header').addClass('scroll');
      $('.js-logo, .js-headerContact, .js-headerContactWrap, .js-menuTrigger').addClass('active');
    } else {
      $('.js-header').removeClass('scroll');
      $('.js-logo, .js-headerContact, .js-headerContactWrap, .js-menuTrigger').removeClass('active');
    }
  }

  var triggerMenuBtn = function() {
    $('.js-menuTrigger').on( 'click', function() {
      $( '.js-gnav' ).toggleClass( 'show' );
    });
  }


  // Trigger Accordion
  var triggerAccordion = function() {
    var $accorLabel = $('.js-accorLabel'),
        $accorCnt = $('.js-accorCnt');
    $accorLabel.click(function () {
      $(this).toggleClass('active').siblings($accorCnt).slideToggle();
    });   
  }  

  // Tabs Control
  var tabsControl = function() {
    var $tabsNav = $('.js-tabsNav'),
        $tabsItem = $('.js-tabsItem'),
        $tabsCnt = $('.js-tabsCnt'),
        $tabsPanel = $('.js-tabsPanel');

    // Setting first view
    $tabsPanel.hide();
    $tabsCnt.each(function () {
        $(this).find($tabsPanel).eq(0).show();
    });
    $tabsNav.each(function () {
        $(this).find($tabsItem).eq(0).addClass('active');
    });

    // Click event
    $tabsItem.on('click', function () {
      var tMenu = $(this).parents($tabsNav).find($tabsItem);
      var tCont = $(this).parents($tabsNav).next($tabsCnt).find($tabsPanel);
      var index = tMenu.index(this);
      tMenu.removeClass('active');
      $(this).addClass('active');
      tCont.hide();
      tCont.eq(index).show();
    });
  } 

  var tabsControl2 = function() {
    var $tabsNav = $('.js-tabsNav'),
        $tabsItem = $('.js-tabsItem'),
        $tabsCnt = $('.js-tabsCnt'),
        $tabsPanel = $('.js-tabsPanel');

    // Setting first view
    $tabsPanel.hide();
    $tabsCnt.each(function () {
        $(this).find($tabsPanel).eq(0).show();
    });
    $tabsNav.each(function () {
        $(this).find($tabsItem).eq(0).addClass('active');
    });

    // Click event
    $tabsItem.on('click', function () {
      var tMenu = $(this).parents($tabsNav).find($tabsItem);
      var tCont = $(this).parents($tabsNav).next($tabsCnt).find($tabsPanel);
      var index = tMenu.index(this);
      tMenu.removeClass('active');
      $(this).addClass('active');
      tCont.hide();
      tCont.eq(index).show();
    });
  } 


  // Slider init
  // Slider (Restaurant detail page)
  var initSliders = function() {
    var $slide01 = $('.js-slider01'); 
    var $slide02 = $('.js-slider02'); 
    var $slide02Nav = $('.js-slider02-nav'); 

    // Slider 01
    if ($slide01.length) {
      // Tabs slider
      $slide01.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        // fade: true,
        prevArrow: '<a class="slick-prev" href="javascript:void(0)"><span></span></a>',
        nextArrow: '<a class="slick-next" href="javascript:void(0)"><span></span></a>',
        // responsive: [{
        //   breakpoint: 768,
        //   settings: {
        //     slidesToShow: 1
        //   }
        // }]
      });
    }

    // Slider 02
    if ($slide02.length) {
      // Tabs slider
      $slide02.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        // fade: true,
        asNavFor: $slide02Nav,
        prevArrow: '<a class="slick-prev" href="javascript:void(0)"><span></span></a>',
        nextArrow: '<a class="slick-next" href="javascript:void(0)"><span></span></a>',
      });
    }

    // Slider02 Nav
    if ($slide02.length && $slide02Nav.length) {
      $slide02Nav.slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: $slide02,
        arrows: false,
        dots: false,
        // centerMode: true,
        focusOnSelect: true,
        responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 4
          }
        }]
      });
    }
  }

  var matchHeight = function() {
    var $elem01 = $('.js-favoriteCnt');
    if ($elem01.length) {
      $elem01.matchHeight();
    }
  }

  // control Nav Menu SP
  var ctrlSidebar = function () {
    var $sideBar = $('.js-sidebar');
    var $openNav = $('.js-openNav');
    var $closeNav = $('.js-closeNav');
    var $subMenuCtrl = $('.js-subMenuCtrl');
    var $subMenu = $('.js-subMenu');

    $openNav.click(function () {
      $('body').addClass('fixed');
      $sideBar.addClass('active');
    });

    $closeNav.click(function () {
      $('body').removeClass('fixed');
      $sideBar.removeClass('active');
    });

    $subMenuCtrl.click(function() {
      $(this).toggleClass('active').siblings($subMenu).slideToggle();
    });
  }

  // add element for mypage edit
  var addFacilities = function () {
    var $btnAdd = $('.js-btnAdd');
    var $facilitiesItem = $('.js-facilitiesItem');

    $btnAdd.click(function () {
      $facilitiesItem.clone().insertAfter(".js-facilitiesItem:last");
    });
  }

  // submit form edit mypage - pets
  var initModalBT = function () {
    var $modalCancel = document.getElementById('modalCancel');

    if ($modalCancel) {
      var _modalCancel = new bootstrap.Modal($modalCancel);
      _modalCancel.show();
    }
  }

  // Date picker init
  var initDatePicker = function() {
    try {
      $.datepicker.setDefaults($.datepicker.regional["ja"]);
      var $datePicker01 = $('.js-datepicker01');
      if ($datePicker01.length) {
        $datePicker01.datepicker({
          firstDay: 1,
          dateFormat: 'yy/mm/dd'
        });
      }
      var $datePicker02 = $('.js-datepicker02');
      if ($datePicker02.length) {
        $datePicker02.datepicker({
          firstDay: 1,
          minDate:3
        });
      }
    } catch(e) {
      console.log(e);
    }
  }

  /* --------------------
  INIT (WINDOW ON LOAD)
  --------------------- */
  // Run all script when DOM has loaded
  var init = function() {
    anchorLink();
    scrollLoad();
    scrollLoad2();
    objectFitImages();
    clickPageTop();
    triggerAccordion();
    initSliders();
    tabsControl();
    triggerBackground();
    ctrlSidebar();
    addFacilities();
    initModalBT();
    matchHeight();
    initDatePicker();
    triggerMenuBtn();
  }

  init();


  /* --------------------
  WINDOW ON RESIZE
  --------------------- */
  $(window).resize(function() {
    wWindow = $(window).outerWidth();
    // if (wWindow <= 1050) {
    //   tabsControl2();
    // }
  });


  /* --------------------
  WINDOW ON SCROLL
  --------------------- */
  $(window).scroll(function() {
    scrollLoad();
    scrollLoad2();
    triggerBackground();
  });

});