<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-reset.php'; ?>
<main class="main mypage">
  <div class="p-mypage">
    <div class="container4">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">パスワードを再設定する</h2>
      </div>
      <div class="p-resetWrap">
        <div class="p-reset">
          <form class="form p-reset--form">
            <div class="form-row">
              <div class="form-input">
                <label class="p-reset--label" for="pw">再設定するパスワード<span>※</span></label>
                <input id="pw" type="text" name="name" class="input" value="" placeholder="半角英数字">
              </div>
              <div class="form-input">
                <label class="p-reset--label" for="pw2">確認のためもう一度入力してください<span>※</span></label>
                <input id="pw2" type="text" name="name" class="input" value="" placeholder="半角英数字">
              </div>
            </div>
            <div class="p-reset--submit"><a href="" class="btn-submit type2 js-btnSave">パスワードを再設定する</a></div>
          </form>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-reset.php'; ?>