<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-reset.php'; ?>
<main class="main mypage">
  <div class="p-mypage">
    <div class="container4">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">パスワードを再設定する</h2>
      </div>
      <div class="p-resetWrap">
        <div class="p-reset">
          <p class="p-contact--completed-ttl align-center">パスワードの再設定が完了しました。</p>
          <div class="p-reset--submit"><a href="/login" class="btn-submit type2 js-btnSave">ログインページへ</a></div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-reset.php'; ?>