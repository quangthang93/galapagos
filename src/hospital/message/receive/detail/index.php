<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">メッセージ</a></li>
      <li>受信メッセージ</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">受信メッセージ</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-hospital.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-messageDetail">
            <div class="p-messageDetail--head">
              <p class="ttl-bold3">お客様からご相談が届きました。</p>
              <p class="date5">2021年04月03日 16:00</p>
            </div>
            <div class="p-messageDetail--cnt">
              <div class="p-messageDetail--cnt-row">
                ご担当者 様 <br><br>いつもご利用ありがとうございます。<br> ガラパゴス運営事務局です。 <br> <br> 先日、苗字 名前 様からご相談が届きましたのでご回答をお願いします。
              </div>
              <div class="p-messageDetail--cnt-row">2021年5月31日　16:30<br> ご相談内容：あそび・散歩 <br> ご相談ペット：犬 <br> <br>
                【ご相談内容】 <br> 臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満気味です。 <br>散歩以外で運動って何かありますか？
                </div>
              <div class="p-messageDetail--cnt-row">ガラパゴス運営事務局</div>
            </div>
            <div class="p-messageDetail--foot align-center">
              <div class="mail-ctrl">
                <a href="" class="link mail-ctrl--prev">前へ</a>
                <a href="" class="link mail-ctrl--next">次へ</a>
              </div>
              <a href="/mypage/message/" class="link-blue">一覧へ戻る</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>