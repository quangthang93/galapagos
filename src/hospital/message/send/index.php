<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>

    <main class="main mypage">
    	<div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/">マイページ</a></li>
          <li><a href="/">メッセージ</a></li>
          <li>送信メッセージ</li>
        </ul>
      </div>
      <div class="p-mypage">
    		<div class="container3">
          <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
          <div class="p-mypage--ttlWrap">
            <h2 class="p-mypage--ttl">送信メッセージ</h2>
            <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
          </div>
    			<div class="p-mypage--inner">
    				<div class="p-mypage--sidebar">
    					<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-hospital.php'; ?>
    				</div>
    				<div class="p-mypage--cnt">
              <div class="p-message">
                <ul class="p-message--list">
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">ガラパゴス運営事務局</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">お客様からご相談が届きました。</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">ガラパゴス運営事務局</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">【感想をお聞かせください】こんにちは。アマノ動…</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">ガラパゴス運営事務局</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">【感想をお聞かせください】こんにちは。アマノ動…</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">ガラパゴス運営事務局</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">お客様からご相談が届きました。</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">一括</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">お客様からご相談が届きました。</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">山田花子</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">お客様からご相談が届きました。</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">ガラパゴス運営事務局</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">お客様からご相談が届きました。</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">ガラパゴス運営事務局</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">お客様からご相談が届きました。</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">ガラパゴス運営事務局</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">お客様からご相談が届きました。</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">ガラパゴス運営事務局</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">お客様からご相談が届きました。</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">ガラパゴス運営事務局</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">お客様からご相談が届きました。</a>
                  </li>
                  <li>
                    <div class="date4Wrap">
                      <div class="date4">2021年04月03日 <span>16:00</span></div>
                      <p class="_name">ガラパゴス運営事務局</p>
                    </div>
                    <a href="/mypage/message/detail" class="p-message--list-link type2 link">お客様からご相談が届きました。</a>
                  </li>
                </ul>
              </div>
              <div class="p-pagination">
                <p class="p-pagination-label">0,000件中｜1〜30件 表示</p>
                <div class="p-pagination-list">
                  <a class="ctrl prev" href=""></a>
                  <a class="active" href="">1</a>
                  <a href="">2</a>
                  <a href="">3</a>
                  <a href="">4</a>
                  <a href="">5</a>
                  <a href="">6</a>
                  <div class="p-pagination-spacer">…</div>
                  <a href="">12</a>
                  <a class="ctrl next" href=""></a>
                </div>
              </div>
            </div>
    			</div>
    		</div>
      </div>
    </main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>