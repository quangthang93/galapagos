<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約設定</a></li>
      <li>予約メニュー</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約メニュー</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-hospital.php'; ?>
        </div>
        <div class="p-mypage--cnt type2">
          <div class="p-reservation">
            <div class="p-reservation--item mb-40">
              <h3 class="ttl-bg">全体の予約枠</h3>
              <div class="p-reservation--item-cnt p-edit p-0">
                <div class="form border-top-none">
                  <div class="form-row type3">
                    <div class="form-label type2">30分（1枠）に<br>受け付ける予約数</div>
                    <div class="form-row--cnt">
                      <div class="form-groupField">
                        <select name="product" class="select type3 input">
                          <option value="">_</option>
                          <option value="">_</option>
                          <option value="" selected="selected">_</option>
                          <option value="">2022</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
            <div class="p-reservation--item">
              <h3 class="ttl-bg">カテゴリー編集</h3>
              <div class="p-reservation--item-cnt p-edit p-0">
                <div class="form border-top-none">
                  <div class="form-row type3">
                    <div class="form-row--cnt">
                      <div class="form-input type-btn mgb-15">
                        <input type="text" name="name" class="input" value="初診" placeholder="初診">
                        <a href="javascript:void(0)" class="btn-del3 mgl-10">
                          <span>削除</span>
                        </a>
                      </div>
                      <div class="form-input type-btn mgb-15">
                        <input type="text" name="name" class="input" value="再診" placeholder="再診">
                        <a href="javascript:void(0)" class="btn-del3 mgl-10">
                          <span>削除</span>
                        </a>
                      </div>
                      <div class="form-input type-btn mgb-15">
                        <input type="text" name="name" class="input" value="予防接種・各種検査" placeholder="予防接種・各種検査">
                        <a href="javascript:void(0)" class="btn-del3 mgl-10">
                          <span>削除</span>
                        </a>
                      </div>
                      <div class="form-input type-btn">
                        <input type="text" name="name" class="input" value="常用薬・常用食" placeholder="常用薬・常用食">
                        <a href="javascript:void(0)" class="btn-del3 mgl-10">
                          <span>削除</span>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
            <div class="p-reservation--item">
              <h3 class="ttl-bg">予約メニュー　一覧</h3>
              <div class="mt-20">
                <div class="tabs">
                  <div class="tabs-navWrapper">
                    <ul class="tabs-nav js-tabsNav">
                      <li class="tabs-item type3 js-tabsItem active">初診</li>
                      <li class="tabs-item type3 js-tabsItem">再診</li>
                      <li class="tabs-item type3 js-tabsItem">予防接種・<br>各種検査</li>
                      <li class="tabs-item type3 js-tabsItem">常用薬・<br>常用食</li>
                    </ul>
                  </div>
                  <div class="tabs-cnt type2 type4 js-tabsCnt">
                    <div class="tabs-panel js-tabsPanel">
                      
                      <div class="p-reserve--itemBox">
                        <div class="p-reserve--item type2 blue">
                          <div class="p-reserve--item-head">
                            <p class="ttl-bold4">混合ワクチン接種</p>
                            <div class="d_flex">
                              <a href="" class="link-blue">コピー</a>
                              <a href="" class="ml-20 link-blue">編集</a>
                            </div>
                          </div>
                          <p class="desc4 mb-10">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                          <div class="p-hospitalReview--item-dateWrap">
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">所要時間</span>
                              <span class="desc4">30分</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">予約枠</span>
                              <span class="desc4">3</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">ステータス</span>
                              <span class="desc4">公開前</span>
                            </div>
                          </div>
                        </div>
                        <div class="p-reserve--item type2 gray">
                          <div class="p-reserve--item-head">
                            <p class="ttl-bold4">狂犬病予防接種</p>
                            <div class="d_flex">
                              <a href="" class="link-blue">コピー</a>
                              <a href="" class="ml-20 link-blue">編集</a>
                            </div>
                          </div>
                          <p class="desc4 mb-10">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                          <div class="p-hospitalReview--item-dateWrap">
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">所要時間</span>
                              <span class="desc4">30分</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">予約枠</span>
                              <span class="desc4">3</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">ステータス</span>
                              <span class="desc4">公開終了</span>
                            </div>
                          </div>
                        </div>
                        <div class="p-reserve--item type2 gray2">
                          <div class="p-reserve--item-head">
                            <p class="ttl-bold4">フィラリア予防</p>
                            <div class="d_flex">
                              <a href="" class="link-blue">コピー</a>
                              <a href="" class="ml-20 link-blue">編集</a>
                            </div>
                          </div>
                          <p class="desc4 mb-10">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                          <div class="p-hospitalReview--item-dateWrap">
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">所要時間</span>
                              <span class="desc4">30分</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">予約枠</span>
                              <span class="desc4">3</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">ステータス</span>
                              <span class="desc4">非表示</span>
                            </div>
                          </div>
                        </div>
                        <div class="p-reserve--item type2">
                          <div class="p-reserve--item-head">
                            <p class="ttl-bold4">ノミ、マダニ予防</p>
                            <div class="d_flex">
                              <a href="" class="link-blue">コピー</a>
                              <a href="" class="ml-20 link-blue">編集</a>
                            </div>
                          </div>
                          <p class="desc4 mb-10">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                          <div class="p-hospitalReview--item-dateWrap">
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">所要時間</span>
                              <span class="desc4">30分</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">予約枠</span>
                              <span class="desc4">3</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">ステータス</span>
                              <span class="desc4">-</span>
                            </div>
                          </div>
                        </div>
                        <div class="p-reserve--item type2">
                          <div class="p-reserve--item-head">
                            <p class="ttl-bold4">ノミ、マダニ予防</p>
                            <div class="d_flex">
                              <a href="" class="link-blue">コピー</a>
                              <a href="" class="ml-20 link-blue">編集</a>
                            </div>
                          </div>
                          <p class="desc4 mb-10">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                          <div class="p-hospitalReview--item-dateWrap">
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">所要時間</span>
                              <span class="desc4">30分</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">予約枠</span>
                              <span class="desc4">3</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">ステータス</span>
                              <span class="desc4">-</span>
                            </div>
                          </div>
                        </div>
                        <div class="p-reserve--item type2">
                          <div class="p-reserve--item-head">
                            <p class="ttl-bold4">ノミ、マダニ予防</p>
                            <div class="d_flex">
                              <a href="" class="link-blue">コピー</a>
                              <a href="" class="ml-20 link-blue">編集</a>
                            </div>
                          </div>
                          <p class="desc4 mb-10">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                          <div class="p-hospitalReview--item-dateWrap">
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">所要時間</span>
                              <span class="desc4">30分</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">予約枠</span>
                              <span class="desc4">3</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">ステータス</span>
                              <span class="desc4">-</span>
                            </div>
                          </div>
                        </div>
                        <div class="p-reserve--item type2">
                          <div class="p-reserve--item-head">
                            <p class="ttl-bold4">ノミ、マダニ予防</p>
                            <div class="d_flex">
                              <a href="" class="link-blue">コピー</a>
                              <a href="" class="ml-20 link-blue">編集</a>
                            </div>
                          </div>
                          <p class="desc4 mb-10">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                          <div class="p-hospitalReview--item-dateWrap">
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">所要時間</span>
                              <span class="desc4">30分</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">予約枠</span>
                              <span class="desc4">3</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">ステータス</span>
                              <span class="desc4">-</span>
                            </div>
                          </div>
                        </div>
                        <div class="p-reserve--item type2">
                          <div class="p-reserve--item-head">
                            <p class="ttl-bold4">ノミ、マダニ予防</p>
                            <div class="d_flex">
                              <a href="" class="link-blue">コピー</a>
                              <a href="" class="ml-20 link-blue">編集</a>
                            </div>
                          </div>
                          <p class="desc4 mb-10">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</p>
                          <div class="p-hospitalReview--item-dateWrap">
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">所要時間</span>
                              <span class="desc4">30分</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">予約枠</span>
                              <span class="desc4">3</span>
                            </div>
                            <div class="div p-hospitalReview--item-dateWrap-child">
                              <span class="label-gray">ステータス</span>
                              <span class="desc4">-</span>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-ctrl2">
                        <div class="btn-submitWrap">
                          <a href="" class="btn-submit type4">保存する</a>
                        </div>
                        <div class="form-ctrl2--link">
                          <a href="" class="link-blue">戻る</a>
                        </div>
                      </div>
                    </div>
                    <div class="tabs-panel js-tabsPanel">tab2</div>
                    <div class="tabs-panel js-tabsPanel">tab3</div>
                    <div class="tabs-panel js-tabsPanel">tab4</div>
                    <div class="tabs-panel js-tabsPanel">tab5</div>
                  </div>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
          </div><!-- ./p-reservation -->
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>