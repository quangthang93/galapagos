<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">探す・相談する</a></li>
      <li><a href="/">施設を探す</a></li>
      <li><a href="/">BiBi犬猫病院</a></li>
      <li>施設予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">施設予約</h2>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebarSearch">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-search.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-message type2">
            <div style="font-size: 14px;">
              <p><span style="color:#5DC1CF; font-size: 16px;"><strong>ご予約について</strong></span></p><br>
              <p><strong>・当院では、動物たちと飼い主様の利便性向上を目的として、『時間帯』予約制を採用しています。</strong></p>
              <br><br>
              <p>※ 1『時間帯予約制とは』</p>
              <p>同じ時間帯に複数名の方が来院される方式であり、例えば、10時~ の予約枠の方は10:00~10:30の時間帯での診察開始が目安となります。</p>
              <p>&nbsp;</p>
              <p>※ 2 動物病院の特性上、生命に関わる緊急対応を要する場合があります。その際には、ご予約の有無に関わらずおまたせしてしまう可能性がございますが日々生命と向き合う現場であること、予めご理解いただけますと幸いです。</p>
              <p>&nbsp;</p>
              <p><strong>・急変時を含め、予約なしでの診察も可能となります。一方で、お待たせすることが予想されますので、可能な限り事前予約をオススメ致します。</strong></p>
              <p>&nbsp;</p>
              <p><strong>・獣医師の指名はお受けできないことをご了承ください。</strong></p>
              <p>&nbsp;</p>
              <br>
              <p><span style="color: #5dc1cf; font-size: 16px;"><strong>獣医師勤務表</strong></span></p>
              <br>
              <div style="color: #004DE6; font-weight: bold">
                <p>平塚　（一般科＋歯科）：　月火水　</p>
                <p>土日 今井　（一般科）&nbsp;   ：　　火水金土日</p>
                <p>佐々木（一般科）　　　：　月　　金土日</p>
                <p>伊從　（皮膚科）　　　：　　　　金土日</p>
                <p>田口　（皮膚科）　　　：　月火　金土日</p>
              </div>
              <br>
              <br>
              <br>
            </div>
          </div>
          <div class="form-ctrl2">
                <div class="btn-submitWrap">
                  <a href="" class="btn-submit type4">保存する</a>
                </div>
                <div class="form-ctrl2--link">
                  <a href="" class="link-blue">編集に戻る</a>
                </div>
              </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer3.php'; ?>