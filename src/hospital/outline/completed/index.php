<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li>登録情報編集</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">登録情報編集</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-hospital.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit">
            <div class="p-edit--dateWrap">
              <span class="label-gray">登録日</span>
              <span class="desc4">2021年10月01日</span>
            </div>
            <form class="form">
              <div class="form-row">
                <div class="form-label">施設区分</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    動物病院
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">施設名</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" value="ガラパゴス動物病院">
                  </div>
                </div>
              </div>
              <div class="form-row border-bottom-none">
                <div class="form-label">郵便番号</div>
                <div class="form-row--cnt">
                  <div class="form-groupField--item">
                    <input type="text" name="name" class="input type3 mgr-20" value="1001000">
                    <a class="form-row--upload-btn btn-blue">住所自動入力</a>
                  </div>
                </div>
              </div>
              <div class="form-row border-bottom-none">
                <div class="form-label">都道府県</div>
                <div class="form-row--cnt">
                  <div class="form-groupField--item">
                    <div class="d_flex between">
                      <select name="sub01" class="select input type6">
                        <option value="">東京都</option>
                        <option value="" selected="selected">東京都</option>
                        <option value="">東京都</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row border-bottom-none">
                <div class="form-label">市区町村</div>
                <div class="form-row--cnt">
                  <div class="form-groupField--item">
                    <div class="d_flex between">
                      <input type="text" name="name" class="input type6" value="港区北青山">
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">以降</div>
                <div class="form-row--cnt">
                  <div class="form-groupField--item">
                    <input type="text" name="name" class="input" value="3-3-5東京建物青山ビル4F">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">施設責任者</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" value="山田太郎">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">施設責任者<br>生年月日</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    &nbsp;&nbsp;1970年12月31日
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">開業届</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    設定済み
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">電話番号</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" value="0312345678">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">メール<br>アドレス</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="email" name="name" class="input" value="info@galapagos.com">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">パスワード</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="password" name="name" class="input" value="xxxxxxxxxxx">
                  </div>
                </div>
              </div>

              <div class="form-ctrl type2">
<!--                 <div class="form-ctrl--item btn-submitWrap">
                  <a href="" class="btn-white3 align-center">戻る</a>
                </div> -->
                <div class="form-ctrl--item btn-submitWrap">
                  <a href="/mypage/edit-infor/completed" class="btn-submit">編集内容を保存する</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->

<!-- modal box -->
  <div class="modal-complete">
    <div class="modal-complete--cnt">
      <div class="modal-complete--cnt-inner">
        <div class="modal-complete--cnt-icon"><img src="<?php echo $PATH;?>/assets/images/common/icon-check.svg" alt=""></div>
        <p class="modal-complete--cnt-msg">登録情報の編集を<br>保存しました。</p>
      </div>
    </div>
  </div>
<!-- ./modal box -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>