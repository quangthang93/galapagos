<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li>予約確認・履歴</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約確認・履歴</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-hospital.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-history">
            <!-- <a href="" class="p-history--signup btn-blue">新規予約登録</a> -->
            <div class="p-edit--dateWrap">
              <span class="label-gray">予約受付日</span>
              <span class="desc4">2021年10月01日</span>
            </div>
            <form class="form">
              <p class="form-row--ttl2 border-bot mgt-30">予約内容</p>
              <div class="form-rowWrap">
                <div class="form-row type3">
                  <div class="form-label">
                    ステータス
                  </div>
                  <div class="form-row--cnt">
                    来訪前
                    <!-- <select name="sub01" class="select input type3">
                      <option value="">来訪前</option>
                      <option value="" selected="selected">来訪前</option>
                      <option value="">来訪前</option>
                    </select> -->
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    予約日時
                  </div>
                  <div class="form-row--cnt">
                    <div class="d_flex between">
                      <select name="sub01" class="select input type4">
                        <option value="">2021年10月 17日</option>
                        <option value="" selected="selected">2021年10月 17日</option>
                        <option value="">2021年10月 17日</option>
                      </select>
                      <select name="sub02" class="select input type4">
                        <option value="">10:00-10:30</option>
                        <option value="" selected="selected">10:00-10:30</option>
                        <option value="">10:00-10:30</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    予約内容
                  </div>
                  <div class="form-row--cnt">
                    初診 | 内科
                    <!-- <div class="d_flex between">
                      <select name="sub01" class="select input type4">
                        <option value="">初診 | 内科</option>
                        <option value="" selected="selected">初診 | 内科</option>
                        <option value="">初診 | 内科</option>
                      </select>
                    </div> -->
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    備考
                  </div>
                  <div class="form-row--cnt">風邪のような症状が2、3日前から見られます。市販の薬を服用させようと試みましたがすぐに吐き出してしまいます。昨日からはエサに混ぜたら食べるようになりました。 普段から服用している薬はありません。</div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    予約メモ
                  </div>
                  <div class="form-row--cnt">
                    <textarea class="input textarea2" id="msg01" name="msg01" rows="4" cols="50">風邪のような症状が2、3日前から見られます。市販の薬を服用させようと試みましたがすぐに吐き出してしまいます。昨日からはエサに混ぜたら食べるようになりました。 普段から服用している薬はありません。</textarea>
                  </div>
                </div>
              </div>
              <p class="form-row--ttl2 border-bot mgt-30">受診するファミリーの情報</p>
              <div class="form-rowWrap">
                <div class="form-row d-block">
                  <div class="p-pets--item-head">
                    <div class="ttl-name">ソラ</div>
                  </div>       
                  <div class="p-pets--item-cntWrap">
                    <div class="p-pets--item-thumb">
                       <img src="<?php echo $PATH;?>/assets/images/mypage/pet01.png" alt="">
                    </div>
                    <div class="p-pets--item-cnt type2">
                      <div class="_item col-lg-6 col-md-6 col-12">
                        <div class="p-pets--item-row">
                          <div class="label-gray">No.</div>
                          <div class="desc4">1203070</div>
                        </div>
                        <div class="p-pets--item-row">
                          <div class="label-gray">生年月日</div>
                          <div class="desc4">2019年04月10日</div>
                        </div>
                        <div class="p-pets--item-row">
                          <div class="label-gray">年齢</div>
                          <div class="desc4">2歳</div>
                        </div>
                        <div class="p-pets--item-row">
                          <div class="label-gray">種類</div>
                          <div class="desc4">猫 | グレータビー</div>
                        </div>
                        <div class="p-pets--item-row">
                          <div class="label-gray">体重</div>
                          <div class="desc4">6kg</div>
                        </div>
                      </div>
                      <div class="_item col-lg-6 col-md-6 col-12">
                        <div class="p-pets--item-row">
                          <div class="label-gray">去勢・避妊</div>
                          <div class="desc4">済</div>
                        </div>
                        <div class="p-pets--item-row">
                          <div class="label-gray">常用薬</div>
                          <div class="desc4">ドロンタール</div>
                        </div>
                        <div class="p-pets--item-row">
                          <div class="label-gray">常用食</div>
                          <div class="desc4">ヒルズサイエンス</div>
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>
              </div>
              <p class="form-row--ttl2 border-bot mgt-30">オーナー様の情報</p>
              <div class="form-rowWrap">
                <div class="form-row type3">
                  <div class="form-label">
                    名前
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">山田 太郎</p>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    電話番号
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">08012345678</p>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    E-mail
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">mailaddress@mail.com</p>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    備考
                  </div>
                  <div class="form-row--cnt">
                    <textarea class="input textarea2" id="msg01" name="msg01" rows="4" cols="50">カルテNo.1234 2021年7月1日初診。 肝臓病の初期症状が見られたため、サプリメントを処方。 今後は経過観察。</textarea>
                  </div>
                </div>
              </div>
              
              <div class="form-ctrl2">
                <div class="btn-submitWrap">
                  <a href="" class="btn-submit type4">編集を保存する</a>
                </div>
                <div class="form-ctrl2--link">
                  <a href="" class="link-blue">戻る</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>