<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約状況</a></li>
      <li>新規予約登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約状況</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-hospital.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">日時選択</h2>
              <div class="p-reserve--list">
                <div class="calendar">
                  <div class="calendar-head">
                    <a href="javascript:void(0)" class="calendar-head--ctrl">前の月</a>
                    <div class="calendar-head--ttl">2021年8月</div>
                    <a href="javascript:void(0)" class="calendar-head--ctrl">次の月</a>
                  </div>
                  <table class="calendar-table">
                    <thead>
                      <tr>
                        <th class="sunday" scope="col"><span>日</span></th>
                        <th scope="col"><span>月</span></th>
                        <th scope="col"><span>火</span></th>
                        <th scope="col"><span>水</span></th>
                        <th scope="col"><span>木</span></th>
                        <th scope="col"><span>金</span></th>
                        <th class="saturday" scope="col"><span>土</span></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><a href="/hospital/reserve/reservation/day/selection">1</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">2</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">3</a></td>
                        <td class="disable"><a href="">4</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">5</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">6</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">7</a></td>
                      </tr>
                      <tr>
                        <td><a href="/hospital/reserve/reservation/day/selection">8</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">9</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">10</a></td>
                        <td class="disable"><a href="">11</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">12</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">13</a></td>
                        <td class="cancel"><a href="">14</a></td>
                      </tr>
                      <tr>
                        <td><a href="/hospital/reserve/reservation/day/selection">15</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">16</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">17</a></td>
                        <td class="disable"><a href="">18</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">19</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">20</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">21</a></td>
                      </tr>
                      <tr>
                        <td><a href="/hospital/reserve/reservation/day/selection">22</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">23</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">24</a></td>
                        <td class="disable"><a href="">25</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">26</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">27</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">28</a></td>
                      </tr>
                      <tr>
                        <td><a href="/hospital/reserve/reservation/day/selection">29</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">30</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">31</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">1</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">2</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">3</a></td>
                        <td><a href="/hospital/reserve/reservation/day/selection">4</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="align-center">
                <a href="" class="btn-white3 align-center">前のページへ戻る</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->


<!-- Modal -->
<div class="modal fade" id="modalCancel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content modal-colection">
      <div class="modal-header">
        <p class="modal-header--ttl">診察開始時間選択</p>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <p class="align-center desc6">
        ご希望の時間帯を選択してください。
      </p>
      <div class="modal-colection--list">
        <div class="modal-colection--list-row">
          <div class="modal-colection--list-row-item"><a href="">09:00 ~ 09:30</a></div>
          <div class="modal-colection--list-row-item"><a href="">09:30 ~ 10:00</a></div>
          <div class="modal-colection--list-row-item cancel"><a href="">10:30 ~ 11:00</a></div>
          <div class="modal-colection--list-row-item"><a href="">11:00 ~ 11:30</a></div>
          <div class="modal-colection--list-row-item"><a href="">11:30 ~ 12:00</a></div>
          <div class="modal-colection--list-row-item"><a href="">13:00 ~ 13:30</a></div>
          <div class="modal-colection--list-row-item"><a href="">13:30 ~ 14:00</a></div>
          <div class="modal-colection--list-row-item"><a href="">14:00 ~ 14:30</a></div>
          <div class="modal-colection--list-row-item"><a href="">14:30 ~ 15:00</a></div>
          <div class="modal-colection--list-row-item"><a href="">15:00 ~ 15:30</a></div>
          <div class="modal-colection--list-row-item"><a href="">15:30 ~ 16:00</a></div>
          <div class="modal-colection--list-row-item"><a href="">16:00 ~ 16:30</a></div>
        </div>
      </div>
      <div class="sp-only3">
        <a class="modal-confirm--close" data-bs-dismiss="modal">閉じる</a>
      </div>
    </div>
  </div>
</div>

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>