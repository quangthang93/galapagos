<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li>マイページ</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">マイページ</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-hospital.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-history">
            <a href="" class="p-history--signup btn-blue">新規予約登録</a>
            <div class="p-edit--dateWrap">
              <span class="label-gray">予約受付日</span>
              <span class="desc4">2021年10月01日</span>
            </div>
            <form class="form">
              <p class="form-row--ttl2 border-bot mgt-30">予約情報</p>
              <div class="form-rowWrap">
                <div class="form-row type3">
                  <div class="form-label">
                    メニュー
                  </div>
                  <div class="form-row--cnt">
                    <select name="sub01" class="select input type3">
                      <option value="">来訪前</option>
                      <option value="" selected="selected">来訪前</option>
                      <option value="">来訪前</option>
                    </select>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    日付
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">2021年10月1日(月)</p>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    診察開始時間
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">09:00 ~ 09:30</p>
                  </div>
                </div>
              </div>
              <p class="form-row--ttl2 border-bot mgt-30">オーナー様の情報</p>
              <div class="form-rowWrap">
                <div class="form-row type3">
                  <div class="form-label">
                    名前
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">山田 太郎</p>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    電話番号
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">08012345678</p>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    E-mail
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">mailaddress@mail.com</p>
                  </div>
                </div>
              </div>
              <p class="form-row--ttl2 border-bot mgt-30">受診するファミリーの情報</p>
              <div class="form-rowWrap">
                <div class="form-row type3">
                  <div class="form-label">
                    名前
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">ムギ</p>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    種類
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">猫｜アメリカンショートヘアー</p>
                  </div>
                </div>
                <div class="form-row type3">
                  <div class="form-label">
                    備考
                  </div>
                  <div class="form-row--cnt">
                    <p class="desc4">食が細く、元気がないようです。</p>
                  </div>
                </div>
              </div>
              
              <div class="form-ctrl type2">
                <div class="form-ctrl--item btn-submitWrap">
                  <a href="" class="btn-white3 align-center">前のページへ戻る</a>
                </div>
                <div class="form-ctrl--item btn-submitWrap">
                  <a href="/mypage/edit-infor/completed" class="btn-submit">予約を確定する</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>