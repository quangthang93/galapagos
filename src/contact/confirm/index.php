<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/pre/">トップページ</a></li>
      <li>お問い合わせ</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">お問い合わせ</h2>
      </div>
      <div class="p-contact">
        <div class="contact-content">

          <div class="p-contact__first">
            <p>入力項目をご確認いただき、送信ボタンを押してください。</p>
          </div>
          <div class="v-contact">
          <div class="u-layout-smaller">
            <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
              <form method="post" action="" enctype="multipart/form-data">
                <div class="c-form">
                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お問い合わせ種別</span>
                    </label>
                    <div class="c-form__row__field">
                      当サイトについて <input type="hidden" name="fullname" value="当サイトについて">
                    </div>
                  </div>
                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">氏名</span>
                    </label>
                    <div class="c-form__row__field">
                      山田 太郎 <input type="hidden" name="fullname" value="山田 太郎">
                    </div>
                  </div>
                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">E-mail</span>
                    </label>
                    <div class="c-form__row__field">
                      ex) example@mail.com <input type="hidden" name="fullname" value="ex) example@mail.com">
                    </div>
                  </div>
                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">施設名・団体名</span>
                    </label>
                    <div class="c-form__row__field">
                      日本動物病院 <input type="hidden" name="fullname" value="日本動物病院">
                    </div>
                  </div>
                  <div class="c-form__row">
                    <label for="nickname" class="c-form__row__label">
                      <span class="c-form__row__label__text">お問い合わせ内容</span>
                    </label>
                    <div class="c-form__row__field">
                      この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。 <input type="hidden" name="fullname" value="この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。この文章はダミーです。">
                    </div>
                  </div>
                </div>
                <ul class="c-contact__action">
                  <li><input type="submit" name="submitConfirm" value="戻る" class="btn-white3 c-contact__action__button">
                    </li>
                  <li><input type="submit" name="submitConfirm" value="送信する" class="btn-blue2 c-contact__action__button">
                    </li>
              </li>
                </ul>
                <input type="hidden" id="mw_wp_form_token" name="mw_wp_form_token" value="5f44e21288"><input type="hidden" name="_wp_http_referer" value="/contact/"><input type="hidden" name="mw-wp-form-form-id" value="215"><input type="hidden" name="mw-wp-form-form-verify-token" value="5568ba116390563b279f106b7571c3e8be9dee21">
              </form>
              <!-- end .mw_wp_form -->
            </div>
          </div>
        </div><!-- ./v-contact -->
        </div><!-- ./contact-content -->
      </div>
    </div>
  </div>

  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>