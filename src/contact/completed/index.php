<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/pre/">トップページ</a></li>
      <li>お問い合わせ</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">お問い合わせ</h2>
      </div>
      <div class="p-contact">
        <div class="contact-content">
          <div class="v-contact">
            <div class="u-layout-smaller align-center">
              <p class="p-contact--completed-ttl">お問い合わせが完了しました。</p>
              <p class="p-contact--completed-txt">お問い合わせ完了のご連絡は自動送信メールで配信されます。メールが届かない方は登録したメールアドレスをご確認後、再度お問い合わせください。</p>
              <a href="/" class="btn-blue2 align-center">トップページへ</a>
            </div>
          </div><!-- ./v-contact -->
        </div><!-- ./contact-content -->
      </div>
    </div>
  </div>

  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>