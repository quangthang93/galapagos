<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/pre/">トップページ</a></li>
      <li>お問い合わせ</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">お問い合わせ</h2>
      </div>
      <div class="p-contact">
        <div class="contact-content">

          <div class="p-contact__first">
            <h3>お問い合わせの前に</h3>
            <p>お客さまから寄せられたよくあるご質問を掲載しています。お問い合わせ前にご確認ください。</p>
            <a href="" class="btn-blue5">よくあるご質問</a>
          </div>

          <div class="v-contact">
            <div class="u-layout-smaller">
              <div id="mw_wp_form_mw-wp-form-215" class="mw_wp_form mw_wp_form_input">
                <form method="post" action="" enctype="multipart/form-data">
                  <div class="c-form">
                    <div class="c-form__row"><label for="" class="c-form__row__label"><span class="c-form__row__label__text">種別</span> <span class="c-form__required">※</span> </label>
                      <div class="c-form__row__field">
                        <div class="c-form__radio">
                          <span class="mwform-radio-field horizontal-item">
                            <label>
                              <input type="radio" name="type" value="当サイトの使い方について" class="horizontal-item">
                              <span class="mwform-radio-field-text">当サイトの使い方について</span>
                            </label>
                          </span>
                          <span class="mwform-radio-field horizontal-item">
                            <label>
                              <input type="radio" name="type" value="【飼い主の方】行きつけ店舗の掲載リクエスト" class="horizontal-item">
                              <span class="mwform-radio-field-text">【飼い主の方】行きつけ店舗の掲載リクエスト</span>
                            </label>
                          </span>
                          <span class="mwform-radio-field horizontal-item">
                            <label>
                              <input type="radio" name="type" value="【施設・団体の方】掲載について" class="horizontal-item">
                              <span class="mwform-radio-field-text">【施設・団体の方】掲載について</span>
                            </label>
                          </span>
                          <span class="mwform-radio-field horizontal-item">
                            <label>
                              <input type="radio" name="type" value="当サイト内の改善や追加機能リクエスト" class="horizontal-item">
                              <span class="mwform-radio-field-text">当サイト内の改善や追加機能リクエスト</span>
                            </label>
                          </span>
                          <span class="mwform-radio-field horizontal-item">
                            <label>
                              <input type="radio" name="type" value="その他" class="horizontal-item">
                              <span class="mwform-radio-field-text">その他</span>
                            </label>
                          </span>
                          <span class="mwform-radio-field horizontal-item">
                            <label>
                              <input type="radio" name="type" value="コラム内容リクエスト" class="horizontal-item">
                              <span class="mwform-radio-field-text">コラム内容リクエスト</span>
                            </label>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="c-form__row"><label for="name" class="c-form__row__label"><span class="c-form__row__label__text">氏名</span> <span class="c-form__required">※</span> </label>
                      <p></p>
                      <div class="c-form__row__field"><input type="text" name="name" id="name" class="c-form__input type3" size="60" value="" placeholder="山田 太郎">
                      </div>
                    </div>
                    <div class="c-form__row"><label for="address2" class="c-form__row__label"><span class="c-form__row__label__text">E-mail</span> <span class="c-form__required">※</span> </label>
                      <p></p>
                      <div class="c-form__row__field"><input type="text" name="address2" id="address2" class="c-form__input" size="60" value="" placeholder="ex) example@mail.com">
                      </div>
                    </div>
                    <div class="c-form__row"><label for="phone" class="c-form__row__label"><span class="c-form__row__label__text">E-mail(確認用)</span> <span class="c-form__required">※</span> </label>
                      <p></p>
                      <div class="c-form__row__field"><input type="text" name="phone" id="phone" class="c-form__input" size="60" value="" placeholder="ex) example@mail.com">
                      </div>
                    </div>
                    <div class="c-form__row"><label for="company" class="c-form__row__label"><span class="c-form__row__label__text">施設名・団体名</span><!--<span class="c-form__required">※</span>--></label>
                      <p></p>
                      <div class="c-form__row__field"><input type="text" name="company" id="company" class="c-form__input type2" size="60" value="" placeholder="病院名、サロン等の施設名、団体名をご入力ください。">
                      </div>
                    </div>
                    <div class="c-form__row is-vertical-top"><label for="content" class="c-form__row__label"><span class="c-form__row__label__text">お問い合わせ内容</span><!--<span class="c-form__required">※</span>--></label>
                      <p></p>
                      <div class="c-form__row__field"><textarea name="content" id="content" class="c-form__textarea" cols="50" rows="8" placeholder="お問い合わせ内容をご入力ください。"></textarea>
                      </div>
                    </div>
                  </div>
                  <ul class="c-contact__action">
                    <li><input type="submit" name="submitConfirm" value="確認画面へ" class="btn-blue2 c-contact__action__button">
                    </li>
                  </ul>
                  <input type="hidden" id="mw_wp_form_token" name="mw_wp_form_token" value="5f44e21288"><input type="hidden" name="_wp_http_referer" value="/contact/"><input type="hidden" name="mw-wp-form-form-id" value="215"><input type="hidden" name="mw-wp-form-form-verify-token" value="5568ba116390563b279f106b7571c3e8be9dee21">
                </form>
                <!-- end .mw_wp_form -->
              </div>
            </div>
          </div><!-- ./v-contact -->
        </div><!-- ./contact-content -->
      </div>
    </div>
  </div>

  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>