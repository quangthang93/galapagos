<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/pre/">トップページ</a></li>
      <li><a href="/news/">ガラパゴスからのお知らせ</a></li>
      <li>ポイントキャンペーン実施中</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <!-- <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">お問い合わせ</h2>
      </div> -->
      <div class="p-contact p-news">
        <div class="p-news--dateWrap">
          <span class="date8 mgr-5">2021.07.08</span>
          <!-- <span class="label-gray">キャンペーン</span> -->
          <span class="label-gray pink">キャンペーン</span>
        </div>
        <h1 class="p-news--ttl">ポイントキャンペーン実施中</h1>
        <div class="p-news--img align-center">
          <img src="<?php echo $PATH;?>/assets/images/news/news01.png" alt="">
        </div>
        <div class="no-reset">
          <div class="mgb-25">
            <h2 class="mgb-25">大見出し</h2>
            <b>強調する文字</b>
            <p>普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト</p>
          </div>
          <div class="mgb-30">
            <h3 class="mgb-25">中見出し</h3>
            <p>普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト普通のテキスト</p>
          </div>
          <p class="mgb-10">
            <a>テキストリンク</a> 
          </p>
          <p class="mgb-10">
            <a href=".pdf">テキストリンク</a> 
          </p>
          <p class="mgb-10">
            <a target="_blank" rel="noopener">テキストリンク</a>
          </p>
          <!-- <div class="mgb-10">
            <a href="" class="link-icon2 arrow">テキストリンク</a>
          </div>
          <div class="mgb-10">
            <a href="" class="link-icon2 pdf">テキストリンク</a>
          </div>
          <div>
            <a href="" class="link-icon2 blank">テキストリンク</a>
          </div> -->
        </div>
      </div>
      <div class="p-news--direct">
        <a href="" class="link-blue">一覧へ戻る</a>
      </div>
    </div>
  </div>

  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>