<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/pre/">トップページ</a></li>
      <li>ガラパゴスからのお知らせ</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">ガラパゴスからのお知らせ</h2>
      </div>
      <div class="p-contact p-news">
        <ul class="p-news--list">
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date8">2021.10.12</span>
              <span class="tag3">キャンペーン</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">ポイントキャンペーン実施中</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date8">2021.10.12</span>
              <span class="tag3">サービス</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">ペット手帳サービスはじめました。</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date8">2021.10.12</span>
              <span class="tag3 pink">重要</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">ページリニューアルに伴う、既存口コミ掲載終了のお知らせ</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date8">2021.10.12</span>
              <span class="tag3">キャンペーン</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">ポイントキャンペーン実施中</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date8">2021.10.12</span>
              <span class="tag3">サービス</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">ペット手帳サービスはじめました。</a>
          </li>
          <li class="p-news--item">
            <div class="p-news--item-dateWrap">
              <span class="date8">2021.10.12</span>
              <span class="tag3 pink">重要</span>
            </div>
            <a href="/news/detail" class="p-news--item-ttl link">ページリニューアルに伴う、既存口コミ掲載終了のお知らせ</a>
          </li>
        </ul>
        <div class="p-pagination">
          <p class="p-pagination-label">0,000件中｜1〜30件 表示</p>
          <div class="p-pagination-list">
            <a class="ctrl prev" href=""></a>
            <a class="active" href="">1</a>
            <a href="">2</a>
            <a href="">3</a>
            <a href="">4</a>
            <a href="">5</a>
            <a href="">6</a>
            <div class="p-pagination-spacer">…</div>
            <a href="">12</a>
            <a class="ctrl next" href=""></a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>