<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li>本日の予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約状況</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-history">
            <a href="" class="p-history--signup btn-blue">新規予約登録</a>
            <div class="p-reservation--item mgb-20">
              <h3 class="ttl-bg">2021年07月19日（月）</h3>
              <div class="p-reservation--item-cnt">
                <div class="p-reservation--table pb-20">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="align-left" scope="col">予約時間</th>
                        <th scope="col">全体の予約枠数</th>
                        <th class="t_date" scope="col">残りの予約枠</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>10:00～10:30</td>
                        <td class="align-center">予約 10件</td>
                        <td class="align-center">残り 1枠</td>
                      </tr>
                      <tr>
                        <td>10:30～11:00</td>
                        <td class="align-center">予約 5件</td>
                        <td class="align-center">残り 2枠</td>
                      </tr>
                      <tr>
                        <td>10:00～10:30</td>
                        <td class="align-center">予約 10件</td>
                        <td class="align-center">残り 1枠</td>
                      </tr>
                      <tr>
                        <td>10:30～11:00</td>
                        <td class="align-center">予約 5件</td>
                        <td class="align-center">残り 2枠</td>
                      </tr>
                      <tr>
                        <td>10:00～10:30</td>
                        <td class="align-center">予約 10件</td>
                        <td class="align-center">残り 1枠</td>
                      </tr>
                      <tr>
                        <td>10:30～11:00</td>
                        <td class="align-center">予約 5件</td>
                        <td class="align-center">残り 2枠</td>
                      </tr>
                      <tr>
                        <td>10:00～10:30</td>
                        <td class="align-center">予約 10件</td>
                        <td class="align-center">残り 1枠</td>
                      </tr>
                      <tr>
                        <td>10:30～11:00</td>
                        <td class="align-center">予約 5件</td>
                        <td class="align-center">残り 2枠</td>
                      </tr>
                      <tr>
                        <td>10:00～10:30</td>
                        <td class="align-center">予約 10件</td>
                        <td class="align-center">残り 1枠</td>
                      </tr>
                      <tr>
                        <td>10:30～11:00</td>
                        <td class="align-center">予約 5件</td>
                        <td class="align-center">残り 2枠</td>
                      </tr>
                      <tr>
                        <td>10:00～10:30</td>
                        <td class="align-center">予約 10件</td>
                        <td class="align-center">残り 1枠</td>
                      </tr>
                      <tr>
                        <td>10:30～11:00</td>
                        <td class="align-center">予約 5件</td>
                        <td class="align-center">残り 2枠</td>
                      </tr>
                      <tr>
                        <td>10:00～10:30</td>
                        <td class="align-center">予約 10件</td>
                        <td class="align-center">残り 1枠</td>
                      </tr>
                      <tr>
                        <td>10:30～11:00</td>
                        <td class="align-center">予約 5件</td>
                        <td class="align-center">残り 2枠</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
            <div class="p-reservation--item mgb-20">
              <h3 class="ttl-bg">予約を検索する</h3>
              <div class="p-reservation--item-cnt type2">
                <form action="">
                  <div class="d_flex center p-20-20 mgb-30">
                    <select name="sub01" class="select input type5">
                      <option value="">日付から選ぶ</option>
                      <option value="" selected="selected">日付から選ぶ</option>
                      <option value="">日付から選ぶ</option>
                    </select>
                    <span class="pc-only3">&nbsp;&nbsp;&nbsp;</span>
                    <select name="sub01" class="select input type5">
                      <option value="">予約内容から選ぶ</option>
                      <option value="" selected="selected">予約内容から選ぶ</option>
                      <option value="">予約内容から選ぶ</option>
                    </select>
                  </div>
                  <div class="align-center">
                    <a href="" class="btn-search"><span>検索</span></a>
                  </div>
                </form>

                <p class="p-reservation--count mgt-40 mgb-20">
                <span>2,345</span>件
                </p>
                <div class="p-reservation--table">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">日時</th>
                        <th scope="col">オーナー様</th>
                        <th scope="col">予約内容</th>
                        <th scope="col">予約受付日</th>
                        <th scope="col">ステータス</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr class="txt-blue">
                        <td>2021年04月03日10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>初診　眼科</td>
                        <td>2021年07月01日 10:30</td>
                        <td>キャンセル</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>予防接種・各種検査　混合ワクチン接種</td>
                        <td>2021年07月01日 10:30</td>
                        <td>来訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>予防接種・各種検査　混合ワクチン接種</td>
                        <td>2021年07月01日 10:30</td>
                        <td>来訪前</td>
                      </tr>
                      <tr class="txt-blue">
                        <td>2021年04月03日10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>初診　眼科</td>
                        <td>2021年07月01日 10:30</td>
                        <td>キャンセル</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>予防接種・各種検査　混合ワクチン接種</td>
                        <td>2021年07月01日 10:30</td>
                        <td>来訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>予防接種・各種検査　混合ワクチン接種</td>
                        <td>2021年07月01日 10:30</td>
                        <td>来訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>初診　眼科</td>
                        <td>2021年07月01日 10:30</td>
                        <td>キャンセル</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="p-pagination">
                    <p class="p-pagination-label">0,000件中｜1〜30件 表示</p>
                    <div class="p-pagination-list">
                      <a class="ctrl prev" href=""></a>
                      <a class="active" href="">1</a>
                      <a href="">2</a>
                      <a href="">3</a>
                      <a href="">4</a>
                      <a href="">5</a>
                      <a href="">6</a>
                      <div class="p-pagination-spacer">…</div>
                      <a href="">12</a>
                      <a class="ctrl next" href=""></a>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>