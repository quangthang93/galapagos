<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li>本日の予約</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約状況</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-history">
            <a href="" class="p-history--signup btn-blue">新規予約登録</a>
            <div class="p-reservation--item mgb-20">
              <h3 class="ttl-bg">10:00～10:30</h3>
              <div class="p-reservation--item-cnt">
                <div class="p-reservation--table">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">オーナー様</th>
                        <th scope="col">ペット</th>
                        <th scope="col">予約内容</th>
                        <th class="pc-only3" scope="col">予約受付日</th>
                        <th scope="col">ステータス</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>鈴木　和夫様</td>
                        <td>ムギ（2歳）</td>
                        <td>カット＆シャンプー（短毛猫種）</td>
                        <td class="pc-only3">2021年07月01日 10:30</td>
                        <td>キャンセル</td>
                      </tr>
                      <tr>
                        <td>鈴木　和夫様</td>
                        <td>ソラ（6歳）</td>
                        <td>カット＆シャンプー（長毛猫種）</td>
                        <td class="pc-only3">2021年07月01日 10:30</td>
                        <td>来訪前</td>
                      </tr>
                      <tr class="txt-blue">
                        <td>佐々木　美子様</td>
                        <td>だいふく（6歳）</td>
                        <td>平日限定超得◎癒*カット＋フルカラー</td>
                        <td class="pc-only3">2021年07月01日 10:30</td>
                        <td>来訪前</td>
                      </tr>
                      <tr class="txt-blue">
                        <td>吉田　友美様</td>
                        <td>だいふく（6歳）</td>
                        <td>平日限定超得◎癒*カット＋フルカラー</td>
                        <td class="pc-only3">2021年07月01日 10:30</td>
                        <td>来訪前</td>
                      </tr>
                      <tr>
                        <td>鈴木　和夫様</td>
                        <td>ソラ（6歳）</td>
                        <td>カット＆シャンプー（長毛猫種）</td>
                        <td class="pc-only3">2021年07月01日 10:30</td>
                        <td>来訪前</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="align-center mt-50 pb-60">
                    <a href="" class="btn-white3 align-center">前のページへ戻る</a>
                  </div>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>