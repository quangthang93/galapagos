<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約状況</a></li>
      <li>新規予約登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約状況</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">メニュー選択</h2>
              <h3 class="ttl-bg2 mt-40">選択中のメニュー</h3>
              <div class="p-reserve--list pb-0">
                <div class="p-reserve--item">
                  <div class="p-reserve--item-head">
                    <p class="ttl-bold8 type2">カット＆シャンプー（短毛犬種）</p>
                    <div class="p-reserve--item-head-infor">
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>60</span>
                          分〜
                        </div>
                      </div>
                      <div class="p-reserve--item-head-infor-child">
                        <span class="label-time">目安時間</span>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,650</span>
                          円
                        </div>
                      </div>
                    </div>
                  </div>
                  <p class="desc6">短毛犬種のカット、シャンプーの基本コースです。爪切り・足裏バリカン・足まわりカット・耳掃除・肛門腺絞り＋全身のカットが含まれます。</p>
                </div>
              </div>
              <h3 class="ttl-bg2 mt-40">オプション</h3>
              <div class="p-salon--options">
                <ul class="p-salon--options-list">
                  <li class="p-salon--options-item">
                    <div class="p-salon--options-item-cnt">
                      <div class="p-salon--options-item-head">
                        <h3 class="ttl-bold8 type3">トリートメント</h3>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>1,000</span>
                          円
                        </div>
                      </div>
                      <p class="desc6">軽めのダメージに作用するクイックタイプのトリートメントです！※薬剤使用施術のみ併用可＊カットやカラーやパーマと一緒にぜひ＊</p>
                    </div>
                    <div class="p-salon--options-item-cbox">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox01" type="checkbox" value="value1">
                        <label for="cbox01"></label>
                      </div>
                    </div>
                  </li>
                  <li class="p-salon--options-item">
                    <div class="p-salon--options-item-cnt">
                      <div class="p-salon--options-item-head">
                        <h3 class="ttl-bold8 type3">リラックスヘッドスパ</h3>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>2,000</span>
                          円
                        </div>
                      </div>
                      <p class="desc6">ナノイオンのスチームを当てる２STEPトリートメント＊しっとり系が好きな方に！！※トリートメントのみはSB代+￥1000</p>
                    </div>
                    <div class="p-salon--options-item-cbox">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox02" type="checkbox" value="value1">
                        <label for="cbox02"></label>
                      </div>
                    </div>
                  </li>
                  <li class="p-salon--options-item">
                    <div class="p-salon--options-item-cnt">
                      <div class="p-salon--options-item-head">
                        <h3 class="ttl-bold8 type3">炭酸ムーススパ </h3>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>3,000</span>
                          円
                        </div>
                      </div>
                      <p class="desc6">オーガニック系のサラッとしたトリートメントです。ボリュームを落としたくない方に＊＊※トリートメントのみの場合はSB＋￥1000</p>
                    </div>
                    <div class="p-salon--options-item-cbox">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox03" type="checkbox" value="value1">
                        <label for="cbox03"></label>
                      </div>
                    </div>
                  </li>
                  <li class="p-salon--options-item">
                    <div class="p-salon--options-item-cnt">
                      <div class="p-salon--options-item-head">
                        <h3 class="ttl-bold8 type3">全身マッサージ</h3>
                        <div class="p-reserve--item-head-infor-child-val">
                          <span>4,000</span>
                          円
                        </div>
                      </div>
                      <p class="desc6">有名メーカーミルボンの人気トリートメント☆しっかり水分補給！多毛乾燥毛の方にオススメ◎※トリートメントのみはSB代+￥1000</p>
                    </div>
                    <div class="p-salon--options-item-cbox">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox04" type="checkbox" value="value1">
                        <label for="cbox04"></label>
                      </div>
                    </div>
                  </li>
                </ul>
              </div>
              <div class="form-ctrl type2">
                <div class="form-ctrl--item btn-submitWrap">
                  <a href="" class="btn-white3 align-center">前のページへ戻る</a>
                </div>
                <div class="form-ctrl--item btn-submitWrap">
                  <a href="/mypage/edit-infor/completed" class="btn-submit">予約日時を選ぶ</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>