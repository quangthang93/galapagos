<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約状況</a></li>
      <li>新規予約登録</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">本日の予約</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reserve">
            <div class="p-reserve--steps">
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">1</span>
                <p class="p-reserve--steps-col-label">メニュー<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">2</span>
                <p class="p-reserve--steps-col-label">日時<br class="sp-only3">選択</p>
              </div>
              <div class="p-reserve--steps-col active">
                <span class="p-reserve--steps-col-number">3</span>
                <p class="p-reserve--steps-col-label">情報<br class="sp-only3">入力</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">4</span>
                <p class="p-reserve--steps-col-label">内容<br class="sp-only3">確認</p>
              </div>
              <div class="p-reserve--steps-col">
                <span class="p-reserve--steps-col-number">5</span>
                <p class="p-reserve--steps-col-label">予約<br class="sp-only3">完了</p>
              </div>
            </div>
            <div class="p-reserve--cnt">
              <h2 class="p-reserve--ttl">情報入力</h2>
              <div class="p-reserve--list p-edit">
                <form class="form">
                  <p class="form-row--ttl border-bot">オーナー様の情報</p>
                  <div class="form-row">
                    <div class="form-label">
                      利用歴
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-radio type2">
                        <span class="form-radio--item type2">
                          <label>
                            <input type="radio" name="cb01" value="初めて" checked>
                            <span class="form-radio--label">初めて</span>
                          </label>
                        </span>
                        <span class="form-radio--item type2">
                          <label>
                            <input type="radio" name="cb01" value="過去に利用したことがある">
                            <span>過去に利用したことがある</span>
                          </label>
                        </span>
                      </div>
                      <div class="mt-10">
                        <a href="/salon/reserve/reservation/enter-information/search" class="form-row--upload-btn btn-blue">利用者情報から選ぶ</a>
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      名前
                      <span class="c-form__required">※</span>
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="text" name="name" class="input" placeholder="本名でご予約ください">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      電話番号
                      <span class="c-form__required">※</span>
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="text" name="name" class="input" placeholder="入力してください">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      E-mail
                      <span class="c-form__required">※</span>
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="email" name="name" class="input" placeholder="入力してください">
                      </div>
                    </div>
                  </div>
                  <p class="form-row--ttl type2">受診するファミリーの情報</p>
                  <div class="form-row border-top">
                    <div class="form-label">
                      名前
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-input">
                        <input type="text" name="name" class="input" placeholder="入力してください">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      種類
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-groupField--item">
                        <div class="form-radio">
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="犬" checked>
                              <span class="form-radio--label">犬</span>
                            </label>
                          </span>
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="猫">
                              <span>猫</span>
                            </label>
                          </span>
                        </div>
                      </div>
                      <div class="form-groupField--item">
                        <div class="form-radio">
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="うさぎ" checked>
                              <span class="form-radio--label">うさぎ</span>
                            </label>
                          </span>
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="フェレット">
                              <span>フェレット</span>
                            </label>
                          </span>
                        </div>
                      </div>
                      <div class="form-groupField--item">
                        <div class="form-radio">
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="鳥類" checked>
                              <span class="form-radio--label">鳥類</span>
                            </label>
                          </span>
                          <span class="form-radio--item">
                            <label>
                              <input type="radio" name="sex" value="その他">
                              <span>その他</span>
                            </label>
                          </span>
                        </div>
                      </div>
                      <div class="form-groupField--item">
                        <p class="desc5 mgb-5">その他の詳細（任意）</p>
                        <input type="text" name="name" class="input" placeholder="品種や詳細を入力してください。">
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      性別
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-radio type2">
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="オス" checked>
                            <span class="form-radio--label">オス</span>
                          </label>
                        </span>
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="メス">
                            <span>メス</span>
                          </label>
                        </span>
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="分からない">
                            <span>分からない</span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">
                      去勢・避妊
                    </div>
                    <div class="form-row--cnt">
                      <div class="form-radio type2">
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="未" checked>
                            <span class="form-radio--label">未</span>
                          </label>
                        </span>
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="済">
                            <span>済</span>
                          </label>
                        </span>
                        <span class="form-radio--item">
                          <label>
                            <input type="radio" name="cb01" value="分からない">
                            <span>分からない</span>
                          </label>
                        </span>
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-label">備考</div>
                    <div class="form-row--cnt">
                      <textarea name="content" id="content" class="c-form__textarea" cols="50" rows="8" placeholder="ペットの症状や普段服用している薬などありましたらご記入ください。"></textarea>
                    </div>
                  </div>
                  <div class="form-ctrl type2">
                    <div class="form-ctrl--item btn-submitWrap">
                      <a href="" class="btn-white3 align-center">前のページへ戻る</a>
                    </div>
                    <div class="form-ctrl--item btn-submitWrap">
                      <a href="/search/confirm-information/" class="btn-submit">入力情報を確認する</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- Modal -->
<div class="modal fade" id="modalCancel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered type2">
    <div class="modal-content modal-colection">
      <div class="modal-header">
        <p class="modal-header--ttl">利用者情報から選ぶ</p>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-form">
        <div class="mb-10">
          <div class="container6">
            <form class="form-search">
              <input class="input hasBtn" type="" name="" placeholder="キーワードで検索する">
              <input class="input-submit" type="summit" value="">
            </form>
            <div class="form-search02">
              <div class="form-search02--input">
                <select name="sub01" class="select input type9 mb-10">
                  <option value="">メニューから選ぶ</option>
                  <option value="" selected="selected">メニューから選ぶ</option>
                  <option value="">メニューから選ぶ</option>
                </select>
                <select name="sub01" class="select input type9">
                  <option value="">動物種から選ぶ</option>
                  <option value="" selected="selected">動物種から選ぶ</option>
                  <option value="">動物種から選ぶ</option>
                </select>
              </div>
              <div class="form form-search02--inner">
                <div class="form-search02--label">最終利用日時から選ぶ</div>
                <div class="form-search02--cnt">
                  <div class="form-input">
                    <div class="form-radio">
                      <span class="form-radio--item type2">
                        <label>
                          <input type="radio" name="sex" value="日付を指定">
                          <span>日付を指定</span>
                        </label>
                      </span>
                    </div>
                    <input readonly="readonly" class="input _date js-datepicker01" type="text" value="2021/12/13">
                  </div>
                </div>
              </div>
              <div class="align-center">
                <a href="" class="btn-search"><span>検索</span></a>
              </div>
            </div>
          </div>
        </div>
        <div class="mgb-20">
          <div class="pt-20 pb-20">
            <div class="p-reservation--countWrap">
              <p class="p-reservation--count">
                <span>2,345</span>件
              </p>
            </div>
            <div class="p-reservation--table tableWrap2">
              <table class="table">
                <thead>
                  <tr>
                    <th class="t_cbox p-0"></th>
                    <th class="align-left" scope="col">No.</th>
                    <th scope="col">オーナー様</th>
                    <th scope="col">ペット</th>
                    <th scope="col">利用回数</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td class="t_cbox align-right">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox01" type="checkbox" value="value1">
                        <label for="cbox01"></label>
                      </div>
                    </td>
                    <td class="align-left">10001</td>
                    <td class="align-center">山田　花子 様</td>
                    <td>犬　キャバリア</td>
                    <td class="align-center">1回</td>
                  </tr>
                  <tr>
                    <td class="t_cbox align-right">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox02" type="checkbox" value="value1">
                        <label for="cbox02"></label>
                      </div>
                    </td>
                    <td class="align-left">10002</td>
                    <td class="align-center">山田　花子 様</td>
                    <td>犬　キャバリア</td>
                    <td class="align-center">1回</td>
                  </tr>
                  <tr>
                    <td class="t_cbox align-right">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox03" type="checkbox" value="value1">
                        <label for="cbox03"></label>
                      </div>
                    </td>
                    <td class="align-left">10003</td>
                    <td class="align-center">山田　花子 様</td>
                    <td>犬　キャバリア</td>
                    <td class="align-center">1回</td>
                  </tr>
                  <tr>
                    <td class="t_cbox align-right">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox04" type="checkbox" value="value1">
                        <label for="cbox04"></label>
                      </div>
                    </td>
                    <td class="align-left">10003</td>
                    <td class="align-center">山田　花子 様</td>
                    <td>犬　キャバリア</td>
                    <td class="align-center">1回</td>
                  </tr>
                  <tr>
                    <td class="t_cbox align-right">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox05" type="checkbox" value="value1">
                        <label for="cbox05"></label>
                      </div>
                    </td>
                    <td class="align-left">10003</td>
                    <td class="align-center">山田　花子 様</td>
                    <td>犬　キャバリア</td>
                    <td class="align-center">1回</td>
                  </tr>
                  <tr>
                    <td class="t_cbox align-right">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox06" type="checkbox" value="value1">
                        <label for="cbox06"></label>
                      </div>
                    </td>
                    <td class="align-left">10003</td>
                    <td class="align-center">山田　花子 様</td>
                    <td>犬　キャバリア</td>
                    <td class="align-center">2回</td>
                  </tr>
                  <tr>
                    <td class="t_cbox align-right">
                      <div class="checkboxWrap">
                        <input class="checkbox2 js-cbox" id="cbox07" type="checkbox" value="value1">
                        <label for="cbox07"></label>
                      </div>
                    </td>
                    <td class="align-left">10003</td>
                    <td class="align-center">山田　花子 様</td>
                    <td>犬　キャバリア</td>
                    <td class="align-center">2回</td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="align-center mt-30 mb-20">
              <a href="" class="btn-blue2"><span>選択した情報を入力する</span></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>