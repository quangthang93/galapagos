<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li>予約確認・履歴</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約確認・履歴</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-hospitalList">
            <div class="p-reservation--item mb-0">
              <div class="p-reservation--item-cnt mt-0 not-table type3">
                <h3 class="p-hospitalList--ttl ttl-bold3 mb-20">予約を検索する</h3>
                <div class="form-searchWrap">
                  <div class="container6">
                    <form class="form-search">
                      <input class="input hasBtn" type="" name="" placeholder="キーワードで検索する">
                      <input class="input-submit" type="summit" value="">
                    </form>
                  </div>
                </div>
                <div class="container6">
                  <form class="form" action="">
                    <div class="d_flex center mgb-30">
                      <select name="sub01" class="select input type5">
                        <option value="">日付から選ぶ</option>
                        <option value="" selected="selected">日付から選ぶ</option>
                        <option value="">日付から選ぶ</option>
                      </select>
                      <span class="pc-only3">&nbsp;&nbsp;&nbsp;</span>
                      <select name="sub01" class="select input type5">
                        <option value="">予約内容から選ぶ</option>
                        <option value="" selected="selected">予約内容から選ぶ</option>
                        <option value="">予約内容から選ぶ</option>
                      </select>
                    </div>
                  
                    <div class="align-center">
                      <a href="" class="btn-search type2"><span>検索</span></a>
                    </div>
                  </form>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
            <div class="p-reservation--item mgb-20">
              <div class="p-reservation--item-cnt mt-0 pt-20 pb-20">

                <div class="p-reservation--countWrap">
                  <p class="p-reservation--count">
                  <span>2,345</span>件
                  </p>
                </div>
                <div class="p-reservation--table">
                  <table class="table">
                    <thead>
                      <tr>
                        <th scope="col">日時</th>
                        <th scope="col">オーナー様</th>
                        <th scope="col">ペット</th>
                        <th scope="col">予約内容</th>
                        <th class="t_think pc-only3" scope="col">備考</th>
                        <th class="pc-only3" scope="col">ステータス</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3">風のような症状が2、3日前からあります。薬も飲ませましたが症…</td>
                        <td class="pc-only3 align-center">未訪前</td>
                      </tr>
                      <tr class="txt-blue">
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3"></td>
                        <td class="pc-only3 align-center">キャンセル</td>
                      </tr>
                      <tr class="txt-blue">
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3">以前から口臭が気になり、歯磨きをしっかり行っていますが、匂…</td>
                        <td class="pc-only3 align-center">未訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3">風のような症状が2、3日前からあります。薬も飲ませましたが症…</td>
                        <td class="pc-only3 align-center">未訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3">風のような症状が2、3日前からあります。薬も飲ませましたが症…</td>
                        <td class="pc-only3 align-center">未訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3">風のような症状が2、3日前からあります。薬も飲ませましたが症…</td>
                        <td class="pc-only3 align-center">未訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3">風のような症状が2、3日前からあります。薬も飲ませましたが症…</td>
                        <td class="pc-only3 align-center">未訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3">風のような症状が2、3日前からあります。薬も飲ませましたが症…</td>
                        <td class="pc-only3 align-center">未訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3">風のような症状が2、3日前からあります。薬も飲ませましたが症…</td>
                        <td class="pc-only3 align-center">未訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3">風のような症状が2、3日前からあります。薬も飲ませましたが症…</td>
                        <td class="pc-only3 align-center">未訪前</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日<br>10:00～10:30</td>
                        <td>山田　花子様</td>
                        <td>ムギ（2歳）</td>
                        <td>初診　眼科</td>
                        <td class="pc-only3">風のような症状が2、3日前からあります。薬も飲ませましたが症…</td>
                        <td class="pc-only3 align-center">未訪前</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="p-pagination mt-40">
                <p class="p-pagination-label">0,000件中｜1〜30件 表示</p>
                <div class="p-pagination-list">
                  <a class="ctrl prev" href=""></a>
                  <a class="active" href="">1</a>
                  <a href="">2</a>
                  <a href="">3</a>
                  <a href="">4</a>
                  <a href="">5</a>
                  <a href="">6</a>
                  <div class="p-pagination-spacer">…</div>
                  <a href="">12</a>
                  <a class="ctrl next" href=""></a>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>