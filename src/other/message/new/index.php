<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">メッセージ</a></li>
      <li>新規メッセージ作成</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">新規メッセージ作成</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-history pdt-40">
            <form class="form">
              <div class="form-row type2">
                <div class="form-label">宛先</div>
                <div class="form-row--cnt">
                  <div class="form-itemRadioInput">
                    <input type="text" name="name" class="input type7" placeholder="患者の名前かカユーザーナンバーを入力してください。" value="">
                    <div class="form-radio">
                      <span class="form-radio--item">
                        <label>
                          <input type="radio" name="type" value="全ての患者">
                          <span class="form-radio--label">全ての患者</span>
                        </label>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row type2">
                <div class="form-label">件名</div>
                <div class="form-row--cnt">
                  <input type="text" name="name" class="input" placeholder="Re:" value="">
                </div>
              </div>
              <div class="form-row type2 align-top">
                <div class="form-label">本文</div>
                <div class="form-row--cnt">
                  <textarea class="input textarea2" name="" id="" cols="30" rows="10" placeholder="入力してください"></textarea>
                </div>
              </div>
              <div class="form-ctrl2">
                <div class="btn-submitWrap">
                  <a href="" class="btn-submit type4">送信する</a>
                </div>
                <div class="form-ctrl2--link">
                  <a href="" class="link-blue">戻る</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>