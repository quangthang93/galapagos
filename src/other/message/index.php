<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>

    <main class="main mypage">
    	<div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>マイページ</li>
        </ul>
      </div>
      <div class="p-mypage">
    		<div class="container3">
          <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
          <div class="p-mypage--ttlWrap">
            <h2 class="p-mypage--ttl">マイページ</h2>
            <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
          </div>
    			<div class="p-mypage--inner">
    				<div class="p-mypage--sidebar">
    					<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
    				</div>
    				<div class="p-mypage--cnt">
    					<ul class="p-mypage--cnt-features">
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a href="">
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-mail01.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">新規メッセージ作成</h3>
                  </a>      
                </li>
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a class="countWrap" href="">
                    <span class="count">01</span>
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-mail02.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">受信メッセージ</h3>
                  </a>      
                </li>
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a href="">
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-mail03.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">送信メッセージ</h3>
                  </a>      
                </li>
    					</ul>
    				</div>
    			</div>
    		</div>
      </div>
    </main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>