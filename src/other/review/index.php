<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li>レビュー</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">レビュー</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-hospitalReview">
            <div class="p-edit--dateWrap">
              <span class="label-gray">登録日</span>
              <span class="desc4">2021年10月01日</span>
            </div>
            <form class="form">
              <div class="p-hospitalReview--head">
                <h3 class="ttl-bold4">総合評価</h3>
                <div class="p-hospitalReview--head-info">
                  <div class="p-hospitalReview--head-rating">
                    <div class="p-hospitalReview--head-rating-stars">
                      <span>
                        <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-big.svg" alt="">
                      </span>
                      <span>
                        <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-big.svg" alt="">
                      </span>
                      <span>
                        <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-big.svg" alt="">
                      </span>
                      <span>
                        <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-big.svg" alt="">
                      </span>
                      <span>
                        <img src="<?php echo $PATH;?>/assets/images/common/star-white-big.svg" alt="">
                      </span>
                    </div>
                    <div class="p-hospitalReview--head-rating-points">
                      4.0 / 5.0
                    </div>
                  </div>
                  <div class="p-hospitalReview--head-count">80件のレビュー</div>
                </div>
                <div class="p-hospitalReview--head-chart">
                  <div class="p-hospitalReview--head-chart-row">
                    <p class="p-hospitalReview--head-chart-row-label">星5つ</p>
                    <div class="p-hospitalReview--head-chart-row-bar">
                      <span style="width: 100%"></span>
                    </div>
                  </div>
                  <div class="p-hospitalReview--head-chart-row">
                    <p class="p-hospitalReview--head-chart-row-label">星4つ</p>
                    <div class="p-hospitalReview--head-chart-row-bar">
                      <span style="width: 70%"></span>
                    </div>
                  </div>
                  <div class="p-hospitalReview--head-chart-row">
                    <p class="p-hospitalReview--head-chart-row-label">星3つ</p>
                    <div class="p-hospitalReview--head-chart-row-bar">
                      <span style="width: 80%"></span>
                    </div>
                  </div>
                  <div class="p-hospitalReview--head-chart-row">
                    <p class="p-hospitalReview--head-chart-row-label">星2つ</p>
                    <div class="p-hospitalReview--head-chart-row-bar">
                      <span style="width: 50%"></span>
                    </div>
                  </div>
                  <div class="p-hospitalReview--head-chart-row">
                    <p class="p-hospitalReview--head-chart-row-label">星1つ</p>
                    <div class="p-hospitalReview--head-chart-row-bar">
                      <span style="width: 20%"></span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="p-hospitalReview--listWrap">
                <div class="p-hospitalReview--heading3Wrap">
                  <h3 class="p-hospitalReview--heading3">口コミ</h3>
                  <span class="_count">32</span>
                </div>
                <div class="p-hospitalReview--list">
                  <div class="p-hospitalReview--item">
                    <div class="p-hospitalReview--item-head">
                      <p class="txt-bold2">ニックネーム</p>
                      <div class="p-hospitalReview--item-head-rating">
                        <div class="p-hospitalReview--item-head-rating-stars">
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-white-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-white-small.svg" alt="">
                          </span>
                        </div>
                        <div class="p-hospitalReview--item-head-rating-points">
                          3.2
                        </div>
                      </div>
                    </div>
                    <div class="p-hospitalReview--item-cnt">
                      <div class="p-hospitalReview--item-desc">
                        知人からの評判通り、院長先生が丁寧に診察してくださいました。 <br>初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。 <br>受付の方の対応も良かったですよ。
                      </div>
                      <div class="p-hospitalReview--item-dateWrap">
                        <div class="div p-hospitalReview--item-dateWrap-child">
                          <span class="label-gray">種類</span>
                          <span class="desc4">犬 | チワワ</span>
                        </div>
                        <div class="div p-hospitalReview--item-dateWrap-child">
                          <span class="label-gray">診察日</span>
                          <span class="desc4">2021年5月2日</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="p-hospitalReview--item">
                    <div class="p-hospitalReview--item-head">
                      <p class="txt-bold2">ニックネーム</p>
                      <div class="p-hospitalReview--item-head-rating">
                        <div class="p-hospitalReview--item-head-rating-stars">
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-white-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-white-small.svg" alt="">
                          </span>
                        </div>
                        <div class="p-hospitalReview--item-head-rating-points">
                          3.2
                        </div>
                      </div>
                    </div>
                    <div class="p-hospitalReview--item-cnt">
                      <div class="p-hospitalReview--item-desc">ワクチンを受けるために受診させていただきました。受付の時に犬を飼うことが初めてなので…と伝えると親切丁寧に教えていただきました。とても優しい先生で、我が家のワンチャンも落ち着いて受診とワクチン接種を受けることができました。</div>
                      <div class="p-hospitalReview--item-dateWrap">
                        <div class="div p-hospitalReview--item-dateWrap-child">
                          <span class="label-gray">種類</span>
                          <span class="desc4">犬 | チワワ</span>
                        </div>
                        <div class="div p-hospitalReview--item-dateWrap-child">
                          <span class="label-gray">診察日</span>
                          <span class="desc4">2021年5月2日</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="p-hospitalReview--item">
                    <div class="p-hospitalReview--item-head">
                      <p class="txt-bold2">ニックネーム</p>
                      <div class="p-hospitalReview--item-head-rating">
                        <div class="p-hospitalReview--item-head-rating-stars">
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-yellow-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-white-small.svg" alt="">
                          </span>
                          <span>
                            <img src="<?php echo $PATH;?>/assets/images/common/star-white-small.svg" alt="">
                          </span>
                        </div>
                        <div class="p-hospitalReview--item-head-rating-points">
                          3.2
                        </div>
                      </div>
                    </div>
                    <div class="p-hospitalReview--item-cnt">
                      <div class="p-hospitalReview--item-desc">うちは14歳の犬を飼っており急に餌を食べなくなって困っていました。 <br>フードを変えたり、温めてみたり試行錯誤をしながらやっと食べさせている状態でした。どこか悪いのかもしれないと心配した家内が、知り合いからこの病院を紹介してもらい動物病院に連れて行きました。</div>
                      <div class="p-hospitalReview--item-dateWrap">
                        <div class="div p-hospitalReview--item-dateWrap-child">
                          <span class="label-gray">種類</span>
                          <span class="desc4">犬 | チワワ</span>
                        </div>
                        <div class="div p-hospitalReview--item-dateWrap-child">
                          <span class="label-gray">診察日</span>
                          <span class="desc4">2021年5月2日</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-ctrl2--link align-center">
                <a href="" class="link-blue">レビューをもっと見る</a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>