<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li>送信メール設定</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">送信メール設定</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-favorite">
            <div class="tabs">
              <div class="tabs-navWrapper">
                <ul class="tabs-nav js-tabsNav">
                  <li class="tabs-item type3"><a href="/hospital/mail/completed/">予約完了メール</a></li>
                  <li class="tabs-item type3 active"><a href="/hospital/mail/remaind/">リマインド<br>メール</a></li>
                  <li class="tabs-item type3"><a href="/hospital/mail/thanks/">お礼メール</a></li>
                </ul>
              </div>
              <div class="tabs-cnt type2 type3">
                <div class="tabs-panel">
                  <div class="p-edit pdt-40">
                    <form class="form">
                      <div class="form-row">
                        <div class="form-label">タイトル</div>
                        <div class="form-row--cnt">
                          <div class="form-input">
                            <input type="text" name="name" class="input" value="" placeholder="">
                          </div>
                        </div>
                      </div>
                      <div class="form-row">
                        <div class="form-label">本文</div>
                        <div class="form-row--cnt">
                          <div class="form-input">
                            <textarea class="input textarea3" name="" id="" cols="30" rows="80"></textarea>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="form-ctrl2">
                    <div class="btn-submitWrap">
                      <a href="" class="btn-submit type4">編集を保存する</a>
                    </div>
                    <div class="form-ctrl2--link">
                      <a href="" class="link-blue">マイページトップへ戻る</a>
                    </div>
                  </div>
                </div>
                <!-- <div class="tabs-panel js-tabsPanel">tab2</div>
                <div class="tabs-panel js-tabsPanel">tab3</div> -->
              </div>
            </div>
          </div>
        </div><!-- ./p-reservation -->
      </div>
    </div>
  </div>
  </div>
</main><!-- ./main -->
<!-- Javascript -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>