<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約設定</a></li>
      <li>受付設定</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">受付設定</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-history pdt-40">
            <form class="form">
              <div class="form-row">
                <div class="form-label">営業曜日</div>
                <div class="form-row--cnt">
                  <div class="list-cbox">
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox01" type="checkbox" value="value1">
                      <label for="cbox01">月曜日</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox02" type="checkbox" value="value1">
                      <label for="cbox02">火曜日</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox03" type="checkbox" value="value1">
                      <label for="cbox03">水曜日</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox04" type="checkbox" value="value1">
                      <label for="cbox04">木曜日</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox05" type="checkbox" value="value1">
                      <label for="cbox05">金曜日</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox06" type="checkbox" value="value1">
                      <label for="cbox06">土曜日</label>
                    </div>
                    <div class="checkboxWrap">
                      <input class="checkbox3 js-cbox" id="cbox07" type="checkbox" value="value1">
                      <label for="cbox07">日曜日</label>
                    </div>
                  </div>
                </div>
              </div>
              <p class="form-row--ttl3 border-bot mgt-30">通常営業時間</p>
              <div class="form-row border-bottom-none">
                <div class="form-label">営業時間</div>
                <div class="form-row--cnt">
                  <div class="form-groupFieldWrap">
                    <div class="form-groupField">
                      <div class="form-groupField--item">
                        <select name="product" class="select input">
                          <option value="" selected="selected">09</option>
                          <option value="">10</option>
                          <option value="">11</option>
                          <option value="">12</option>
                        </select>
                      </div>
                      <span class="_2dots">:</span>
                      <div class="form-groupField--item">
                        <select name="product" class="select input">
                          <option value="" selected="selected">00</option>
                          <option value="">30</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-groupFieldWrap--connect">〜</div>
                    <div class="form-groupField">
                      <div class="form-groupField--item">
                        <select name="product" class="select input">
                          <option value="" selected="selected">18</option>
                          <option value="">19</option>
                          <option value="">20</option>
                        </select>
                        <span class="_2dots">:</span>
                      </div>
                      <div class="form-groupField--item">
                        <select name="product" class="select input">
                          <option value="" selected="selected">00</option>
                          <option value="">30</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">休憩時間</div>
                <div class="form-row--cnt">
                  <div class="form-groupFieldWrap">
                    <div class="form-groupField">
                      <div class="form-groupField--item">
                        <select name="product" class="select input">
                          <option value="" selected="selected">12</option>
                          <option value="">13</option>
                          <option value="">14</option>
                          <option value="">15</option>
                        </select>
                      </div>
                      <span class="_2dots">:</span>
                      <div class="form-groupField--item">
                        <select name="product" class="select input">
                          <option value="" selected="selected">00</option>
                          <option value="">30</option>
                        </select>
                      </div>
                    </div>
                    <div class="form-groupFieldWrap--connect">〜</div>
                    <div class="form-groupField">
                      <div class="form-groupField--item">
                        <select name="product" class="select input">
                          <option value="">10</option>
                          <option value="">11</option>
                          <option value="">12</option>
                          <option value="" selected="selected">13</option>
                        </select>
                        <span class="_2dots">:</span>
                      </div>
                      <div class="form-groupField--item">
                        <select name="product" class="select input">
                          <option value="" selected="selected">00</option>
                          <option value="">30</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <p class="form-row--ttl3 border-bot mgt-30">曜日ごとの営業時間</p>
              <div class="form-rowWrap2">
                <div class="form-row">
                  <div class="form-label">曜日</div>
                  <div class="form-row--cnt">
                    <div class="list-cbox">
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02-1" type="checkbox" value="value1">
                        <label for="cbox02-1">月曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02-2" type="checkbox" value="value1">
                        <label for="cbox02-2">火曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02-3" type="checkbox" value="value1">
                        <label for="cbox02-3">水曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02-4" type="checkbox" value="value1">
                        <label for="cbox02-4">木曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02-5" type="checkbox" value="value1">
                        <label for="cbox02-5">金曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02-6" type="checkbox" value="value1">
                        <label for="cbox02-6">土曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02-7" type="checkbox" value="value1">
                        <label for="cbox02-7">日曜日</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-label">営業時間</div>
                  <div class="form-row--cnt">
                    <div class="form-groupFieldWrap">
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select input">
                            <option value="" selected="selected">09</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                          </select>
                        </div>
                        <span class="_2dots">:</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select input">
                            <option value="" selected="selected">00</option>
                            <option value="">30</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-groupFieldWrap--connect">〜</div>
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select input">
                            <option value="" selected="selected">18</option>
                            <option value="">19</option>
                            <option value="">20</option>
                          </select>
                          <span class="_2dots">:</span>
                        </div>
                        <div class="form-groupField--item">
                          <select name="product" class="select input">
                            <option value="" selected="selected">00</option>
                            <option value="">30</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="form-row">
                  <div class="form-label">休憩時間</div>
                  <div class="form-row--cnt">
                    <div class="form-groupFieldWrap">
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select input">
                            <option value="" selected="selected">12</option>
                            <option value="">13</option>
                            <option value="">14</option>
                            <option value="">15</option>
                          </select>
                        </div>
                        <span class="_2dots">:</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select input">
                            <option value="" selected="selected">00</option>
                            <option value="">30</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-groupFieldWrap--connect">〜</div>
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select input">
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                            <option value="" selected="selected">13</option>
                          </select>
                          <span class="_2dots">:</span>
                        </div>
                        <div class="form-groupField--item">
                          <select name="product" class="select input">
                            <option value="" selected="selected">00</option>
                            <option value="">30</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="btn-del2Wrap align-center mgb-20">
                  <a href="javascript:void(0)" class="btn-del2">
                    <span>削除</span>
                  </a>
                </div>
              </div>
              <div class="align-center mgt-15">
                <a href="javascript:void(0)" class="btn-add2 js-btnAdd"></a>
              </div>
              <div class="form-ctrl2">
                <div class="btn-submitWrap">
                  <a href="" class="btn-submit type4">登録する</a>
                </div>
                <div class="form-ctrl2--link">
                  <a href="" class="link-blue">戻る</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>