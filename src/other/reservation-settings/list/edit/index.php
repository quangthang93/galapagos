<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li><a href="/">予約設定</a></li>
      <li>予約メニュー編集</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約メニュー編集</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt type2">
          <div class="p-edit p-history pdt-40">
            <form class="form border-top-none">
              <div class="form-row border-top type3">
                <div class="form-label">カテゴリ</div>
                <div class="form-row--cnt">
                  <select name="sub01" class="select input type2">
                    <option value="">選択してください</option>
                    <option value="" selected="selected">初診</option>
                    <option value="">選択してください</option>
                  </select>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">メニュー名</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" value="混合ワクチン接種" placeholder="入力してください">
                  </div>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">メニュー 説明</div>
                <div class="form-row--cnt">
                  <textarea name="content" id="content" class="c-form__textarea" cols="50" rows="8" placeholder="入力してください">病気の治療のためには知識を身につけておくことがとても大切なのです。病気のことやケア方法などを知っておきましょう。定期的な検診も受け付けています。</textarea>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">所要時間</div>
                <div class="form-row--cnt">
                  <div class="form-groupField">
                    <select name="product" class="select type3 input">
                      <option value="">_</option>
                      <option value="">_</option>
                      <option value="" selected="selected">_</option>
                      <option value="">2022</option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">予約枠</div>
                <div class="form-row--cnt">
                  <p class="desc5 mb-10">同一時間内に、受け付けられる件数</p>
                  <select name="product" class="select type3 input">
                    <option value="">_</option>
                    <option value="">_</option>
                    <option value="" selected="selected">1</option>
                    <option value="">_</option>
                  </select>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">料金</div>
                <div class="form-row--cnt">
                  <div class="form-input type-btn">
                    <div class="_num"><input type="text" name="name" class="input" value="5,500" placeholder="入力してください"></div>
                    <div class="_unit"><input type="text" name="name" class="input mgl-10" value="" placeholder="円(税込)〜"></div>
                  </div>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">受付時間</div>
                <div class="form-row--cnt">
                  <div class="form-radio">
                    <span class="form-radio--item type2">
                      <label>
                        <input type="radio" name="type" value="営業時間内すべて">
                        <span class="form-radio--label">営業時間内すべて</span>
                      </label>
                    </span>
                    <span class="form-radio--item type2">
                      <label>
                        <input type="radio" name="type" value="特定の曜日、時間" checked>
                        <span>特定の曜日、時間</span>
                      </label>
                    </span>
                  </div>
                </div>
              </div>
              <div class="form border-top-none border-bottom pb-20">
                <div class="form-rowWrap2 mt-0">
                  <div class="form-row border-bottom-none">
                    <div class="list-cbox type4">
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox01" type="checkbox" value="value1">
                        <label for="cbox01">月曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox02" type="checkbox" value="value1">
                        <label for="cbox02">火曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox03" type="checkbox" value="value1">
                        <label for="cbox03">水曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox04" type="checkbox" value="value1">
                        <label for="cbox04">木曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox05" type="checkbox" value="value1">
                        <label for="cbox05">金曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox06" type="checkbox" value="value1">
                        <label for="cbox06">土曜日</label>
                      </div>
                      <div class="checkboxWrap">
                        <input class="checkbox3 js-cbox" id="cbox07" type="checkbox" value="value1">
                        <label for="cbox07">日曜日</label>
                      </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-groupFieldWrap type3">
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">00</option>
                            <option value="">10</option>
                            <option value="">11</option>
                            <option value="">12</option>
                          </select>
                        </div>
                        <span class="_2dots">:</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">00</option>
                            <option value="">30</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-groupFieldWrap--connect type2">〜</div>
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">00</option>
                            <option value="">19</option>
                            <option value="">20</option>
                          </select>
                          <span class="_2dots">:</span>
                        </div>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">00</option>
                            <option value="">30</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="btn-del2Wrap align-center mgb-20">
                    <a href="javascript:void(0)" class="btn-del2">
                      <span>削除</span>
                    </a>
                  </div>
                </div>
                <div class="align-center mgt-15">
                  <a href="javascript:void(0)" class="btn-add2 js-btnAdd"></a>
                </div>
              </div>
              <div class="form-row type3">
                <div class="form-label">掲載期間</div>
                <div class="form-row--cnt">
                  <div class="form-radio">
                    <span class="form-radio--item type2">
                      <label>
                        <input type="radio" name="type" value="常に掲載">
                        <span class="form-radio--label">常に掲載</span>
                      </label>
                    </span>
                    <span class="form-radio--item type2">
                      <label>
                        <input type="radio" name="type" value="特定の期間" checked>
                        <span>特定の期間</span>
                      </label>
                    </span>
                  </div>
                </div>
              </div>
              <div class="form border-top-none">
                <div class="form-rowWrap2 mt-0">
                  <div class="form-row">
                    <div class="form-groupFieldWrap type4">
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">2021</option>
                            <option value="">2022</option>
                          </select>
                        </div>
                        <span class="_2dots">年</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">月</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">日</span>
                      </div>
                      <div class="form-groupFieldWrap--connect">〜</div>
                      <div class="form-groupField">
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">2021</option>
                            <option value="">2022</option>
                          </select>
                        </div>
                        <span class="_2dots">年</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">月</span>
                        <div class="form-groupField--item">
                          <select name="product" class="select small input">
                            <option value="" selected="selected">01</option>
                            <option value="">02</option>
                          </select>
                        </div>
                        <span class="_2dots">日</span>
                      </div>
                    </div>
                  </div>
                  <div class="btn-del2Wrap align-center mgb-20">
                    <a href="javascript:void(0)" class="btn-del2">
                      <span>削除</span>
                    </a>
                  </div>
                </div>
                <div class="align-center mgt-15">
                  <a href="javascript:void(0)" class="btn-add2 js-btnAdd"></a>
                </div>
              </div>
              <div class="form-row type3 border-top mt-20">
                <div class="form-label">ステータス</div>
                <div class="form-row--cnt">
                  <div class="form-radio">
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="掲載前">
                        <span class="form-radio--label">掲載前</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="掲載中" checked>
                        <span>掲載中</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="掲載終了" checked>
                        <span>掲載終了</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="非表示" checked>
                        <span>非表示</span>
                      </label>
                    </span>
                  </div>
                </div>
              </div>
              <div class="form border-top-none">
                <div class="form-ctrl2">
                  <div class="btn-submitWrap">
                    <a href="" class="btn-submit type4">保存する</a>
                  </div>
                  <div class="mt-30 align-center">
                    <a href="javascript:void(0)" class="btn-del2">
                      <span>削除</span>
                    </a>
                  </div>
                  <div class="mt-30">
                    <a href="" class="link-blue">戻る</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>