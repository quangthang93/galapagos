<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/">マイページ</a></li>
      <li>飼い主情報</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">飼い主情報</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-hospitalList">
            <div class="p-reservation--item p-edit p-message mgb-20">
              <div class="p-reservation--item-cnt mt-0">
                <form class="form border-top-none">
                  <p class="form-row--ttl2 border-bot mt-10">オーナー様の情報</p>
                  <div class="form-rowWrap">
                    <div class="form-row type3">
                      <div class="form-label">
                        名前
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">山田 太郎</p>
                      </div>
                    </div>
                    <div class="form-row type3">
                      <div class="form-label">
                        電話番号
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">08012345678</p>
                      </div>
                    </div>
                    <div class="form-row type3">
                      <div class="form-label">
                        E-mail
                      </div>
                      <div class="form-row--cnt">
                        <p class="desc4">mailaddress@mail.com</p>
                      </div>
                    </div>
                  </div>
                  <p class="form-row--ttl2 border-bot mt-40">オーナー様の情報</p>
                  <div class="form-rowWrap">
                    <div class="form-row d-block">
                      <div class="p-pets--item-head">
                        <div class="ttl-name">ソラ</div>
                      </div>
                      <div class="p-pets--item-cntWrap">
                        <div class="p-pets--item-thumb">
                          <img src="<?php echo $PATH;?>/assets/images/mypage/pet01.png" alt="">
                        </div>
                        <div class="p-pets--item-cnt type2">
                          <div class="_item col-lg-6 col-md-6 col-12">
                            <div class="p-pets--item-row">
                              <div class="label-gray">No.</div>
                              <div class="desc4">1203070</div>
                            </div>
                            <div class="p-pets--item-row">
                              <div class="label-gray">登録日</div>
                              <div class="desc4">2021年10月01日</div>
                            </div>
                            <div class="p-pets--item-row">
                              <div class="label-gray">年齢</div>
                              <div class="desc4">2歳</div>
                            </div>
                            <div class="p-pets--item-row">
                              <div class="label-gray">種類</div>
                              <div class="desc4">猫 | グレータビー</div>
                            </div>
                            <div class="p-pets--item-row">
                              <div class="label-gray">体重</div>
                              <div class="desc4">6kg</div>
                            </div>
                          </div>
                          <div class="_item col-lg-6 col-md-6 col-12">
                            <div class="p-pets--item-row">
                              <div class="label-gray">去勢・避妊</div>
                              <div class="desc4">済</div>
                            </div>
                            <div class="p-pets--item-row">
                              <div class="label-gray">常用薬</div>
                              <div class="desc4">ドロンタール</div>
                            </div>
                            <div class="p-pets--item-row">
                              <div class="label-gray">常用食</div>
                              <div class="desc4">ヒルズサイエンス</div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-row">
                      <div class="form-label">
                        飼い主メモ
                      </div>
                      <div class="form-row--cnt">
                        <textarea class="input textarea2" id="msg01" name="msg01" rows="4" cols="50">カルテNo.1234 2021年7月1日初診。 肝臓病の初期症状が見られたため、サプリメントを処方。 今後は経過観察。</textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-ctrl2">
                    <div class="btn-submitWrap">
                      <a href="" class="btn-submit type4">編集を保存する</a>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            <div class="p-reservation--item mgb-20">
              <h3 class="ttl-bg">利用履歴</h3>
              <div class="p-reservation--item-cnt pt-20 pb-20">
                <div class="p-reservation--countWrap">
                  <p class="p-reservation--count">
                    <span>3</span>件
                  </p>
                </div>
                <div class="p-reservation--table">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="t_date align-left" scope="col">最終ご利用日</th>
                        <th class="align-left" scope="col">利用内容</th>
                        <th scope="col">ステータス</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>2021年10月17日</td>
                        <td class="align-left">再診　内科</td>
                        <td class="align-center">来訪済</td>
                      </tr>
                      <tr>
                        <td>2021年10月17日</td>
                        <td class="align-left">再診　内科</td>
                        <td class="align-center">来訪済</td>
                      </tr>
                      <tr>
                        <td>2021年10月17日</td>
                        <td class="align-left">再診　内科</td>
                        <td class="align-center">来訪済</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->


<!-- modal box -->
  <div class="modal-complete">
    <div class="modal-complete--cnt">
      <div class="modal-complete--cnt-inner">
        <div class="modal-complete--cnt-icon"><img src="<?php echo $PATH;?>/assets/images/common/icon-check.svg" alt=""></div>
        <p class="modal-complete--cnt-msg">編集を<br>保存しました。</p>
      </div>
    </div>
  </div>
<!-- ./modal box -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>