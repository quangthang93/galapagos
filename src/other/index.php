<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header4.php'; ?>

    <main class="main mypage">
    	<div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li>マイページ</li>
        </ul>
      </div>
      <div class="p-mypage">
    		<div class="container3">
          <div class="sidebar--name sp-only">soup*spoon<span> さん</span></div>
          <div class="p-mypage--ttlWrap">
            <h2 class="p-mypage--ttl">マイページ</h2>
            <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
          </div>
    			<div class="p-mypage--inner">
    				<div class="p-mypage--sidebar">
    					<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar-salon.php'; ?>
    				</div>
    				<div class="p-mypage--cnt">
    					<ul class="p-mypage--cnt-features">
    						<li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a href="">
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-calendar02.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">予約状況</h3>
                  </a>      
                </li>
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a href="">
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-calendar.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">予約確認・履歴</h3>
                  </a>      
                </li>
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a href="">
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-calendar03.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">予約設定</h3>
                  </a>      
                </li>
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a href="">
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-send.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">送信メール設定</h3>
                  </a>      
                </li>
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a href="">
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-people.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">登録情報編集</h3>
                  </a>      
                </li>
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a href="">
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-building2.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">ペット施設情報編集</h3>
                  </a>      
                </li>
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a class="countWrap" href="">
                    <span class="count">01</span>
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-mail.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">メッセージ</h3>
                  </a>      
                </li>
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a href="">
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-chat.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">レビュー</h3>
                  </a>      
                </li>
                <li class="p-mypage--cnt-features-item col-lg-3 col-md-6 col-6">
                  <a href="">
                    <div class="p-mypage--cnt-features-icon">
                      <img src="<?php echo $PATH;?>/assets/images/mypage/icon-health2.svg" alt="">
                    </div>
                    <h3 class="p-mypage--cnt-features-ttl">飼い主情報</h3>
                  </a>      
                </li>
    					</ul>
    				</div>
    			</div>
    		</div>
      </div>
    </main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>