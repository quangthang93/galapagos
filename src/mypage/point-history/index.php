<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li>ポイント履歴</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">ポイント履歴</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reservation">
            <div class="p-reservation--pointsBox">
              <div class="p-reservation--pointsBox-inner">
                <p class="txt-bold2 align-center">これまで寄付したポイント</p>
                <div class="p-reservation--pointsBox-points align-center">
                  <p class="p-reservation--pointsBox-points-value">800 <span>P</span></p>
                </div>
                <p class="desc6">寄附とお心寄せをいただきまして誠にありがとうございます。</p>
              </div>
            </div>
            <div class="p-reservation--item">
              <h3 class="ttl-bg">ポイント履歴一覧</h3>
              <div class="p-reservation--item-cnt">
                <div class="p-reservation--table">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="t_date" scope="col">日付</th>
                        <th class="t_name" scope="col">施設・団体名</th>
                        <th class="" scope="col">ご利用</th>
                        <th class="t_points" scope="col">ポイント</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>2021年04月03日</td>
                        <td>アニマルサロンひまわり</td>
                        <td class="align-center">
                          <a href="javascript:void(0)" class="btn-like type2 dis">レビュー投稿</a>
                        </td>
                        <td class="align-center">+30pt</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日</td>
                        <td>BiBi犬猫病院</td>
                        <td class="align-center">
                          <a href="javascript:void(0)" class="btn-like type2">ギフト寄付</a>
                        </td>
                        <td class="align-center">-30pt</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日</td>
                        <td>アニマルサロンひまわり</td>
                        <td class="align-center">
                          <a href="javascript:void(0)" class="btn-like type2 dis">レビュー投稿</a>
                        </td>
                        <td class="align-center">+30pt</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日</td>
                        <td>BiBi犬猫病院</td>
                        <td class="align-center">
                          <a href="javascript:void(0)" class="btn-like type2">ギフト寄付</a>
                        </td>
                        <td class="align-center">-30pt</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日</td>
                        <td>アニマルサロンひまわり</td>
                        <td class="align-center">
                          <a href="javascript:void(0)" class="btn-like type2 dis">レビュー投稿</a>
                        </td>
                        <td class="align-center">+30pt</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日</td>
                        <td>BiBi犬猫病院</td>
                        <td class="align-center">
                          <a href="javascript:void(0)" class="btn-like type2">ギフト寄付</a>
                        </td>
                        <td class="align-center">-30pt</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日</td>
                        <td>アニマルサロンひまわり</td>
                        <td class="align-center">
                          <a href="javascript:void(0)" class="btn-like type2 dis">レビュー投稿</a>
                        </td>
                        <td class="align-center">+30pt</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日</td>
                        <td>BiBi犬猫病院</td>
                        <td class="align-center">
                          <a href="javascript:void(0)" class="btn-like type2">ギフト寄付</a>
                        </td>
                        <td class="align-center">-30pt</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日</td>
                        <td>アニマルサロンひまわり</td>
                        <td class="align-center">
                          <a href="javascript:void(0)" class="btn-like type2 dis">レビュー投稿</a>
                        </td>
                        <td class="align-center">+30pt</td>
                      </tr>
                      <tr>
                        <td>2021年04月03日</td>
                        <td>BiBi犬猫病院</td>
                        <td class="align-center">
                          <a href="javascript:void(0)" class="btn-like type2">ギフト寄付</a>
                        </td>
                        <td class="align-center">-30pt</td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="p-reservation--viewmore align-center">
                    <a href="" class="link-blue">もっと見る</a>
                  </div>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
          </div><!-- ./p-reservation -->
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>