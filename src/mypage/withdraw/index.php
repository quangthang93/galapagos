<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>

    <main class="main mypage">
    	<div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/mypage">マイページ</a></li>
          <li>退会</li>
        </ul>
      </div>
      <div class="p-mypage">
    		<div class="container3">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
          <div class="p-mypage--ttlWrap">
            <h2 class="p-mypage--ttl">退会</h2>
            <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
          </div>
    			<div class="p-mypage--inner">
    				<div class="p-mypage--sidebar">
    					<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
    				</div>
    				<div class="p-mypage--cnt">
    					<div class="p-edit p-withdraw">
    						<div class="p-noResult">
                  <h3 class="ttl-bold3">一度退会すると、<br class="sp-only3">ご登録いただいたデータは全て消去し、<br>元には戻せません。</h3>
                  <p class="txt-alert">現在ご登録いただいているメールアドレスでの<br class="sp-only3">再登録もできません。</p>
                  <div class="p-withdraw--points">
                    <div class="p-withdraw--points-label">失効ポイント</div>
                    <div class="p-withdraw--points-value">1040<span> P</span></div>
                  </div>
                  <div class="btn-blueWrap">
                    <a href="" class="btn-blue2">退会する</a>
                  </div>
                  <div class="">
                    <a href="/mypage" class="link-blue">マイページトップに戻る</a>
                  </div>
                </div>
    					</div>
    				</div>
    			</div>
    		</div>
      </div>
    </main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>