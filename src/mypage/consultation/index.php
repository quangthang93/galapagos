<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li>ご相談内容</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">ご相談内容</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-consultation">
            <div class="p-edit--dateWrap">
              <span class="tag2">犬/あそび・散歩</span>
              <span class="date-icon">2021年10月01日</span>
            </div>
            <div class="p-consultation--cnt">
              <div class="p-consultation--quest">
                <div class="p-consultation--quest-box desc6">臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満気味です。 散歩以外で運動って何かありますか？</div>
              </div>
              <div class="p-consultation--ansWrap">
                <p class="p-consultation--ans-ttl ttl-bold3">獣医師からの回答</p>
                <div class="p-consultation--ans">
                  <div class="p-consultation--ans-box desc6">
                    無理にリードを引っ張らない 散歩が苦手な犬にとっては、リードを強く引っ張られることで嫌な思いをし、さらに散歩に対して悪い印象をもってしまいます。 歩かないときは、愛犬が歩き出すまで待って、また立ち止まったら歩き出すのを待つ、ということを繰り返しましょう。
                  </div>
                </div>
              </div>
              <div class="p-consultation--review align-center">
                <a href="javascript:void(0)" class="btn-like"><span>参考になった</span></a>
              </div>
            </div>
            <div class="p-consultation--direct">
              <a href="" class="link-blue">相談履歴一覧へ戻る</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>