<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li>ご相談内容</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">ご相談内容</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-consultation">
            <div class="p-edit--dateWrap">
              <span class="tag2">犬/あそび・散歩</span>
              <span class="date-icon">2021年10月01日</span>
            </div>
            <div class="p-consultation--cnt">
              <div class="p-consultation--quest">
                <div class="p-consultation--quest-box desc6">臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満気味です。 散歩以外で運動って何かありますか？</div>
              </div>
              <div class="p-consultation--ansWrap">
                <p class="p-consultation--ans-ttl ttl-bold3">獣医師からの回答</p>
                <div class="p-consultation--ans">
                  <div class="p-consultation--ans-box desc6">
                    無理にリードを引っ張らない 散歩が苦手な犬にとっては、リードを強く引っ張られることで嫌な思いをし、さらに散歩に対して悪い印象をもってしまいます。 歩かないときは、愛犬が歩き出すまで待って、また立ち止まったら歩き出すのを待つ、ということを繰り返しましょう。
                  </div>
                </div>
              </div>
              <div class="p-consultation--review align-center">
                <a href="javascript:void(0)" class="link-blue" data-bs-toggle="modal" data-bs-target="#modalReview">この回答を評価する</a>
              </div>
            </div>
            <div class="p-consultation--direct">
              <a href="" class="link-blue">相談履歴一覧へ戻る</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->

<!-- Modal -->
<div class="modal fade" id="modalCancel" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content modal-confirm">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <p class="modal-confirm--ttl">評価ありがとうございます。</p>
      <div class="p-withdraw--points type2">
        <div class="p-withdraw--points-label">失効ポイント</div>
        <div class="p-withdraw--points-value">5<span> P</span></div>
      </div>
      <div class="mgt-30 mgb-20">
        <a class="modal-confirm--close" data-bs-dismiss="modal">閉じる</a>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalReview" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content modal-confirm">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <p class="modal-confirm--msg">この相談の回答について<br>評価をお願いします。</p>
      <div class="align-center mgb-10">
        <a href="/mypage/consultation/review-completed" class="btn-blue">参考になった</a>
      </div>
      <div class="align-center mgb-10">
        <a href="/mypage/consultation/review-completed" class="btn-white">参考にならなかった</a>        
      </div>
      <div class="mgb-20">
        <a class="modal-confirm--close" data-bs-dismiss="modal">今は評価しない</a>
      </div>
    </div>
  </div>
</div>

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>