<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li>レビュー</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">レビュー</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-review">
            <div class="p-reservation--detail-head">
              <a href="" class="btn-blue4">新しくレビュー投稿する</a>
              <span class="txt-guide link check js-favoriteCheck" data-txt="選択した相談履歴を削除する">すべてを選択する</span>
            </div>
            <ul class="p-review--list js-favoriteList">
              <li class="p-review--item">
                <div class="p-review--item-head">
                  <div class="ttl-rating">BiBi犬猫病院</div>
                  <div class="checkboxWrap">
                    <input class="checkbox2 js-cbox" id="cbox01" type="checkbox" value="value1">
                    <label for="cbox01"></label>
                  </div>
                </div>       
                <div class="p-review--item-cntWrap">
                  <div class="p-review--item-cnt col-lg-6 col-md-6 col-12">
                    <div class="p-review--item-row">
                      <div class="label-gray">投稿日</div>
                      <div class="desc4">2021年5月17日</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">獣医師の対応</div>
                      <div class="desc4">十分に話を聞いてくれた</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">スタッフの対応</div>
                      <div class="desc4">明るい</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">料金</div>
                      <div class="desc4">普通</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">施設</div>
                      <div class="desc4">キレイ</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">総合評価</div>
                      <div class="desc4">満足した</div>
                    </div>
                  </div>
                  <div class="p-review--item-comment col-lg-6 col-md-6 col-12">
                    <div class="p-review--item-comment-box">
                      <p class="ttl-bold5">コメント</p>
                      <p class="desc4">知人からの評判通り、院長先生が丁寧に診察してくださいました。初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。受付の方の対応も良かったです。</p>
                    </div>
                  </div>
                </div>             
              </li>
              <li class="p-review--item">
                <div class="p-review--item-head">
                  <div class="ttl-rating">BiBi犬猫病院</div>
                  <div class="checkboxWrap">
                    <input class="checkbox2 js-cbox" id="cbox02" type="checkbox" value="value1">
                    <label for="cbox02"></label>
                  </div>
                </div>       
                <div class="p-review--item-cntWrap">
                  <div class="p-review--item-cnt col-lg-6 col-md-6 col-12">
                    <div class="p-review--item-row">
                      <div class="label-gray">投稿日</div>
                      <div class="desc4">2021年5月17日</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">獣医師の対応</div>
                      <div class="desc4">十分に話を聞いてくれた</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">スタッフの対応</div>
                      <div class="desc4">明るい</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">料金</div>
                      <div class="desc4">普通</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">施設</div>
                      <div class="desc4">キレイ</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">総合評価</div>
                      <div class="desc4">満足した</div>
                    </div>
                  </div>
                  <div class="p-review--item-comment col-lg-6 col-md-6 col-12">
                    <div class="p-review--item-comment-box">
                      <p class="ttl-bold5">コメント</p>
                      <p class="desc4">知人からの評判通り、院長先生が丁寧に診察してくださいました。初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。受付の方の対応も良かったです。</p>
                    </div>
                  </div>
                </div>             
              </li>
              <li class="p-review--item">
                <div class="p-review--item-head">
                  <div class="ttl-rating">BiBi犬猫病院</div>
                  <div class="checkboxWrap">
                    <input class="checkbox2 js-cbox" id="cbox03" type="checkbox" value="value1">
                    <label for="cbox03"></label>
                  </div>
                </div>       
                <div class="p-review--item-cntWrap">
                  <div class="p-review--item-cnt col-lg-6 col-md-6 col-12">
                    <div class="p-review--item-row">
                      <div class="label-gray">投稿日</div>
                      <div class="desc4">2021年5月17日</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">獣医師の対応</div>
                      <div class="desc4">十分に話を聞いてくれた</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">スタッフの対応</div>
                      <div class="desc4">明るい</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">料金</div>
                      <div class="desc4">普通</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">施設</div>
                      <div class="desc4">キレイ</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">総合評価</div>
                      <div class="desc4">満足した</div>
                    </div>
                  </div>
                  <div class="p-review--item-comment col-lg-6 col-md-6 col-12">
                    <div class="p-review--item-comment-box">
                      <p class="ttl-bold5">コメント</p>
                      <p class="desc4">知人からの評判通り、院長先生が丁寧に診察してくださいました。初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。受付の方の対応も良かったです。</p>
                    </div>
                  </div>
                </div>             
              </li>
              <li class="p-review--item">
                <div class="p-review--item-head">
                  <div class="ttl-rating">BiBi犬猫病院</div>
                  <div class="checkboxWrap">
                    <input class="checkbox2 js-cbox" id="cbox04" type="checkbox" value="value1">
                    <label for="cbox04"></label>
                  </div>
                </div>       
                <div class="p-review--item-cntWrap">
                  <div class="p-review--item-cnt col-lg-6 col-md-6 col-12">
                    <div class="p-review--item-row">
                      <div class="label-gray">投稿日</div>
                      <div class="desc4">2021年5月17日</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">獣医師の対応</div>
                      <div class="desc4">十分に話を聞いてくれた</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">スタッフの対応</div>
                      <div class="desc4">明るい</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">料金</div>
                      <div class="desc4">普通</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">施設</div>
                      <div class="desc4">キレイ</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">総合評価</div>
                      <div class="desc4">満足した</div>
                    </div>
                  </div>
                  <div class="p-review--item-comment col-lg-6 col-md-6 col-12">
                    <div class="p-review--item-comment-box">
                      <p class="ttl-bold5">コメント</p>
                      <p class="desc4">知人からの評判通り、院長先生が丁寧に診察してくださいました。初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。受付の方の対応も良かったです。</p>
                    </div>
                  </div>
                </div>             
              </li>
              <li class="p-review--item">
                <div class="p-review--item-head">
                  <div class="ttl-rating">BiBi犬猫病院</div>
                  <div class="checkboxWrap">
                    <input class="checkbox2 js-cbox" id="cbox05" type="checkbox" value="value1">
                    <label for="cbox05"></label>
                  </div>
                </div>       
                <div class="p-review--item-cntWrap">
                  <div class="p-review--item-cnt col-lg-6 col-md-6 col-12">
                    <div class="p-review--item-row">
                      <div class="label-gray">投稿日</div>
                      <div class="desc4">2021年5月17日</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">獣医師の対応</div>
                      <div class="desc4">十分に話を聞いてくれた</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">スタッフの対応</div>
                      <div class="desc4">明るい</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">料金</div>
                      <div class="desc4">普通</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">施設</div>
                      <div class="desc4">キレイ</div>
                    </div>
                    <div class="p-review--item-row">
                      <div class="label-gray">総合評価</div>
                      <div class="desc4">満足した</div>
                    </div>
                  </div>
                  <div class="p-review--item-comment col-lg-6 col-md-6 col-12">
                    <div class="p-review--item-comment-box">
                      <p class="ttl-bold5">コメント</p>
                      <p class="desc4">知人からの評判通り、院長先生が丁寧に診察してくださいました。初めて犬を飼うことになったので、不安でいっぱいだったのですが、大したことのない疑問にも丁寧に教えてくださり、ありがたかったです。受付の方の対応も良かったです。</p>
                    </div>
                  </div>
                </div>             
              </li>
            </ul>
            <div class="align-center mgt-40">
              <a href="" class="link-blue">もっと見る</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="p-favorite--ctrl js-favoriteCtrl">
    <div class="p-favorite--ctrl-inner">
      <a href="" class="btn-blue">選択したレビューを削除する</a>
      <span class="link-reset link js-favoriteReset">リセット</span>
    </div>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>