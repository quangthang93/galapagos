<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li>レビュー</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">レビュー</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-review">
            <div class="p-favorite--noresult">
              <h3 class="ttl-bold3">レビュー投稿はありません。</h3>
              <p class="desc6">マイページの予約履歴、<br> またはメッセージからレビュー投稿でポイント獲得。</p>
              <div class="btn-blueWrap">
                <a href="" class="btn-blue2">予約履歴</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="p-favorite--ctrl js-favoriteCtrl">
    <div class="p-favorite--ctrl-inner">
      <a href="" class="btn-blue">選択したレビューを削除する</a>
      <span class="link-reset link js-favoriteReset">リセット</span>
    </div>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>