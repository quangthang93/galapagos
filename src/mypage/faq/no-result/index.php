<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li>ご相談</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">ご相談</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-faq">
            <div class="tabs">
              <div class="tabs-navWrapper">
                <ul class="tabs-nav js-tabsNav">
                  <li class="tabs-item type2">相談履歴</li>
                  <li class="tabs-item type2 active">クリップした他の相談</li>
                </ul>
              </div>
              <div class="tabs-cnt js-tabsCnt">
                <div class="tabs-panel">
                  <!-- <div class="p-faq--cnt"> -->
                    <div class="p-favorite--noresult">
                      <h3 class="ttl-bold3">他の人の相談をクリップしましょう。</h3>
                      <p class="desc6">役に立ったご相談はクリップして<br> いつでも参照できるようにしましょう。</p>
                      <div class="btn-blueWrap">
                        <a href="" class="btn-blue2">他の人の相談を見る</a>
                      </div>
                    </div>
                  <!-- </div> -->
                </div>
              </div>
            </div>
          </div><!-- ./p-reservation -->
        </div>
      </div>
    </div>
  </div>

</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>