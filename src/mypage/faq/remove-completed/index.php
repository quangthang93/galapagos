<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li>ご相談</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">ご相談</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-faq">
            <div class="tabs">
              <div class="tabs-navWrapper">
                <ul class="tabs-nav js-tabsNav">
                  <li class="tabs-item type2">相談履歴</li>
                  <li class="tabs-item type2 active">クリップした他の相談</li>
                </ul>
              </div>
              <div class="tabs-cnt js-tabsCnt">
                <div class="tabs-panel js-tabsPanel">
                  <div class="p-faq--cnt">
                    <ul class="p-faq--list">
                      <li class="p-faq--item">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                            <a href="javascript:void(0)" class="btn-like"><span>参考になった</span></a>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                            <a href="javascript:void(0)" class="btn-like dis"><span>参考にならなかった</span></a>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                            <a href="javascript:void(0)" class="link-blue">この回答を評価する</a>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                            <a href="javascript:void(0)" class="btn-like"><span>参考になった</span></a>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                            <a href="javascript:void(0)" class="btn-like"><span>参考になった</span></a>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                            <a href="javascript:void(0)" class="btn-like"><span>参考になった</span></a>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                            <a href="javascript:void(0)" class="btn-like"><span>参考になった</span></a>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                            <a href="javascript:void(0)" class="btn-like"><span>参考になった</span></a>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <div class="align-center">
                      <a href="" class="link-blue">もっと見る</a>
                    </div>
                  </div>
                </div>
                <div class="tabs-panel js-tabsPanel">
                  <div class="p-faq--cnt">
                    <div class="p-faq--cnt-head">
                      <span class="txt-guide link check js-favoriteCheck" data-txt="選択した相談履歴を削除する">すべてを選択する</span>
                    </div>
                    <ul class="p-faq--list js-favoriteList">
                      <li class="p-faq--item type2">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動し…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                        <div class="p-faq--item-cbox">
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox01" type="checkbox" value="value1">
                            <label for="cbox01"></label>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item type2">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動し…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                        <div class="p-faq--item-cbox">
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox02" type="checkbox" value="value1">
                            <label for="cbox02"></label>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item type2">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動し…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                        <div class="p-faq--item-cbox">
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox03" type="checkbox" value="value1">
                            <label for="cbox03"></label>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item type2">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動し…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                        <div class="p-faq--item-cbox">
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox04" type="checkbox" value="value1">
                            <label for="cbox04"></label>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item type2">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動し…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                        <div class="p-faq--item-cbox">
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox05" type="checkbox" value="value1">
                            <label for="cbox05"></label>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item type2">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動し…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                        <div class="p-faq--item-cbox">
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox06" type="checkbox" value="value1">
                            <label for="cbox06"></label>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item type2">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動し…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                        <div class="p-faq--item-cbox">
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox07" type="checkbox" value="value1">
                            <label for="cbox07"></label>
                          </div>
                        </div>
                      </li>
                      <li class="p-faq--item type2">
                        <div class="p-faq--item-inner">
                          <div class="p-faq--item-tag">
                            <span class="tag2 mgr-20">犬/あそび・散歩</span>
                          </div>
                          <div class="p-faq--item-cnt desc6">
                            臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動し…<a class="link-blue" href="javascript:void(0)">[詳しく見る]</a>
                          </div>
                        </div>
                        <div class="p-faq--item-cbox">
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox08" type="checkbox" value="value1">
                            <label for="cbox08"></label>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <div class="align-center">
                      <a href="" class="link-blue">もっと見る</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div><!-- ./p-reservation -->
        </div>
      </div>
    </div>
  </div>
  <div class="p-favorite--ctrl js-favoriteCtrl">
    <div class="p-favorite--ctrl-inner">
      <a href="" class="btn-blue">クリップした他の相談を削除する</a>
      <span class="link-reset link js-favoriteReset">リセット</span>
    </div>
  </div>

  <!-- modal box -->
  <div class="modal-complete">
    <div class="modal-complete--cnt">
      <div class="modal-complete--cnt-inner">
        <div class="modal-complete--cnt-icon"><img src="<?php echo $PATH;?>/assets/images/common/icon-check.svg" alt=""></div>
        <p class="modal-complete--cnt-msg align-center">クリップした他の相談を<br>削除しました</p>
      </div>
    </div>
  </div>
<!-- ./modal box -->
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>