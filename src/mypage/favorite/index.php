<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li>お気に入り</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">お気に入り</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-favorite">
            <div class="tabs">
              <div class="tabs-navWrapper">
                <ul class="tabs-nav js-tabsNav">
                  <li class="tabs-item js-tabsItem active">動物病院</li>
                  <li class="tabs-item js-tabsItem">トリミング<br class="sp-only3">サロン</li>
                  <li class="tabs-item js-tabsItem">その他施設</li>
                </ul>
              </div>
              <div class="tabs-cnt js-tabsCnt">
                <div class="tabs-panel js-tabsPanel">
                  <div class="p-favorite--guide">
                    <span class="txt-guide link check js-favoriteCheck" data-txt="選択したお気に入りを削除する">すべてを選択する</span>
                  </div>
                  <ul class="p-favorite--list js-favoriteList row">
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストは...</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox01" type="checkbox" value="value1">
                            <label for="cbox01"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox02" type="checkbox" value="value1">
                            <label for="cbox02"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox03" type="checkbox" value="value1">
                            <label for="cbox03"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox04" type="checkbox" value="value1">
                            <label for="cbox04"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox05" type="checkbox" value="value1">
                            <label for="cbox05"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox06" type="checkbox" value="value1">
                            <label for="cbox06"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox07" type="checkbox" value="value1">
                            <label for="cbox07"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox08" type="checkbox" value="value1">
                            <label for="cbox08"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox09" type="checkbox" value="value1">
                            <label for="cbox09"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox10" type="checkbox" value="value1">
                            <label for="cbox10"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox11" type="checkbox" value="value1">
                            <label for="cbox11"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox12" type="checkbox" value="value1">
                            <label for="cbox12"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="tabs-panel js-tabsPanel">
                  <div class="p-favorite--guide">
                    <span class="txt-guide link check js-favoriteCheck">すべてを選択する</span>
                  </div>
                  <ul class="p-favorite--list js-favoriteList row">
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox02" type="checkbox" value="value1">
                            <label for="cbox02"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox03" type="checkbox" value="value1">
                            <label for="cbox03"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox11" type="checkbox" value="value1">
                            <label for="cbox11"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox12" type="checkbox" value="value1">
                            <label for="cbox12"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
                <div class="tabs-panel js-tabsPanel">
                  <div class="p-favorite--guide">
                    <span class="txt-guide link check js-favoriteCheck">すべてを選択する</span>
                  </div>
                  <ul class="p-favorite--list js-favoriteList row">
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox01" type="checkbox" value="value1">
                            <label for="cbox01"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                    <li class="p-favorite--item col-lg-3 col-md-6 col-6">
                      <div class="p-favorite--item-inner">
                        <div class="thumb">
                          <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/favorite01.png" alt="">
                        </div>
                        <div class="p-favorite--item-inner-cnt js-favoriteCnt">
                          <p class="ttl-blue">このテキストはキャプションです。</p>
                          <p class="ttl-location">東京都世田谷区</p>
                        </div>
                        <div class="p-favorite--item-direct">
                          <a href="" class="btn-blue3">予約する</a>
                          <div class="checkboxWrap">
                            <input class="checkbox2 js-cbox" id="cbox02" type="checkbox" value="value1">
                            <label for="cbox02"></label>
                          </div>
                        </div>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div><!-- ./p-reservation -->
        </div>
      </div>
    </div>
  </div>

  <div class="p-favorite--ctrl js-favoriteCtrl">
    <div class="p-favorite--ctrl-inner">
      <a href="" class="btn-blue">選択したお気に入りを削除する</a>
      <span class="link-reset link js-favoriteReset">リセット</span>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>