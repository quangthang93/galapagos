<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li>予約確認・履歴</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約確認・履歴</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-reservation">
            <div class="p-reservation--item">
              <h3 class="ttl-bg">予約状況</h3>
              <div class="p-reservation--item-cnt">
                <div class="p-reservation--head row">
                  <div class="p-reservation--head-left col-lg-10 col-md-12 col-12">
                    <div class="p-reservation--head-nameWrap">
                      <div class="p-reservation--head-name">
                        <p class="ttl-bold4">BiBi犬猫病院</p>
                        <span class="label-green">動物病院</span>
                      </div>
                      <div class="ttl-nameWrap"><span class="ttl-name male">ムギ</span></div>
                    </div>
                    <div class="p-reservation--head-timeWrap row">
                      <div class="p-reservation--head-timeWrap-col col-lg-6 col-md-6 col-12">
                        <span class="label-gray">予約日時</span>
                        <p class="date5">2021年04月03日 10:00～10:30</p>
                      </div>
                      <div class="p-reservation--head-timeWrap-col col-lg-6 col-md-6 col-12">
                        <span class="label-gray">所在地</span>
                        <p class="date5">東京都世田谷区 <a href="" class="link-map" target="_blank"><span>MAP</span></a></p>
                      </div>
                    </div>
                  </div>
                  <div class="p-reservation--head-right col-lg-2 col-md-12 col-12">
                    <div class="row">
                      <a href="" class="btn-white2 col-lg-12 col-md-6 col-6">詳細を見る</a>
                      <a href="" class="btn-white2 col-lg-12 col-md-6 col-6">予約変更</a>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
            <div class="p-reservation--item">
              <h3 class="ttl-bg">ご利用履歴一覧</h3>
              <div class="p-reservation--item-cnt">
                <div class="p-reservation--table">
                  <table class="table">
                    <thead>
                      <tr>
                        <th class="t_date" scope="col">日時</th>
                        <th class="t_name" scope="col">施設名</th>
                        <th class="pc-only3" scope="col">ペット</th>
                        <th class="pc-only3" scope="col">診察内容</th>
                        <th class="t_review" scope="col">レビュー</th>
                        <th class="t_points pc-only3" scope="col">ポイント</th>
                        <th class="t_detail" scope="col">詳細</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</th>
                        <td>BiBi犬猫病院</td>
                        <td class="pc-only3">ムギ</td>
                        <td class="pc-only3 align-center">内科</td>
                        <td><span class="txt-rating align-center">満足</span></td>
                        <td class="pc-only3 align-center">0P</td>
                        <td class="align-center"><a href="" class="link-blue">詳細</a></td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</th>
                        <td>ペットカンパニーパイン</td>
                        <td class="pc-only3">アーリアジャスール</td>
                        <td class="pc-only3 align-center">狂犬病予防接種</td>
                        <td><span class="txt-rating sad align-center">不満足</span></td>
                        <td class="pc-only3 align-center">0P</td>
                        <td class="align-center"><a href="" class="link-blue">詳細</a></td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</th>
                        <td>ペットカンパニーパイン下北沢店</td>
                        <td class="pc-only3">ハナコ</td>
                        <td class="pc-only3 align-center">混合ワクチン接種</td>
                        <td class="align-center"><a href="" class="link-blue">投稿する</a></td>
                        <td class="pc-only3 align-center">0P</td>
                        <td class="align-center"><a href="" class="link-blue">詳細</a></td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</th>
                        <td>BiBi犬猫病院</td>
                        <td class="pc-only3">ムギ</td>
                        <td class="pc-only3 align-center">内科</td>
                        <td><span class="txt-rating align-center">満足</span></td>
                        <td class="pc-only3 align-center">0P</td>
                        <td class="align-center"><a href="" class="link-blue">詳細</a></td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</th>
                        <td>ペットカンパニーパイン</td>
                        <td class="pc-only3">アーリアジャスール</td>
                        <td class="pc-only3 align-center">狂犬病予防接種</td>
                        <td><span class="txt-rating sad align-center">不満足</span></td>
                        <td class="pc-only3 align-center">0P</td>
                        <td class="align-center"><a href="" class="link-blue">詳細</a></td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</th>
                        <td>ペットカンパニーパイン下北沢店</td>
                        <td class="pc-only3">ハナコ</td>
                        <td class="pc-only3 align-center">混合ワクチン接種</td>
                        <td class="align-center"><a href="" class="link-blue">投稿する</a></td>
                        <td class="pc-only3 align-center">0P</td>
                        <td class="align-center"><a href="" class="link-blue">詳細</a></td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</th>
                        <td>BiBi犬猫病院</td>
                        <td class="pc-only3">ムギ</td>
                        <td class="pc-only3 align-center">内科</td>
                        <td><span class="txt-rating align-center">満足</span></td>
                        <td class="pc-only3 align-center">0P</td>
                        <td class="align-center"><a href="" class="link-blue">詳細</a></td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</th>
                        <td>ペットカンパニーパイン</td>
                        <td class="pc-only3">アーリアジャスール</td>
                        <td class="pc-only3 align-center">狂犬病予防接種</td>
                        <td><span class="txt-rating sad align-center">不満足</span></td>
                        <td class="pc-only3 align-center">0P</td>
                        <td class="align-center"><a href="" class="link-blue">詳細</a></td>
                      </tr>
                      <tr>
                        <td>2021年04月03日 10:00～10:30</th>
                        <td>ペットカンパニーパイン下北沢店</td>
                        <td class="pc-only3">ハナコ</td>
                        <td class="pc-only3 align-center">混合ワクチン接種</td>
                        <td class="align-center"><a href="" class="link-blue">投稿する</a></td>
                        <td class="pc-only3 align-center">0P</td>
                        <td class="align-center"><a href="" class="link-blue">詳細</a></td>
                      </tr>
                    </tbody>
                  </table>
                  <div class="p-reservation--viewmore align-center">
                    <a href="" class="link-blue">もっと見る</a>
                  </div>
                </div>
              </div>
            </div><!-- ./p-reservation--item -->
          </div><!-- ./p-reservation -->
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>