<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li><a href="/mypage/reservation">予約確認・履歴</a></li>
      <li>予約状況詳細</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約状況詳細</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-reservation--detail">
            <div class="p-reservation--detail-head">
              <p class="ttl-bold4">BiBi犬猫病院</p>
              <div class="p-reservation--detail-head-tags">
                <span class="label-green">動物病院</span>
                <span class="label-white">予約中</span>
              </div>
            </div>
            <div class="form">
              <div class="form-row">
                <div class="form-label">予約日</div>
                <div class="form-row--cnt">
                  2021年04月03日(火)
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">予約時間</div>
                <div class="form-row--cnt">
                  10:00～10:30
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">アクセス</div>
                <div class="form-row--cnt">
                  <p>東京都世田谷区北沢2-28-8</p>
                  <div class="mgt-10 mgb-10"><a href="https://goo.gl/maps/Y5k5h2vUeaYiyd2H9" class="link-map" target="_blank"><span>MAP</span></a></div>
                  <p>下北沢駅北口から出て、鎌倉通りを北へ進みます。徒歩7分で着きます。</p>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">電話番号</div>
                <div class="form-row--cnt">
                  03-1234-5678
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">種別</div>
                <div class="form-row--cnt">
                  初診
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">診察内容</div>
                <div class="form-row--cnt">
                  内科
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">備考</div>
                <div class="form-row--cnt">
                  風邪のような症状が2、3日前から見られます。市販の薬を服用させようと試みましたがすぐに吐き出してしまいます。昨日からはエサに混ぜたら食べるようになりました。 普段から服用している薬はありません。
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">ペット</div>
                <div class="form-row--cnt">
                  <div class="mgb-15"><span class="ttl-name">ソラ</span></div>
                  <div class="_itemWrap">
                    <div class="_item">
                      <div class="label-gray">年齢</div>
                      <div class="_item-value">2歳</div>
                    </div>
                    <div class="_item">
                      <div class="label-gray">種類</div>
                      <div class="_item-value">猫 | グレータビー</div>
                    </div>
                    <div class="_item">
                      <div class="label-gray">体重</div>
                      <div class="_item-value">猫 | 6kg</div>
                    </div>
                    <div class="_item">
                      <div class="label-gray">常用薬</div>
                      <div class="_item-value">ドロンタール</div>
                    </div>
                    <div class="_item">
                      <div class="label-gray">常用食</div>
                      <div class="_item-value">ヒルズサイエンス</div>
                    </div>
                    <div class="_item">
                      <div class="label-gray">去勢・避妊</div>
                      <div class="_item-value">済</div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">付与予定ポイント</div>
                <div class="form-row--cnt">
                  <a href="" class="txt-bold link-green">20P(キャンセルの場合付与されません)</a>
                </div>
              </div>
            </div>
            <div class="p-reservation--timeBox">
              <p class="p-reservation--timeBox-msg">予約の変更、キャンセルは病院または店舗に直接お電話ください。</p>
              <div class="align-center">
                <span class="label-gray2">soup*spoon</span>
              </div>
              <div class="mgt-10">
                <a href="" class="phone">03-1234-5678</a>
              </div>
              <div class="mgt-5">
                <p class="date7">平日 8:00～17:00 / 土曜 8:00～13:00</p>
              </div>
            </div>
            <div class="align-center mgt-40">
              <a href="" class="link-blue">一覧へ戻る</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>