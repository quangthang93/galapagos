<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li><a href="/mypage/reservation">予約確認・履歴</a></li>
      <li>ご利用履歴詳細</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">予約状況詳細</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit p-reservation--detail">
            <div class="p-reservation--detail-head">
              <p class="ttl-bold4">BiBi犬猫病院</p>
              <div class="p-reservation--detail-head-tags">
                <span class="label-green mgr-2">動物病院</span>
                <span class="label-gray3">キャンセル</span>
              </div>
            </div>
            <div class="form">
              <div class="form-row">
                <div class="form-label">ご利用日</div>
                <div class="form-row--cnt">
                  2021年04月03日(火)
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">時間</div>
                <div class="form-row--cnt">
                  10:00～10:30
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">種別</div>
                <div class="form-row--cnt">
                  初診
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">診察内容</div>
                <div class="form-row--cnt">
                  内科
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">備考</div>
                <div class="form-row--cnt">風邪のような症状が2、3日前から見られます。市販の薬を服用させようと試みましたがすぐに吐き出してしまいます。昨日からはエサに混ぜたら食べるようになりました。 普段から服用している薬はありません。</div>
              </div>
              <div class="form-row">
                <div class="form-label">付与予定ポイント</div>
                <div class="form-row--cnt">
                  <a href="" class="txt-bold link-green">20P</a>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">レビュー</div>
                <div class="form-row--cnt">
                  <div class="label-box">
                    <a href="" class="link-blue">投稿する</a>
                    <span class="label-msg">レビュー投稿で<br class="sp-only3">サンクスポイント5Pプレゼント！</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="align-center mgt-40">
              <a href="" class="link-blue">一覧へ戻る</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>