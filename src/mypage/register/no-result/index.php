<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>

    <main class="main mypage">
    	<div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/mypage">マイページ</a></li>
          <li>登録ペット</li>
        </ul>
      </div>
      <div class="p-mypage">
    		<div class="container3">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
          <div class="p-mypage--ttlWrap">
            <h2 class="p-mypage--ttl">登録ペット</h2>
            <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
          </div>
    			<div class="p-mypage--inner">
    				<div class="p-mypage--sidebar">
    					<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
    				</div>
    				<div class="p-mypage--cnt">
    					<div class="p-edit">
    						<div class="p-noResult">
                  <h3 class="ttl-bold3">ペット情報を登録してください。</h3>
                  <p class="desc6">ペット情報を登録すると、<br>施設の予約や専門家への相談が <br>スムーズに行えるようになります。</p>
                  <div class="btn-blueWrap">
                    <a href="" class="btn-blue2">ペット情報を登録する</a>
                  </div>
                  <div class="">
                    <a href="" class="link-blue">今は登録しない</a>
                  </div>
                </div>
    					</div>
    				</div>
    			</div>
    		</div>
      </div>
    </main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>