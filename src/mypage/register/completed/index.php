<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>

    <main class="main mypage">
    	<div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/mypage">マイページ</a></li>
          <li>登録ファミリー</li>
        </ul>
      </div>
      <div class="p-mypage">
    		<div class="container3">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
          <div class="p-mypage--ttlWrap type2">
            <h2 class="p-mypage--ttl">登録ファミリー</h2>
            <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
            <a href="" class="btn-blue3 type2">新規ファミリー登録</a>
          </div>
    			<div class="p-mypage--inner">
    				<div class="p-mypage--sidebar">
    					<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
    				</div>
    				<div class="p-mypage--cnt">
    					<ul class="p-pets">
    						<li class="p-pets--item">
                  <div class="p-pets--item-head">
                    <div class="ttl-name">ソラ</div>
                    <a href="" class="link-blue2 pc-only">ファミリー情報を編集する</a>
                  </div>       
                  <div class="p-pets--item-cntWrap">
                    <div class="p-pets--item-thumb">
                       <img src="<?php echo $PATH;?>/assets/images/mypage/pet01.png" alt="">
                    </div>
                    <div class="p-pets--item-cnt">
                      <div class="p-pets--item-row">
                        <div class="label-gray">登録日</div>
                        <div class="desc4">2021年10月01日</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">生年月日</div>
                        <div class="desc4">2019年04月10日</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">年齢</div>
                        <div class="desc4">2歳</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">種類</div>
                        <div class="desc4">猫 | グレータビー</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">体重</div>
                        <div class="desc4">6kg</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">常用薬</div>
                        <div class="desc4">ドロンタール</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">常用食</div>
                        <div class="desc4">ヒルズサイエンス</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">かかりつけ</div>
                        <div class="desc4"><a href="" target="_blank" class="link-blue">ファミリーカンパニーパイン</a></div>
                      </div>
                    </div>
                  </div>    
                  <a href="" class="link-blue2 sp-only2">ファミリー情報を編集する</a>             
                </li>
                <li class="p-pets--item">
                  <div class="p-pets--item-head">
                    <div class="ttl-name male">ソラ</div>
                    <a href="" class="link-blue2 pc-only">ファミリー情報を編集する</a>
                  </div>       
                  <div class="p-pets--item-cntWrap">
                    <div class="p-pets--item-thumb">
                       <img src="<?php echo $PATH;?>/assets/images/mypage/pet02.png" alt="">
                    </div>
                    <div class="p-pets--item-cnt">
                      <div class="p-pets--item-row">
                        <div class="label-gray">登録日</div>
                        <div class="desc4">2021年10月01日</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">生年月日</div>
                        <div class="desc4">2019年04月10日</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">年齢</div>
                        <div class="desc4">2歳</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">種類</div>
                        <div class="desc4">猫 | グレータビー</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">体重</div>
                        <div class="desc4">6kg</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">常用薬</div>
                        <div class="desc4">ビープロン beePRON プロポリス 30cc スポイトタイプ ファミリー用</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">常用食</div>
                        <div class="desc4">ヒルズサイエンス</div>
                      </div>
                      <div class="p-pets--item-row">
                        <div class="label-gray">かかりつけ</div>
                        <div class="desc4">
                          <a href="" target="_blank" class="link-blue">ファミリーカンパニーパイン</a>,
                          <a href="" target="_blank" class="link-blue">BiBi犬猫病院</a>,
                          <a href="" target="_blank" class="link-blue">アマノ動物病院</a>
                        </div>
                      </div>
                    </div>
                  </div>    
                  <a href="" class="link-blue2 sp-only2">ファミリー情報を編集する</a>             
                </li>
    					</ul>
    				</div>
    			</div>
    		</div>
      </div>
    </main><!-- ./main -->



    <!-- modal box -->
      <div class="modal-complete">
        <div class="modal-complete--cnt">
          <div class="modal-complete--cnt-inner">
            <div class="modal-complete--cnt-icon"><img src="<?php echo $PATH;?>/assets/images/common/icon-check.svg" alt=""></div>
            <p class="modal-complete--cnt-msg">ファミリー情報を<br>更新しました</p>
          </div>
        </div>
      </div>
    <!-- ./modal box -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>