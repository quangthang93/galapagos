<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li><a href="/mypage/register">登録ペット</a></li>
      <li>登録ペット編集</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">登録ペット編集</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit">
            <div class="p-edit--dateWrap">
              <span class="label-gray">登録日</span>
              <span class="desc4">2021年10月01日</span>
            </div>
            <form class="form">
              <div class="form-row">
                <div class="form-label">写真</div>
                <div class="form-row--cnt">
                  <div class="form-row--upload">
                    <div class="form-row--upload-thumb">
                      <img class="cover" src="<?php echo $PATH;?>/assets/images/mypage/pet02.png" alt="">
                    </div>
                    <a class="form-row--upload-btn btn-blue">ファイルを選択</a>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">名前※</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" value="ムギ">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">性別※</div>
                <div class="form-row--cnt">
                  <div class="form-radio">
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="sex" value="オス" checked>
                        <span class="form-radio--label">オス</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="sex" value="メス">
                        <span>メス</span>
                      </label>
                    </span>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">生年月日</div>
                <div class="form-row--cnt">
                  <div class="form-groupField">
                    <div class="form-groupField--item">
                      <select name="product" class="select input">
                        <option value="">2019</option>
                        <option value="">2020</option>
                        <option value="" selected="selected">2021</option>
                        <option value="">2022</option>
                      </select>
                      <span>年</span>
                    </div>
                    <div class="form-groupField--item">
                      <select name="product" class="select input">
                        <option value="" selected="selected">01</option>
                        <option value="">02</option>
                        <option value="">03</option>
                      </select>
                      <span>月</span>
                    </div>
                    <div class="form-groupField--item">
                      <select name="product" class="select input">
                        <option value="" selected="selected">01</option>
                        <option value="">02</option>
                        <option value="">03</option>
                      </select>
                      <span>日</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">種類※</div>
                <div class="form-row--cnt">
                  <div class="form-radio">
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="犬">
                        <span class="form-radio--label">犬</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="猫" checked>
                        <span>猫</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="うさぎ">
                        <span>うさぎ</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="フェレット">
                        <span>フェレット</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="鳥類">
                        <span>鳥類</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="type" value="その他">
                        <span>その他</span>
                      </label>
                    </span>
                  </div>
                  <div class="form-input mgt-20">
                    <input type="text" name="name" class="input" value="グレータビー">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">体重</div>
                <div class="form-row--cnt">
                  <div class="form-groupField">
                    <div class="form-groupField--item">
                      <input type="text" name="name" class="input type2" value="6">
                      <span>kg</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">常用薬</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" value="ビープロン beePRON プロポリス 30cc スポイトタイプ ペット用">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">常用食</div>
                <div class="form-row--cnt">
                  <div class="form-input">
                    <input type="text" name="name" class="input" value="ピュリナワン キャット F.L.U.T.H.ケア 1歳以上">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">よく利用する施設</div>
                <div class="form-row--cnt">
                  <p class="desc5">予約履歴に残っている施設から選択できます</p>
                  <div class="form-facilities mgt-5 js-facilitiesList">
                    <div class="mgt-5 js-facilitiesItem">
                      <select name="sub01" class="select input type2">
                        <option value="">ペットカンパニーパイン</option>
                        <option value="" selected="selected">ペットカンパニーパイン</option>
                        <option value="">ペットカンパニーパイン</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-facilities--add align-center mgt-15">
                    <a href="javascript:void(0)" class="btn-add js-btnAdd">追加する</a>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">去勢・避妊</div>
                <div class="form-row--cnt">
                  <div class="form-radio">
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="castration" value="済" checked>
                        <span class="form-radio--label">済</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="castration" value="未">
                        <span>未</span>
                      </label>
                    </span>
                  </div>
                </div>
              </div>
              <div class="form-ctrl">
                <div class="form-ctrl--item btn-submitWrap">
                  <a href="/mypage/register/edit-complete" class="btn-submit js-btnSave">編集を保存する</a>
                </div>
                <div class="form-ctrl--item">
                  <a href="/mypage/register/edit/confirm" class="btn-del js-btnDel" data-bs-toggle="modal" data-bs-target="#modalConfirm"><span>削除する</span></a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->

<!-- Modal -->
<div class="modal fade" id="modalConfirm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content modal-confirm">
      <div class="modal-header">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <p class="modal-confirm--ttl">ペット情報の削除</p>
      <p class="modal-confirm--msg">削除した情報は戻すことができません。<br> ペット情報を削除しますか？</p>
      <a class="btn-white">いいえ</a>
      <a class="btn-blue">はい</a>
      <div><a class="modal-confirm--close" data-bs-dismiss="modal">閉じる</a></div>
    </div>
  </div>
</div>

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>