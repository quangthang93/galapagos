<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>

    <main class="main mypage">
    	<div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/mypage">マイページ</a></li>
          <li>メッセージ</li>
        </ul>
      </div>
      <div class="p-mypage">
    		<div class="container3">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
          <div class="p-mypage--ttlWrap">
            <h2 class="p-mypage--ttl">登録ペット</h2>
            <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
          </div>
    			<div class="p-mypage--inner">
    				<div class="p-mypage--sidebar">
    					<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
    				</div>
    				<div class="p-mypage--cnt">
    					<div class="p-message">
    						<ul class="p-message--list">
                  <li>
                    <div class="date4">2021年04月03日 <span>16:00</span></div>
                    <a href="/mypage/message/detail" class="p-message--list-link link blue">獣医師からご相談の返信が届きました。</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">獣医師からご相談の返信が届きました。</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">【感想をお聞かせください】こんにちは。アマノ動物病院です。この度はアマノ動物病院…</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">獣医師からご相談の返信が届きました。</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">【感想をお聞かせください】こんにちは。アマノ動物病院です。この度はアマノ動物病院…</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">獣医師からご相談の返信が届きました。</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">【感想をお聞かせください】こんにちは。アマノ動物病院です。この度はアマノ動物病院…</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">獣医師からご相談の返信が届きました。</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">【感想をお聞かせください】こんにちは。アマノ動物病院です。この度はアマノ動物病院…</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">獣医師からご相談の返信が届きました。</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">【感想をお聞かせください】こんにちは。アマノ動物病院です。この度はアマノ動物病院…</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">獣医師からご相談の返信が届きました。</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">【感想をお聞かせください】こんにちは。アマノ動物病院です。この度はアマノ動物病院…</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">獣医師からご相談の返信が届きました。</a>
                  </li>
                  <li>
                    <div class="date4">2021年04月03日 16:00</div>
                    <a href="/mypage/message/detail" class="p-message--list-link link">【感想をお聞かせください】こんにちは。アマノ動物病院です。この度はアマノ動物病院…</a>
                  </li>
                </ul>
    					</div>
              <div class="p-pagination">
                <p class="p-pagination-label">0,000件中｜1〜30件 表示</p>
                <div class="p-pagination-list">
                  <a class="ctrl prev" href=""></a>
                  <a class="active" href="">1</a>
                  <a href="">2</a>
                  <a href="">3</a>
                  <a href="">4</a>
                  <a href="">5</a>
                  <a href="">6</a>
                  <div class="p-pagination-spacer">…</div>
                  <a href="">12</a>
                  <a class="ctrl next" href=""></a>
                </div>
              </div>
    				</div>
    			</div>
    		</div>
      </div>
    </main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>