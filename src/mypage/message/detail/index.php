<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>

    <main class="main mypage">
    	<div class="breadcrumb">
        <ul>
          <li><a href="/">トップページ</a></li>
          <li><a href="/mypage">マイページ</a></li>
          <li>メッセージ</li>
        </ul>
      </div>
      <div class="p-mypage">
    		<div class="container3">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
          <div class="p-mypage--ttlWrap">
            <h2 class="p-mypage--ttl">登録ペット</h2>
            <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
          </div>
    			<div class="p-mypage--inner">
    				<div class="p-mypage--sidebar">
    					<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
    				</div>
    				<div class="p-mypage--cnt">
    					<div class="p-messageDetail">
    						<div class="p-messageDetail--head">
                  <p class="ttl-bold3">獣医師からご相談の返信が届きました。</p>
                  <p class="date5">2021年04月03日 16:00</p>
                </div>
                <div class="p-messageDetail--cnt">
                  <div class="p-messageDetail--cnt-row">
                    苗字 名前 様 <br><br>いつもご利用ありがとうございます。<br> ガラパゴス運営事務局です。 <br> <br> 先日、苗字 名前 様からお寄せいただいた質問に対し、 <br> 獣医師から回答が届きましたのでお知らせいたします。
                  </div>
                  <div class="p-messageDetail--cnt-row">2021年5月31日　16:30<br>  獣医師：松尾　昌 <br> ご相談内容：あそび・散歩 <br> ご相談ペット：犬 <br> <br>
                  【ご相談内容】 <br> 臆病な性格なため、自転車や車のお出かけは好きなのですが、歩いての散歩だと他の犬がいると思い怖がり、散歩へ行こうとしません。なので運動しないので肥満気味です。 散歩以外で運動って何かありますか？ 
                  <br> <br> <br> 【回答】 無理にリードを引っ張らない 散歩が苦手な犬にとっては、リードを強く引っ張られることで嫌な思いをし、さらに散歩に対して悪い印象をもってしまいます。 歩かないときは、愛犬が歩き出すまで待って、また立ち止まったら歩き出すのを待つ、ということを繰り返しましょう。</div>
                  <div class="p-messageDetail--cnt-row">お役に立ちましたか？<br>  この回答に対するレビューをお願いします。 <br> 下記のURLからレビュー投稿ページへリンクします。 <br><br> <a href="https://garapagos.com/review/01234/ " class="link-blue">https://garapagos.com/review/01234/ </a><br><br>引き続き、ガラパゴスをよろしくお願いします。 <br><br>ガラパゴス運営事務局</div>
                </div>
                <div class="p-messageDetail--foot align-center">
                  <a href="/mypage/message/" class="link-blue">一覧へ戻る</a>
                </div>
    					</div>
    				</div>
    			</div>
    		</div>
      </div>
    </main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>