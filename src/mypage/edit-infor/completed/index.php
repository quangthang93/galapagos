<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header2.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/">トップページ</a></li>
      <li><a href="/mypage">マイページ</a></li>
      <li>登録情報編集</li>
    </ul>
  </div>
  <div class="p-mypage">
    <div class="container3">
      <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/mypage-infor.php'; ?>
      <div class="p-mypage--ttlWrap">
        <h2 class="p-mypage--ttl">登録情報編集</h2>
        <div class="p-mypage--navCtrl js-openNav"><span>メニュー</span></div>
      </div>
      <div class="p-mypage--inner type2">
        <div class="p-mypage--sidebar">
          <?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/sidebar.php'; ?>
        </div>
        <div class="p-mypage--cnt">
          <div class="p-edit">
            <div class="p-edit--dateWrap">
              <span class="label-gray">登録日</span>
              <span class="desc4">2021年10月01日</span>
            </div>
            <form class="form">
              <div class="form-row">
                <div class="form-label">名前</div>
                <div class="form-row--cnt">
                  <div class="form-input important">
                    <input type="text" name="name" class="input" value="入力してください">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">電話番号</div>
                <div class="form-row--cnt">
                  <div class="form-input important">
                    <input type="text" name="name" class="input" value="08012345678">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">E-mail</div>
                <div class="form-row--cnt">
                  <div class="form-input important">
                    <input type="email" name="name" class="input" value="mailaddress@mail.com">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">住所</div>
                <div class="form-row--cnt">
                  <div class="form-groupField--item important mgb-10">
                    <input type="text" name="name" class="input type3 mgr-20" value="1001000">
                    <a class="form-row--upload-btn btn-blue">住所自動入力</a>
                  </div>
                  <div class="form-groupField--item important">
                    <div class="d_flex between mgb-15">
                      <select name="sub01" class="select input type4">
                        <option value="">東京都</option>
                        <option value="" selected="selected">東京都</option>
                        <option value="">東京都</option>
                      </select>
                      <input type="text" name="name" class="input type4" value="港区北青山">
                    </div>
                    <input type="text" name="name" class="input" value="3-3-5東京建物青山ビル4F">
                  </div>
                  <div class="form-groupField--item">
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">性別</div>
                <div class="form-row--cnt">
                  <div class="form-radio">
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="sex" value="男性" checked>
                        <span class="form-radio--label">男性</span>
                      </label>
                    </span>
                    <span class="form-radio--item">
                      <label>
                        <input type="radio" name="sex" value="女性">
                        <span>女性</span>
                      </label>
                    </span>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">生年月日</div>
                <div class="form-row--cnt">
                  <div class="form-groupField">
                    <div class="form-groupField--item">
                      <select name="product" class="select input">
                        <option value="">2019</option>
                        <option value="">2020</option>
                        <option value="" selected="selected">2021</option>
                        <option value="">2022</option>
                      </select>
                      <span>年</span>
                    </div>
                    <div class="form-groupField--item">
                      <select name="product" class="select input">
                        <option value="" selected="selected">01</option>
                        <option value="">02</option>
                        <option value="">03</option>
                      </select>
                      <span>月</span>
                    </div>
                    <div class="form-groupField--item">
                      <select name="product" class="select input">
                        <option value="" selected="selected">01</option>
                        <option value="">02</option>
                        <option value="">03</option>
                      </select>
                      <span>日</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">現在の<br class="sp-only3">パスワード</div>
                <div class="form-row--cnt">
                  <p class="desc5">半角英数字</p>
                  <input type="text" name="name" class="input mgt-5" value="password5478">
                </div>
              </div>
              <div class="form-row">
                <div class="form-label">新しい<br class="sp-only3">パスワード</div>
                <div class="form-row--cnt">
                  <p class="desc5">半角英数字</p>
                  <input type="text" name="name" class="input mgt-5" value="">
                </div>
              </div>
              <div class="form-ctrl type2">
                <div class="form-ctrl--item btn-submitWrap">
                  <a href="" class="btn-white3 align-center">戻る</a>
                </div>
                <div class="form-ctrl--item btn-submitWrap">
                  <a href="/mypage/edit-infor/completed" class="btn-submit">編集を保存する</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->

<!-- modal box -->
  <div class="modal-complete">
    <div class="modal-complete--cnt">
      <div class="modal-complete--cnt-inner">
        <div class="modal-complete--cnt-icon"><img src="<?php echo $PATH;?>/assets/images/common/icon-check.svg" alt=""></div>
        <p class="modal-complete--cnt-msg">登録情報の編集を<br>保存しました。</p>
      </div>
    </div>
  </div>
<!-- ./modal box -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer2.php'; ?>