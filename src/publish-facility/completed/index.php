<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/pre/">トップページ</a></li>
      <li><a href="/publish-facility/">掲載ご希望の施設様へ</a></li>
      <li>掲載のお申込み</li>
    </ul>
  </div>

  <div class="container3">
    <div class="p-mypage--ttlWrap">
      <h2 class="p-mypage--ttl">掲載のお申込み</h2>
    </div>

    <div class="p-publish-form">
      <div class="p-publish-form__content">

        <h3 class="p-publish-form__title">お申込みが完了しました。</h3>
        <p class="p-publish-form__txt">掲載のお申込みをいただき、ありがとうございます。<br>入力内容を確認の後、改めて担当者より詳細のご連絡を申し上げます。</p>

        <div class="form-ctrl type2">
          <div class="form-ctrl--item btn-submitWrap">
            <a href="" class="btn-blue2">トップページへ</a>
          </div>
        </div>
      </div><!-- ./contact-content -->
    </div>
  </div>

  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>