<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php'; ?>
<main class="main mypage">
  <div class="breadcrumb">
    <ul>
      <li><a href="/pre/">トップページ</a></li>
      <li><a href="/publish-facility/">掲載ご希望の施設様へ</a></li>
      <li>掲載のお申込み</li>
    </ul>
  </div>

  <div class="container3">
    <div class="p-mypage--ttlWrap">
      <h2 class="p-mypage--ttl">掲載のお申込み</h2>
    </div>

    <div class="p-publish-form">
      <div class="p-publish-form__content">

        <div class="p-publish-form__first">
          <p>施設掲載のお申込みは、こちらで受け付けております。<br>お手数ですが、以下の必要項目の入力にご協力お願いします。<br>管理画面を作成し、担当者より改めて詳細のご連絡を申し上げます。</p>
        </div>

        <form class="form">
        <div class="form-row">
            <div class="form-label">施設区分<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-radio">
                <span class="form-radio--item">
                  <label>
                    <input type="radio" name="type" value="動物病院" checked>
                    <span class="form-radio--label">動物病院</span>
                  </label>
                </span>
                <span class="form-radio--item">
                  <label>
                    <input type="radio" name="type" value="トリミングサロン">
                    <span>トリミングサロン</span>
                  </label>
                </span>
                <span class="form-radio--item">
                  <label>
                    <input type="radio" name="type" value="上記以外の施設">
                    <span>上記以外の施設</span>
                  </label>
                </span>
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">施設名<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-input">
                <input type="text" name="name" class="input type10" value="" placeholder="ガラパゴス動物病院">
              </div>
            </div>
          </div>
          <div class="form-row2">
            <div class="form-label">郵便番号<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-groupField">
                <div class="form-groupField--item">
                  <input type="text" name="name" class="input type3 mgr-10" placeholder="1001000">
                  <a class="form-row--upload-btn btn-blue">住所自動入力</a>
                </div>
              </div>
            </div>
          </div>
          <div class="form-row2">
            <div class="form-label">都道府県<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-groupField">
                <div class="form-groupField--item">
                  <select name="product" class="select input type10">
                  <option value="">北海道</option>
                  <option value="">青森県</option>
                  <option value="">岩手県</option>
                  <option value="">宮城県</option>
                  <option value="">秋田県</option>
                  <option value="">山形県</option>
                  <option value="">福島県</option>
                  <option value="">茨城県</option>
                  <option value="">栃木県</option>
                  <option value="">群馬県</option>
                  <option value="">埼玉県</option>
                  <option value="">千葉県</option>
                  <option value="" selected>東京都</option>
                  <option value="">神奈川県</option>
                  <option value="">新潟県</option>
                  <option value="">富山県</option>
                  <option value="">石川県</option>
                  <option value="">福井県</option>
                  <option value="">山梨県</option>
                  <option value="">長野県</option>
                  <option value="">岐阜県</option>
                  <option value="">静岡県</option>
                  <option value="">愛知県</option>
                  <option value="">三重県</option>
                  <option value="">滋賀県</option>
                  <option value="">京都府</option>
                  <option value="">大阪府</option>
                  <option value="">兵庫県</option>
                  <option value="">奈良県</option>
                  <option value="">和歌山県</option>
                  <option value="">鳥取県</option>
                  <option value="">島根県</option>
                  <option value="">岡山県</option>
                  <option value="">広島県</option>
                  <option value="">山口県</option>
                  <option value="">徳島県</option>
                  <option value="">香川県</option>
                  <option value="">愛媛県</option>
                  <option value="">高知県</option>
                  <option value="">福岡県</option>
                  <option value="">佐賀県</option>
                  <option value="">長崎県</option>
                  <option value="">熊本県</option>
                  <option value="">大分県</option>
                  <option value="">宮崎県</option>
                  <option value="">鹿児島県</option>
                  <option value="">沖縄県</option>
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="form-row2">
            <div class="form-label">市区町村<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-input">
                <input type="text" name="name" class="input type10" placeholder="港区北青山">
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">以降<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-input">
                <input type="text" name="name" class="input type10" placeholder="3-3-5 東京建物青山ビル4F">
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">施設責任者<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-input">
                <input type="text" name="name" class="input type10" placeholder="山田 太郎">
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">施設責任者生年月日<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-groupField">
                <div class="form-groupField--item">
                  <select name="product" class="select input">
                    <option value="" selected="selected">1950</option>
                    <option value="">1951</option>
                    <option value="">1952</option>
                    <option value="">1953</option>
                    <option value="">1954</option>
                    <option value="">1955</option>
                  </select>
                  <span>年</span>
                  <select name="product" class="select input">
                    <option value="" selected="selected">01</option>
                    <option value="">02</option>
                    <option value="">03</option>
                    <option value="">04</option>
                    <option value="">05</option>
                    <option value="">06</option>
                    <option value="">07</option>
                    <option value="">08</option>
                    <option value="">09</option>
                    <option value="">10</option>
                    <option value="">11</option>
                    <option value="">12</option>
                  </select>
                  <span>月</span>
                  <select name="product" class="select input">
                    <option value="" selected="selected">01</option>
                    <option value="">02</option>
                    <option value="">03</option>
                    <option value="">04</option>
                    <option value="">05</option>
                    <option value="">06</option>
                    <option value="">07</option>
                    <option value="">08</option>
                    <option value="">09</option>
                    <option value="">10</option>
                    <option value="">11</option>
                    <option value="">12</option>
                    <option value="">13</option>
                    <option value="">14</option>
                    <option value="">15</option>
                    <option value="">16</option>
                    <option value="">17</option>
                    <option value="">18</option>
                    <option value="">19</option>
                    <option value="">20</option>
                    <option value="">21</option>
                    <option value="">22</option>
                    <option value="">23</option>
                    <option value="">24</option>
                    <option value="">25</option>
                    <option value="">26</option>
                    <option value="">27</option>
                    <option value="">28</option>
                    <option value="">29</option>
                    <option value="">30</option>
                    <option value="">31</option>
                  </select>
                  <span>日</span>
                </div>
              </div>
              <p class="desc5 mgt-10">※パスワードを紛失した場合、再設定に必要になります。</p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">開業届</div>
            <div class="form-row--cnt">
              <div class="form-row--upload">
                <a class="form-row--upload-btn btn-blue">ファイルを選択</a>
              </div>
              <p class="desc5 mgt-10">※画像ファイル、もしくはPDFファイルを選択してください。（5MB以下）<br>※ホームページなど開業の実態確認ができるものが無い場合、確認できる書類の添付をお願い致します。</p>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">電話番号<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-input">
                <input type="text" name="name" class="input type6" placeholder="半角数字、ハイフン(-)不要">
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">ご連絡先e-mail<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-input">
                <input type="text" name="name" class="input type6" placeholder="ex) example@mail.com">
              </div>
            </div>
          </div>
          <div class="form-row">
            <div class="form-label">パスワード<span class="c-form__required">※</span></div>
            <div class="form-row--cnt">
              <div class="form-input">
                <input type="text" name="name" class="input type6" placeholder="半角英数字6文字以上">
              </div>
            </div>
          </div>
          <div class="form-ctrl">
            <div class="form-ctrl--item btn-submitWrap">
              <a href="/mypage/register/edit-complete" class="btn-submit js-btnSave">確認画面へ</a>
            </div>
          </div>
        </form>
      </div><!-- ./contact-content -->
    </div>
  </div>

  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>