<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header-pre.php'; ?>
<main class="main">

  <div class="p-publish p-publish--facility">
    <section class="p-publish__sec01">  
      <div class="breadcrumb">
        <ul>
          <li><a href="/pre/">トップページ</a></li>
          <li>掲載ご希望の施設様へ</li>
        </ul>
      </div>
      
      <div class="container3">
        <div class="p-mypage--ttlWrap">
          <h2 class="p-mypage--ttl">掲載ご希望の施設様へ</h2>
        </div>

        <div class="try">
          <h3>掲載のお申込み
            <span>
              <img src="<?php echo $PATH;?>/assets/images/publish/title-try.svg" alt="Try!">
            </span>
          </h3>
          <a href="" class="btn-blue6">お申込み希望の方はコチラ</a>
        </div>

        <div class="about">
          <div class="about__content">
            <div class="about__title">
              <h3><img src="<?php echo $PATH;?>/assets/images/publish/title-communication.svg" alt=""></h3>
              <span><img src="<?php echo $PATH;?>/assets/images/publish/bg-line-03.svg" alt=""></span>
            </div>
            <p class="about__h">小動物業界の負担を減らすために、<br>獣医とトリマーが <span>” 現場で使える ”</span> に<br>こだわり作りました。</p>
            <p class="about__txt">動物用店舗の、店舗による、店舗のためのページ。<br>ペットオーナーと店舗をスムーズに繋ぐことで、<br>業界ならではの課題を解決します。</p>
          </div>
          <img src="<?php echo $PATH;?>/assets/images/publish/illust-01.png" alt="" class="about__img">
        </div>
        
        <div class="cost">
          <table>
            <thead>
              <tr>
                <th>初期費用</th>
                <th>予約毎<br>手数料</th>
                <th>月額料金</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>無料</td>
                <td>なし</td>
                <td>5,000円<span>/月</span></td>
              </tr>
            </tbody>
          </table>
          <span class="cost__note">※来店完了時や口コミ投稿時に、ユーザーへ付与されるポイントは実費のみ追加でかかります。</span>
          <div class="cost__msg">
            <p><span>ご利用いただき、ご満足いただけ</span><span>なかった場合は利用料を</span><span>ご返金いたします</span></p>
          </div>
        </div>
      </div>
    </section>

    <section class="p-publish__sec02">
      <div class="container3">

        <div class="point">
          <div class="point__title">
            <h3>サービスの特徴</h3>
            <picture>
              <source srcset="<?php echo $PATH;?>/assets/images/publish/bg-line-04.svg" media="(min-width: 1051px)">
              <img src="<?php echo $PATH;?>/assets/images/publish/bg-line-06.png" alt="">
            </picture>
          </div>
          <ul class="point__list">
            <li>
              <div class="point__heart">POINT<span>1</span></div>
              <img src="<?php echo $PATH;?>/assets/images/publish/icon-smartphone.svg" alt="">
              <h4>簡単・スムーズな操作で<br>使いやすい</h4>
              <p>簡便な操作性はもちろん、導入のしやすさにもこだわりました。</p>

              <ul>
                <li>予約が苦手なペットオーナーにも配慮</li>
                <li>今までの予約受付方法から移行や併用がスムーズ</li>
              </ul>
            </li>

            <li>
              <div class="point__heart">POINT<span>2</span></div>
              <img src="<?php echo $PATH;?>/assets/images/publish/icon-chart.svg" alt="">
              <h4>コスト・雑務のカット</h4>
              <p>一般予約だけでなく、常備薬やフードなど、受け取り予約もWeb化することでメリットが沢山。</p>

              <ul>
                <li>ＴＥＬ業務の削減</li>
                <li>聞き間違い・発注忘れ等のトラブルの回避</li>
                <li>顧客管理ツールとしての利用</li>
                <li>メッセージ機能で集客力UP</li>
              </ul>
            </li>

            <li>
              <div class="point__heart">POINT<span>3</span></div>
              <img src="<?php echo $PATH;?>/assets/images/publish/icon-home.svg" alt="">
              <h4>差別化に貢献</h4>
              <p>ペットオーナーが”本当に知りたい” 店舗の情報を見える化することで今までは差別化しづらかった店舗の特徴を明確にします。</p>

              <ul>
                <li>得意な分野</li>
                <li>予約OKなメニュー</li>
                <li>こだわりポイント</li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </section>

    <section class="p-publish__sec03">
      <div class="container3">
        <div class="try">
          <h3>掲載のお申込み
            <span>
              <img src="<?php echo $PATH;?>/assets/images/publish/title-try.svg" alt="Try!">
            </span>
          </h3>
          <a href="" class="btn-blue6">お申込み希望の方はコチラ</a>
        </div>
        <div class="info">
          <div class="info__mail">
            <h4>掲載について</h4>
            <a href="">お問い合わせ</a>
          </div>
          <div class="info__tel">
            <h4>お電話でのお問い合わせはコチラ</h4>
            <a href="tel:042-508-2999"><img src="<?php echo $PATH;?>/assets/images/publish/txt-tel.svg" alt=""></a>
            <p>受付時間：平日9:00-18:00</p>
          </div>
        </div>
      </div>
    </section>
  </div>

  <div class="btn-stoke">
    <a href="/faq/"><img src="<?php echo $PATH;?>/assets/images/common/btn-tofaq.png" alt="よくあるご質問"></a>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer-pre.php'; ?>